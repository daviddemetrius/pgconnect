<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* WEB APP */

$config["CMSName"]              = "P&G Connect Panel";
$config["CMSHeaderPrefix"]      = "P&G Connect";
$config["SweetAlertHeader"]     = "P&G Connect";

/* FORGOT PASSWORD TOKEN */

$config["user_reset_token_length"]   = 120;
$config["user_reset_token_lifetime"] = 480;

/* MAIL */

$config["mail_smtp_host"] = "";
$config["mail_smtp_user"] = "";
$config["mail_smtp_pass"] = "";

$config["sendmail"]["mailpath"] = "/usr/sbin/sendmail";
$config["sendmail"]["protocol"] = "sendmail";
$config["sendmail"]["mailtype"] = "html";
$config["sendmail"]["charset"]  = "utf-8";

$config["smtp"]["protocol"] = "smtp";
$config["smtp"]["smtp_host"] = "";
$config["smtp"]["smtp_user"] = "";
$config["smtp"]["smtp_pass"] = "";
$config["smtp"]["mailtype"] = "html";
$config["smtp"]["charset"]  = "utf-8";

$config["smtptest"]["protocol"] = "smtp";
$config["smtptest"]["smtp_host"] = "smtp.mailtrap.io";
$config["smtptest"]["smtp_user"] = "3a7ee98b7a5548";
$config["smtptest"]["smtp_pass"] = "76ee61b9735300";
$config["smtptest"]["smtp_port"] = 2525;
$config["smtptest"]["crlf"] = "\r\n";
$config["smtptest"]["newline"] = "\r\n";
$config["smtptest"]["mailtype"] = "html";
$config["smtptest"]["charset"]  = "utf-8";
?>