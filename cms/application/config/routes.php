<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/

$route['register'] = "home/register";
$route['event/(:any)'] = "home/event/$1";
$route['eventform/(:any)'] = "home/eventform/$1";
$route['cms'] = "cms/dashboard";
$route['cms/accounts/reps/(:num)'] = 'cms/accounts/repdetails/$1';
$route['cms/accounts/reps/(:num)/(:any)/(:any)'] = 'cms/accounts/repdetails/$1/$2/$3';
$route['cms/accounts/reps/statistics/(:num)'] = 'cms/accounts/repstatistics/$1';
$route['cms/accounts/admin/(:num)'] = 'cms/accounts/admindetails/$1';
$route['cms/accounts/admin/(:num)/(:any)/(:any)'] = 'cms/accounts/admindetails/$1/$2/$3';
$route['cms/accounts/reps/new'] = 'cms/accounts/createnewrep';
$route['cms/accounts/reps/new/(:num)'] = 'cms/accounts/createnewrep/$1';
$route['cms/accounts/reps/visitdetails/(:num)'] = 'cms/accounts/downloadvisitdetails/$1';
$route['cms/accounts/admin/new'] = 'cms/accounts/createnewadmin';

$route['cms/modules/pages/(:num)'] = 'cms/modules/pagedetails/$1';
$route['cms/modules/pages/(:num)/(:any)/(:any)'] = 'cms/modules/pagedetails/$1/$2/$3';
$route['cms/modules/pages/new'] = 'cms/modules/createnewpage';

$route['cms/modules/(:num)'] = 'cms/modules/moduledetails/$1';
$route['cms/modules/(:num)/(:any)/(:any)'] = 'cms/modules/moduledetails/$1/$2/$3';
$route['cms/modules/new'] = 'cms/modules/createnewmodule';

$route['cms/masterdata/segments/(:num)'] = 'cms/masterdata/segdetails/$1';
$route['cms/masterdata/segments/(:num)/(:any)/(:any)'] = 'cms/masterdata/segdetails/$1/$2/$3';
$route['cms/masterdata/segments/new'] = 'cms/masterdata/newsegments';

$route['cms/masterdata/printall/(:num)'] = 'cms/masterdata/printallpromo/$1';
$route['cms/masterdata/download/(:num)'] = 'cms/masterdata/downloadpromocustomer/$1';
$route['cms/masterdata/promos/(:num)'] = 'cms/masterdata/promodetails/$1';
$route['cms/masterdata/promos/(:num)/(:any)/(:any)'] = 'cms/masterdata/promodetails/$1/$2/$3';
$route['cms/masterdata/promos/new'] = 'cms/masterdata/newpromo';
$route['cms/masterdata/promos/new/(:num)'] = 'cms/masterdata/newpromo/$1';
$route['cms/masterdata/promos/qr/(:num)'] = 'cms/masterdata/promoqr/$1';
$route['cms/masterdata/promos/deactivate/(:num)'] = 'cms/masterdata/deactivatepromo/$1';
$route['cms/masterdata/promos/reactivate/(:num)'] = 'cms/masterdata/reactivatepromo/$1';

$route['cms/masterdata/accsegments/(:num)'] = 'cms/masterdata/accsegdetails/$1';
$route['cms/masterdata/accsegments/(:num)/(:any)/(:any)'] = 'cms/masterdata/accsegdetails/$1/$2/$3';
$route['cms/masterdata/accsegments/new'] = 'cms/masterdata/newaccsegments';

$route['cms/masterdata/accchannel/(:num)'] = 'cms/masterdata/accchanneldetails/$1';
$route['cms/masterdata/accchannel/(:num)/(:any)/(:any)'] = 'cms/masterdata/accchanneldetails/$1/$2/$3';
$route['cms/masterdata/accchannel/new'] = 'cms/masterdata/newaccchannel';

$route['cms/masterdata/rooms/(:num)'] = 'cms/masterdata/roomdetail/$1';
$route['cms/masterdata/rooms/(:num)/(:any)/(:any)'] = 'cms/masterdata/roomdetail/$1/$2/$3';
$route['cms/masterdata/rooms/new'] = 'cms/masterdata/newroom';

$route['cms/masterdata/specialty/(:num)'] = 'cms/masterdata/specialtydetail/$1';
$route['cms/masterdata/specialty/(:num)/(:any)/(:any)'] = 'cms/masterdata/specialtydetail/$1/$2/$3';
$route['cms/masterdata/specialty/new'] = 'cms/masterdata/newspecialty';

$route['cms/modules/group/(:num)'] = 'cms/modules/groupdetail/$1';
$route['cms/modules/group/(:num)/(:any)/(:any)'] = 'cms/modules/groupdetail/$1/$2/$3';
$route['cms/modules/group/new'] = 'cms/modules/createnewgroup';

$route['cms/modules/category/(:num)'] = 'cms/modules/categorydetail/$1';
$route['cms/modules/category/(:num)/(:any)/(:any)'] = 'cms/modules/categorydetail/$1/$2/$3';
$route['cms/modules/category/new'] = 'cms/modules/createnewcategory';

$route['cms/modules/cycles/(:num)'] = 'cms/modules/cycledetail/$1';
$route['cms/modules/cycles/(:num)/(:any)/(:any)'] = 'cms/modules/cycledetail/$1/$2/$3';
$route['cms/modules/cycles/new'] = 'cms/modules/createnewcycle';

$route['cms/brand/house/(:num)'] = 'cms/brand/housedetail/$1';
$route['cms/brand/house/(:num)/(:any)/(:any)'] = 'cms/brand/housedetail/$1/$2/$3';
$route['cms/brand/house/new'] = 'cms/brand/newhouse';

$route['cms/brand/category/(:num)'] = 'cms/brand/categorydetail/$1';
$route['cms/brand/category(:num)/(:any)/(:any)'] = 'cms/brand/categorydetail/$1/$2/$3';
$route['cms/brand/category/new'] = 'cms/brand/newcategory';

$route['cms/brand/issue/(:num)'] = 'cms/brand/issuedetail/$1';
$route['cms/brand/issue/(:num)/(:any)/(:any)'] = 'cms/brand/issuedetail/$1/$2/$3';
$route['cms/brand/issue/new'] = 'cms/brand/newissue';

$route['cms/brand/products/(:num)'] = 'cms/brand/productdetail/$1';
$route['cms/brand/products/(:num)/(:any)/(:any)'] = 'cms/brand/productdetail/$1/$2/$3';
$route['cms/brand/products/new'] = 'cms/brand/newproduct';

$route['cms/regions/territory/(:num)'] = 'cms/regions/territorydetail/$1';
$route['cms/regions/territory/(:num)/(:any)/(:any)'] = 'cms/regions/territorydetail/$1/$2/$3';
$route['cms/regions/territory/new'] = 'cms/regions/newterritory';

$route['cms/regions/province/(:num)'] = 'cms/regions/provincedetail/$1';
$route['cms/regions/province/(:num)/(:any)/(:any)'] = 'cms/regions/provincedetail/$1/$2/$3';
$route['cms/regions/province/new'] = 'cms/regions/newprovince';

$route['cms/regions/city/(:num)'] = 'cms/regions/citydetail/$1';
$route['cms/regions/city/(:num)/(:any)/(:any)'] = 'cms/regions/citydetail/$1/$2/$3';
$route['cms/regions/city/new'] = 'cms/regions/newcity';

$route['cms/regions/district/(:num)'] = 'cms/regions/districtdetail/$1';
$route['cms/regions/district/(:num)/(:any)/(:any)'] = 'cms/regions/districtdetail/$1/$2/$3';
$route['cms/regions/district/new'] = 'cms/regions/newdistrict';

$route['cms/accounts/reptype/(:num)'] = 'cms/accounts/reptypedetails/$1';
$route['cms/accounts/reptype/(:num)/(:any)/(:any)'] = 'cms/accounts/reptypedetails/$1/$2/$3';
$route['cms/accounts/reptype/new'] = 'cms/accounts/createnewreptype';

$route['cms/quiz/survey/(:num)'] = 'cms/quiz/surveydetails/$1';
$route['cms/quiz/survey/(:num)/(:any)/(:any)'] = 'cms/quiz/surveydetails/$1/$2/$3';
$route['cms/quiz/survey/new'] = 'cms/quiz/createnewsurvey';

$route['cms/quiz/cycles/(:num)'] = 'cms/quiz/cycledetail/$1';
$route['cms/quiz/cycles/(:num)/(:any)/(:any)'] = 'cms/quiz/cycledetail/$1/$2/$3';
$route['cms/quiz/cycles/new'] = 'cms/quiz/createnewcycle';

$route['cms/masterdata/accounts/(:num)'] = 'cms/masterdata/hospitaldetail/$1';
$route['cms/masterdata/accounts/(:num)/(:any)/(:any)'] = 'cms/masterdata/hospitaldetail/$1/$2/$3';
$route['cms/masterdata/accounts/new'] = 'cms/masterdata/createnewhospital';
$route['cms/masterdata/accounts/unassigned'] = 'cms/masterdata/unassignedaccs';
$route['cms/masterdata/accounts/unapproved'] = 'cms/masterdata/unapprovedaccs';

$route['cms/masterdata/specialists/(:num)'] = 'cms/masterdata/specdetail/$1';
$route['cms/masterdata/specialists/(:num)/(:any)/(:any)'] = 'cms/masterdata/specdetail/$1/$2/$3';
$route['cms/masterdata/specialists/new'] = 'cms/masterdata/createnewspec';

$route['cms/masterdata/events/(:num)'] = 'cms/masterdata/eventdetail/$1';
$route['cms/masterdata/events/(:num)/(:any)/(:any)'] = 'cms/masterdata/eventdetail/$1/$2/$3';
$route['cms/masterdata/events/new'] = 'cms/masterdata/createnewevent';