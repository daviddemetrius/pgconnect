<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Home extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index_get()
	{
		// $this->load->view('home/testpages/welcome_message.php');
	}

	public function verify_post(){
		$response = $this->post("verify");
		$request = array(
			"response" => $response,
			"secret" => "6LdugZoUAAAAAPvrOfIKDuU683e4-fvV7GOiYO2h"
		);
		$data = http_build_query($request);
		$r = $this->doCurlPost("https://www.google.com/recaptcha/api/siteverify", $data);
		if ($r->success){
			echo "true";
		} else {
			echo "false";
		}
	}

	private function doCurlPost($url, $data){
		try{
			// echo $data;
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 0); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			// $result = json_decode(curl_exec($ch)); 
			$result = curl_exec($ch); 
			$info = curl_getinfo($ch);
			curl_close($ch); 
			// var_dump($info);
			if (is_array($result)){
				return $result;
			} else {
				if (is_object($result)){
					return $result;
				} else {
					return json_decode($result);
				}				
			}
		} catch(Exception $e){
			return false;
		}
	}
}