<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Web extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index_get()
	{
		// $this->load->view('home/testpages/welcome_message.php');
	}

	public function login_post()
	{
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$username = $this->post("username");
		$password = $this->post("password");
		$userid = $this->API_Accounts->login($username, $password);

		if($userid == -1) {
			$this->response([
				'code' => 0,
				'message' => "Wrong credentials."
			], REST_Controller::HTTP_BAD_REQUEST);
		} else if($userid == -2) {
			$this->response([
				'code' => 0,
				'message' => "Account Inactive. Please contact HQ."
			], REST_Controller::HTTP_BAD_REQUEST);
		} else {
			$userinfo = $this->API_Accounts->getInfo($userid);
			$this->response([
				'code' => 1,
				'message' => "",
				"data" => $userinfo
			], 200);
		}
	}

	public function checkCoupon_post(){
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$repid = $this->post("repid");
		$coupon = $this->post("coupon");
		$r = $this->db->where("repid", $repid)->get('representatives');
		if (!$r->num_rows()){
			$this->response([
				'code' => 0,
				'message' => 'User Invalid.'
			], REST_Controller::HTTP_BAD_REQUEST);
			return false;
		}
		$hospitalid = $r->row()->associatedaccount;
		$this->db->where("repid", $repid)->update("representatives", array('lastpushupdated' => date("Y-m-d H:i:s")));

		$b = $this->db->where("hospitalid", $hospitalid)->where("uniquecode", $coupon)->where("codeusage", 1)->where("DATE(dateregged)", date("Y-m-d"))->join("promotions", "promotions.promoid = customers.promoid")->get("customers");
		if ($b->num_rows()){
			$apotiksh = $this->db->where("repid in", "(SELECT repid from representatives where associatedaccount = $hospitalid)", false)->count_all_results("representativescanhistory");
			if ($b->row()->dailylimit < $apotiksh){
				$this->response([
					'code' => 0,
					'message' => 'Limit promo untuk apotik sudah tercapai.'
				], REST_Controller::HTTP_OK);
			} else{
				$this->response([
					'code' => 1,
					'message' => '',
					'coupon' => $b->row()
				], REST_Controller::HTTP_OK);
			}
		} else {
			$this->response([
				'code' => 0,
				'message' => 'Kupon tidak terdaftar atau sudah terpakai.'
			], REST_Controller::HTTP_OK);
		}
	}

	public function useCoupon_post(){
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$repid = $this->post("repid");
		$coupon = $this->post("coupon");
		$r = $this->db->where("repid", $repid)->get('representatives');
		if (!$r->num_rows()){
			$this->response([
				'code' => 0,
				'message' => 'User Invalid.'
			], REST_Controller::HTTP_BAD_REQUEST);
			return false;
		}
		$hospitalid = $r->row()->associatedaccount;

		$b = $this->db->where("hospitalid", $hospitalid)->where("uniquecode", $coupon)->where("codeusage", 1)->where("DATE(dateregged)", date("Y-m-d"))->join("promotions", "promotions.promoid = customers.promoid")->get("customers");
		if ($b->num_rows()){
			$apotiksh = $this->db->where("repid in", "(SELECT repid from representatives where associatedaccount = $hospitalid)", false)->count_all_results("representativescanhistory");
			if ($b->row()->dailylimit < $apotiksh){
				$this->response([
					'code' => 0,
					'message' => 'Limit harian untuk apotik sudah tercapai.'
				], REST_Controller::HTTP_OK);
				return false;
			}

			$this->db->where("uniquecode", $coupon)->update("customers", array("codeusage" => 0));
			$disctext = $b->row()->snk;
			if ($b->row()->discountamount != 0){
				if ($b->row()->discounttype == 0){
					$disctext = "Diskon Rp. ".$b->row()->discountamount." ";
				} else {
					$disctext = "Diskon ".$b->row()->discountamount."% ";
				}
				$disctext .= $b->row()->snk;
			}
			$this->db->insert("representativescanhistory", array("repid" => $repid, "uniquecode" => $coupon, "amountcut" => $b->row()->discountamount, "cuttype" => $b->row()->discounttype, "discdesc" => $disctext));
			$this->response([
				'code' => 1,
				'message' => ''
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'code' => 0,
				'message' => 'Kupon tidak terdaftar atau sudah terpakai.'
			], REST_Controller::HTTP_OK);
		}
	}

	public function history_get(){
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->get("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$repid = $this->get("repid");
		$r = $this->db->where("repid", $repid)->join("hospital", "associatedaccount = hospitalid")->get('representatives');
		if (!$r->num_rows()){
			$this->response([
				'code' => 0,
				'message' => 'User Invalid.'
			], REST_Controller::HTTP_BAD_REQUEST);
			return false;
		}		
		$repname = $r->row()->repname;
		$hospitalname = $r->row()->hospitalname;

		$coups = $this->db->select("c.uniquecode, date(datescanned) as date, time(datescanned) as time, discdesc, email, fullname, contactno, '$repname' as repname, '$hospitalname' as hospitalname")->order_by("datescanned", "desc")->join("customers c", "c.uniquecode = rps.uniquecode")->where("rps.repid", $repid)->get("representativescanhistory rps")->result();
		$return = array();
		if (count($coups) > 0){
			$prevdate = $coups[0]->date;
			$temp = array();
			foreach ($coups as $coup){
				if ($prevdate != $coup->date){
					$return[] = array(
						"date" => $prevdate,
						"entry" => $temp
					);
					$temp = array();
					$prevdate = $coup->date;
				}
				$temp[] = $coup;
			}
			$return[] = array(
				"date" => $prevdate,
				"entry" => $temp
			);
		}
		$this->response([
			'code' => 1,
			'message' => '',
			'history' => $return
		], REST_Controller::HTTP_OK);
	}

	public function passcheck_get()
	{
		date_default_timezone_set("Asia/Jakarta");
		if ($this->get("APIKEY") != "pL@ybo0k4p1key="){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key..'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$username = $this->get("id");
		$password = $this->get("pw");
		$userid = $this->API_Accounts->passcheck($username, $password);

		if($userid == -1) {
			$this->response([
				'code' => 0,
				'message' => "Wrong credentials."
			], 200);
		} else if($userid == -2) {
			$this->response([
				'code' => 0,
				'message' => "Account Inactive. Please contact HQ."
			], 200);
		} else {
			$this->response([
				'code' => 1,
				'message' => "",
			], 200);
		}
	}

	private function checkAPIKey($apikey){
		if ($apikey == "Ph@rm4CY#4P!key="){
			return true;
		} else {
			return false;
		}

	}

	public function push_post(){
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model("API_Accounts");
		$userid = $this->post("userid");
		$pushtoken = $this->post("token");

		$pushtype = 2;
		$this->API_Accounts->updatePush($userid, $pushtoken, $pushtype);
		$this->response(array("data" => true, "code" => 1, "message" => ""), 200);
	}

	public function getdata_post()
	{
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$userid = $this->post("userid");
		$lastupdated = $this->post("lastupdated");
		if ($this->_empty($userid)){
			$this->response([
				'message' => "Please input an ID",
				'status' => 0
			], 400);
		} else {
			// ini_set('memory_limit', '800M');
			if ($lastupdated == "-"){
				$returndata = new stdClass();
				$returndata->scheduleapproval = $this->API_Accounts->getApprovalStatus($userid);
				$returndata->schedulecancellation = $this->API_Accounts->getCancelStatus($userid);
				echo json_encode(array("data" => $returndata, "status" => 1, "message" => ""), JSON_NUMERIC_CHECK);//, 200);	
			} else {
				if (!$this->validateDate($lastupdated)){
					$lastupdated = "2000-01-01 00:00:00";
				}
				$returndata = new stdClass();
				$returndata->house = $this->API_Accounts->getAllHouses();
				$returndata->modulegroup = $this->API_Accounts->getAllGroups();
				$returndata->pagecategory = $this->API_Accounts->getPageCategories();
				$returndata->hospitalsegment = $this->API_Accounts->getAllHospitalSegment();
				$returndata->hospitaltype = $this->API_Accounts->getAllHospitalTypes();
				$returndata->hospitals = $this->API_Accounts->getAssignedHospitals($userid);
				$returndata->pages = $this->API_Accounts->getAllPagesAfterDate($lastupdated);
				$returndata->modules = $this->API_Accounts->getAllFilteredModules($userid);
				$returndata->generalmodules = $this->API_Accounts->getAllFilteredGeneralModules($userid);
				$returndata->quiz = $this->API_Accounts->getAllFilteredQuiz($userid);
				$returndata->inbox = $this->API_Accounts->getAllInboxItem($userid);
				$returndata->servertime = date("Y-m-d H:i:s");

				$this->response([
					'message' => "",
					"data" => $returndata,
					'code' => 1
				], 200);
			}
		}
	}


	public function getinbox_post()
	{
		date_default_timezone_set("Asia/Jakarta");
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'code' => 0,
				'message' => 'Invalid Key.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
		$this->load->model('API_Accounts');
		$userid = $this->post("userid");
		$lastupdated = $this->post("lastupdated");
		if ($this->_empty($userid)){
			$this->response([
				'message' => "Please input an ID",
				'status' => 0
			], 400);
		} else {
			$returndata = new stdClass();
			$returndata->inbox = $this->API_Accounts->getAllInboxItem($userid);

			$this->response([
				'message' => "",
				"data" => $returndata,
				'code' => 1
			], 200);
		}
	}

	private function validateDate($date, $format = 'Y-m-d H:i:s')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public function sync_post()
	{
		$this->load->model('API_Accounts');
		date_default_timezone_set("Asia/Jakarta");
		$this->API_Accounts->storePost($this->input->post(), 1);
		if (!$this->checkAPIKey($this->post("APIKEY"))){
			$this->response([
				'status' => 0,
				'message' => 'Invalid Key.'
			], 200);
		}
		$userid = $this->post("userid");
		if ($this->_empty($userid)){
			$this->response([
				'message' => "Please input an ID",
				'status' => 0
			], 200
		);
		} else {
			$modarr = json_decode(json_encode($this->post("moduledata")));
			$pagearr = json_decode(json_encode($this->post("pagedata")));
			$quizarr = json_decode(json_encode($this->post("quizdata")));

			if (!$this->_empty($modarr)){
				// echo json_encode($modarr);
				foreach ($modarr as $moduledata){
					$internalsessid = $moduledata->sessid;
					$sessionid = $this->API_Accounts->checkDetailingSessionExistence($userid, $internalsessid);
					if ($sessionid > 0){
						//already exist, delete and replace
						$modstat = array(
							"moduleid" => $moduledata->moduleid,
							"schedid" => $sessionid,
							"accessdate" => $moduledata->accessdate,
							"accesstime" => $moduledata->accessduration
						);
						$this->API_Accounts->updateDetailingSessionModule($modstat, $sessionid);
					} else {
						$repsessdata = array(
							"internalsessionid" => $internalsessid,
							"sessiondate" => $moduledata->accessdate,
							"totalduration" => $moduledata->accessduration,
							"repid" => $userid
						);
						$sessionid = $this->API_Accounts->createRepDetailingSession($repsessdata);
						$modstat = array(
							"moduleid" => $moduledata->moduleid,
							"schedid" => $sessionid,
							"accessdate" => $moduledata->accessdate,
							"accesstime" => $moduledata->accessduration
						);
						$this->API_Accounts->updateDetailingSessionModule($modstat, $sessionid);
					}
				}
			}
			if (!$this->_empty($pagearr)){
				$previnternalid = "";
				foreach ($pagearr as $pagedata){
					$internalsessid = $pagedata->sessid;
					$sessionid = $this->API_Accounts->checkDetailingSessionExistence($userid, $internalsessid);
					if ($sessionid > 0){
					} else {
						//invalid page statistics data
						continue;
					}
					if ($previnternalid != $internalsessid){
						$this->API_Accounts->clearDetailingSessionPageData($sessionid);
					}
					$pgstat = array(
						"pageid" => $pagedata->pageid,
						"schedid" => $sessionid,
						"accessdate" => $pagedata->accessdate,
						"accesstime" => $pagedata->accessduration,
						"moduleid" => $pagedata->moduleid
					);
					$this->API_Accounts->insertDetailingSessionPageData($pgstat);
					$previnternalid = $internalsessid;
				}
			}
			if (!$this->_empty($quizarr)){
				$previnternalid = "";
				foreach ($quizarr as $quizdata){
					$internalsessid = $quizdata->sessid;
					$sessionid = $this->API_Accounts->checkSurveySessionExistence($userid, $internalsessid);
					if ($sessionid > 0){
					} else {
						//session hasnt been created
						$qzstat = array(
							"internalsessionid" => $internalsessid,
							"quizcycleid" => $quizdata->quizid,
							"sessiondate" => $quizdata->accessdate,
							"totalduration" => $quizdata->sessionduration,
							"repid" => $userid
						);
						$sessionid = $this->API_Accounts->insertSurveySessionQuizData($qzstat);
					}
					if ($previnternalid != $internalsessid){
						$this->API_Accounts->clearSurveySessionQuizData($sessionid);
					}
					$ansstat = array(
						"sessionid" => $sessionid,
						"quizid" => $quizdata->questionid,
						"quizquestion" => $quizdata->question,
						"quizanswers" => $quizdata->answer
					);
					$this->API_Accounts->insertSurveySessionAnswerData($ansstat);
					$previnternalid = $internalsessid;
				}
			}
			$this->response([
				'message' => "Successfully Synced", "status" => 1, "time" => date("Y-m-d H:i:s")
			], 200
		);
		}
	}

	private function _empty($var)
	{
		return (empty($var) || ($var == "''"));
	}
}

