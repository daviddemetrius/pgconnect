<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper("url");
	}

	public function index(){
		$data['events'] = $this->db->order_by("datestart", "desc")->get("event")->result();
		$this->load->view("landing.php", $data);
	}

	public function event($id){
		$data['detail'] = $this->db->where("eventid", $id)->get("event")->row();
		$this->load->view("detail.php", $data);
	}

	public function eventform($id){
		$data['id'] = $id;
		$data['detail'] = $this->db->where("dateend >", date("Y-m-d"))->where("eventid", $id)->get("event");
		if ($data['detail']->num_rows() > 0){
			$data['detail'] = $data['detail']->row();
		} else {
			redirect("event/$id");
		}
		$this->load->view("form.php", $data);	
	}

	public function finddocs(){
		$q = $this->input->get("query");
		$cls = new stdClass();
		$cls->query = $q;
		$arr = $this->db->query("SELECT specid as data, concat(specialistname,', ',specialty,'. ',institution) as value from specialist where specialistname like '%$q%' and obsolete = 0 limit 10")->result();;
		$cls->suggestions = $arr;
		echo json_encode($cls);
	}

	public function sendNew(){
		$eventid = $this->input->post("eventid");
		$name = $this->input->post("name");
		$specialty = $this->input->post("specialty");
		$institution = $this->input->post("institution");
		$phone = $this->input->post("phone");
		$contactus = $this->input->post("contactus");

		$this->db->insert("specialist", array(
			"specialistname" => $name,
			"specialty" => $specialty,
			"institution" => $institution,
			"phone" => $phone,
			"xmlhcpid" => "NEW"
		));
		$id = $this->db->insert_id();
		if ($contactus == "ya"){
			$this->db->where("specid", $id)->update("specialist", array("xmlhcpid" => "NEW$id"));
		} else {
			$this->db->where("specid", $id)->update("specialist", array("xmlhcpid" => "NEWNOSUB$id"));
		}
		$this->db->insert("eventattendance", array("specid" => $id, "eventid" => $eventid, "rsvpdate" => date("Y-m-d H:i:s")));
		$this->db->set("goingcount", "goingcount+1", false)->where("eventid", $eventid)->update("event");

		$uniquecode = date("YmdHis")."-".$id."-".$this->input->post("eventid");
		$params['data'] = $uniquecode;
		$params['level'] = 'H';
		$params['size'] = 300;
		$params['savename'] = FCPATH."img/qr/$uniquecode.png";
		$this->load->library("ciqrcode");
		$this->ciqrcode->generate($params);
		echo $uniquecode;
	}

	public function sendCpl(){
		$this->db->insert("eventattendance", array("specid" => $this->input->post('spid'), "eventid" => $this->input->post('eventid'), "rsvpdate" => date("Y-m-d H:i:s")));	
		$this->db->set("goingcount", "goingcount+1", false)->where("eventid", $this->input->post('eventid'))->update("event");

		$uniquecode = date("YmdHis")."-".$this->input->post("spid")."-".$this->input->post("eventid");
		$params['data'] = $uniquecode;
		$params['level'] = 'H';
		$params['size'] = 300;
		$params['savename'] = FCPATH."img/qr/$uniquecode.png";
		$this->load->library("ciqrcode");
		$this->ciqrcode->generate($params);
		echo $uniquecode;
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	private function sendnewslettermail($subject, $recipients, $body){
		$this->load->library('email');
		$this->email->initialize($this->config->item("sendmail"));
		$arr = array();
		// $this->load->model("Home_Main");
		// if ($)
		foreach ($recipients as $mail){
			$mesg = $this->load->view('newsletter.php',$body,true);
			$this->email->from('info@pharmacycare.id', "Pharmacy Care");
		    	$this->email->to($mail);
		    	$this->email->subject($subject);
		    	$this->email->message($mesg);
			$b = $this->email->send();
			// echo $b;
			// echo $this->email->print_debugger();
			// echo "Sent?";
		}
	}
	private function doCurlPost($url, $data){
		try{
			// echo $data;
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 0); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			// $result = json_decode(curl_exec($ch)); 
			$result = curl_exec($ch); 
			$info = curl_getinfo($ch);
			curl_close($ch); 
			// var_dump($info);
			if (is_array($result)){
				return $result;
			} else {
				if (is_object($result)){
					return $result;
				} else {
					return json_decode($result);
				}				
			}
		} catch(Exception $e){
			return false;
		}
	}
}
