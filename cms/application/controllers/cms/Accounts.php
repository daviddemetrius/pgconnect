<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller {

	function __construct()
	{
		parent::__construct();
date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	}

	public function reptype()
	{
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_rept"; 
		$data['HeaderSuffix'] = "Representative Type"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Representative Type";
		$data['listicon'] = "th-list";
		$data['tabletitles'] = array("Representative Type");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Accounts->searchRepTypes($srcquery, $page, $limit);
			$data['totalpage'] = ceil(count($data['items'])/10);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Accounts->getAllRepTypesPaged($page, $limit);
			$data['totalpage'] = $this->CMS_Accounts->getTotalRepTypePages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['sitelink'] = site_url("cms/accounts/reptype");
		$this->load->view('cms/reptype_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function admin()
	{
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_admin"; 
		$data['HeaderSuffix'] = "Admin"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['accounts'] = $this->CMS_Accounts->searchAdmins($srcquery, $page, $limit);
			$data['totalpage'] = ceil(count($data['accounts'])/10);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['accounts'] = $this->CMS_Accounts->getAllAdminsPaged($page, $limit);
			$data['totalpage'] = $this->CMS_Accounts->getTotalAdminPages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['sitelink'] = site_url("cms/accounts/admin");
		$this->load->view('cms/user_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function admindetails($id, $msg = "none", $type = "success") {
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			if ($id != $this->session->userdata("user_id")){
				redirect("cms");
			}			
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_admin"; 
		$data['HeaderSuffix'] = "Admin"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$data['account'] = $this->CMS_Accounts->getAccountInfo($id);
		$data['srcrep'] = $this->CMS_Accounts->getRepsForSearch($id);
		$data['srcdm'] = $this->CMS_Accounts->getDMForSearch($id);
		$data['srcrm'] = $this->CMS_Accounts->getRMForSearch($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/user_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function messaging() {
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_messager"; 
		$data['HeaderSuffix'] = "Messaging"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$data['messages'] = $this->CMS_Accounts->getMessageHistory();
		$msg = $this->session->flashdata("msg");
		$type = $this->session->flashdata("type");
		if ($msg){
			$data['SwalMsg'] = $msg;
			$data['SwalType'] = $type;
		}


		$this->load->view('cms/util_message', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function commitSendMsg(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$msg = $this->input->post("msg");
		$title = $this->input->post("title");
		$this->load->model("CMS_Accounts");
		$r = $this->CMS_Accounts->sendMessage($msg, $title);
		if ($r){
			$this->session->set_flashdata("msg", "Message Sent");
			$this->session->set_flashdata("type", "success");
		} else {
			$this->session->set_flashdata("msg", "Message was not sent");
			$this->session->set_flashdata("type", "error");
		}
		$pushids = $this->CMS_Accounts->getAvailablePushIDs();
		$this->sendPush($pushids, $title, $msg);
		redirect("cms/accounts/messaging");
	}

	public function editAdmin(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			if ($this->input->post("id") != $this->session->userdata("user_id")){
				redirect("cms");
			}
		}
		$userid = $this->input->post("id");
		if ($this->input->post("tier") >= $this->session->userdata("tier")){
			if ($this->session->userdata("user_id") != $userid){
				redirect("cms/accounts/agents/$userid/You are not allowed to promote this user to this rank/error");
			}			
		}

		$data = array(
			"fullname" => $this->input->post("fullname"),
			"contact" => $this->input->post("phone"),
			"email" => $this->input->post("email")
		);
		if ($this->session->userdata("tier") == 999){
			$data['tier'] = 999;
		}
		$pw  = $this->input->post("password");
		if (!($pw == null || $pw == "")){
			$data["password"] = password_hash($this->input->post("password"), PASSWORD_BCRYPT, ['cost' => 12]);
		}
		$tier = $this->input->post("tier");
		if (!($tier == null || $tier == "")){
			$data["tier"] = $tier;
		}
		$this->load->model("CMS_Accounts");
		echo var_dump($data);
		$b = $this->CMS_Accounts->updateAccount($data, $userid);
		if ($b){
			redirect("cms/accounts/admin/$userid/");
		} else {
			redirect("cms/accounts/admin/$userid/Operation Failed. Please try again./error");
		}
	}

	public function createnewrep($hospitalid){
		if ($this->session->userdata("tier") != 999){
			$r = $this->db->where("hospitalid", $hospitalid)->where("createdby", $this->session->userdata("user_id"))->get("hospital");
			if (!$r->num_rows()){
				redirect("cms/masterdata/accounts");
			}			
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[representatives.loginid]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('repname', 'Full Name', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$data['hospitalid'] = $hospitalid;
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "userdropdown"; 
			$data['datatype'] = "ad_reps"; 
			$data['HeaderSuffix'] = "Representatives"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model("CMS_Regions");
			$this->load->model("CMS_Accounts");
			$this->load->view('cms/rep_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Accounts');
			$arr = array(
				"loginid" => $this->input->post("username"),
				"loginpass" => password_hash($this->input->post("password"), PASSWORD_BCRYPT, ['cost' => 12]),
				"createdby" => $this->session->userdata("user_id"),
				"phone" => $this->input->post("phone"),
				"repname" => $this->input->post("repname"),
				"address" => $this->input->post("address"),
				"associatedaccount" => $hospitalid,
			);
			$result = $this->CMS_Accounts->createNewRep($arr);
			
			if ($result == "success") {
				redirect("cms/masterdata/accounts/$hospitalid");
			}
			else { //if fail
				$data['dropdown'] = "userdropdown"; 
				$data['datatype'] = "ad_reps"; 
				$data['HeaderSuffix'] = "Representatives"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$this->load->model("CMS_Regions");
				$this->load->model("CMS_Accounts");
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/rep_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function createnewadmin(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[cmsuser.username]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('fullname', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->input->post("tier") && $this->input->post("tier") >= $this->session->userdata("tier")){
			$data['SwalMsg'] = "You are not allowed to register a user of that tier.";
			$data['SwalType'] = "error";
		}
		
		if ($this->form_validation->run() == FALSE || ($this->input->post("tier") > $this->session->userdata("tier"))) {
			$data['dropdown'] = "userdropdown"; 
			$data['datatype'] = "ad_admin"; 
			$data['HeaderSuffix'] = "Admin"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model("CMS_Accounts");
			$this->load->view('cms/user_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Accounts');
			$arr = array(
				"username" => $this->input->post("username"),
				"password" => password_hash($this->input->post("password"), PASSWORD_BCRYPT, ['cost' => 12]),
				"fullname" => $this->input->post("fullname"),
				"contact" => $this->input->post("phone"),
				"tier" => $this->input->post("tier"),
				"email" => $this->input->post("email")
			);
			$result = $this->CMS_Accounts->createNewAccount($arr);
			
			if ($result == "success") {
				redirect ('cms/accounts/admin');
			}
			else { //if fail
				$data['dropdown'] = "userdropdown"; 
				$data['datatype'] = "ad_admin"; 
				$data['HeaderSuffix'] = "Admin"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$this->load->model("CMS_Accounts");
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/user_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function reps()
	{
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_reps"; 
		$data['HeaderSuffix'] = "Representatives"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['accounts'] = $this->CMS_Accounts->searchReps($srcquery, $page, $limit);
			$data['totalpage'] = ceil(count($data['accounts'])/10);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['accounts'] = $this->CMS_Accounts->getAllRepsPaged($page, $limit);
			$data['totalpage'] = $this->CMS_Accounts->getTotalRepPages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['sitelink'] = site_url("cms/accounts/reps");
		$this->load->view('cms/rep_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function repdetails($id, $msg = "none", $type = "success") {
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			$r = $this->db->where("repid", $id)->where("createdby", $this->session->userdata("user_id"))->get("representatives");
			if (!$r->num_rows()){
				redirect("cms/masterdata/accounts");
			}
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_reps"; 
		$data['HeaderSuffix'] = "Representatives"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$this->load->model("CMS_Masterdata");
		$this->load->model("CMS_Regions");
		$data["territorylist"] = $this->CMS_Regions->getTerritories();
		$data['districtlist'] = $this->CMS_Regions->getDistrictsForSearch();
		$data['reptype'] = $this->CMS_Accounts->getRepTypes();
		$data['account'] = $this->CMS_Accounts->getRepInfo($id);
		// $data['hospitallist'] = $this->CMS_Masterdata->getHospitalsForSearch();
		$data['city'] = $this->CMS_Accounts->getCities();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->model("CMS_Statistics");

		$this->load->view('cms/rep_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function repstatistics($id){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_reps"; 
		$data['HeaderSuffix'] = "Representatives"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$this->load->model("CMS_Statistics");
		$data['account'] = $this->CMS_Accounts->getRepInfo($id);
		$data['rsAccCoverage'] = $this->CMS_Statistics->rsGetAccCoverage($data['account']->territoryid); //v
		$data['rsHcpCoverage'] = $this->CMS_Statistics->rsGetHcpCoverage($data['account']->territoryid); //v
		$month = date("m");
		if ($this->input->get("month")){
			$month = $this->input->get("month");
		}
		$year = date("Y");
		if ($this->input->get("year")){
			$year = $this->input->get("year");
		}
		if ($this->input->get("all")){
			$month = null;
			$year = null;
		}

		$data['rsModuleData'] = $this->CMS_Statistics->rsGetModuleStatistics($id, $month, $year);
		$data['rsPlanEffectiveness'] = $this->CMS_Statistics->rsGetPlanEffectiveness($id, $month, $year); 
		$data['rsAccReach'] = $this->CMS_Statistics->rsGetAccReach($id, $month, $year, $data['account']->territoryid);
		$data['rsPDHcpReach'] = $this->CMS_Statistics->rsGetPDHcpReach($id, $month, $year, $data['account']->territoryid);
		$data['rsMWHcpReach'] = $this->CMS_Statistics->rsGetMWHcpReach($id, $month, $year, $data['account']->territoryid);
		$data['rsOTHcpReach'] = $this->CMS_Statistics->rsGetOTHcpReach($id, $month, $year, $data['account']->territoryid);
		$data['rs1on1Data'] = $this->CMS_Statistics->rsGet1on1Data($id, $month, $year);
		$data['rsGroupData'] = $this->CMS_Statistics->rsGetGroupData($id, $month, $year);
		$data['rsFullDays'] = $this->CMS_Statistics->rsGetFullDays($id, $month, $year);
		$data['rsHalfDays'] = $this->CMS_Statistics->rsGetHalfDays($id, $month, $year);

		$this->load->view('cms/rep_statistics', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function searchSpecsInHospital(){
		$hospitalid = $this->input->get("hospitalid");
		$this->load->model("CMS_Masterdata");
		$specs = $this->CMS_Masterdata->getSpecsForSearch($hospitalid);
		// $specs = $this->CMS_Masterdata->getSpecsForSearchTwo($hospitalid);
		echo json_encode($specs);
	}

	public function editRep(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			$r = $this->db->where("repid", $this->input->post("id"))->where("createdby", $this->session->userdata("user_id"))->get("representatives");
			if (!$r->num_rows()){
				redirect("cms/masterdata/accounts");
			}			
		}
		$userid = $this->input->post("id");

		$data = array(
			"phone" => $this->input->post("phone"),
			"repname" => $this->input->post("repname"),
			"address" => $this->input->post("address"),
		);

		$pw  = $this->input->post("loginpass");
		if (!($pw == null || $pw == "")){
			$data["loginpass"] = password_hash($this->input->post("loginpass"), PASSWORD_BCRYPT, ['cost' => 12]);
		}
		$this->load->model("CMS_Accounts");
		$b = $this->CMS_Accounts->updateRep($data, $userid);
		if ($b){
			redirect("cms/accounts/reps/$userid/");
		} else {
			redirect("cms/accounts/reps/$userid/Operation Failed. Please try again./error");
		}
	}

	public function deactivateAccount(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$id = $this->input->post("id");
		$this->load->model("CMS_Accounts");
		if ($this->CMS_Accounts->getTier($id) >= $this->session->userdata("tier")){
			echo "You are not allowed to do this operation"; 
		} else {
			echo $this->CMS_Accounts->deactivateAccount($id);
		}
	}

	public function reactivateAccount(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$id = $this->input->post("id");
		$this->load->model("CMS_Accounts");
		if ($this->CMS_Accounts->getTier($id) >= $this->session->userdata("tier")){
			echo "You are not allowed to do this operation"; 
		} else {
			echo $this->CMS_Accounts->reactivateAccount($id);
		}
	}

	public function deactivate($agentid){
		$this->load->model("CMS_Accounts");
		$b = $this->CMS_Accounts->deactivateRep($agentid);
		if ($b == "You are not allowed to do this operation"){
			echo $b;
		} else {
			redirect("cms/masterdata/accounts/$b");
		}
	}

	public function reactivate($agentid){
		$this->load->model("CMS_Accounts");
		$b = $this->CMS_Accounts->reactivateRep($agentid);
		if ($b == "You are not allowed to do this operation"){
			echo $b;
		} else {
			redirect("cms/masterdata/accounts/$b");
		}
	}

	public function getsurveydata(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$sessionid = $this->input->post("sessionid");
		$this->load->model("CMS_Accounts");
		$visitData = $this->CMS_Accounts->getSurveyData($sessionid);
		if (!$visitData){
			echo "Tidak ada data untuk sesi ini.";
		} else {
			echo "
			Tanggal Sesi: ".date("j F Y H:i", strtotime($visitData->sessiondate))." <br/>
			Durasi Sesi: $visitData->totalduration s <br/>
			Tanggal Sesi Dikirim: ".date("j F Y H:i", strtotime($visitData->datesubmitted))." <br/><br/>

			<br/>------------Jawaban------------<br/><br/>
			";
			if (sizeof($visitData->answers) <= 0){
				echo "Tidak ada Data Jawaban.<br/><br/>";
			} else {
				foreach ($visitData->answers as $mod){
					echo "
					<strong>$mod->quizquestion</strong><br/>
					$mod->quizanswers <br/><br/>
					";
				}
			}
		}
		// var_dump($visitData);
	}

	public function getsessiondata(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$scheduleid = $this->input->post("schedid");
		$this->load->model("CMS_Accounts");
		$visitData = $this->CMS_Accounts->getSessionData($scheduleid);
		if (!$visitData){
			echo "Tidak ada data modul untuk sesi ini.";
		} else {
			echo "
			Tanggal Sesi: ".date("j F Y H:i", strtotime($visitData->sessiondate))." <br/>
			Durasi Sesi: $visitData->totalduration s <br/>
			Tanggal Sesi Dikirim: ".date("j F Y H:i", strtotime($visitData->submitteddate))." <br/><br/>

			<br/>------------Statistik Modul------------<br/><br/>
			";
			if (sizeof($visitData->moduleStatistics) <= 0){
				echo "Tidak ada Data Modul.<br/><br/>";
			} else {
				foreach ($visitData->moduleStatistics as $mod){
					echo "
					<strong>$mod->moduletitle</strong><br/>
					Diakses tanggal: ".date("j F Y H:i:s", strtotime(($mod->accessdate=="0000-00-00 00:00:00"?$visitData->sessiondate:$mod->accessdate)))." <br/>
					Durasi Akses: $mod->accesstime s <br/><br/>
					";
				}
			}
			/*echo "
			<br/>------------Statistik Halaman------------<br/><br/>
			";
			if (sizeof($visitData->pageStatistics) <= 0){
				echo "Tidak ada Data Halaman.<br/><br/>";
			} else {
				foreach ($visitData->pageStatistics as $pg){
					echo "<strong>$pg->pagetitle</strong><br/>";
					echo "<img src='".base_url()."assets/thumbs/".$pg->pageid.".jpg' width=110><br/>";
					echo "Diakses tanggal: ".date("j F Y H:i:s", strtotime($pg->accessdate))." <br/>";
					echo "Durasi Akses: $pg->accesstime s <br/>";
				}
			}*/

		}
		// var_dump($visitData);
	}

	private function sendAPNS($deviceToken, $message){
		// $deviceToken = '8845ba7c41e95e12caea6381ea6f01b5cd7b59a52feb9005e0727a65a4105dc2a0';

		$passphrase = '';

		// $message = 'Your message';


		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
		    exit("Failed to connect: $err $errstr" . PHP_EOL);

		echo 'Connected to APNS' . PHP_EOL;


		$body['aps'] = array(
			'alert' => array(
			        'body' => $message,
			        'action-loc-key' => 'Danone E-Detailing',
			)
		);

		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;


		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result)
		    echo 'Message not delivered' . PHP_EOL;
		else
		    echo 'Message successfully delivered' . PHP_EOL;

		fclose($fp);
	}

	private function sendPush($regIds, $title, $body){
		$msg = array
          	(
			'body' 	=> $body,
			'title'	=> $title,
          	);
		$fields = array
		(
			'registration_ids'		=> $regIds,
			'notification'	=> $msg
		);
		//FCM API end-point
		$url = 'https://fcm.googleapis.com/fcm/send';
		//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
		$server_key = 'AAAAN_ANJLM:APA91bHTqJ2gYvdEf5qsqqfVc4JNM9dGTWq-q_fFyMOSTbnyNlX3bmTDnryatGDO540cKFYrF3VfwmoMBWW-YCFvo0lXwZomlIElVeLbi3mQGs9kVr1RHw3Hd8jqlPZN5wXrW-8KYbCp';
		//header with content_type api key
		$headers = array(
		    	'Content-Type:application/json',
		    	'Authorization:key='.$server_key
		);
		//CURL request to route notification to FCM connection server (provided by Google)
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		if ($result === FALSE) {
		    	die('Oops! FCM Send Error: ' . curl_error($ch));
		}

		curl_close($ch);
	}

	public function createnewreptype(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('typename', 'Representative Type', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "userdropdown";
			$data['datatype'] = "ad_rept"; 
			$data['HeaderSuffix'] = "Rep Type"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model('CMS_Accounts');
			$data['houses'] = $this->CMS_Accounts->getHouses();
			$data['accsegs'] = $this->CMS_Accounts->getAccSegments();
			$data['modules'] = $this->CMS_Accounts->getAvailableModules();
			$this->load->view('cms/reptype_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"typename" => $this->input->post("typename")
			);
			$this->load->model('CMS_Accounts');
			$houses = $this->input->post("houses")?$this->input->post("houses"):array();
			$accsegs = $this->input->post("accsegs")?$this->input->post("accsegs"):array();
			$modules = $this->input->post("modules")?$this->input->post("modules"):array();
			$result = $this->CMS_Accounts->createNewRepType($arr, $houses, $accsegs, $modules);
			
			if ($result == "success") {
				redirect ('cms/accounts/reptype');
			}
			else {
				$data['dropdown'] = "userdropdown";
				$data['datatype'] = "ad_rept"; 
				$data['HeaderSuffix'] = "Rep Type"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->model('CMS_Accounts');
				$data['houses'] = $this->CMS_Accounts->getHouses();
				$data['accsegs'] = $this->CMS_Accounts->getAccSegments();
				$data['modules'] = $this->CMS_Accounts->getAvailableModules();
				$this->load->view('cms/reptype_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}
	public function reptypedetails($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "userdropdown";
		$data['datatype'] = "ad_rept"; 
		$data['HeaderSuffix'] = "Rep Type"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Accounts");
		$data['reptype'] = $this->CMS_Accounts->getRepTypeDetail($id);
		$data['houses'] = $this->CMS_Accounts->getHouses();
		$data['accsegs'] = $this->CMS_Accounts->getAccSegments();
		$data['modules'] = $this->CMS_Accounts->getAvailableModules();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/reptype_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function editRepType(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("id");

		$data = array(
			"typename" => $this->input->post("typename")
		);
		$houses = $this->input->post("houses")?$this->input->post("houses"):array();
		$accsegs = $this->input->post("accsegs")?$this->input->post("accsegs"):array();
		$modules = $this->input->post("modules")?$this->input->post("modules"):array();

		$this->load->model("CMS_Accounts");
		$b = $this->CMS_Accounts->updateRepType($data, $moduleid, $houses, $accsegs, $modules);

		if ($b){
			redirect("cms/accounts/reptype/$moduleid/");
		} else {
			redirect("cms/accounts/reptype/$moduleid/Operation Failed. Please try again./error");
		}
	}

	public function downloadvisitdetails($repid){
		ini_set('memory_limit', '-1');
		$limit = 1000;
		$current = 1;
		$b = true;

		$month = $this->input->get("m")?$this->input->get("m"):date("m");
		$year = $this->input->get("y")?$this->input->get("y"):date("Y");
		$repdetails = $this->db->where("repid", $repid)->join("reptype rt", "rt.typeid = r.reptype", "left")->get("representatives r");
		if ($repdetails->num_rows() <= 0){
			redirect("");
		}
		$repdetails = $repdetails->row();
		$filename = $repdetails->xmlnacd.'PlanDetails'.$month.'-'.$year;

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		fputcsv($output, array("Schedule Date", "Visit Date", "Visit Duration", "NR Code", "Rep Name", "Rep Type", "ACC Segment", "ACC Code", "ACC Name", "HCP Specialty", "HCP Code", "HCP Name", "Schedule ID", "Visit Type", "Plan Status", "Modules", "ITR"));
		$schedules = $this->db->select("rs.*, av.visittime, av.visitend, av.visitnumber, av.visitduration, av.submitteddate, av.visitrating")->where("repid", $repid)->join("actualvisit av", "av.schedid = rs.schedid", "left")->where("MONTH(rs.scheddate)", $month, false)->where("YEAR(rs.scheddate)", $year, false)->get('representativeschedule rs')->result();
		// echo json_encode($schedules);
		$curMo = date("m");
		$curY = date("Y");
		foreach ($schedules as $sch){
			$scheddate = $sch->scheddate;
			$visitdate = $sch->visittime;
			$duration = $sch->visitduration;
			$nrcode = $repdetails->xmlnacd;
			$repname = $repdetails->repname;
			$reptype = $repdetails->typename;
			$schedid = $sch->schedid;
			$visittype = $sch->type;
			$statustext = "Planned";
			if ($year < $curY){
				//different year, lesser year
				if ($sch->visited == 1){
					$statustext = "Visited";
					if (date("Y-m-d", strtotime($sch->scheddate)) != date("Y-m-d", strtotime($sch->visittime))){
						$statustext = "Rescheduled";
					}
					if (date("Y-m", strtotime($sch->createddate)) == "$year-$month"){
						$statustext = "Ad-hoc";
					}
				} else if ($sch->deleted == 1) {
					$statustext = "Deleted";
				} else if ($sch->approved == 1) {
					$statustext = "Missed";
				}
			} else if ($month < $curMo){
				//different month, lesser month
				if ($sch->visited == 1){
					$statustext = "Visited";
					if (date("Y-m-d", strtotime($sch->scheddate)) != date("Y-m-d", strtotime($sch->visittime))){
						$statustext = "Rescheduled";
					}
					if (date("Y-m", strtotime($sch->createddate)) == "$year-$month"){
						$statustext = "Ad-hoc";
					}
				} else if ($sch->deleted == 1) {
					$statustext = "Deleted";
				} else if ($sch->approved == 1) {
					$statustext = "Missed";
				}
			} else {
				//current/next month
				if ($sch->visited == 1){
					$statustext = "Visited";
					if (date("Y-m-d", strtotime($sch->scheddate)) != date("Y-m-d", strtotime($sch->visittime))){
						$statustext = "Rescheduled";
					}
					if (date("Y-m", strtotime($sch->createddate)) == "$year-$month"){
						$statustext = "Ad-hoc";
					}
				} else if ($sch->deleted == 1) {
					$statustext = "Deleted";
				}
			}
			$accseg = "";
			$acccode = "";
			$accname = "";
			$hcpspec = "";
			$hcpname = "";
			$hcpcode = "";
			if ($visittype == "1On1"){
				$scheduledhcps = $this->db->select("h.xmlaccid, h.hospitalid, h.hospitalname, hs.segmentation, sp.xmlhcpid, sp.specid, sp.specialistname, spc.specialtyname")->where("schedid", $sch->schedid)->join("hospital h", "h.hospitalid = sv.hospitalid")->join("hospitalsegment hs", "hs.segmentid = h.segmentid", "left")->join("specialist sp", "sp.specid = sv.specid")->join("specialty spc", "spc.specialtyid = sp.specialty", "left")->get("schedulevisit sv");
				if ($scheduledhcps->num_rows()){
					$assgn = $scheduledhcps->row();
					$accseg = $assgn->segmentation;
					$acccode = $assgn->hospitalid;
					$accname = $assgn->hospitalname;
					$hcpspec = $assgn->specialtyname;
					$hcpname = $assgn->specialistname;
					$hcpcode = $assgn->specid;
				}
			} else {
				$scheduledhcps = $this->db->select("h.xmlaccid, h.hospitalid, h.hospitalname, hs.segmentation, sp.xmlhcpid, sp.specid, sp.specialistname, spc.specialtyname")->where("schedid", $sch->schedid)->join("hospital h", "h.hospitalid = sv.hospitalid")->join("hospitalsegment hs", "hs.segmentid = h.segmentid", "left")->join("specialist sp", "sp.specid = sv.specid")->join("specialty spc", "spc.specialtyid = sp.specialty", "left")->get("schedulevisit sv");
				if ($scheduledhcps->num_rows()){
					$scheduledhcps = $scheduledhcps->result();
					$accarr = array();
					$accdelim = "";
					$hcpdelim = "";
					foreach ($scheduledhcps as $hcpdata){
						if (!in_array($hcpdata->hospitalid, $accarr)){
							$accarr[] = $hcpdata->hospitalid;
							$accseg .= $accdelim.$hcpdata->segmentation;
							$acccode .= $accdelim.$hcpdata->hospitalid;
							$accname .= $accdelim.$hcpdata->hospitalname;
							$accdelim = "\n";
						}
						$hcpspec .= $hcpdelim.$hcpdata->specialtyname;
						$hcpname .= $hcpdelim."[".$hcpdata->hospitalid."]-".$hcpdata->specialistname;
						$hcpcode .= $hcpdelim.$hcpdata->specid;
						$hcpdelim = "\n";
					}
				}
			}
			$itr = 0;
			$modules = "";
			$itrdata = $this->db->where("schedid", $sch->schedid)->get("closingdataitr");
			if ($itrdata->num_rows()){
				$itrdata = $itrdata->row();
				$itr = $this->db->query("SELECT count(distinct(productid)) as cnt from closingdataitrproducts where intention = 1 and itrid = $itrdata->itrid")->row()->cnt;
			}
			$moduledata = $this->db->where("schedid", $sch->schedid)->join("contentmodules cm", "cm.moduleid = ms.moduleid")->where("ms.accesstime >", 10)->get("modulestatistics ms")->result();
			foreach ($moduledata as $mod){
				$modules .= "$mod->moduletitle - $mod->accesstime s";
				if ($mod->isadded == 1){
					$modules .= " - From Library";
				}
				$modules .= "\n";
			}
			fputcsv($output, array($scheddate, $visitdate, $duration, $nrcode, $repname, $reptype, $accseg, $acccode, $accname, $hcpspec, $hcpcode, $hcpname, $schedid, $visittype, $statustext, $modules, $itr));
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}
}

