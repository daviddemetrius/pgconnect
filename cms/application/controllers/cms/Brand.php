<?php
class Brand extends CI_Controller {

	function __construct()
	{
		parent::__construct();
date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	}

	public function house()
	{
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "branddropdown";
		$data['datatype'] = "bd_house"; 
		$data['HeaderSuffix'] = "House"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Brand");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "House";
		$data['listicon'] = "home";
		$data['tabletitles'] = array("House Name", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Brand->searchHouses($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Brand->getTotalSearchHousePages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Brand->getAllHousesPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Brand->getTotalHousePages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/brand/house");
		$this->load->view('cms/house_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function newhouse(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('housename', 'House Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "branddropdown";
			$data['datatype'] = "bd_house"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "House"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/house_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Brand');
			$arr = array(
				"housename" => $this->input->post("housename"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Brand->createNewHouse($arr);
			
			if ($result == "success") {
				redirect ('cms/brand/house');
			}
			else { //if fail
				$data['dropdown'] = "branddropdown";
				$data['datatype'] = "bd_house"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "House"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/house_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function housedetail($id, $msg = "none", $type = "success") {
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "branddropdown";
		$data['datatype'] = "bd_house"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "House"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Brand");
		$data['housedetails'] = $this->CMS_Brand->getHouseDetails($id);
		// $data['categorylist'] = $this->CMS_Brand->getAllHouseCats();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/house_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function editHouse(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$id = $this->input->post("houseid");
		$name = $this->input->post("housename");

		$arr = array(
			"housename" => $this->input->post("housename"),
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Brand");
		$result = $this->CMS_Brand->editHouse($arr, $id);
		if ($result){
			redirect("cms/brand/house/$id/");
		} else {
			redirect("cms/brand/house/$id/Edit Failed. Please try again./error");
		}
	}
}
?>