<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	}

	function logout() {
		$this->session->sess_destroy();
		redirect('cms/login');
	}

	public function index()
	{
		$data['datatype'] = "s_HmBrd"; 
		$data['HeaderSuffix'] = "Dashboard"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		// $data['totalActiveHospital'] = $this->db->where("obsolete", 0)->count_all_results("hospital");
		// $data['totalCustomers'] = $this->db->where("year(dateregged)", date("Y"))->count_all_results("customers");
		// $data['totalUsedCoupons'] = $this->db->where("year(dateregged)", date("Y"))->where("codeusage", 0)->count_all_results("customers");
		// $lastmo = date("Y-m-d 00:00:00", strtotime("-1 month"));
		// $data['totalActiveReps'] = $this->db->where("lastpushupdated >", $lastmo)->where("obsolete")->count_all_results("representatives");
		$this->load->view('cms/blankpage', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}
}

