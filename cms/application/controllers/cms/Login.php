<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('syncp_logged_in') == TRUE)
	    	{
	        	redirect("cms");
	    	}
	}
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('pwd', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->session->userdata('fflcp_logged_in') == TRUE)
		{
			redirect("cms");
		}
		else {
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('cms/panel_login');
			}
			else
			{
				$this->load->model('CMS_Accounts');
				
				$result = $this->CMS_Accounts->loginUser();
				
				if ($result == "success") {
					redirect ('cms');
				}
				else { //if fail
					$data['login'] = "fail";
					$this->load->view('cms/panel_login', $data);
				}
			}
		}
	}
	public function testLogin()
	{
		$this->load->model('CMS_Accounts');
		$this->CMS_Accounts->initAdministrator();
	}
	public function importStuff(){	
		ini_set('max_execution_time', 3000);	
		$row = 2;
		$masterarr = array();
		$cityarr = array();
		$sel = "INSERT into specialist (xmlhcpid, specialistname, specialty, institutioncode, institution, medrepcode, aplcode, phone) values ";
		if (($handle = fopen("doctorlist.csv", "r")) !== FALSE) {
		   	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		   		if (in_array($data[0], $cityarr)){
		   			continue;
		   		}
		        $row++;
		        if ($row == 1){
		        	continue;
		        }
		        // $num = count($data);
		        // echo "<p> $num fields in line $row: <br /></p>\n";
		        if ($data[2] == ""){
		        	continue;
		        }
		        $repcode = $data[0];
		        $name = $data[1];
		        $reptype = $data[3];
		        $institutioncode = $data[4];
		        $institution = $data[7];
		        $medrepcode = $data[5];
		        $aplcode = $data[6];
		        $phone = $data[8];

		        $cityarr[] = $data[0];
		        $sel .= "('$repcode','$name','$reptype','$institutioncode','$institution','$medrepcode','$aplcode','$phone'),";
		        // echo $data[1];
	    	}
	    	echo $sel;
	    	fclose($handle);
		}	
		// $this->db->insert_batch("representatives", $masterarr);
	}
}

