<?php
class Masterdata extends CI_Controller {

	function __construct()
	{
		parent::__construct();
date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
	}

	public function accounts()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_accs"; 
		$data['HeaderSuffix'] = "Accounts"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$srcsel = $this->input->get("f");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Apotik";
		$data['listicon'] = "home";
		$data['tabletitles'] = array("Region", "Distributor", "Branch", "Code", "Name", "Print");
		$data['tablesorting'] = array("region", "distributor", "branch", "customercode", "hospitalname", "");

		if ($srcquery || $srcsel){
			$data['addllink'] .= "&q=$srcquery";
			$data['addllink'] .= "&f=$srcsel";
			$data['items'] = $this->CMS_Masterdata->searchHospitals($srcquery, $page, $limit, $sorting, $srcsel);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchHospitalPages($limit, $srcquery, $srcsel);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;			
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllHospitalsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalHospitalPages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['f'] = $srcsel;
		$data['sitelink'] = site_url("cms/masterdata/accounts");
		$this->load->view('cms/hospital_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function promos()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_promos"; 
		$data['HeaderSuffix'] = "Promos"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$srcsel = $this->input->get("f");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Promo";
		$data['listicon'] = "usd";
		$data['tabletitles'] = array("Name", "Cut", "Valid Until", "");
		$data['tablesorting'] = array("promoname", "", "promoend", "");

		if ($srcquery || $srcsel){
			$data['addllink'] .= "&q=$srcquery";
			$data['addllink'] .= "&f=$srcsel";
			$data['items'] = $this->CMS_Masterdata->searchPromos($srcquery, $page, $limit, $sorting, $srcsel);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchPromoPages($limit, $srcquery, $srcsel);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;			
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllPromosPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalPromoPages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['f'] = $srcsel;
		$data['sitelink'] = site_url("cms/masterdata/promos");
		$this->load->view('cms/promo_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function unassignedaccs()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_accs"; 
		$data['HeaderSuffix'] = "Accounts"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Account";
		$data['listicon'] = "home";
		$data['tabletitles'] = array("Code", "Name", "Channel", "Segment", "Address", "City");
		$data['tablesorting'] = array("xmlaccid", "hospitalname", "hospitaltype", "segmentation", "address", "city");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchUnassignedHospitals($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchUnassignedHospitalPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllUnassignedHospitalsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalUnassignedHospitalPages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/accounts/unassigned");
		$this->load->view('cms/hospital_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function unapprovedaccs()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_accs"; 
		$data['HeaderSuffix'] = "Accounts"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Account";
		$data['listicon'] = "home";
		$data['tabletitles'] = array("Code", "Name", "Channel", "Segment", "HCP Count", "TM Count", "Assignment");
		$data['tablesorting'] = array("xmlaccid", "hospitalname", "hospitaltype", "segmentation", "", "", "");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchUnapprovedHospitals($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchUnapprovedHospitalPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllUnapprovedHospitalsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalUnapprovedHospitalPages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/accounts/unapproved");
		$this->load->view('cms/hospital_appr_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function createnewhospital(){
		if ($this->session->userdata("tier") == 1){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('hospitalname', 'Apotik Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$this->load->model('CMS_Regions');
		$this->load->model('CMS_Masterdata');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_accs"; 
			$data['HeaderSuffix'] = "Apotik"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$data['city'] = $this->CMS_Regions->getCitys();
			$data['segments'] = $this->CMS_Masterdata->getAccSegments();
			$data['channels'] = $this->CMS_Masterdata->getHospitalTypes();
			$data['promos'] = $this->CMS_Masterdata->getAllPromos();
			$this->load->view('cms/hospital_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"hospitalname" => $this->input->post("hospitalname"),
				"phone" => $this->input->post("phone"),
				"address" => $this->input->post("address"),
				"lat" => str_replace(",", ".", $this->input->post("lat")),
				"lng" => str_replace(",", ".", $this->input->post("lng")),
				"createdby" => $this->session->userdata("user_id"),
				"customercode" => $this->input->post("customercode"),
				"branch" => $this->input->post("branch"),
				"distributor" => $this->input->post("distributor"),
				"mgt" => $this->input->post("mgt"),
				"region" => $this->input->post("region")

			);
			$promos = $this->input->post("promos")?$this->input->post("promos"):array();
			$result = $this->CMS_Masterdata->createNewHospital($arr, $promos);
			
			if ($result == "success") {
				redirect ('cms/masterdata/accounts');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_accs"; 
				$data['HeaderSuffix'] = "Apotik"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$data['city'] = $this->CMS_Regions->getCitys();
				$data['segments'] = $this->CMS_Masterdata->getAccSegments();
				$data['channels'] = $this->CMS_Masterdata->getHospitalTypes();
				$data['promos'] = $this->CMS_Masterdata->getAllPromos();
				$this->load->view('cms/hospital_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function newpromo(){

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('promoname', 'Promo Name', 'required');
		$this->form_validation->set_rules('promoend', 'Promo Validity', 'required');
		$this->form_validation->set_rules('discountamount', 'Discount Amount', 'required');
		$this->form_validation->set_rules('dailylimit', 'Daily Usage Limit', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$this->load->model("CMS_Masterdata");

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_promos"; 
			$data['HeaderSuffix'] = "Promos"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$data['hospitals'] = $this->CMS_Masterdata->getAllHospitals();
			$this->load->view('cms/promo_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"promonumber" => $this->generateRandomDigits(),
				"promoname" => $this->input->post("promoname"),
				"discounttype" => $this->input->post("discounttype"),
				"discountamount" => $this->input->post("discountamount"),
				"promoend" => $this->input->post("promoend"),
				"snk" => $this->input->post("snk"),
				"dailylimit" => $this->input->post("dailylimit")
			);
			$hospitals = $this->input->post("hospitals")?$this->input->post("hospitals"):array();
			$result = $this->CMS_Masterdata->createNewPromo($arr, $hospitals);
			
			if ($result == "success") {
				redirect ("cms/masterdata/promos");
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_promos"; 
				$data['HeaderSuffix'] = "Promos"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$data['hospitals'] = $this->CMS_Masterdata->getAllHospitals();
				$this->load->view('cms/promo_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function promoqr($id){
		$promos = $this->db->where("promoaccounts.hospitalid", $id)->join("promoaccounts", "promoaccounts.promoid = promotions.promoid")->where("promotions.obsolete", 0)->join("hospital", "hospital.hospitalid = promoaccounts.hospitalid")->get("promotions");
		if ($promos->num_rows()){
			$data['promos'] = $promos->result();
			$this->load->view('cms/qrcode_img', $data);
		}
	}

	public function printall(){
		$promos = $this->db->join("promoaccounts", "promoaccounts.promoid = promotions.promoid")->join("hospital", "hospital.hospitalid = promoaccounts.hospitalid")->order_by("hospital.branch")->where("promotions.obsolete", 0)->where("hospital.obsolete", 0)->get("promotions");
		if ($promos->num_rows()){
			$data['promos'] = $promos->result();
			$this->load->view('cms/qrcode_batch', $data);
		}
	}

	public function printallpromo($promoid){
		$promos = $this->db->join("promoaccounts", "promoaccounts.promoid = promotions.promoid")->join("hospital", "hospital.hospitalid = promoaccounts.hospitalid")->order_by("hospital.branch")->where("promotions.promoid", $promoid)->where("promotions.obsolete", 0)->where("hospital.obsolete", 0)->get("promotions");
		if ($promos->num_rows()){
			$data['promos'] = $promos->result();
			$this->load->view('cms/qrcode_batch', $data);
		}
	}

	public function deactivatepromo($promoid){
		$promo = $this->db->where("promoid", $promoid)->where("associatedaccount in", "(SELECT hospitalid from hospital where createdby = ".$this->session->userdata("user_id").")", false)->get("promotions");
		if ($promo->num_rows()){
			$this->db->where("promoid", $promoid)->update("promotions", array("obsolete" => 1));
			redirect("cms/masterdata/accounts/".$promo->row()->associatedaccount);
		} else {
			redirect("cms");
		}
	}

	public function reactivatepromo($promoid){
		$promo = $this->db->where("promoid", $promoid)->where("associatedaccount in", "(SELECT hospitalid from hospital where createdby = ".$this->session->userdata("user_id").")", false)->get("promotions");
		if ($promo->num_rows()){
			$this->db->where("promoid", $promoid)->update("promotions", array("obsolete" => 0));
			redirect("cms/masterdata/accounts/".$promo->row()->associatedaccount);
		} else {
			redirect("cms");
		}
	}

	public function hospitaldetail($id, $msg = "none", $type = "success") {
		// $referrer = $_SERVER['HTTP_REFERER'];
		if ($this->session->flashdata("editref") == true){
			$referrer = $this->session->userdata("oldref");
			$this->session->set_flashdata("editref", false);
		}
		// $this->session->set_userdata("oldref", $referrer);
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_accs"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Accounts"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$this->load->model("CMS_Masterdata");
		$data['details'] = $this->CMS_Masterdata->getHospitalDetails($id);
		if (!$data['details']){
			redirect("cms/masterdata/accounts");
		}
		$data['reps'] = $this->CMS_Masterdata->getApotekReps($id);
		// $data['promos'] = $this->CMS_Masterdata->getApotekPromos($id);
		$data['events'] = $this->CMS_Masterdata->getApotekEvents($id);
		$data['promos'] = $this->CMS_Masterdata->getAllPromos();
		$data['custs'] = $this->CMS_Masterdata->getPromoCodesByHospital($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		// $data['referrer'] = $referrer;

		$this->load->view('cms/hospital_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function promodetails($id, $msg = "none", $type = "success") {
		// $referrer = $_SERVER['HTTP_REFERER'];
		if ($this->session->flashdata("editref") == true){
			$referrer = $this->session->userdata("oldref");
			$this->session->set_flashdata("editref", false);
		}
		// $this->session->set_userdata("oldref", $referrer);
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_promos"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Promos"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$this->load->model("CMS_Masterdata");
		$data['details'] = $this->CMS_Masterdata->getPromoDetails($id);
		if (!$data['details']){
			redirect("cms/masterdata/accounts");
		}
		$data['reps'] = $this->CMS_Masterdata->getPromoCodes($id);
		$data['hospitals'] = $this->CMS_Masterdata->getAllHospitals();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		// $data['referrer'] = $referrer;

		$this->load->view('cms/promo_detail', $data);
		$this->load->view('cms/_footer', $data);
	}


	public function editHospital(){
		if ($this->session->userdata("tier") == 1){
			redirect("cms");
		}
		$id = $this->input->post("hospitalid");
		// $name = $this->input->post("territory");

		
		$arr = array(
			"hospitalname" => $this->input->post("hospitalname"),
			"phone" => $this->input->post("phone"),
			"address" => $this->input->post("address"),
			"lat" => str_replace(",", ".", $this->input->post("lat")),
			"lng" => str_replace(",", ".", $this->input->post("lng")),
			"lastupdated" => date("Y-m-d H:i:s"),
			"customercode" => $this->input->post("customercode"),
			"branch" => $this->input->post("branch"),
			"distributor" => $this->input->post("distributor"),
			"mgt" => $this->input->post("mgt"),
			"region" => $this->input->post("region")
		);
		$this->load->model("CMS_Masterdata");
		$promos = $this->input->post("promos")?$this->input->post("promos"):array();
		$result = $this->CMS_Masterdata->editHospital($arr, $id, $promos);
		$this->session->set_flashdata("editref", true);
		if ($result){
			redirect("cms/masterdata/accounts/$id/");
		} else {
			redirect("cms/masterdata/accounts/$id/Edit Failed. Please try again./error");
		}
	}


	public function editPromo(){
		if ($this->session->userdata("tier") == 1){
			redirect("cms");
		}
		$id = $this->input->post("promoid");
		// $name = $this->input->post("territory");

		
		$arr = array(
			"promoname" => $this->input->post("promoname"),
			"discounttype" => $this->input->post("discounttype"),
			"discountamount" => $this->input->post("discountamount"),
			"promoend" => $this->input->post("promoend"),
			"snk" => $this->input->post("snk"),
			"dailylimit" => $this->input->post('dailylimit')
		);
		$this->load->model("CMS_Masterdata");
		$hospitals = $this->input->post("hospitals")?$this->input->post("hospitals"):array();
		$result = $this->CMS_Masterdata->editPromo($arr, $id, $hospitals);

		if ($result){
			redirect("cms/masterdata/promos/$id/");
		} else {
			redirect("cms/masterdata/promos/$id/Edit Failed. Please try again./error");
		}
	}

	public function rooms()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_rooms"; 
		$data['HeaderSuffix'] = "Rooms"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Room";
		$data['listicon'] = "stop";
		$data['tabletitles'] = array("Room Name", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchRooms($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchRoomPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllRoomsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalRoomPages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/rooms");
		$this->load->view('cms/room_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function roomdetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_rooms"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Room"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$data['roomdetails'] = $this->CMS_Masterdata->getRoomDetails($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/room_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function newroom(){
		if ($this->session->userdata("tier") <= 3){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('roomname', 'Room Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_rooms"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "Room"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/room_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Masterdata');
			$arr = array(
				"roomname" => $this->input->post("roomname"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Masterdata->createNewRooms($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/rooms');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_rooms"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "Room"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/room_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editRoom(){
		if ($this->session->userdata("tier") <= 3){
			redirect("cms");
		}
		$id = $this->input->post("roomid");
		$name = $this->input->post("room");

		$arr = array(
			"roomname" => $this->input->post("room"),
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editRoom($arr, $id);
		if ($result){
			redirect("cms/masterdata/rooms/$id/");
		} else {
			redirect("cms/masterdata/rooms/$id/Edit Failed. Please try again./error");
		}
	}

	public function specialists()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_docs"; 
		$data['HeaderSuffix'] = "Doctors"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcsel = $this->input->get('f');
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Doctors";
		$data['listicon'] = "user";
		$data['tabletitles'] = array("Code", "Name", "Specialty", "Institution", "Phone");
		$data['tablesorting'] = array("xmlhcpid", "specialistname", "specialty", "institution", "phone");

		if ($srcquery || $srcsel){
			$data['addllink'] .= "&q=$srcquery";
			$data['addllink'] .= "&f=$srcsel";
			$data['items'] = $this->CMS_Masterdata->searchHCPs($srcquery, $page, $limit, $sorting, $srcsel);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchHCPPages($limit, $srcquery, $srcsel);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllHCPsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalHCPPages($limit);
		}

		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['f'] = $srcsel;
		$data['sitelink'] = site_url("cms/masterdata/specialists");
		$this->load->view('cms/specialist_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function events()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_evts"; 
		$data['HeaderSuffix'] = "Events"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcsel = $this->input->get('f');
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Events";
		$data['listicon'] = "file";
		$data['tabletitles'] = array("Title", "Start", "End", "RSVP Count");
		$data['tablesorting'] = array("eventtitle", "datestart", "dateend", "goingcount");

		if ($srcquery || $srcsel){
			$data['addllink'] .= "&q=$srcquery";
			$data['addllink'] .= "&f=$srcsel";
			$data['items'] = $this->CMS_Masterdata->searchEvents($srcquery, $page, $limit, $sorting, $srcsel);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchEventPages($limit, $srcquery, $srcsel);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllEventsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalEventPages($limit);
		}

		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['f'] = $srcsel;
		$data['sitelink'] = site_url("cms/masterdata/events");
		$this->load->view('cms/events_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function eventdetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_evts"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Events"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$data['details'] = $this->CMS_Masterdata->getEventDetails($id);
		$data['attendees'] = $this->CMS_Masterdata->getEventAttendees($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/event_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function createnewspec(){
		if ($this->session->userdata("tier") == 1){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('specialistname', 'HCP Name', 'required');
		$this->form_validation->set_rules('xmlhcpid', 'HCP Code', 'required|is_unique[specialist.xmlhcpid]');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$this->load->model('CMS_Regions');
		$this->load->model('CMS_Masterdata');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_docs"; 
			$data['HeaderSuffix'] = "Specialists"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/specialist_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"xmlhcpid" => $this->input->post("xmlhcpid"),
				"specialistname" => $this->input->post("specialistname"),
				"phone" => $this->input->post("phone"),
				"address" => $this->input->post("address"),
				"specialty" => $this->input->post("specialty"),
				"createdby" => $this->session->userdata("user_id"),
				"institutioncode" => $this->input->post("inscode"),
				"institution" => $this->input->post("insname"),
				"medrepcode" => $this->input->post("medrepcode"),
				"aplcode" => $this->input->post("aplcode"),
			);
			if ($this->session->userdata("tier") < 3){
				$arr['approved'] = 0;
			}
			$result = $this->CMS_Masterdata->createNewSpecialist($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/specialists');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_docs"; 
				$data['HeaderSuffix'] = "Specialists"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during Hospital Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/specialist_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function createnewevent(){
		if ($this->session->userdata("tier") == 1){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('eventtitle', 'Event Title', 'required');
		$this->form_validation->set_rules('datestart', 'Date Start', 'required');
		$this->form_validation->set_rules('dateend', 'Date End', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$this->load->model('CMS_Regions');
		$this->load->model('CMS_Masterdata');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_evts"; 
			$data['HeaderSuffix'] = "Events"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/events_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"eventtitle" => $this->input->post("eventtitle"),
				"eventsubtitle" => $this->input->post("eventsubtitle"),
				"datestart" => $this->input->post("datestart"),
				"dateend" => $this->input->post("dateend"),
				"eventprice" => $this->input->post("eventprice"),
				"eventcategory" => $this->input->post("eventcategory"),
				"eventvenue" => $this->input->post("eventvenue"),
				"facilities" => $this->input->post("medrepcode"),
				"venuelat" => $this->input->post("venuelat"),
				"venuelng" => $this->input->post("venuelng"),
				"participants" => $this->input->post("aplcode"),
				"description" => $this->input->post("description")
			);
			if ($this->session->userdata("tier") < 3){
				
			}
			$result = $this->CMS_Masterdata->createNewEvent($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/events');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_evts"; 
				$data['HeaderSuffix'] = "Events"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during Hospital Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/events_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editEvent(){
		if ($this->session->userdata("tier") < 1){
			redirect("cms");
		}
		$id = $this->input->post("eventid");
		// $name = $this->input->post("territory");

		$arr = array(
			"eventtitle" => $this->input->post("eventtitle"),
			"eventsubtitle" => $this->input->post("eventsubtitle"),
			"datestart" => $this->input->post("datestart"),
			"dateend" => $this->input->post("dateend"),
			"eventprice" => $this->input->post("eventprice"),
			"eventcategory" => $this->input->post("eventcategory"),
			"eventvenue" => $this->input->post("eventvenue"),
			"facilities" => $this->input->post("facilities"),
			"venuelat" => $this->input->post("venuelat"),
			"venuelng" => $this->input->post("venuelng"),
			"participants" => $this->input->post("participants"),
			"description" => $this->input->post("description")
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editEvent($arr, $id);
		if ($result){
			redirect("cms/masterdata/events/$id/");
		} else {
			redirect("cms/masterdata/events/$id/Edit Failed. Please try again./error");
		}
	}

	public function specdetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_docs"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Doctor"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$this->load->model("CMS_Masterdata");
		$data['details'] = $this->CMS_Masterdata->getSpecialistDetails($id);
		$data['city'] = $this->CMS_Regions->getCitys();
		$data['segments'] = $this->CMS_Masterdata->getHcpSegments();
		$data['channels'] = $this->CMS_Masterdata->getSpecialties();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/specialist_detail', $data);
		$this->load->view('cms/_footer', $data);
	}


	public function editSpecialist(){
		if ($this->session->userdata("tier") < 1){
			redirect("cms");
		}
		$id = $this->input->post("specid");
		// $name = $this->input->post("territory");

		$arr = array(
			"specialistname" => $this->input->post("specialistname"),
			"phone" => $this->input->post("phone"),
			"address" => $this->input->post("address"),
			"specialty" => $this->input->post("specialty"),
			"institutioncode" => $this->input->post("inscode"),
			"institution" => $this->input->post("insname"),
			"medrepcode" => $this->input->post("medrepcode"),
			"aplcode" => $this->input->post("aplcode"),
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editSpecialist($arr, $id);
		if ($result){
			redirect("cms/masterdata/specialists/$id/");
		} else {
			redirect("cms/masterdata/specialists/$id/Edit Failed. Please try again./error");
		}
	}

	public function specialty()
	{
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_spcy"; 
		$data['HeaderSuffix'] = "Specialties"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Specialty";
		$data['listicon'] = "cutlery";
		$data['tabletitles'] = array("Specialty", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchSpecialtys($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchSpecialtyPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllSpecialtysPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSpecialtyPages($limit);
		}
				
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/specialty");
		$this->load->view('cms/specialty_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function specialtydetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_spcy"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Specialty"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$data['specialtydetails'] = $this->CMS_Masterdata->getSpecialtyDetails($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/specialty_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function newspecialty(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('specialtyname', 'Specialty Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_spcy"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "Specialty"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/specialty_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Masterdata');
			$arr = array(
				"specialtyname" => $this->input->post("specialtyname"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Masterdata->createNewSpecialties($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/specialty');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_spcy"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "Specialty"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/specialty_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editSpecialty(){
		$id = $this->input->post("specialtyid");
		$name = $this->input->post("specialty");

		$arr = array(
			"specialtyname" => $this->input->post("specialty"),
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editSpecialty($arr, $id);
		if ($result){
			redirect("cms/masterdata/specialty/$id/");
		} else {
			redirect("cms/masterdata/specialty/$id/Edit Failed. Please try again./error");
		}
	}

	public function editAccSegName(){
		$id = $this->input->post("segmentid");
		$name = $this->input->post("segmentation");

		$arr = array(
			"segmentation" => $name,
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editAccSegment($arr, $id);
		if ($result){
			redirect("cms/masterdata/accsegments/$id/");
		} else {
			redirect("cms/masterdata/accsegments/$id/Edit Failed. Please try again./error");
		}
	}

	public function editAccChannelName(){
		$id = $this->input->post("typeid");
		$name = $this->input->post("channel");

		$arr = array(
			"typename" => $name,
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editAccChannel($arr, $id);
		if ($result){
			redirect("cms/masterdata/accchannel/$id/");
		} else {
			redirect("cms/masterdata/accchannel/$id/Edit Failed. Please try again./error");
		}
	}

	public function editHcpSegName(){
		$id = $this->input->post("segmentid");
		$name = $this->input->post("segmentation");

		$arr = array(
			"segmentation" => $name,
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Masterdata");
		$result = $this->CMS_Masterdata->editHcpSegment($arr, $id);
		if ($result){
			redirect("cms/masterdata/segments/$id/");
		} else {
			redirect("cms/masterdata/segments/$id/Edit Failed. Please try again./error");
		}
	}

	public function segments()
	{
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_segs"; 
		$data['HeaderSuffix'] = "HCP Segments"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "HCP Segment";
		$data['listicon'] = "align-center";
		$data['tabletitles'] = array("Segment", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchSegments($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchSegmentPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllSegmentsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSegmentPages($limit);
		}
				
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/segments");
		$this->load->view('cms/hcpseg_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function newsegments(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('segmentation', 'HCP Segment Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_segs"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "HCP Segments"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/hcpseg_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Masterdata');
			$arr = array(
				"segmentation" => $this->input->post("segmentation"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Masterdata->createNewSegments($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/segments');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_segs"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "HCP Segments"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/hcpseg_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function segdetails($id, $msg = "none", $type = "success"){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_segs"; 
		$data['HeaderSuffix'] = "HCP Segments"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$data['segment'] = $this->CMS_Masterdata->getHCPSegmentInfo($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/hcpseg_detail', $data);
		$this->load->view('cms/_footer', $data);		
	}

	public function accsegdetails($id, $msg = "none", $type = "success"){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_hsegs"; 
		$data['HeaderSuffix'] = "ACC Segments"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$data['segment'] = $this->CMS_Masterdata->getACCSegmentInfo($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/accseg_detail', $data);
		$this->load->view('cms/_footer', $data);		
	}

	public function newaccsegments(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('segmentation', 'ACC Segment Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_hsegs"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "ACC Segments"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/accseg_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Masterdata');
			$arr = array(
				"segmentation" => $this->input->post("segmentation"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Masterdata->createNewAccseg($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/accsegments');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_hsegs"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "ACC Segments"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during Segment Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/accseg_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editaccsegvisit(){
		$accid = $this->input->post("id");
		$segments = $this->input->post("segmentid");
		$segmentvisits = $this->input->post("visits");
		$this->load->model("CMS_Masterdata");

		foreach ($segments as $key=>$segid){
			$arr = array("visits" => $segmentvisits[$key]);
			$this->CMS_Masterdata->editVisit($arr, $segid, $accid);
		}
		redirect("cms/masterdata/accsegments/$accid");
	}

	public function edithcpsegvisit(){
		$hcpid = $this->input->post("id");
		$segments = $this->input->post("segmentid");
		$segmentvisits = $this->input->post("visits");
		$this->load->model("CMS_Masterdata");

		foreach ($segments as $key=>$segid){
			$arr = array("visits" => $segmentvisits[$key]);
			$this->CMS_Masterdata->editVisit($arr, $hcpid, $segid);
		}
		redirect("cms/masterdata/segments/$hcpid");
	}

	public function accsegments()
	{
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_hsegs"; 
		$data['HeaderSuffix'] = "ACC Segments"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "ACC Segment";
		$data['listicon'] = "align-center";
		$data['tabletitles'] = array("Segment", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchACCSegments($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchACCSegmentPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllACCSegmentsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalACCSegmentPages($limit);
		}
				
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/accsegments");
		$this->load->view('cms/accseg_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function accchannel()
	{
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_hchannel"; 
		$data['HeaderSuffix'] = "ACC Channels"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "ACC Channels";
		$data['listicon'] = "align-center";
		$data['tabletitles'] = array("Channel", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Masterdata->searchACCChannels($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalSearchACCChannelPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Masterdata->getAllACCChannelsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Masterdata->getTotalACCChannelPages($limit);
		}
				
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/masterdata/accchannel");
		$this->load->view('cms/accchannel_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function newaccchannel(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('channel', 'ACC Channel Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "datadropdown";
			$data['datatype'] = "dd_hchannel"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "ACC Channels"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/accchannel_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Masterdata');
			$arr = array(
				"typename" => $this->input->post("channel"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Masterdata->createNewChannel($arr);
			
			if ($result == "success") {
				redirect ('cms/masterdata/accchannel');
			}
			else { //if fail
				$data['dropdown'] = "datadropdown";
				$data['datatype'] = "dd_hchannel"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "ACC Channels"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during Channel Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/accchannel_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function accchanneldetails($id, $msg = "none", $type = "success"){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}
		$data['dropdown'] = "datadropdown";
		$data['datatype'] = "dd_hchannel"; 
		$data['HeaderSuffix'] = "ACC Channel"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Masterdata");
		$data['segment'] = $this->CMS_Masterdata->getACCChannelInfo($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/accchannel_detail', $data);
		$this->load->view('cms/_footer', $data);		
	}


	public function searchhcp(){
		$q = $this->input->get("query");
		$cls = new stdClass();
		$cls->query = $q;
		$this->load->model("CMS_Masterdata");
		$cls->suggestions = $this->CMS_Masterdata->searchHcp($q);
		echo json_encode($cls);
	}

	public function addHospitalRoom(){
		$hcpid = $this->input->post("hcpid");
		$roomid = $this->input->post("roomid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->addHospitalRoom($hcpid, $roomid, $hospitalid);
	}

	public function editHospitalRoom(){
		$hcpid = $this->input->post("hcpid");
		$roomid = $this->input->post("roomid");
		$hospitalid = $this->input->post("hospitalid");
		$oldroomid = $this->input->post("oldroomid");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->editHospitalRoom($hcpid, $roomid, $hospitalid, $oldroomid);
	}

	public function deleteHospitalRoom(){
		$hcpid = $this->input->post('specid');
		$roomid = $this->input->post("roomid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->deleteHospitalRoom($hcpid, $roomid, $hospitalid);		
	}

	public function addHospitalTerritory(){
		$territoryid = $this->input->post("territoryid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->addHospitalTerritory($territoryid, $hospitalid);
	}

	public function deleteHospitalTerritory(){
		$territoryid = $this->input->post("territoryid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->deleteHospitalTerritory($territoryid, $hospitalid);		
	}


	public function deactivateAccount(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->deactivateAccount($id);
	}

	public function reactivateAccount(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->reactivateAccount($id);
	}

	public function approveAccount(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		if ($this->session->userdata("tier") <= 3){
			echo "You are not allowed to do this operation"; 
		} else {
			echo $this->CMS_Masterdata->approveAccount($id);
		}
	}
	public function rejectAccount(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		if ($this->session->userdata("tier") <= 3){
			echo "You are not allowed to do this operation"; 
		} else {
			echo $this->CMS_Masterdata->rejectAccount($id);
		}
	}
	public function deactivateHCP(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->deactivateHCP($id);
	}

	public function reactivateHCP(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->reactivateHCP($id);
	}

	public function approveHCP(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		if ($this->session->userdata("tier") <= 3){
			echo "You are not allowed to do this operation"; 
		} else {
			echo $this->CMS_Masterdata->approveHCP($id);
		}
	}

	public function rejectHCP(){
		$id = $this->input->post("id");
		$this->load->model("CMS_Masterdata");
		if ($this->session->userdata("tier") <= 3){
			echo "You are not allowed to do this operation"; 
		} else {
			echo $this->CMS_Masterdata->rejectHCP($id);
		}
	}

	public function download(){
		$y = date("Y");
		$customers = $this->db->where("YEAR(dateregged)", $y, false)->order_by("dateregged", "desc")->select("MONTH(dateregged) as month, region, branch, customercode, hospitalname, fullname, contactno, email, uniquecode, codeusage as codestatus, promoname")->join("hospital", "hospital.hospitalid = customers.hospitalid")->join("promotions", "promotions.promoid = customers.promoid")->get("customers")->result();
		$this->iterateCustomerData($customers, "All Customer Data $y");
	}

	public function downloadpromocustomer($promoid){
		$y = date("Y");
		$customers = $this->db->where("YEAR(dateregged)", $y, false)->order_by("dateregged", "desc")->select("MONTH(dateregged) as month, region, branch, customercode, hospitalname, fullname, contactno, email, uniquecode, codeusage as codestatus, promoname")->join("hospital", "hospital.hospitalid = customers.hospitalid")->join("promotions", "promotions.promoid = customers.promoid")->where("customers.promoid", $promoid)->get("customers")->result();
		$this->iterateCustomerData($customers, "All Customer Data $y ".$customers[0]->promoname);
	}

	private function iterateCustomerData($customers, $filename){
		ini_set('memory_limit', '-1');
		$limit = 1000;
		$current = 1;
		$b = true;
		
		$filename = str_replace(array("/", "\\"), "", $filename);

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		fputcsv($output, array("Month", "Region", "Branch", "Apotik Code", "Apotik Name", "Customer Name", "Customer Phone", "Customer E-mail", "Unique Code", "Status", "Promo"));

		$transidarr = array();
		foreach ($customers as $sch){
			fputcsv($output, array($sch->month, $sch->region, $sch->branch, "'".$sch->customercode, $sch->hospitalname, $sch->fullname, "'".$sch->contactno, $sch->email, $sch->uniquecode, ($sch->codestatus==1?"Unused":"Claimed"), $sch->promoname));
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}
	
	public function downloadevtattendees(){
		$evtid = $this->input->get('evtid');
		ini_set('memory_limit', '-1');
		
		$evtdtl = $this->db->where("eventid", $evtid)->get("event")->row();
		$filename = $evtdtl->eventtitle.'Attendees.csv';
		$filepath = $_SERVER["DOCUMENT_ROOT"] . $evtdtl->eventtitle.'Attendees.csv';
		$output = fopen($filepath, 'w+');

		$this->db->from("specialist ei");
		$this->db->join("eventattendance ea", "ea.specid = ei.specid");
		$this->db->where("ea.eventid", $evtid);
		fputcsv($output, array("RSVP Date", "Spec Code", "Name", "Specialty", "Address", "Phone", "Institution", "Institution Code", "Med Rep Code", "APL Code"));
		$schedules = $this->db->get()->result();
		foreach ($schedules as $sch){
			fputcsv($output, array($sch->rsvpdate ,$sch->xmlhcpid, $sch->specialistname, $sch->specialty, $sch->address, $sch->phone, $sch->institution, $sch->institutioncode, $sch->medrepcode, $sch->aplcode));
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}

	private function generateRandomDigits($length = 10) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}
?>