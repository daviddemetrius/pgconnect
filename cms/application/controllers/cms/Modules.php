<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$arrayOfFiles = array();
class Modules extends CI_Controller {

	function __construct()
	{
		parent::__construct();
date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	}

	public function index()
	{
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_mods"; 
		$data['HeaderSuffix'] = "Modules"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$srcsel = $this->input->get("f");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		
		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['addllink'] .= "&f=$srcsel";
			$data['modules'] = $this->CMS_Modules->searchModules($srcquery, $page, $limit, $sorting, $srcsel);
			$data['totalpage'] = $this->CMS_Modules->getTotalSearchModulePages($limit, $srcquery, $srcsel);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['modules'] = $this->CMS_Modules->getAllModulesPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalModulePages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['f'] = $srcsel;
		$data['sitelink'] = site_url("cms/modules/");
		$this->load->view('cms/module_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function pages()
	{
		if (!in_array($this->session->userdata("tier"), array(999, 2)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_pages"; 
		$data['HeaderSuffix'] = "Pages"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$srcsel = $this->input->get("f");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		
		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['addllink'] .= "&f=$srcsel";
			$data['pages'] = $this->CMS_Modules->searchPages($srcquery, $page, $limit, $sorting, $srcsel);
			$data['totalpage'] = $this->CMS_Modules->getTotalSearchPagePages($limit, $srcquery, $srcsel);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['pages'] = $this->CMS_Modules->getAllPagesPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalPagePages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['f'] = $srcsel;
		$data['sitelink'] = site_url("cms/modules/pages");
		$this->load->view('cms/modulepages_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function cycles()
	{
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_cycle"; 
		$data['HeaderSuffix'] = "Cycles"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		
		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['cycles'] = $this->CMS_Modules->searchCycles($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalSearchCyclePages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['cycles'] = $this->CMS_Modules->getAllCyclesPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalCyclePages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/modules/cycles");
		$this->load->view('cms/cycle_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function category()
	{
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_cat"; 
		$data['HeaderSuffix'] = "Page Category"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Page Category";
		$data['listicon'] = "link";
		$data['tabletitles'] = array("Category Name");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Modules->searchPageCategorys($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalSearchPageCategoryPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Modules->getAllPageCategorysPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalPageCategoryPages($limit);
		}
				
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/modules/category");
		$this->load->view('cms/pagecat_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function group()
	{
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_grp"; 
		$data['HeaderSuffix'] = "Module Group"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Module Group";
		$data['listicon'] = "link";
		$data['tabletitles'] = array("Group Name");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Modules->searchModuleGroups($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalSearchModuleGroupPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Modules->getAllModuleGroupsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Modules->getTotalModuleGroupPages($limit);
		}
				
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/modules/group");
		$this->load->view('cms/modulegroup_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function pagedetails($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999, 2)) ){
			redirect("cms");
		}
		$referrer = $_SERVER['HTTP_REFERER'];
		if ($this->session->flashdata("editref") == true){
			$referrer = $this->session->userdata("oldref");
			$this->session->set_flashdata("editref", false);
		}
		$this->session->set_userdata("oldref", $referrer);
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_pages"; 
		$data['HeaderSuffix'] = "Pages"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$data['pagedata'] = $this->CMS_Modules->getPageInfo($id);
		$data['pagecats'] = $this->CMS_Modules->getAllPageCategories();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$data['referrer'] = $referrer;
		$this->load->view('cms/modulepages_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function groupdetail($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_grp"; 
		$data['HeaderSuffix'] = "Module Group"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$data['groupdata'] = $this->CMS_Modules->getGroupInfo($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/modulegroup_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function cycledetail($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_cycle"; 
		$data['HeaderSuffix'] = "Cycle Detail"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$data['cycledata'] = $this->CMS_Modules->getCycleDetail($id);
		$data['hcpsegs'] = $this->CMS_Modules->getHCPSegments();
		$data['accsegs'] = $this->CMS_Modules->getACCSegments();
		$data['reptypes'] = $this->CMS_Modules->getRepTypes();
		$data['specialties'] = $this->CMS_Modules->getHCPSpecialties();
		$data['modulelist'] = $this->CMS_Modules->getModulesForSearch();
		$data['rooms'] = $this->CMS_Modules->getAllRooms();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/cycle_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function moduledetails($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$referrer = $_SERVER['HTTP_REFERER'];
		if ($this->session->flashdata("editref") == true){
			$referrer = $this->session->userdata("oldref");
			$this->session->set_flashdata("editref", false);
		}
		$this->session->set_userdata("oldref", $referrer);
		$data['dropdown'] = "moduledropdown";
		$data['datatype'] = "md_mods"; 
		$data['HeaderSuffix'] = "Modules"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$data['houses'] = $this->CMS_Modules->getHouses();
		$data['module'] = $this->CMS_Modules->getModuleInfo($id);
		$data['pagelist'] = $this->CMS_Modules->getAllPagesForSearch();
		$data['grouplist'] = $this->CMS_Modules->getAllGroupsForSearch();
		$data['reptypes'] = $this->CMS_Modules->getRepTypes();
		$data['hospitals'] = $this->CMS_Modules->getHospitals();
		$data['modcats'] = $this->CMS_Modules->getModuleGroups();
		$data['referrer'] = $referrer;
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/module_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function createnewpage(){
		if (!in_array($this->session->userdata("tier"), array(999, 2)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$allowedTypes = array("image/jpg", "image/jpeg", "image/png", "image/gif");
		
		$this->form_validation->set_rules('pagetitle', 'Page Title', 'required');
		if (empty($_FILES['userfile']['name'])){
		    	$this->form_validation->set_rules('uefile', 'Page File', 'required');
		} else if(!in_array($_FILES['userfile']['type'], $allowedTypes)){
			$this->form_validation->set_rules('uefile', 'File Type Image', 'required');
		}

		if (empty($_FILES['thumbimg']['name'])){
		    	$this->form_validation->set_rules('thumbimg', 'Thumbnail Image', 'required');
		} else if(!in_array($_FILES['thumbimg']['type'], $allowedTypes)){
			$this->form_validation->set_rules('uefile', 'File Type Image', 'required');
		}
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		$this->load->model('CMS_Modules');
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "moduledropdown";
			$data['datatype'] = "md_pages"; 
			$data['HeaderSuffix'] = "Pages"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$data['pagecats'] = $this->CMS_Modules->getAllPageCategories();
			$this->load->view('cms/modulepages_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$extent = "jpg";
			$filename = $_FILES['userfile']['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if ($ext == "gif"){
				$extent = "gif";
			}
			$arr = array(
				"pagetitle" => $this->input->post("pagetitle"),
				"pagecategory" => $this->input->post("pagecategory"),
				"description" => $this->input->post("description"),
				"pagename" => $extent,
				"obsolete" => 0,
				"pagehtmllink" => $extent,
				"createdby" => $this->session->userdata("user_id"),
				"folderlisting" => $extent,
				"created" => date("Y-m-d H:i:s"),
				"lastupdated" => date("Y-m-d H:i:s")
			);
			$insertid = $this->CMS_Modules->createNewPage($arr);

			
			if ($insertid) {

				$config['upload_path']          = './assets/pages/';
		        $config['allowed_types']        = 'jpg|png|jpeg|gif'; 
		        $config['file_name']        	= $insertid.'.'.$extent; 
		        $config['overwrite']	        = true; 
				$this->load->library('upload', $config);
		        $this->upload->do_upload('userfile');

				$config['upload_path']          = './assets/thumbs/';
		        $config['allowed_types']        = 'jpg|jpeg|png|bmp'; 
		        $config['file_name']        	= $insertid.'.jpg'; 
		        $config['mod_mime_fix']        	= false; 
		        $config['overwrite']	        = true; 
		        $this->upload->initialize($config);
		        $this->upload->do_upload('thumbimg');

				redirect ('cms/modules/pages/');
			}
			else {
				$data['dropdown'] = "moduledropdown";
				$data['datatype'] = "md_pages"; 
				$data['HeaderSuffix'] = "Pages"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['pagecats'] = $this->CMS_Modules->getAllPageCategories();
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/modulepages_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function createnewmodule(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$allowedTypes = array("application/zip", "application/x-zip", "application/octet-stream", "application/x-zip-compressed");
		
		$this->form_validation->set_rules('moduletitle', 'Module Title', 'required');
		$this->form_validation->set_rules('moduledesc', 'Module Description', 'required');
		$this->form_validation->set_rules('periodstart', 'Active Period Start', 'required');
		$this->form_validation->set_rules('periodend', 'Active Period End', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "moduledropdown";
			$data['datatype'] = "md_mods"; 
			$data['HeaderSuffix'] = "Modules"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model('CMS_Modules');
			$data['houses'] = $this->CMS_Modules->getHouses();
			$data['reptypes'] = $this->CMS_Modules->getRepTypes();
			$data['hospitals'] = $this->CMS_Modules->getHospitals();
			$data['modcats'] = $this->CMS_Modules->getModuleGroups();
			$this->load->view('cms/module_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"moduletitle" => $this->input->post("moduletitle"),
				"moduledesc" => $this->input->post("moduledesc"),
				"periodstart" => $this->input->post("periodstart"),
				"periodend" => $this->input->post("periodend"),
				"generaltype" => 0,
				"obsolete" => 1,
				"createdby" => $this->session->userdata("user_id")
			);
			$this->load->model('CMS_Modules');
			$reptypes = $this->input->post("reptype")?$this->input->post("reptype"):array();
			$hospitals = $this->input->post("hospitals")?$this->input->post("hospitals"):array();
			$modcats = $this->input->post("modcats")?$this->input->post("modcats"):array();
			$result = $this->CMS_Modules->createNewModule($arr, $reptypes, $hospitals, $modcats);
			
			if ($result == "success") {
				redirect ('cms/modules/');
			}
			else {
				$data['dropdown'] = "moduledropdown";
				$data['datatype'] = "md_mods"; 
				$data['HeaderSuffix'] = "Modules"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$data['houses'] = $this->CMS_Modules->getHouses();
				$data['reptypes'] = $this->CMS_Modules->getRepTypes();
				$data['hospitals'] = $this->CMS_Modules->getHospitals();
				$data['modcats'] = $this->CMS_Modules->getModuleGroups();
				$this->load->view('cms/module_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function quiz($moduleid = null){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		if ($moduleid == null){
			redirect("cms/modules");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('surveytext', 'Quiz question', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "moduledropdown";
			$data['datatype'] = "md_mods"; 
			$data['HeaderSuffix'] = "Modules"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model('CMS_Modules');
			$data['moduleid'] = $moduleid;
			$this->load->view('cms/survey_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"quiztext" => $this->input->post("surveytext"),
				"availability" => 1,
				"quiztype" => $this->input->post("surveytype"),
				"moduleid" => $moduleid
			);
			$this->load->model('CMS_Modules');
			$answers = $this->input->post("answers")?$this->input->post("answers"):array();
			$anstxt = "";
			$delim = "";
			foreach ($answers as $vals){
				$anstxt .= $delim.$vals;
				$delim = "xvz:zvx";
			}
			$arr['quizanswer'] = $anstxt;
			$result = $this->CMS_Modules->createNewQuiz($arr);
			
			if ($result == "success") {
				redirect ('cms/modules/'.$moduleid);
			}
			else {
				$data['dropdown'] = "moduledropdown";
				$data['datatype'] = "md_mods"; 
				$data['HeaderSuffix'] = "Modules"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$data['moduleid'] = $moduleid;
				$this->load->view('cms/survey_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function deletequiz($moduleid, $quizid){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$this->load->model("CMS_Modules");
		$this->CMS_Modules->deactivateQuiz($quizid, $moduleid);
		redirect("cms/modules/$moduleid");
	}

	public function activatequiz($moduleid, $quizid){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$this->load->model("CMS_Modules");
		$this->CMS_Modules->activateQuiz($quizid, $moduleid);
		redirect("cms/modules/$moduleid");
	}

	public function createnewcycle(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$allowedTypes = array("application/zip", "application/x-zip", "application/octet-stream", "application/x-zip-compressed");
		
		$this->form_validation->set_rules('cycletitle', 'Cycle Title', 'required');
		$this->form_validation->set_rules('periodstart', 'Date Start', 'required');
		$this->form_validation->set_rules('periodend', 'Date End', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		// var_dump($this->input->post());
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "moduledropdown";
			$data['datatype'] = "md_cycle"; 
			$data['HeaderSuffix'] = "Cycles"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model('CMS_Modules');
			$data['hcpsegs'] = $this->CMS_Modules->getHCPSegments();
			$data['accsegs'] = $this->CMS_Modules->getACCSegments();
			$data['reptypes'] = $this->CMS_Modules->getRepTypes();
			$data['specialties'] = $this->CMS_Modules->getHCPSpecialties();
			$data['rooms'] = $this->CMS_Modules->getAllRooms();
			$this->load->view('cms/cycle_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"cycletitle" => $this->input->post("cycletitle"),
				"periodstart" => $this->input->post("periodstart"),
				"periodend" => $this->input->post("periodend"),
				"obsolete" => 1,
				"createdby" => $this->session->userdata("user_id")
			);

			$cyclevisittype = $this->input->post("cyclevisittype");
			$reptypes = $this->input->post("reptype")?$this->input->post("reptype"):array();
			$accsegments = $this->input->post("accsegments")?$this->input->post("accsegments"):array();
			$hcpsegments = $this->input->post("hcpsegments")?$this->input->post("hcpsegments"):array();
			$specs = $this->input->post("specs")?$this->input->post("specs"):array();
			$rooms = $this->input->post("rooms")?$this->input->post("rooms"):array();

			$this->load->model('CMS_Modules');
			$result = $this->CMS_Modules->createNewCycle($arr, $cyclevisittype, $reptypes, $accsegments, $hcpsegments, $specs, $rooms);
			
			if ($result == "success") {
				redirect ('cms/modules/cycles/');
			}
			else {
				$data['dropdown'] = "moduledropdown";
				$data['datatype'] = "md_cycle"; 
				$data['HeaderSuffix'] = "Cycles"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$data['hcpsegs'] = $this->CMS_Modules->getHCPSegments();
				$data['accsegs'] = $this->CMS_Modules->getACCSegments();
				$data['reptypes'] = $this->CMS_Modules->getRepTypes();
				$data['specialties'] = $this->CMS_Modules->getHCPSpecialties();
				$data['rooms'] = $this->CMS_Modules->getAllRooms();
				$this->load->view('cms/cycle_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function createnewgroup(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$allowedTypes = array("application/zip", "application/x-zip", "application/octet-stream", "application/x-zip-compressed");
		
		$this->form_validation->set_rules('groupname', 'Group Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "moduledropdown";
			$data['datatype'] = "md_grp"; 
			$data['HeaderSuffix'] = "Module Group"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/modulegroup_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"groupname" => $this->input->post("groupname"),
				"filterfor" => $this->input->post('filterfor')
			);
			$this->load->model('CMS_Modules');

			$result = $this->CMS_Modules->createNewModuleGroup($arr);
			
			if ($result == "success") {
				redirect ('cms/modules/group');
			}
			else {
				$data['dropdown'] = "moduledropdown";
				$data['datatype'] = "md_grp"; 
				$data['HeaderSuffix'] = "Module Group"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/modulegroup_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function createnewcategory(){
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$allowedTypes = array("application/zip", "application/x-zip", "application/octet-stream", "application/x-zip-compressed");
		
		$this->form_validation->set_rules('categoryname', 'Category Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "moduledropdown";
			$data['datatype'] = "md_cat"; 
			$data['HeaderSuffix'] = "Page Category"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/pagecat_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"categoryname" => $this->input->post("categoryname"),
			);
			$this->load->model('CMS_Modules');
			$result = $this->CMS_Modules->createNewPageCategory($arr);
			
			if ($result == "success") {
				redirect ('cms/modules/category');
			}
			else {
				$data['dropdown'] = "moduledropdown";
				$data['datatype'] = "md_cat"; 
				$data['HeaderSuffix'] = "Page Category"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/pagecat_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editpagecatname(){
		$newval = $this->input->post("newval");
		$pagecatid = $this->input->post("id");
		echo $this->db->where("categoryid", $pagecatid)->update("pagecategory", array("categoryname" => $newval));
	}

	public function editPage(){
		if (!in_array($this->session->userdata("tier"), array(999, 2)) ){
			redirect("cms");
		}
		$pageid = $this->input->post("id");

		$data = array(
			"pagetitle" => $this->input->post("pagetitle"),
			"pagecategory" => $this->input->post("pagecategory"),
			"description" => $this->input->post("description"),
			"lastupdated" => date("Y-m-d H:i:s")
		);

		if ($this->input->post("active") == 1){
			$data['obsolete'] = 0;
		} else {
			$data['obsolete'] = 1;
		}

		$this->load->model("CMS_Modules");

		$this->load->library('upload');
		if (!empty($_FILES['userfile']['name']))
		{
			$extent = "jpg";
			$filename = $_FILES['userfile']['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if ($ext == "gif"){
				$extent = "gif";
			}
			$config['upload_path']          = './assets/pages/';
	        $config['allowed_types']        = 'jpg|jpeg|gif|png'; 
	        $config['file_name']        	= $pageid.".".$extent; 
	        $config['mod_mime_fix']        	= false; 
	        $config['overwrite']	        = true; 
	        $this->upload->initialize($config);
	        $this->upload->do_upload('userfile');
	        $data['pagehtmllink'] = $extent;
	        $data['pagename'] = $extent;
	        $data['folderlisting'] = $extent;
		}

		$b = $this->CMS_Modules->updatePage($data, $pageid);

		if (!empty($_FILES['thumbimg']['name']))
		{
			$config['upload_path']          = './assets/thumbs/';
		        $config['allowed_types']        = 'jpg|png|bmp'; 
		        $config['file_name']        	= $pageid.".jpg"; 
		        $config['mod_mime_fix']        	= false; 
		        $config['overwrite'] 		= true;
		        $this->upload->initialize($config);
		        $this->upload->do_upload('thumbimg');
		}

		$this->session->set_flashdata("editref", true);
		if ($b){
			redirect("cms/modules/pages/$pageid/");
		} else {
			redirect("cms/modules/pages/$pageid/Operation Failed. Please try again./error");
		}
	}

	public function editModule(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("id");

		$data = array(
			"moduletitle" => $this->input->post("moduletitle"),
			"modulehouse" => $this->input->post("modulehouse"),
			"moduledesc" => $this->input->post("moduledesc"),
			"periodstart" => $this->input->post("periodstart"),
			"periodend" => $this->input->post("periodend"),
			"generaltype" => $this->input->post("generaltype"),
		);
		$reptypes = $this->input->post("reptype")?$this->input->post("reptype"):array();
		$hospitals = $this->input->post("hospitals")?$this->input->post("hospitals"):array();
		$modcats = $this->input->post("modcats")?$this->input->post("modcats"):array();

		if ($this->input->post("active") == 1){
			$data['obsolete'] = 0;
		} else {
			$data['obsolete'] = 1;
		}

		$this->load->model("CMS_Modules");
		$b = $this->CMS_Modules->updateModule($data, $moduleid, $reptypes, $hospitals, $modcats);

		$this->session->set_flashdata("editref", true);
		if ($b){
			redirect("cms/modules/$moduleid/");
		} else {
			redirect("cms/modules/$moduleid/Operation Failed. Please try again./error");
		}
	}

	public function editCycle(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$cycleid = $this->input->post("id");

		$arr = array(
			"cycletitle" => $this->input->post("cycletitle"),
			"periodstart" => $this->input->post("periodstart"),
			"periodend" => $this->input->post("periodend")
		);

		$cyclevisittype = $this->input->post("cyclevisittype");
		$reptypes = $this->input->post("reptype")?$this->input->post("reptype"):array();
		$accsegments = $this->input->post("accsegments")?$this->input->post("accsegments"):array();
		$hcpsegments = $this->input->post("hcpsegments")?$this->input->post("hcpsegments"):array();
		$specs = $this->input->post("specs")?$this->input->post("specs"):array();
		$rooms = $this->input->post("rooms")?$this->input->post("rooms"):array();

		if ($this->input->post("active") == 1){
			$arr['obsolete'] = 0;
		} else {
			$arr['obsolete'] = 1;
		}

		$this->load->model("CMS_Modules");
		$b = $this->CMS_Modules->editCycle($cycleid, $arr, $cyclevisittype, $reptypes, $accsegments, $hcpsegments, $specs, $rooms);

		if ($b == "success"){
			redirect("cms/modules/cycles/$cycleid/");
		} else {
			redirect("cms/modules/cycles/$cycleid/Operation Failed. Please try again./error");
		}
	}

	public function editModuleGroup(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("id");

		$data = array(
			"groupname" => $this->input->post("groupname"),
			"filterfor" => $this->input->post("filterfor")
		);

		$this->load->model("CMS_Modules");
		$b = $this->CMS_Modules->updateModuleGroup($data, $moduleid);

		if ($b){
			redirect("cms/modules/group/$moduleid/");
		} else {
			redirect("cms/modules/group/$moduleid/Operation Failed. Please try again./error");
		}
	}

	public function updatemodulepages(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("moduleid");
		$pagenumberarr = $this->input->post("pagenumber");
		$pageidarr = $this->input->post("pageid");
		$bookmarkarr = $this->input->post("bookmark");

		$this->load->model("CMS_Modules");
		$b = $this->CMS_Modules->erasePages($moduleid);
		foreach ($pagenumberarr as $key=>$pagenumber){
			$bkmark = 0;
			if (!empty($bookmarkarr[$key])){
				$bkmark = 1;
			}
			$data = array(
				"moduleid" => $moduleid,
				"pageid" => $pageidarr[$key],
				"pagenumber" => ($key+1),
				"bookmark" => $bkmark
			);
			$this->CMS_Modules->addModulePage($data);
		}
		redirect("cms/modules/$moduleid");
	}

	private function rrmdir($dir) { 
	   	if (is_dir($dir)) { 
	     		$objects = scandir($dir); 
	     		foreach ($objects as $object) { 
	       			if ($object != "." && $object != "..") { 
	         			if (is_dir($dir."/".$object))
	           				$this->rrmdir($dir."/".$object);
	         			else
	           				unlink($dir."/".$object); 
	       			} 
	     		}
	     	rmdir($dir); 
	   	} 
	}

	public function preview($id){
		$this->load->model("CMS_Modules");
		$data['pages'] = $this->CMS_Modules->getModulePagesForPreview($id);
		$this->load->view("cms/module_preview", $data);
	}

	public function addGroup(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("mid");
		$groupid = $this->input->post("gid");
		$this->load->model("CMS_Modules");
		echo $this->CMS_Modules->addModuleToGroup($moduleid, $groupid);
	}

	public function deleteGroup(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("mid");
		$groupid = $this->input->post("gid");
		$this->load->model("CMS_Modules");
		echo $this->CMS_Modules->deleteGroupFromModule($moduleid, $groupid);
	}

	public function addModuleToCycle(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("moduleid");
		$cycleid = $this->input->post("cycleid");
		$visitnumber = $this->input->post("visitnumber");
		$this->load->model("CMS_Modules");
		echo $this->CMS_Modules->addModuleToCycle($moduleid, $cycleid, $visitnumber);		
	}

	public function deleteModuleFromCycle(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("moduleid");
		$cycleid = $this->input->post("cycleid");
		$visitnumber = $this->input->post("visitnumber");
		$this->load->model("CMS_Modules");
		echo $this->CMS_Modules->deleteModuleFromCycle($moduleid, $cycleid, $visitnumber);		
	}

	private function listFolderFiles($dir, $folder = NULL){
		global $arrayOfFiles;
		$ffs = scandir($dir);

		unset($ffs[array_search('.', $ffs, true)]);
		unset($ffs[array_search('..', $ffs, true)]);

		if (count($ffs) < 1)
    		return;

		foreach($ffs as $ff){
        		if(is_dir($dir.'/'.$ff)) $this->listFolderFiles($dir.'/'.$ff, "$dir/$ff");
        		else {
        			$filepath = $folder."/".$ff;
        			$filepath = str_replace("//", "/", $filepath);
        			$arrayOfFiles[] = base_url().$filepath;
        		}
    		}
	}

	public function togglepage(){
		if (!in_array($this->session->userdata("tier"), array(999, 2)) ){
			redirect("cms");
		}
		$pageid = $this->input->post("pageid");
		$this->load->model("CMS_Modules");
		echo $this->CMS_Modules->togglePageObsolete($pageid);
	}

	public function togglemodule(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$moduleid = $this->input->post("moduleid");
		$this->load->model("CMS_Modules");
		echo $this->CMS_Modules->toggleModuleObsolete($moduleid);
	}
}
?>