<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quiz extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	}

	public function survey()
	{
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "closingdropdown";
		$data['datatype'] = "cd_survey"; 
		$data['HeaderSuffix'] = "Survey"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Closing");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		
		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['modules'] = $this->CMS_Closing->searchSurveys($srcquery, $page, $limit);
			$data['totalpage'] = ceil(count($data['modules'])/10);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['modules'] = $this->CMS_Closing->getAllSurveysPaged($page, $limit);
			$data['totalpage'] = $this->CMS_Closing->getTotalSurveyPages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['sitelink'] = site_url("cms/quiz/survey");
		$this->load->view('cms/survey_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function createnewsurvey(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('surveytext', 'Survey Question', 'required');
		$this->form_validation->set_rules('correctanswer', 'Correct Answer', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "closingdropdown";
			$data['datatype'] = "cd_survey"; 
			$data['HeaderSuffix'] = "Survey"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model('CMS_Accounts');
			$data['reptypes'] = $this->CMS_Accounts->getRepTypes();
			$this->load->view('cms/survey_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"surveytext" => $this->input->post("surveytext"),
				"availability" => 1,
				"surveytype" => $this->input->post("surveytype"),
				"correctanswer" => $this->input->post('correctanswer')
			);
			$this->load->model('CMS_Closing');
			$reptypes = $this->input->post("reptypes")?$this->input->post("reptypes"):array();
			$answers = $this->input->post("answers")?$this->input->post("answers"):array();
			$anstxt = "";
			$delim = "";
			foreach ($answers as $vals){
				$anstxt .= $delim.$vals;
				$delim = "xvz:zvx";
			}
			$arr['surveychoices'] = $anstxt;
			$result = $this->CMS_Closing->createNewSurvey($arr, $reptypes);
			
			if ($result == "success") {
				redirect ('cms/quiz/survey');
			}
			else {
				$data['dropdown'] = "closingdropdown";
				$data['datatype'] = "cd_survey"; 
				$data['HeaderSuffix'] = "Survey"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->model('CMS_Accounts');
				$data['reptypes'] = $this->CMS_Accounts->getRepTypes();
				$this->load->view('cms/survey_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function surveydetails($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "closingdropdown";
		$data['datatype'] = "cd_survey"; 
		$data['HeaderSuffix'] = "Survey"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model('CMS_Accounts');
		$data['reptypes'] = $this->CMS_Accounts->getRepTypes();
		$this->load->model("CMS_Closing");
		$data['survey'] = $this->CMS_Closing->getSurveyDetails($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/survey_detail', $data);
		$this->load->view('cms/_footer', $data);
	}
	public function editSurvey(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$cycleid = $this->input->post("id");

		$arr = array(
			"surveytext" => $this->input->post("surveytext"),
			"availability" => 1,
			"surveytype" => $this->input->post("surveytype"),
			"correctanswer" => $this->input->post("correctanswer")
		);
		$this->load->model('CMS_Closing');
		$reptypes = $this->input->post("reptypes")?$this->input->post("reptypes"):array();
		$answers = $this->input->post("answers")?$this->input->post("answers"):array();
		$anstxt = "";
		$delim = "";
		foreach ($answers as $vals){
			$anstxt .= $delim.$vals;
			$delim = "xvz:zvx";
		}
		$arr['surveychoices'] = $anstxt;
		$this->load->model("CMS_Closing");
		$result = $this->CMS_Closing->editSurvey($cycleid, $arr, $reptypes);

		if ($result){
			redirect("cms/quiz/survey/$cycleid/");
		} else {
			redirect("cms/quiz/survey/$cycleid/Operation Failed. Please try again./error");
		}
	}


	public function cycles()
	{
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "closingdropdown";
		$data['datatype'] = "cd_cycle"; 
		$data['HeaderSuffix'] = "Survey Cycles"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Closing");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		
		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['cycles'] = $this->CMS_Closing->searchCycles($srcquery, $page, $limit);
			$data['totalpage'] = ceil(count($data['cycles'])/10);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['cycles'] = $this->CMS_Closing->getAllCyclesPaged($page, $limit);
			$data['totalpage'] = $this->CMS_Closing->getTotalCyclePages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['sitelink'] = site_url("cms/quiz/cycles");
		$this->load->view('cms/surveycycle_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function createnewcycle(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$allowedTypes = array("application/zip", "application/x-zip", "application/octet-stream", "application/x-zip-compressed");
		
		$this->form_validation->set_rules('cycletitle', 'Cycle Title', 'required');
		$this->form_validation->set_rules('periodstart', 'Date Start', 'required');
		$this->form_validation->set_rules('periodend', 'Date End', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		// var_dump($this->input->post());
		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "closingdropdown";
			$data['datatype'] = "cd_cycle"; 
			$data['HeaderSuffix'] = "Survey Cycles"; 
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->model('CMS_Modules');
			$data['hcpsegs'] = $this->CMS_Modules->getHCPSegments();
			$data['accsegs'] = $this->CMS_Modules->getACCSegments();
			$data['reptypes'] = $this->CMS_Modules->getRepTypes();
			$data['specialties'] = $this->CMS_Modules->getHCPSpecialties();
			$data['rooms'] = $this->CMS_Modules->getAllRooms();
			$this->load->view('cms/surveycycle_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"cycletitle" => $this->input->post("cycletitle"),
				"periodstart" => $this->input->post("periodstart"),
				"periodend" => $this->input->post("periodend"),
				"obsolete" => 1,
				"createdby" => $this->session->userdata("user_id")
			);

			$reptypes = $this->input->post("reptype")?$this->input->post("reptype"):array();
			$cyclevisittype = $this->input->post("cyclevisittype");
			$accsegments = $this->input->post("accsegments")?$this->input->post("accsegments"):array();
			$hcpsegments = $this->input->post("hcpsegments")?$this->input->post("hcpsegments"):array();
			$specs = $this->input->post("specs")?$this->input->post("specs"):array();
			$rooms = $this->input->post("rooms")?$this->input->post("rooms"):array();

			$this->load->model('CMS_Closing');
			$result = $this->CMS_Closing->createNewCycle($arr, $reptypes, $accsegments, $hcpsegments, $specs, $rooms, $cyclevisittype);
			
			if ($result == "success") {
				redirect ('cms/quiz/cycles/');
			}
			else {
				$data['dropdown'] = "closingdropdown";
				$data['datatype'] = "cd_cycle"; 
				$data['HeaderSuffix'] = "Survey Cycles"; 
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->model("CMS_Modules");
				$data['hcpsegs'] = $this->CMS_Modules->getHCPSegments();
				$data['accsegs'] = $this->CMS_Modules->getACCSegments();
				$data['reptypes'] = $this->CMS_Modules->getRepTypes();
				$data['specialties'] = $this->CMS_Modules->getHCPSpecialties();
				$data['rooms'] = $this->CMS_Modules->getAllRooms();
				$this->load->view('cms/surveycycle_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function cycledetail($id, $msg = "none", $type = "success"){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$data['dropdown'] = "closingdropdown";
		$data['datatype'] = "cd_cycle"; 
		$data['HeaderSuffix'] = "Survey Cycle Detail"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Modules");
		$this->load->model("CMS_Closing");
		$data['cycledata'] = $this->CMS_Closing->getCycleDetail($id);
		$data['reptypes'] = $this->CMS_Modules->getRepTypes();
		$data['questionlist'] = $this->CMS_Closing->getAllSurveysForSearch();
		$data['hcpsegs'] = $this->CMS_Modules->getHCPSegments();
		$data['accsegs'] = $this->CMS_Modules->getACCSegments();
		$data['specialties'] = $this->CMS_Modules->getHCPSpecialties();
		$data['rooms'] = $this->CMS_Modules->getAllRooms();
		
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$this->load->view('cms/surveycycle_detail', $data);
		$this->load->view('cms/_footer', $data);
	}
	public function editCycle(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$cycleid = $this->input->post("id");

		$arr = array(
			"cycletitle" => $this->input->post("cycletitle"),
			"periodstart" => $this->input->post("periodstart"),
			"periodend" => $this->input->post("periodend")
		);

		$reptypes = $this->input->post("reptype")?$this->input->post("reptype"):array();
		$cyclevisittype = $this->input->post("cyclevisittype");
		$accsegments = $this->input->post("accsegments")?$this->input->post("accsegments"):array();
		$hcpsegments = $this->input->post("hcpsegments")?$this->input->post("hcpsegments"):array();
		$specs = $this->input->post("specs")?$this->input->post("specs"):array();
		$rooms = $this->input->post("rooms")?$this->input->post("rooms"):array();

		if ($this->input->post("active") == 1){
			$arr['obsolete'] = 0;
		} else {
			$arr['obsolete'] = 1;
		}

		$this->load->model("CMS_Closing");
		$b = $this->CMS_Closing->editCycle($cycleid, $arr, $reptypes, $cyclevisittype, $accsegments, $hcpsegments, $specs, $rooms);

		if ($b == "success"){
			redirect("cms/quiz/cycles/$cycleid/");
		} else {
			redirect("cms/quiz/cycles/$cycleid/Operation Failed. Please try again./error");
		}
	}
	public function updateclosingquestions(){
		if (!in_array($this->session->userdata("tier"), array(999, 3)) ){
			redirect("cms");
		}
		$cycleid = $this->input->post("cycleid");
		$pagenumberarr = $this->input->post("pagenumber");
		$pageidarr = $this->input->post("pageid");

		$this->load->model("CMS_Closing");
		$b = $this->CMS_Closing->eraseQuestions($cycleid);
		foreach ($pagenumberarr as $key=>$pagenumber){
			$bkmark = 0;
			if (!empty($bookmarkarr[$key])){
				$bkmark = 1;
			}
			$data = array(
				"cycleid" => $cycleid,
				"questionid" => $pageidarr[$key],
				"order" => ($key+1)
			);
			$this->CMS_Closing->addModuleQuestion($data);
		}
		redirect("cms/quiz/cycles/$cycleid");
	}


	public function searchacc(){
		$q = $this->input->get("query");
		$cls = new stdClass();
		$cls->query = $q;
		$this->load->model("CMS_Closing");
		$cls->suggestions = $this->CMS_Closing->searchAcc($q);
		echo json_encode($cls);
	}

	public function searchhcp(){
		$q = $this->input->get("query");
		$acc = $this->input->get("accid");
		$cls = new stdClass();
		$cls->query = $q;
		$this->load->model("CMS_Closing");
		$cls->suggestions = $this->CMS_Closing->searchHcp($q, $acc);
		echo json_encode($cls);
	}
}