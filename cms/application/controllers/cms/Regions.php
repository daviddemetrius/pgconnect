<?php
class Regions extends CI_Controller {

	function __construct()
	{
		parent::__construct();
date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
		if (!in_array($this->session->userdata("tier"), array(999)) ){
			redirect("cms");
		}
	}

	public function territory()
	{
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_terr"; 
		$data['HeaderSuffix'] = "Territory"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Territory";
		$data['listicon'] = "globe";
		$data['tabletitles'] = array("Territory Name", "ID", "Master ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Regions->searchTerritorys($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalSearchTerritorysPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Regions->getAllTerritorysPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalTerritorysPages($limit);
		}
		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/regions/territory");
		$this->load->view('cms/territory_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function newterritory(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('territoryname', 'Territory Name', 'required');
		$this->form_validation->set_rules('masterid', 'Territory Code', 'required|is_unique[territory.masterid]');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$this->load->model('CMS_Regions');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "territorydropdown";
			$data['datatype'] = "td_terr"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "Territory"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$data['district'] = $this->CMS_Regions->getDistrict();
			$this->load->view('cms/territory_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"territoryname" => $this->input->post("territoryname"),
				"createdby" => $this->session->userdata("user_id"),
				"masterid" => $this->input->post("masterid"),
				"districtid" => $this->input->post('districtid')
			);
			$result = $this->CMS_Regions->createNewTerritorys($arr);
			
			if ($result == "success") {
				redirect ('cms/regions/territory');
			}
			else { //if fail
				$data['dropdown'] = "territorydropdown";
				$data['datatype'] = "td_terr"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "Territory"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$data['district'] = $this->CMS_Regions->getDistrict();
				$this->load->view('cms/territory_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function territorydetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_terr"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Territory"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$data['territorydetails'] = $this->CMS_Regions->getTerritoryDetails($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}
		$data['district'] = $this->CMS_Regions->getDistrict();

		$this->load->view('cms/territory_detail', $data);
		$this->load->view('cms/_footer', $data);
	}


	public function editTerritory(){
		$id = $this->input->post("territoryid");
		$name = $this->input->post("territory");

		$arr = array(
			"lastupdated" => date("Y-m-d H:i:s"),
			"districtid" => $this->input->post('districtid')
		);
		if (!empty($this->input->post("masterid"))){
			$arr["masterid"] = $this->input->post("masterid");
		}
		if (!empty($this->input->post("territoryname"))) {
			$arr["territoryname"] = $this->input->post("territoryname");
		}
		$this->load->model("CMS_Regions");
		$result = $this->CMS_Regions->editTerritory($arr, $id);
		if ($result){
			redirect("cms/regions/territory/$id/");
		} else {
			redirect("cms/regions/territory/$id/Edit Failed. Please try again./error");
		}
	}

	public function province()
	{
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_prov"; 
		$data['HeaderSuffix'] = "Region"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Region";
		$data['listicon'] = "map-marker";
		$data['tabletitles'] = array("Region Name", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Regions->searchProvinces($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalSearchProvincePages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Regions->getAllProvincesPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalProvincePages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/regions/province");
		$this->load->view('cms/province_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function provincedetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_prov"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Region"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$data['provincedetails'] = $this->CMS_Regions->getProvinceDetails($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/province_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function newprovince(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('provincename', 'Region Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "territorydropdown";
			$data['datatype'] = "td_prov"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "Region"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/province_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Regions');
			$arr = array(
				"provincename" => $this->input->post("provincename"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Regions->createNewProvinces($arr);
			
			if ($result == "success") {
				redirect ('cms/regions/province');
			}
			else { //if fail
				$data['dropdown'] = "territorydropdown";
				$data['datatype'] = "td_prov"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "Region"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/province_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editProvince(){
		$id = $this->input->post("provinceid");
		$name = $this->input->post("province");

		$arr = array(
			"provincename" => $this->input->post("province"),
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Regions");
		$result = $this->CMS_Regions->editProvince($arr, $id);
		if ($result){
			redirect("cms/regions/province/$id/");
		} else {
			redirect("cms/regions/province/$id/Edit Failed. Please try again./error");
		}
	}

	public function city()
	{
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_city"; 
		$data['HeaderSuffix'] = "Area"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "Area";
		$data['listicon'] = "pushpin";
		$data['tabletitles'] = array("Area Name", "Region", "ID");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Regions->searchCitys($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalSearchCityPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Regions->getAllCitysPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalCityPages($limit);
		}
		

		
		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/regions/city");
		$this->load->view('cms/city_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function citydetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_city"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "Area"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$data['citydetails'] = $this->CMS_Regions->getCityDetails($id);
		// $data['provincedetails'] = $this->CMS_Regions->getProvinceDetails($id);
		$data['provinces'] = $this->CMS_Regions->getProvinces();
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/city_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function newcity(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('cityname', 'Area Name', 'required');
		$this->form_validation->set_rules('cityid', 'Area ID', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		$this->load->model('CMS_Regions');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "territorydropdown";
			$data['datatype'] = "td_city"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "Area"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$data['provinces'] = $this->CMS_Regions->getProvinces();
			$this->load->view('cms/city_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$arr = array(
				"cityname" => $this->input->post("cityname"),
				"createdby" => $this->session->userdata("user_id"),
				"provinceid" => $this->input->post("provinceselect"),
				"cityid" => $this->input->post("cityid")
			);
			$result = $this->CMS_Regions->createNewCitys($arr);
			
			if ($result == "success") {
				redirect ('cms/regions/city');
			}
			else { //if fail
				$data['dropdown'] = "territorydropdown";
				$data['datatype'] = "td_city"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "Area"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during City Data creation. Please try again with a different ID or contact an administrator!";
				$data['SwalType'] = "error";
				$data['provinces'] = $this->CMS_Regions->getProvinces();
				$this->load->view('cms/city_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function editCity(){
		$id = $this->input->post("cityid");
		$name = $this->input->post("city");

		$arr = array(
			"cityname" => $this->input->post("city"),
			"lastupdated" => date("Y-m-d H:i:s"),
			"provinceid" => $this->input->post("provinceselect"),
			"cityid" => $this->input->post("cityid")
		);
		$this->load->model("CMS_Regions");
		$result = $this->CMS_Regions->editCity($arr, $id);
		if ($result){
			redirect("cms/regions/city/$id/");
		} else {
			redirect("cms/regions/city/$id/Edit Failed. Please try again./error");
		}
	}

	public function district(){

		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_dist"; 
		$data['HeaderSuffix'] = "District"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$page = $this->input->get("p");
		$limit = $this->input->get("l");
		$srcquery = $this->input->get("q");
		$sorting = $this->input->get("s");
		$data['addllink'] = "";
		if (!$page) {
			$page = 1;
		}
		if (!$limit) {
			$limit = 10;
		} else {
			$data['addllink'] .= "&l=$limit";
		}
		$data['listtitle'] = "District";
		$data['listicon'] = "road";
		$data['tabletitles'] = array("District Name", "ID", "Area Name", "Region Name");

		if ($srcquery){
			$data['addllink'] .= "&q=$srcquery";
			$data['items'] = $this->CMS_Regions->searchDistricts($srcquery, $page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalSearchDistrictPages($limit, $srcquery);
			if ($data['totalpage'] == 0){
				$data['totalpage'] = 1;
			}
		} else {
			$data['items'] = $this->CMS_Regions->getAllDistrictsPaged($page, $limit, $sorting);
			$data['totalpage'] = $this->CMS_Regions->getTotalDistrictPages($limit);
		}
		

		$data['page'] = $page;
		$data['limit'] = $limit;
		$data['q'] = $srcquery;
		$data['s'] = $sorting;
		$data['sitelink'] = site_url("cms/regions/district");
		$this->load->view('cms/district_list', $data);
		$this->load->view('cms/_footer', $data);
		// echo "Hi";
	}

	public function newdistrict(){
		if ($this->session->userdata("tier") <= 2){
			redirect("cms");
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('districtname', 'District Name', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');

		if ($this->form_validation->run() == FALSE) {
			$data['dropdown'] = "territorydropdown";
			$data['datatype'] = "td_dist"; //for sidebar link class:active addition
			$data['HeaderSuffix'] = "District"; //for sidebar link class:active addition
			$this->load->view('cms/_header', $data);
			$this->load->view('cms/_sidebar', $data);
			$this->load->view('cms/district_new', $data);
			$this->load->view('cms/_footer', $data);
		}
		else {
			$this->load->model('CMS_Regions');
			$arr = array(
				"districtname" => $this->input->post("districtname"),
				"createdby" => $this->session->userdata("user_id")
			);
			$result = $this->CMS_Regions->createNewDistrict($arr);
			
			if ($result == "success") {
				redirect ('cms/regions/district');
			}
			else { //if fail
				$data['dropdown'] = "territorydropdown";
				$data['datatype'] = "td_dist"; //for sidebar link class:active addition
				$data['HeaderSuffix'] = "District"; //for sidebar link class:active addition
				$this->load->view('cms/_header', $data);
				$this->load->view('cms/_sidebar', $data);
				$data['SwalMsg'] = "An error has occured during House Data creation. Please try again or contact an administrator!";
				$data['SwalType'] = "error";
				$this->load->view('cms/district_new', $data);
				$this->load->view('cms/_footer', $data);
			}
		}
	}

	public function districtdetail($id, $msg = "none", $type = "success") {
		$data['dropdown'] = "territorydropdown";
		$data['datatype'] = "td_dist"; //for sidebar link class:active addition
		$data['HeaderSuffix'] = "District"; //for sidebar link class:active addition
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Regions");
		$data['districtdetails'] = $this->CMS_Regions->getDistrictDetails($id);
		if ($msg != "none"){
			$data['SwalMsg'] = urldecode($msg);
			$data['SwalType'] = $type;
		}

		$this->load->view('cms/district_detail', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function editDistrict(){
		$id = $this->input->post("districtid");
		$name = $this->input->post("district");

		$arr = array(
			"districtname" => $this->input->post("district"),
			"lastupdated" => date("Y-m-d H:i:s")
		);
		$this->load->model("CMS_Regions");
		$result = $this->CMS_Regions->editDistrict($arr, $id);
		if ($result){
			redirect("cms/regions/district/$id/");
		} else {
			redirect("cms/regions/district/$id/Edit Failed. Please try again./error");
		}
	}

	public function deleteTerritoryAssignment(){
		$territoryid = $this->input->post("territoryid");
		$specid = $this->input->post("specid");
		$roomid = $this->input->post("roomid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Regions");
		echo $this->CMS_Regions->deleteTerritoryAssignment($territoryid, $specid, $roomid, $hospitalid);
	}

	public function deleteHospitalTerritoryAssignment(){
		$territoryid = $this->input->post("territoryid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Masterdata");
		echo $this->CMS_Masterdata->deleteHospitalTerritory($territoryid, $hospitalid);
	}

	public function refreshTerritoryAssignment(){
		$hospitalid = $this->input->post('hospitalid');
		$territoryid = $this->input->post("territoryid");
		$this->load->model("CMS_Regions");
		echo $this->CMS_Regions->refreshTerritoryAssignment($territoryid, $hospitalid);	
	}

	public function addTerritoryAssignment(){
		$territoryid = $this->input->post("territoryid");
		$specid = $this->input->post("hcpid");
		$roomid = $this->input->post("roomid");
		$hospitalid = $this->input->post("hospitalid");
		$this->load->model("CMS_Regions");
		echo $this->CMS_Regions->addTerritoryAssignment($territoryid, $specid, $roomid, $hospitalid);		
	}

	public function searchacc(){
		$q = $this->input->get("query");
		$tid = $this->input->get("territoryid");
		$cls = new stdClass();
		$cls->query = $q;
		$this->load->model("CMS_Regions");
		$cls->suggestions = $this->CMS_Regions->searchAcc($q, $tid);
		echo json_encode($cls);
	}

	public function searchhcp(){
		$q = $this->input->get("query");
		$acc = $this->input->get("accid");
		$cls = new stdClass();
		$cls->query = $q;
		$this->load->model("CMS_Regions");
		$cls->suggestions = $this->CMS_Regions->searchHcp($q, $acc);
		echo json_encode($cls);
	}

	public function downloadCoverage($territory){
		ini_set('memory_limit', '-1');
		$limit = 1000;
		$current = 1;
		$b = true;
		$productdtl = $this->db->where("r.territoryid", $territory)->join("representatives rp", "rp.territory = r.territoryid")->get("territory r");
		if ($productdtl->num_rows() <= 0){
			redirect("cms/regions/territory/$territory");
		} else {
			$productdtl = $productdtl->row();
		}
		$filename = str_replace(array("/", "\\"), "", $productdtl->masterid).'TerritoryDetails';

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		fputcsv($output, array("Territory ID", "Territory Name", "Assigned To", "Hospital ID", "ACC Name", "HCP ID", "HCP Name", "Room Name", "ACC Segment", "HCP Specialty"));
		$schedules = $this->db->select("xmlaccid, hospitalname, xmlhcpid, specialistname, sp.specid, r.hospitalid, hs.segmentation, specialtyname, ro.roomname")->where("r.territoryid", $territory)->join("hospital bi", "bi.hospitalid = r.hospitalid")->join("specialist sp", "sp.specid = r.specid")->join("room ro", "ro.roomid = r.roomid")->join("specialty sy", "sy.specialtyid = sp.specialty")->join("hospitalsegment hs", "bi.segmentid = hs.segmentid")->get("territoryassignment r")->result();
		// echo json_encode($schedules);
		$transidarr = array();
		foreach ($schedules as $sch){
			$roomname = $sch->roomname;
			fputcsv($output, array($productdtl->masterid, $productdtl->territoryname, $productdtl->repname." ".$productdtl->xmlnacd, $sch->hospitalid, $sch->hospitalname, $sch->specid, $sch->specialistname, $roomname, $sch->segmentation, $sch->specialtyname));
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}

	public function downloadCoverageOld($territory){
		
	}
}
?>