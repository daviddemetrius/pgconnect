<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Statistics extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	}

	public function productitr(){
		$data['dropdown'] = "statdropdown";
		$data['datatype'] = "sd_pitr"; 
		$data['HeaderSuffix'] = "Product ITR"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Statistics");
		if ($this->input->get("month")){
			$month = $this->input->get("month");
			if ($month > 12 || $month < 1){
				$month = date("n");
			}
		} else {
			$month = date("n");
		}

		if ($this->input->get("year")){
			$year = $this->input->get("year");
			if ($year > 12 || $year < 1){
				$year = date("Y");
			}
		} else {
			$year = date("Y");
		}

		$data['products'] = $this->CMS_Statistics->getAllDanoneProductITR($month, $year);
		// var_dump($data['products']);
		$data['y'] = $year;
		$data['m'] = $month;
		$this->load->view('cms/stat_proditr', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function productshare(){
		$data['dropdown'] = "statdropdown";
		$data['datatype'] = "sd_pshare"; 
		$data['HeaderSuffix'] = "Product Share"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Statistics");
		if ($this->input->get("month")){
			$month = $this->input->get("month");
			if ($month > 12 || $month < 1){
				$month = date("n");
			}
		} else {
			$month = date("n");
		}

		if ($this->input->get("year")){
			$year = $this->input->get("year");
			if ($year < 1){
				$year = date("Y");
			}
		} else {
			$year = date("Y");
		}

		$data['products'] = $this->CMS_Statistics->getAllProductsShare($month, $year);
		// var_dump($data['products']);
		$data['y'] = $year;
		$data['m'] = $month;
		$this->load->view('cms/stat_prodshare', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function getProductITRACC(){
		$foryear = $this->input->get("year");
		$formonth = $this->input->get("month");
		$productid = $this->input->get("productid");
		$this->load->model("CMS_Statistics");
		$itr = $this->CMS_Statistics->getProductITRACC($foryear, $formonth, $productid);
		echo "<div class='row alternating'>
			<div class='col-xs-1'>ID</div>
			<div class='col-xs-4'>Name</div>
			<div class='col-xs-2'>Territory</div>
			<div class='col-xs-2'>Segment</div>
			<div class='col-xs-3'>Uploader</div>
		</div>";
		foreach($itr as $product){
			$uploader = "Admin";
			if ($product->submittedby != NULL){
				$uploader = $this->CMS_Statistics->getUploaderInfo($product->submittedby);
			}
			echo "
			<div class='row alternating'>
				<div class='col-xs-1'>$product->xmlaccid</div>
				<div class='col-xs-4'>$product->hospitalname</div>
				<div class='col-xs-2'>$product->territoryname</div>
				<div class='col-xs-2'>$product->segmentation</div>
				<div class='col-xs-3'>$uploader</div>
			</div>
			";
		}
	}

	public function getProductShareACC(){
		$foryear = $this->input->get("year");
		$formonth = $this->input->get("month");
		$productid = $this->input->get("productid");
		$this->load->model("CMS_Statistics");
		$itr = $this->CMS_Statistics->getProductShareACC($foryear, $formonth, $productid);
		echo "<div class='row alternating'>
			<div class='col-xs-1'>ID</div>
			<div class='col-xs-3'>Name</div>
			<div class='col-xs-2'>Territory</div>
			<div class='col-xs-2'>Segment</div>
			<div class='col-xs-2'>Uploader</div>
			<div class='col-xs-1'>Birth</div>
			<div class='col-xs-1'>Peds</div>
		</div>";
		foreach($itr as $product){
			$uploader = "Admin";
			if ($product->submittedby != NULL){
				$uploader = $this->CMS_Statistics->getUploaderInfo($product->submittedby);
			}
			echo "
			<div class='row alternating'>
				<div class='col-xs-1'>$product->xmlaccid</div>
				<div class='col-xs-3'>$product->hospitalname</div>
				<div class='col-xs-2'>$product->territoryname</div>
				<div class='col-xs-2'>$product->segmentation</div>
				<div class='col-xs-2'>$uploader</div>
				<div class='col-xs-1'>$product->birth</div>
				<div class='col-xs-1'>$product->peds</div>
			</div>
			";
		}
	}

	public function moduledata(){
		$data['dropdown'] = "statdropdown";
		$data['datatype'] = "sd_mod"; 
		$data['HeaderSuffix'] = "Monthly Module Data"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Statistics");
		$data['reps'] = $this->CMS_Statistics->getAllRepsUnder();
		$repidtxt = "(";
		$territorytxt = "(";
		$delim = "";
		foreach ($data['reps'] as $rep){
			$repidtxt .= $delim.$rep->repid;
			$territorytxt .= $delim.$rep->territoryid;
			$delim = ",";
		}
		$repidtxt .= ")";
		$territorytxt .= ")";
		$month = date("m");
		if ($this->input->get("month")){
			$month = $this->input->get("month");
		}
		$year = date("Y");
		if ($this->input->get("year")){
			$year = $this->input->get("year");
		}
		if ($this->input->get("all")){
			$month = null;
			$year = null;
		}
		$data['y'] = $year;
		$data['m'] = $month;
		$data['ModuleData'] = $this->CMS_Statistics->dmGetModuleStatistics($data['reps'], $month, $year);
		
		$this->load->view('cms/stat_moduledata', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function coverage(){
		$data['dropdown'] = "statdropdown";
		$data['datatype'] = "sd_cov"; 
		$data['HeaderSuffix'] = "Monthly Coverage Data"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Statistics");
		$data['reps'] = $this->CMS_Statistics->getAllRepsUnder();
		$repidtxt = "(";
		$territorytxt = "(";
		$delim = "";
		foreach ($data['reps'] as $rep){
			$repidtxt .= $delim.$rep->repid;
			$territorytxt .= $delim.$rep->territoryid;
			$delim = ",";
		}
		$repidtxt .= ")";
		$territorytxt .= ")";
		$month = date("m");
		if ($this->input->get("month")){
			$month = $this->input->get("month");
		}
		$year = date("Y");
		if ($this->input->get("year")){
			$year = $this->input->get("year");
		}
		if ($this->input->get("all")){
			$month = null;
			$year = null;
		}
		$data['y'] = $year;
		$data['m'] = $month;
		// $data['ModuleData'] = $this->CMS_Statistics->dmGetCoverageData($data['reps'], $month, $year);
		// $this->load->view('cms/stat_moduledata', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function visit(){
		$data['dropdown'] = "statdropdown";
		$data['datatype'] = "sd_visit"; 
		$data['HeaderSuffix'] = "Monthly Visit Data"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->model("CMS_Statistics");
		$data['reps'] = $this->CMS_Statistics->getAllRepsUnder();
		$repidtxt = "(";
		$territorytxt = "(";
		$delim = "";
		foreach ($data['reps'] as $rep){
			$repidtxt .= $delim.$rep->repid;
			$territorytxt .= $delim.$rep->territoryid;
			$delim = ",";
		}
		$repidtxt .= ")";
		$territorytxt .= ")";
		$month = date("m");
		if ($this->input->get("month")){
			$month = $this->input->get("month");
		}
		$year = date("Y");
		if ($this->input->get("year")){
			$year = $this->input->get("year");
		}
		if ($this->input->get("all")){
			$month = null;
			$year = null;
		}
		$data['y'] = $year;
		$data['m'] = $month;
		// $data['ModuleData'] = $this->CMS_Statistics->dmGetCoverageData($data['reps'], $month, $year);
		// $this->load->view('cms/stat_moduledata', $data);
		$this->load->view('cms/_footer', $data);
	}

}