<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilities extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		// if ($this->session->userdata('syncp_logged_in') == TRUE)
	 //    	{
	 //        	redirect("cms");
	 //    	}
	}
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('pwd', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span><br/>');
		$this->form_validation->set_message('required', '* %s required');
		
		if ($this->session->userdata('fflcp_logged_in') == TRUE)
		{
			redirect("cms");
		}
		else {
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('cms/panel_login');
			}
			else
			{
				$this->load->model('CMS_Accounts');
				
				$result = $this->CMS_Accounts->loginUser();
				
				if ($result == "success") {
					redirect ('cms');
				}
				else { //if fail
					$data['login'] = "fail";
					$this->load->view('cms/panel_login', $data);
				}
			}
		}
	}
	public function generatePageStatistics(){
		ini_set('memory_limit', '-1');
		// echo "'schedid','pageid','moduleid','accesstime','accessdate','feedback'<br/>";
		$limit = 1000;
		$current = 1;
		$b = true;

		$filename = 'PageStatisticsWModule';

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		fputcsv($output, array("schedid", "pageid", "moduleid", "accesstime", "accessdate", "feedback"));
		$current = $this->input->get("from");
		$limit = $this->input->get("limit");
		$max = $this->input->get("max");
		if (!$limit){
			$limit = 1000;
		}
		if (!$current){
			$current = 1;
		}
		if (!$max){
			$max = 10000;
		}
		
		while ($b){
			// $pgstat = $this->db->select("pageid, schedid, accesstime as seconds, accessdate, feedback", false)->order_by("accessdate", "asc")->where("schedid >", $limit*($current-1))->where("schedid <=", $limit*$current)->get("pagestatistics")->result();
			$mdstat = $this->db->select("moduleid, schedid, accesstime as seconds, accessdate", false)->order_by("accessdate", "asc")->where("schedid >", $limit*($current-1))->where("schedid <=", $limit*$current)->get("modulestatistics")->result();
			if ($limit*$current >= $max){
				// echo $this->db->last_query();
				// echo "broke at ".$current;
				break;
			}
			$current++;
			$i = 0;
			foreach ($mdstat as $mds){
				$secs = $mds->seconds;
				$pgs = $this->db->select("pageid, schedid, accesstime as seconds, accessdate, feedback, $mds->moduleid as moduleid")->order_by("accessdate", "asc")->where("schedid", $mds->schedid)->where("pageid in", "(SELECT pageid from modulepages where moduleid = $mds->moduleid)", false)->get("pagestatistics")->result();
				foreach ($pgs as $pgstat){
					fputcsv($output, array($pgstat->schedid, $pgstat->pageid, $pgstat->moduleid, $pgstat->seconds, $pgstat->accessdate, $pgstat->feedback));
				}
				// for (; $i < count($pgstat); $i++){
				// 	$dur = $pgstat[$i]->seconds;
				// 	$secs -= $dur;
				// 	$pgstat[$i]->moduleid = $mds->moduleid;
				// 	if ($secs <= 0){
				// 		break;
				// 	}
				// 	fputcsv($output, array($pgstat[$i]->schedid, $pgstat[$i]->pageid, $pgstat[$i]->moduleid, $pgstat[$i]->seconds, $pgstat[$i]->accessdate, $pgstat[$i]->feedback));
				// 	// echo "'".$pgstat[$i]->schedid."','".$pgstat[$i]->pageid."','".$pgstat[$i]->moduleid."','".$pgstat[$i]->seconds."','".$pgstat[$i]->accessdate."','".$pgstat[$i]->feedback."'<br/>";
				// 	// $pagestats[] = $pgstat[$i];

				// }
			}
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}
	public function generateClosingDataShare(){
		ini_set('memory_limit', '-1');
		// echo "'schedid','pageid','moduleid','accesstime','accessdate','feedback'<br/>";
		$limit = 1000;
		$current = 1;
		$b = true;

		$filename = 'ClosingDaraShareProducts';

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		fputcsv($output, array("shareid", "productid", "birth", "peds"));
		$current = $this->input->get("from");
		$limit = $this->input->get("limit");
		$max = $this->input->get("max");
		if (!$limit){
			$limit = 1000;
		}
		if (!$current){
			$current = 1;
		}
		if (!$max){
			$max = 10000;
		}
		
		while ($b){
			// $pgstat = $this->db->select("pageid, schedid, accesstime as seconds, accessdate, feedback", false)->order_by("accessdate", "asc")->where("schedid >", $limit*($current-1))->where("schedid <=", $limit*$current)->get("pagestatistics")->result();
			$mdstat = $this->db->where("shareid >", $limit*($current-1))->where("shareid <=", $limit*$current)->get("closingdatashareproducts")->result();
			if ($limit*$current >= $max){
				// echo $this->db->last_query();
				// echo "broke at ".$current;
				break;
			}
			$current++;
			$i = 0;
			foreach ($mdstat as $mds){
				fputcsv($output, array($mds->shareid, $mds->productid, $mds->birth, $mds->peds));
				// for (; $i < count($pgstat); $i++){
				// 	$dur = $pgstat[$i]->seconds;
				// 	$secs -= $dur;
				// 	$pgstat[$i]->moduleid = $mds->moduleid;
				// 	if ($secs <= 0){
				// 		break;
				// 	}
				// 	fputcsv($output, array($pgstat[$i]->schedid, $pgstat[$i]->pageid, $pgstat[$i]->moduleid, $pgstat[$i]->seconds, $pgstat[$i]->accessdate, $pgstat[$i]->feedback));
				// 	// echo "'".$pgstat[$i]->schedid."','".$pgstat[$i]->pageid."','".$pgstat[$i]->moduleid."','".$pgstat[$i]->seconds."','".$pgstat[$i]->accessdate."','".$pgstat[$i]->feedback."'<br/>";
				// 	// $pagestats[] = $pgstat[$i];

				// }
			}
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}
	public function generateClosingDataITR(){
		ini_set('memory_limit', '-1');
		// echo "'schedid','pageid','moduleid','accesstime','accessdate','feedback'<br/>";
		$limit = 1000;
		$current = 1;
		$b = true;

		$filename = 'ClosingDaraITRProducts';

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		fputcsv($output, array("itrid", "productid", "intention"));
		$current = $this->input->get("from");
		$limit = $this->input->get("limit");
		$max = $this->input->get("max");
		if (!$limit){
			$limit = 1000;
		}
		if (!$current){
			$current = 1;
		}
		if (!$max){
			$max = 10000;
		}
		$showOnlyOne = false;
		if ($this->input->get("one") == 1){
			$showOnlyOne = true;
		}
		
		while ($b){
			// $pgstat = $this->db->select("pageid, schedid, accesstime as seconds, accessdate, feedback", false)->order_by("accessdate", "asc")->where("schedid >", $limit*($current-1))->where("schedid <=", $limit*$current)->get("pagestatistics")->result();
			$this->db->where("intention", 1);
			$mdstat = $this->db->where("itrid >", $limit*($current-1))->where("itrid <=", $limit*$current)->get("closingdataitrproducts")->result();
			if ($limit*$current >= $max){
				// echo $this->db->last_query();
				// echo "broke at ".$current;
				break;
			}
			$current++;
			$i = 0;
			foreach ($mdstat as $mds){
				fputcsv($output, array($mds->itrid, $mds->productid, $mds->intention));
				// for (; $i < count($pgstat); $i++){
				// 	$dur = $pgstat[$i]->seconds;
				// 	$secs -= $dur;
				// 	$pgstat[$i]->moduleid = $mds->moduleid;
				// 	if ($secs <= 0){
				// 		break;
				// 	}
				// 	fputcsv($output, array($pgstat[$i]->schedid, $pgstat[$i]->pageid, $pgstat[$i]->moduleid, $pgstat[$i]->seconds, $pgstat[$i]->accessdate, $pgstat[$i]->feedback));
				// 	// echo "'".$pgstat[$i]->schedid."','".$pgstat[$i]->pageid."','".$pgstat[$i]->moduleid."','".$pgstat[$i]->seconds."','".$pgstat[$i]->accessdate."','".$pgstat[$i]->feedback."'<br/>";
				// 	// $pagestats[] = $pgstat[$i];

				// }
			}
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
		
		ini_set('memory_limit', '128M');
	}
	public function hospitalterritoryassigner(){
		ini_set('memory_limit', '-1');
		$this->load->model("CMS_Masterdata");
		$this->CMS_Masterdata->HCgetAssignmentData();
		
	}

	public function hospitalconsolidator(){
		ini_set('memory_limit', '-1');
		$this->load->model("CMS_Masterdata");
		$hospitals = $this->CMS_Masterdata->HCgetRedundantACCEntries();
		$consolidatedacc = array();
		foreach ($hospitals as $key=>$hospital){
			if (!array_key_exists($hospital->xmlaccid, $consolidatedacc)){
				$accobj = new stdClass();
				$accobj->xmlaccid = $hospital->xmlaccid;
				$accobj->firstaccid = $hospital->hospitalid;
				$accobj->arrayofterritories = array();
				$accobj->arrayofterritories[] = $hospital->territory;
				$accobj->arrayofaccids = array();
				$consolidatedacc[$hospital->xmlaccid] = $accobj;
			} else {
				$consolidatedacc[$hospital->xmlaccid]->arrayofterritories[] = $hospital->territory;
				$consolidatedacc[$hospital->xmlaccid]->arrayofaccids[] = $hospital->hospitalid;
			}
		}
		$this->db->trans_begin();

		foreach ($consolidatedacc as $xmlacc=>$accdata){
			$tmlist = array();
			$tmvalues = "";
			$delim = "";
			foreach ($accdata->arrayofterritories as $territory){
				if (in_array($territory, $tmlist)){
					continue;
				} 
				$tmlist[] = $territory;
				$tmvalues .= $delim . "($accdata->firstaccid, $territory)";
				$delim = ",";
			}
			$b = $this->CMS_Masterdata->HCinsertIntoHospitalTerritory($tmvalues);

			$hospitalvalues = "(";
			$delim = "";
			foreach ($accdata->arrayofaccids as $redundantaccid){
				$hospitalvalues .= $delim . "'".$redundantaccid."'";
				$delim = ",";
			}
			$hospitalvalues .= ")";
			$b = $this->CMS_Masterdata->HCupdateClosingBirth($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateClosingDelivery($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateClosingITR($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateClosingPotency($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateClosingShare($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateClosingSurvey($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateHospitalRooms($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateRepDesignation($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCupdateScheduleVisit($hospitalvalues, $accdata->firstaccid);
			$b = $this->CMS_Masterdata->HCdeleteHospitalEntries($hospitalvalues);
		}

		if ($this->db->trans_status() === FALSE)
		{
			echo "<br/><br/>Rolling Back....";
		        $this->db->trans_rollback();
		}
		else
		{
			echo "<br/><br/>Committing...";
		        $this->db->trans_rollback();
		}
	}

	public function uploadSQLite(){
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	    	$data['dropdown'] = "utildropdown"; 
	    	$data['datatype'] = "u_sqlite"; 
		$data['HeaderSuffix'] = "Upload SQLite"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->view('cms/util_sqlite', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function uploadShare(){
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	    	$data['dropdown'] = "utildropdown"; 
	    	$data['datatype'] = "u_ushr"; 
		$data['HeaderSuffix'] = "Bulk Upload Share"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->view('cms/util_share', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function uploadITR(){
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	    	$data['dropdown'] = "utildropdown"; 
	    	$data['datatype'] = "u_uitr"; 
		$data['HeaderSuffix'] = "Bulk Upload ITR"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$this->load->view('cms/util_itr', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function downloadITRForm(){
		ini_set('memory_limit', '-1');
		$filename = str_replace(array("/", "\\"), "", 'ITRForm');

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		$this->db->from("product ei");
		$this->db->order_by("productname", "asc");
		$this->db->where("danoneflag", 1);
		$this->db->where("obsolete", 0);
		$prods = $this->db->get()->result();
		$this->db->from("hospital ei");
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		$this->db->join("city c", "c.cityid = city", "left");
		if ($this->session->userdata("tier") <= 2.5){
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
		}
		$schedules = $this->db->get()->result();
		
		fputcsv($output, array("PRIME ACC ID", "ACC Name", "Month Number", "Year Number", "PRIME Product ID", "Product Name", "ITR (1 or 0)"));
		$mo = date("m");
		$y = date("Y");
		$this->load->model("CMS_Closing");
		foreach ($schedules as $sch){
			$itr = $this->CMS_Closing->getITRDataACCOnly($y, $mo, $sch->hospitalid);
			foreach ($itr as $prd){
				fputcsv($output, array($sch->hospitalid, $sch->hospitalname, $mo, $y, $prd->productid, $prd->productname, $prd->intention));
			}			
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);
	}

	public function downloadShareform(){
		ini_set('memory_limit', '-1');
		$filename = str_replace(array("/", "\\"), "", 'ShareForm');

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		$this->db->from("product ei");
		$this->db->order_by("danoneflag desc", "productname asc");
		$this->db->where("obsolete", 0);
		$prods = $this->db->get()->result();
		$this->db->from("hospital ei");
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		$this->db->join("city c", "c.cityid = city", "left");
		if ($this->session->userdata("tier") <= 2.5){
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
		}
		$schedules = $this->db->get()->result();
		
		fputcsv($output, array("PRIME ACC ID", "ACC Name", "Month Number", "Year Number", "PRIME Product ID", "Product Name", "Birth Count", "Peds Count"));
		$this->load->model("CMS_Closing");
		$mo = date("m");
		$y = date("Y");
		foreach ($schedules as $sch){
			$share = $this->CMS_Closing->getShareDataACCOnly($y, $mo, $sch->hospitalid);
			foreach ($share as $prd){
				fputcsv($output, array($sch->hospitalid, $sch->hospitalname, $mo, $y, $prd->productid, $prd->productname, $prd->share, $prd->category));
			}			
		}
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);		
	}

	public function downloadProducts(){
		$filename = str_replace(array("/", "\\"), "", 'ProductList');

		$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
		$output = fopen($filepath, 'w+');

		$this->db->from("product ei");
		$this->db->order_by("danoneflag desc", "productname asc");
		
		fputcsv($output, array("PRIME Product ID", "Product Name", "Danone Product"));
		$schedules = $this->db->get()->result();
		foreach ($schedules as $sch){
			
			fputcsv($output, array($sch->productid, $sch->productname, ($sch->danoneflag==1?"Yes":"")));
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		header('Content-Length: ' . filesize($filepath)); 
		echo readfile($filepath);		
	}

	public function commitUploadSQLite(){
		if ($this->session->userdata('syncp_logged_in') != TRUE)
	    	{
	        	redirect("cms/login");
	    	}
	    	$config['upload_path']          = './assets/';
	        $config['allowed_types']        = 'sql|sqlite|csv|db'; 
	        $config['overwrite']        	= true; 
		$this->load->library('upload', $config);
	        $this->upload->do_upload('userfile');
	        $filename = $this->upload->data('file_name');
	        $pagename = $this->upload->data("raw_name");

	        $db = new PDO("sqlite:./assets/".$filename);
		$query = $db->query("select * from SCHEDULE_ENTITY");
		$scheddata = array();
		$visitdata = array();
		$userid = 0;
		while ($row = $query->fetch(PDO::FETCH_ASSOC))
		{
		  	$schedentry = new stdClass();
		  	$visitentry = new stdClass();
		  	$repschedid = $row["REPSCHEDID"];
		  	$scheddate = $row["SCHEDDATE"];
		  	$specids = $row['SPECIALISTID'];
		  	$hospitalids = $row['HOSPITALID'];
		  	$moveddate = $row['MOVED_DATE'];
		  	$duration = $row['DURATION'];
		  	$visitnumber = $row['COUNT_VISITED'];
		  	$visited = $row['VISITED'];
		  	if ($visited == 1){
		    		$modentry = array();
		    		$pgentry = array();
		    		$modstats = $db->query("select MODULEID, MIN(ACCESSDATE) as ACCESSDATE, SUM(SECONDS) as SECONDS, 0 as ISADDED from TIMER_ENTITY where REPSCHEDID = '$repschedid' group by MODULEID");
		    		while ($modrow = $modstats->fetch(PDO::FETCH_ASSOC)){
		      			$modcls = new stdClass();
		      			$moduleid = $modrow["MODULEID"];
		      			$accessdate = $modrow["ACCESSDATE"];
		      			$accessduration = $modrow["SECONDS"];
		      			$modcls->moduleid = $moduleid;
		      			$modcls->accessdate = $accessdate;
		      			$modcls->accessduration = $accessduration;
		      			$modcls->isadded = 0;
		      			$modentry[] = $modcls;
		    		}
		    		$pgstats = $db->query("select PAGEID, MODULEID, ACCESSDATE, SECONDS, 0 as ISADDED from TIMER_ENTITY where REPSCHEDID = '$repschedid' ");
		    		while ($pgrow = $pgstats->fetch(PDO::FETCH_ASSOC)){
		      			$pgcls = new stdClass();
		      			$pgcls->pageid = $pgrow["PAGEID"];
		      			$pgcls->accessdate = $pgrow["ACCESSDATE"];
		      			$pgcls->accessduration  = $pgrow['SECONDS'];
		      			$pgcls->feedback = "";
		      			$pgcls->moduleid = $pgrow['MODULEID'];
		      			$pgentry[] = $pgcls;
		    		}
		    		$visitentry->pagestatistic = $pgentry;
		    		$visitentry->modulestatistic = $modentry;
		    		$visitentry->internalscheduleid = $repschedid;
		    		$visittime = $scheddate;
		    		if (trim($moveddate) != ""){
		      			$visittime = $moveddate;
		    		}
		    		$visitentry->visittime = $visittime;
		    		$visitentry->lat = 0;
		    		$visitentry->lng = 0;
		    		$visitentry->duration = $duration;
		    		$visitentry->visitrating = 0;
		    		$visitentry->visitnumber = $visitnumber;
		    		$visitdata[] = $visitentry;
		  	}
		  	$hcps = explode("x;x", $specids);
		  	// echo $specids."<br/>";
		  	$accids = explode("x;x", $hospitalids);
		  	$hcplist = array();
		  	foreach ($hcps as $key=>$hcp){
			    	$hcpentry = new stdClass();
			    	$hcpentry->specialistid = $hcp;
			    	$hcpentry->hospitalid = $accids[$key];
			    	$hcplist[] = $hcpentry;
		  	}
		  	$schedentry->internalscheduleid = $repschedid;
		  	$schedentry->dateofvisit = $scheddate;
		  	$schedentry->hcplist = $hcplist;
		  	$scheddata[] = $schedentry;
		  	$userid = explode("-", $repschedid)[0];
		}
		$this->processScheduleData($visitdata, $scheddata, $userid);
	    	$data['dropdown'] = "utildropdown"; 
	    	$data['datatype'] = "u_sqlite"; 
		$data['HeaderSuffix'] = "Upload SQLite"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$data['msg'] = count($visitdata)." Visits Processed.<br/>".count($scheddata)." Schedules Processed.<br/>These data has been processed for user <a href='".site_url("cms/accounts/reps/".$userid)."' target='blank'>#$userid</a>";
		$this->load->view('cms/util_sqlite', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function commitUploadShare(){
		if ($this->session->userdata('syncp_logged_in') != TRUE)
    	{
        	redirect("cms/login");
    	}
    	$config['upload_path']          = './assets/';
        $config['allowed_types']        = 'csv'; 
        $config['overwrite']        	= true; 
		$this->load->library('upload', $config);
        $this->upload->do_upload('userfile');
        $filename = $this->upload->data('file_name');
        $pagename = $this->upload->data("raw_name");
        $row = 0;
	    $processed = 0;
	    $processdata = array();
	    if (($handle = fopen("assets/$pagename.csv", "r")) !== FALSE) {
		   	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $row++;
		        if ($row == 1){
		        	continue;
		        }
		        
		        // $num = count($data);
		        // echo "<p> $num fields in line $row: <br /></p>\n";
		        $arr = array();
		        $accid = $data[0];
		        $month = $data[2];
		        $year = $data[3];
		        $productid = $data[4];
		        $birth = $data[6];
		        $peds = $data[7];
		        
		        if ($this->session->userdata("tier") <= 2.5){
					$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
				}
		        $teri =  $this->db->where("hospitalid", $data[0])->get("hospital");
		        if ($teri->num_rows() > 0){
		        	$processed++;
		        	if (!isset($processdata[$accid])){
		        		$processdata[$accid] = array();
		        	}
		        	if (!isset($processdata[$accid][$year])){
		        		$processdata[$accid][$year] = array();
		        	}
		        	if (!isset($processdata[$accid][$year][$month])){
		        		$processdata[$accid][$year][$month] = array();
		        	}
		        	$processdata[$accid][$year][$month][] = array("productid" => $productid, "birth" => $birth, "peds" => $peds);

		        } else {
		        	continue;
		        }
		              
	    	}
	    	foreach ($processdata as $accid=>$datatiertwo){
	    		foreach ($datatiertwo as $year=>$datatierthree){
	    			foreach ($datatierthree as $month=>$sharedata){
	    				$this->load->model("CMS_Closing");
	    				// echo $year."<br/>";
	    				// echo $month."<br/>";
	    				// echo $accid."<br/>";
	    				// var_dump($sharedata);
	    				$this->CMS_Closing->saveShareDataACCOnly($year, $month, $accid, $sharedata);
	    				// break;
	    			}
	    			// break;
	    		}
	    		// break;
	    	}
	    	fclose($handle);
		}

	    $data['dropdown'] = "utildropdown"; 
	    $data['datatype'] = "u_ushr"; 
		$data['HeaderSuffix'] = "Bulk Upload Share"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$data['msg'] = $processed." Entries Processed.";
		$this->load->view('cms/util_share', $data);
		$this->load->view('cms/_footer', $data);
	}

	public function commitUploadITR(){
		if ($this->session->userdata('syncp_logged_in') != TRUE)
    	{
        	redirect("cms/login");
    	}
    	$config['upload_path']          = './assets/';
        $config['allowed_types']        = 'csv'; 
        $config['overwrite']        	= true; 
		$this->load->library('upload', $config);
        $this->upload->do_upload('userfile');
        $filename = $this->upload->data('file_name');
        $pagename = $this->upload->data("raw_name");
        $row = 0;
	    $processed = 0;
	    $processdata = array();
	    if (($handle = fopen("assets/$pagename.csv", "r")) !== FALSE) {
		   	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $row++;
		        if ($row == 1){
		        	continue;
		        }
		        
		        // $num = count($data);
		        // echo "<p> $num fields in line $row: <br /></p>\n";
		        $arr = array();
		        $accid = $data[0];
		        $month = $data[2];
		        $year = $data[3];
		        $productid = $data[4];
		        $itr = $data[6];
		        
		        if ($this->session->userdata("tier") <= 2.5){
					$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
				}
		        $teri =  $this->db->where("hospitalid", $data[0])->get("hospital");
		        if ($teri->num_rows() > 0){
		        	$processed++;
		        	if (!isset($processdata[$accid])){
		        		$processdata[$accid] = array();
		        	}
		        	if (!isset($processdata[$accid][$year])){
		        		$processdata[$accid][$year] = array();
		        	}
		        	if (!isset($processdata[$accid][$year][$month])){
		        		$processdata[$accid][$year][$month] = array();
		        	}
		        	$processdata[$accid][$year][$month][] = array("productid" => $productid, "intention" => $itr);

		        } else {
		        	continue;
		        }
		              
	    	}
	    	foreach ($processdata as $accid=>$datatiertwo){
	    		foreach ($datatiertwo as $year=>$datatierthree){
	    			foreach ($datatierthree as $month=>$sharedata){
	    				$this->load->model("CMS_Closing");
	    				// echo $year."<br/>";
	    				// echo $month."<br/>";
	    				// echo $accid."<br/>";
	    				// var_dump($sharedata);
	    				$this->CMS_Closing->saveITRDataACCOnly($year, $month, $accid, $sharedata);
	    				// break;
	    			}
	    			// break;
	    		}
	    		// break;
	    	}
	    	fclose($handle);
		}

	    $data['dropdown'] = "utildropdown"; 
	    $data['datatype'] = "u_uitr"; 
		$data['HeaderSuffix'] = "Bulk Upload ITR"; 
		$this->load->view('cms/_header', $data);
		$this->load->view('cms/_sidebar', $data);
		$data['msg'] = $processed." Entries Processed.";
		$this->load->view('cms/util_itr', $data);
		$this->load->view('cms/_footer', $data);
	}

	private function processScheduleData($visitdata, $scheduledata, $userid){
		$this->load->model("API_Accounts");
		if (!$this->_empty($scheduledata)){
			$sendMail = 0;
			foreach ($scheduledata as $schedule){
				$repschedid = $schedule->internalscheduleid;
				$schedid = $this->API_Accounts->checkScheduleExistence($userid, $repschedid);
				if ($schedid > 0){
					if ($this->API_Accounts->checkApproved($schedid)){ //exists & has been approved, so not updatable
						continue; 
					} else { // exists, not approved. update existing
						if (isset($schedule->deleted)){
							if ($schedule->deleted == 1){
								$this->API_Accounts->markDeleted($schedid);
								continue;
							}
						}
						$type = "1On1";
						$planneddate = $schedule->dateofvisit;
						$plannedhcps = $schedule->hcplist;
						if (count($plannedhcps) > 1){
							$type = "Group";
						}
						$data = array(
							"type" => $type,
							"scheddate" => $planneddate
						);
						// var_dump($data);
						$this->API_Accounts->updateSchedule($data, $plannedhcps, $schedid);
					}
				} else if ($schedid == 0){ //create new
					if (isset($schedule->deleted)){
						if ($schedule->deleted == 1){
							continue;
						}
					}
					$type = "1On1";
					$planneddate = $schedule->dateofvisit;
					$plannedhcps = $schedule->hcplist;
					if (count($plannedhcps) > 1){
						$type = "Group";
					}
					$data = array(
						"repid" => $userid,
						"repschedid" => $repschedid,
						"type" => $type,
						"scheddate" => $planneddate,
						"approved" => 0,
						"visited" => 0,
						"deleted" => 0
					);
					// var_dump($data);
					$this->API_Accounts->createSchedule($data, $plannedhcps);
					if (date("Y-m", strtotime($planneddate)) == date("Y-m")){
						//adhoc;
					} else {
						$sendMail = 1;
					}
					
				} else {
					
				}
			}
		}
		if (!$this->_empty($visitdata)){
			foreach ($visitdata as $actualvisit){
				$repschedid = $actualvisit->internalscheduleid;
				$schedid = $this->API_Accounts->checkScheduleExistence($userid, $repschedid);
				if ($schedid > 0){
					$exist = $this->API_Accounts->checkExistingVisitData($schedid);
					if ($exist){
						continue;
					}
					$visittime = $actualvisit->visittime;
					$visitlat = $actualvisit->lat;
					$visitlng = $actualvisit->lng;
					$duration = $actualvisit->duration;
					$visitnumber = $actualvisit->visitnumber;
					$visitrating = 0;
					if (isset($actualvisit->visitrating)){
						$visitrating = $actualvisit->visitrating;
					}
					$data = array(
						"schedid" => $schedid,
						"visittime" => $visittime,
						"visitlat" => $visitlat,
						"visitlng" => $visitlng ,
						"visitduration" => $duration,
						"visitnumber" => $visitnumber,
						"visitrating" => $visitrating
					);
					$this->API_Accounts->recordActualVisit($data);
					$this->API_Accounts->markVisited($schedid);

					$modulestats = $actualvisit->modulestatistic;
					foreach ($modulestats as $mod){
						$data = array(
							"moduleid" => $mod->moduleid,
							"schedid" => $schedid,
							"accessdate" => $mod->accessdate,
							"accesstime" => $mod->accessduration
						);
						if (isset($mod->isadded)){
							$data['isadded'] = $mod->isadded;
						}
						$this->API_Accounts->recordModuleStatistic($data);
					}	
					$pagestats = $actualvisit->pagestatistic;
					foreach ($pagestats as $page){
						$feedback = "";
						if (isset($page->feedback)){
							$feedback = $page->feedback;
						}
						$data = array(
							"pageid" => $page->pageid,
							"schedid" => $schedid,
							"accessdate" => $page->accessdate,
							"accesstime" => $page->accessduration,
							"feedback" => $feedback
						);
						if (isset($page->moduleid)){
							$data['moduleid'] = $page->moduleid;
						}
						$this->API_Accounts->recordPageStatistic($data);
					}
					if (isset($actualvisit->signatureImg)){
						$actualvisit->signatureImg = str_replace(' ', '+', $actualvisit->signatureImg);
						file_put_contents('images/uploads/signatures/'.$schedid.'.jpg', base64_decode($actualvisit->signatureImg));

					}
					if (isset($actualvisit->photoImg)){
						$actualvisit->photoImg = str_replace(' ', '+', $actualvisit->photoImg);
						file_put_contents('images/uploads/photos/'.$schedid.'.jpg', base64_decode($actualvisit->photoImg));
					}
				} else {
					continue;
				}
			}
		}
	}

	private function _empty($var)
	{
	        return (empty($var) || ($var == "''"));
	}
}

