<?php
class API_Accounts extends CI_Model
{
	/******* PUBLISHER SECTION *******/

	
	function login($username, $password)
	{
		$this->db->select('repid, loginpass, obsolete');
		$this->db->from('representatives');
		$this->db->where('loginid', $username);

		$query = $this->db->get();

		if ($query->num_rows() && password_verify($password, $query->row()->loginpass)){
			if ($query->row()->obsolete == 1){
				return -2;
			}
			$this->db->where("repid", $query->row()->repid)->update("representatives", array('lastpushupdated' => date("Y-m-d H:i:s")));
			return $query->row()->repid;
		} else {
			return -1;
		}		
	}
	function passcheck($username, $password)
	{
		$this->db->select('repid, loginpass, obsolete');
		$this->db->from('representatives');
		$this->db->where('repid', $username);

		$query = $this->db->get();

		if ($query->num_rows() && password_verify($password, $query->row()->loginpass)){
			if ($query->row()->obsolete == 1){
				return -2;
			}
			return $query->row()->repid;
		} else {
			return -1;
		}		
	}

	function getInfo($id){
		$this->db->select("r.repid, r.repname, r.phone, r.address, hospitalname");
		$this->db->where("repid", $id);
		$this->db->from("representatives r");
		$this->db->join("hospital", "associatedaccount = hospitalid");
		$info = $this->db->get()->row();
		$last30days = date("Y-m-d 00:00:00", strtotime("-30 days"));
		$info->scanhistory = $this->db->where("repid", $info->repid)->where("datescanned >", $last30days)->order_by("datescanned", "desc")->get("representativescanhistory")->result();
		return $info;
	}

	function getAllInboxItem($id){
		$last30days = date("Y-m-d H:i:s", strtotime("-30 days"));
		$r =  $this->db->where("repid", $id)->where("datesent >", $last30days)->order_by("datesent", "desc")->get("representativeinbox")->result();
		foreach ($r as $inboxitem){
			$inboxitem->msgid = strtotime($inboxitem->datesent)."".$id;
		}
		return $r;
	}

	function checkExistingSchedule($repid, $repschedid){
		$sched = $this->db->select("schedid, approved, visited")->from("representativeschedule")->where("repid", $repid)->where("repschedid", $repschedid)->get();
		if ($sched->num_rows() > 0){
			if ($sched->row()->visited == 0 && $sched->row()->approved == 0){
				return $sched->row()->schedid;
			} else {
				return 0;
			}
			
		} else {
			return 0;
		}
	}

	function checkScheduleExistence($repid, $repschedid){
		$sched = $this->db->select("schedid, approved, visited")->from("representativeschedule")->where("repid", $repid)->where("repschedid", $repschedid)->get();
		if ($sched->num_rows() > 0){
			return $sched->row()->schedid;
		} else {
			return 0;
		}
	}

	function checkApproved($schedid){
		$b = $this->db->select("approved")->from("representativeschedule")->where("schedid", $schedid)->get()->row()->approved;
		if ($b == 1){
			return true;
		} else {
			return false;
		}
	} 

	function getApprovalStatus($repid){
		$res = $this->db->select("IFNULL(approveddate, '') as approveddate, repschedid")->where("repid", $repid)->where("approved", 1)->where("deleted", 0)->get("representativeschedule");
		if ($res->num_rows()){
			return $res->result();
		} else {
			return array();
		}
	}

	function getCancelStatus($repid){
		$res = $this->db->select("IFNULL(deleteddate, '') as deleteddate, repschedid")->where("repid", $repid)->where("deleted", 1)->get("representativeschedule");
		if ($res->num_rows()){
			return $res->result();
		} else {
			return array();
		}
	}

	function getAssignedSpecialists($repid){
		$territoryid = $this->db->where("repid", $repid)->get('representatives')->row()->territory;
		$this->db->select("CONCAT(rd.hospitalid, '-', rd.specid, '-', rd.roomid) as hsrid, rd.specid as specialistid, rd.hospitalid, rd.roomid, sp.segmentation as hcpsegmentid, sp.specialty as specialtyid, sp.specialistname, h.hospitalname, r.roomname, se.segmentation as hcpsegmentation, spc.specialtyname as specialty, h.address as hospitaladdress, IFNULL(sp.phone, 0) as specialistphone, IFNULL(spv.visits,3) as maxmonthlyvisits, 1 as assigned");
		$this->db->from("territoryassignment rd");
		$this->db->where("rd.territoryid", $territoryid);
		// $this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
		$this->db->join("specialist sp", "rd.specid = sp.specid and sp.obsolete = 0");
		$this->db->join("hospital h", "h.hospitalid = rd.hospitalid and h.obsolete = 0");
		$this->db->join("room r", "rd.roomid = r.roomid");
		$this->db->join("segmentation se", "se.segmentid = sp.segmentation");
		$this->db->join("specialty spc", "spc.specialtyid = sp.specialty");
		$this->db->join("specialtyvisit spv", "spv.segmentid = sp.segmentation AND spv.hospitalsegment = h.segmentid", "left");
		$res = $this->db->get();
		if ($res->num_rows() > 0){
			$returndata = $res->result();
			$thrmonthsago = date("Y-m-d H:i:s", strtotime("first day of two months ago"));
			$this->db->select("CONCAT(rd.hospitalid, '-', rd.specid, '-', hr.roomid) as hsrid, rd.specid as specialistid, rd.hospitalid, hr.roomid, sp.segmentation as hcpsegmentid, sp.specialty as specialtyid, sp.specialistname, h.hospitalname, r.roomname, se.segmentation as hcpsegmentation, spc.specialtyname as specialty, h.address as hospitaladdress, IFNULL(sp.phone, 0) as specialistphone, IFNULL(spv.visits,3) as maxmonthlyvisits, 0 as assigned");
			$this->db->from("schedulevisit rd");
			$this->db->where("rd.specid NOT IN", "(select ta.specid from territoryassignment ta where ta.territoryid = $territoryid)", false);
			$this->db->join("representativeschedule rs", "rs.schedid = rd.schedid and rs.visited = 1 and repid = $repid and scheddate > '".$thrmonthsago."'");
			$this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
			$this->db->join("specialist sp", "rd.specid = sp.specid and sp.obsolete = 0");
			$this->db->join("hospital h", "h.hospitalid = rd.hospitalid and h.obsolete = 0");
			$this->db->join("room r", "hr.roomid = r.roomid");
			$this->db->join("segmentation se", "se.segmentid = sp.segmentation");
			$this->db->join("specialty spc", "spc.specialtyid = sp.specialty");
			$this->db->join("specialtyvisit spv", "spv.segmentid = sp.segmentation AND spv.hospitalsegment = h.segmentid", "left");
			$addlres = $this->db->get();
			if ($addlres->num_rows() > 0){
				$returndata = array_merge($returndata, $addlres->result());
			}
			return $returndata;
		} else {
			return array();
		}
	}

	function getAssignedHospitals($repid){
		$reptype = $this->db->where("repid", $repid)->get('representatives')->row()->reptype;
		$this->db->from("hospital h");
		// $this->db->where("h.segmentid in", "(select segmentid from reptypeaccseg where typeid = $reptype)", false);
		$this->db->where("obsolete", 0);
		$this->db->where("approved", 1);

		$res = $this->db->get();
		if ($res->num_rows() > 0){
			$processed = $res->result();
			return $processed;
		} else {
			return array();
		}
	}

	function getAssignedRooms($hospitalarray, $repid){
		$territoryid = $this->db->where("repid", $repid)->get('representatives')->row()->territory;
		$roomarr = array();
		foreach ($hospitalarray as $hosp){
			$this->db->distinct("roomid");
			$this->db->select("CONCAT(rd.hospitalid, '-', rd.roomid) as hrid, rd.hospitalid, rd.roomid, r.roomname");
			$this->db->from("territoryassignment rd");
			$this->db->where("rd.territoryid", $territoryid);
			$this->db->where("rd.hospitalid", $hosp->hospitalid);
			// $this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
			$this->db->join("room r", "rd.roomid = r.roomid");		
			$rooms = $this->db->get()->result();
			foreach ($rooms as $key=>$r){
				$roomarr[] = $r;
			}
		}
		return $roomarr;
	}

	function updateSchedule($data, $hcparray, $schedid){
		$b = $this->db->where("schedid", $schedid)->update("representativeschedule", $data);
		$c = $this->db->where('schedid', $schedid)->delete("schedulevisit");

		if ($b && $c){
			foreach ($hcparray as $hcp){
				if ($hcp->hospitalid == ""){
					continue;
				}
				if ($hcp->specialistid == ""){
					continue;
				}
				if (!$this->checkHospitalExistence($hcp->hospitalid)){
					// continue;
				}
				if (!$this->checkSpecialistExistence($hcp->specialistid)){
					// continue;
				}
				$data = array(
					"schedid" => $schedid,
					"specid" => $hcp->specialistid,
					"hospitalid" => $hcp->hospitalid
				);
				$this->db->insert("schedulevisit", $data);
			}
			return true;
		} else {
			return false;
		}
	}

	function createSchedule($data, $hcparray){
		$b = $this->db->insert("representativeschedule", $data);
		$schedid = $this->db->insert_id();
		if ($b){
			foreach ($hcparray as $hcp){
				if ($hcp->hospitalid == ""){
					continue;
				}
				if ($hcp->specialistid == ""){
					continue;
				}
				if (!$this->checkHospitalExistence($hcp->hospitalid)){
					// continue;
				}
				if (!$this->checkSpecialistExistence($hcp->specialistid)){
					// continue;
				}
				$data = array(
					"schedid" => $schedid,
					"specid" => $hcp->specialistid,
					"hospitalid" => $hcp->hospitalid
				);
				$this->db->insert("schedulevisit", $data);
			}
			return true;
		} else {
			return false;
		}
	}

	private function checkHospitalExistence($hospitalid){
		$b = $this->db->where("hospitalid", $hospitalid)->get("hospital");
		if ($b->num_rows()){
			return $hospitalid;
		} else {
			$this->db->insert("hospital", array("hospitalid" => $hospitalid, "hospitalname" => "ADDED HOSPITAL", "xmlaccid" => "ADD-".$hospitalid));
			return $hospitalid;
		}
	}

	private function checkSpecialistExistence($specid){
		$b = $this->db->where("specid", $specid)->get("specialist");
		if ($b->num_rows()){
			return $specid;
		} else {
			$this->db->insert("specialist", array("specid" => $specid, "specialistname" => "ADDED SPECIALIST", "xmlhcpid" => "ADD-".$specid));
			return $specid;
		}
	}

	function storePost($postdata, $userid){
		return $this->db->insert("base64test", array("base64" => json_encode($postdata), "base642" => $userid. " " .date("Y-m-d")));
	}

	function getAllPages(){
		$pages = $this->db->select("cp.pageid, cp.pagetitle, cp.description as pagedescription, cp.pagename as pagename, cp.pagehtmllink as downloadlink, pc.categoryid, pc.categoryname, cp.folderlisting")->join("pagecategory pc", "categoryid = pagecategory")->get("contentpages cp");
		if ($pages->num_rows() > 0){
			$pages = $pages->result();
			foreach ($pages as $page){
				$page->thumbnail = base_url()."assets/thumbs/".$page->downloadlink.".jpg";
				$page->downloadlink = base_url()."assets/pages/".$page->downloadlink;
				if ($page->folderlisting != null){
				    $page->folderlisting = json_decode($page->folderlisting);
				    $links = "";
					$delim = "";
					foreach ($page->folderlisting as $link){
						$links .= $delim.$link;
						$delim = ",";
					}
					$page->folderlisting = $links;
				}
				
				
			}
			return $pages;
		} else {
			return array();
		}
	}

	function getAllPagesAfterDate($date){
		$pages = $this->db->select("cp.pageid, cp.pagetitle, cp.description as pagedescription, cp.pagename as pagename, cp.pagehtmllink as downloadlink, pc.categoryid, pc.categoryname, cp.folderlisting, cp.lastupdated, cp.obsolete")->join("pagecategory pc", "categoryid = pagecategory", "left")->where("obsolete", 0)->get("contentpages cp");
		if ($pages->num_rows() > 0){
			$pages = $pages->result();
			foreach ($pages as $page){
				$page->thumbnail = base_url()."assets/thumbs/".$page->pageid.".jpg";
				$page->downloadlink = base_url()."assets/pages/".$page->pageid.".".$page->downloadlink;
				$page->downloadflag = 0;
				$page->delete = 0;
				if ($page->lastupdated > $date){
					$page->downloadflag = 2;
				}
				if ($page->obsolete == 1){
					$page->downloadflag = 0;	
					$page->delete = 1;
				}
			}
			return $pages;
		} else {
			return array();
		}
	}

	function getAllHouses(){
		return $this->db->select("houseid, housename")->get("house")->result();
	}

	function getAllGroups(){
		return $this->db->select("groupid, groupname")->get("grouping")->result();
	}

	function getPageCategories(){
		return $this->db->select("categoryid, categoryname")->get("pagecategory")->result();
	}

	function getAllSpecialties(){
		return $this->db->select("specialtyid, specialtyname")->get("specialty")->result();
	}

	function getAllHospitalSegment(){
		return $this->db->select("segmentid as accsegmentid, segmentation as accsegmentation")->get("hospitalsegment")->result();
	}

	function getAllHospitalTypes(){
		return $this->db->get("hospitaltype")->result();
	}

	function getAllHCPSegment(){
		return $this->db->select("segmentid as hcpsegmentid, segmentation as hcpsegmentation")->get("segmentation")->result();
	}

	function getAllModules(){
		$modules = $this->db->select("cm.moduleid, cm.moduletitle as modulename, houseid, housename, cm.moduledesc as moduledescription")->where("obsolete", 0)->join("house", "house.houseid = cm.modulehouse")->get("contentmodules cm");
		if ($modules->num_rows() > 0){
			$modules = $modules->result();
			foreach ($modules as $mod){
				$mod->grouping = $this->db->select("g.groupid as groupid, groupname as groupname")->where("moduleid", $mod->moduleid)->join("grouping g", "g.groupid = mg.groupid")->get("modulegroup mg")->result();

				$modulepages = $this->db->select("pageid")->order_by("pagenumber", "asc")->where("moduleid", $mod->moduleid)->get("modulepages")->result();
				$mod->pages = array();
				foreach ($modulepages as $p){
					$mod->pages[] = $p->pageid;
				}
				
				$pagestring = "";
				$delim = "";
				foreach ($mod->pages as $pnum){
				    $pagestring .= $delim."'".$pnum."'";
				    $delim = ",";
				}
				$mod->pages = $pagestring;
			}
			return $modules;
		} else{
			return array();
		}
	}

	function getAllFilteredModules($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$modules = $this->db->select("cm.moduleid, cm.moduletitle as modulename, houseid, housename, cm.moduledesc as moduledescription, obsolete, periodstart, periodend")->where("generaltype", 0)->join("house", "house.houseid = cm.modulehouse")->get("contentmodules cm");
		if ($modules->num_rows() > 0){
			$modules = $modules->result();
			foreach ($modules as $mod){
				$toBeHidden = $this->db->where("reptypeid", $reptype)->where("moduleid", $mod->moduleid)->from("modulereptype")->get();
				$mod->hidden = 1;
				if ($toBeHidden->num_rows()){
					$mod->hidden = 0;
				}
				if (date("Y-m-d H:i:s") >= $mod->periodend || date("Y-m-d H:i:s") < $mod->periodstart){
					$mod->hidden = 1;
				}
				if ($mod->obsolete == 1){
					$mod->hidden = 1;
				}

				$mod->grouping = $this->db->select("mg.moduleid, g.groupid as groupid, groupname as groupname")->where("moduleid", $mod->moduleid)->join("grouping g", "g.groupid = mg.groupid")->get("modulegroup mg")->result();
				$mod->accounts = $this->db->where("moduleid", $mod->moduleid)->get("moduleaccounts")->result();
				$mod->pages = $this->db->select("mp.moduleid, mp.pagenumber, mp.pageid, mp.bookmark")->order_by("pagenumber", "asc")->where("moduleid", $mod->moduleid)->join("contentpages cp", "cp.pageid = mp.pageid")->where("cp.obsolete", 0)->get("modulepages mp")->result();
			}
			return $modules;
		} else{
			return array();
		}
	}

	function getAllFilteredGeneralModules($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$modules = $this->db->select("cm.moduleid, cm.moduletitle as modulename, houseid, housename, cm.moduledesc as moduledescription, obsolete, periodstart, periodend")->where("generaltype", 1)->join("house", "house.houseid = cm.modulehouse")->get("contentmodules cm");
		if ($modules->num_rows() > 0){
			$modules = $modules->result();
			foreach ($modules as $mod){
				$toBeHidden = $this->db->where("reptypeid", $reptype)->where("moduleid", $mod->moduleid)->from("modulereptype")->get();
				$mod->hidden = 1;
				if ($toBeHidden->num_rows()){
					$mod->hidden = 0;
				}
				if (date("Y-m-d H:i:s") >= $mod->periodend || date("Y-m-d H:i:s") < $mod->periodstart){
					$mod->hidden = 1;
				}
				if ($mod->obsolete == 1){
					$mod->hidden = 1;
				}

				$mod->grouping = $this->db->select("mg.moduleid, g.groupid as groupid, groupname as groupname")->where("moduleid", $mod->moduleid)->join("grouping g", "g.groupid = mg.groupid")->get("modulegroup mg")->result();
				$mod->accounts = $this->db->where("moduleid", $mod->moduleid)->get("moduleaccounts")->result();
				$mod->pages = $this->db->select("mp.moduleid, mp.pagenumber, mp.pageid, mp.bookmark")->order_by("pagenumber", "asc")->where("moduleid", $mod->moduleid)->join("contentpages cp", "cp.pageid = mp.pageid")->where("cp.obsolete", 0)->get("modulepages mp")->result();
			}
			return $modules;
		} else{
			return array();
		}
	}

	function getAllFilteredQuiz($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$modules = $this->db->select("c.cycleid, cycletitle")->where("obsolete", 0)->where("periodend >", date("Y-m-d H:i:s"))->where("periodstart <", date("Y-m-d H:i:s"))->where("c.cycleid IN", "(SELECT cycleid from closingsurveycyclereptype where reptypeid = $reptype)", false)->where("c.cycleid NOT IN", "(SELECT quizcycleid FROM surveysession where repid = $userid)", false)->get("closingsurveycycle c");
		// echo $this->db->last_query();
		if ($modules->num_rows() > 0){
			$modules = $modules->result();
			foreach ($modules as $mod){
				$mod->questions = $this->db->where("cycleid", $mod->cycleid)->join("closingsurvey cs", "cs.surveyid = csq.questionid")->order_by("order", "asc")->get("closingsurveycyclequestions csq")->result();;
			}
			return $modules;
		} else{
			return array();
		}
	}

	function getAllCycles(){
		$cycles = $this->db->select("c.cycleid, cycletitle, periodstart, periodend")->get("cycle c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			foreach ($cycles as $cycle){
				$cycle->visittype = $this->db->select("visittype")->where("cycleid", $cycle->cycleid)->get("cyclefiltervisittype")->row()->visittype;
				$modulecycle = $this->db->select("GROUP_CONCAT(moduleid separator ',') as moduleid, visitnumber")->where("cycleid", $cycle->cycleid)->order_by("visitnumber", "asc")->group_by("visitnumber")->get("cyclemodules")->result();
				$filteraccsegment = $this->db->select("hospitalsegment")->where("cycleid", $cycle->cycleid)->get('cyclefilteraccsegment')->result();
				$filterspecialty = $this->db->select("specialtyid")->where("cycleid", $cycle->cycleid)->get('cyclefilterspecialty')->result();
				$filterhcpsegment = $this->db->select("segmentid")->where("cycleid", $cycle->cycleid)->get('cyclefiltersegment')->result();
				$filterroom = $this->db->select("roomid")->where("cycleid", $cycle->cycleid)->get('cyclefilterroom')->result();
				
				$cycle->filterspecialty = "";
				$cycle->filterhcpsegment = "";
				$cycle->filteraccsegment = "";
				$cycle->filterroom = "";
				$cycle->modulecycle = "";
				$delim = "";
				foreach ($modulecycle as $mc){
				    $cycle->modulecycle .= $delim . $mc->visitnumber. ":". $mc->moduleid;
				    $delim = ";";
				}
				$delim = "";
				foreach ($filteraccsegment as $p){
					$cycle->filteraccsegment .= $delim . $p->hospitalsegment;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterspecialty as $p){
					$cycle->filterspecialty .= $delim . $p->specialtyid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterhcpsegment as $p){
					$cycle->filterhcpsegment .= $delim . $p->segmentid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterroom as $p){
					$cycle->filterroom .= $delim . $p->roomid;
					$delim = ",";
				}
			}
			return $cycles;
		} else {
			return array();
		}
	}

	function getAllFilteredCycles($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$cycles = $this->db->select("c.cycleid, cycletitle, periodstart, periodend")->where("c.cycleid in", "(SELECT DISTINCT(crt.cycleid) from cyclefilterreptype crt where reptypeid = $reptype)", false)->where("obsolete", 0)->get("cycle c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			foreach ($cycles as $cycle){
				$cycle->visittype = $this->db->select("visittype")->where("cycleid", $cycle->cycleid)->get("cyclefiltervisittype")->row()->visittype;
				$modulecycle = $this->db->select("GROUP_CONCAT(moduleid separator ',') as moduleid, visitnumber")->where("cycleid", $cycle->cycleid)->order_by("visitnumber", "asc")->group_by("visitnumber")->get("cyclemodules")->result();
				$filteraccsegment = $this->db->select("hospitalsegment")->where("cycleid", $cycle->cycleid)->get('cyclefilteraccsegment')->result();
				$filterspecialty = $this->db->select("specialtyid")->where("cycleid", $cycle->cycleid)->get('cyclefilterspecialty')->result();
				$filterhcpsegment = $this->db->select("segmentid")->where("cycleid", $cycle->cycleid)->get('cyclefiltersegment')->result();
				$filterroom = $this->db->select("roomid")->where("cycleid", $cycle->cycleid)->get('cyclefilterroom')->result();
				
				$cycle->filterspecialty = "";
				$cycle->filterhcpsegment = "";
				$cycle->filteraccsegment = "";
				$cycle->filterroom = "";
				$cycle->modulecycle = "";
				$delim = "";
				foreach ($modulecycle as $mc){
				    $cycle->modulecycle .= $delim . $mc->visitnumber. ":". $mc->moduleid;
				    $delim = ";";
				}
				$delim = "";
				foreach ($filteraccsegment as $p){
					$cycle->filteraccsegment .= $delim . $p->hospitalsegment;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterspecialty as $p){
					$cycle->filterspecialty .= $delim . $p->specialtyid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterhcpsegment as $p){
					$cycle->filterhcpsegment .= $delim . $p->segmentid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterroom as $p){
					$cycle->filterroom .= $delim . $p->roomid;
					$delim = ",";
				}
			}
			return $cycles;
		} else {
			return array();
		}
	}

	function getAllHealthIssues(){
		return $this->db->get("healthissues")->result();
	}
	function getAllFilteredProducts($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$cycles = $this->db->select("p.productid, productname, danoneflag as isdanoneproduct, housename")->where("c.typeid", $reptype)->join("product p", "p.productid = c.productid and p.obsolete = 0")->join("housecategorization hc", "hc.hcid = p.house", "left")->join("house h", "h.houseid = hc.houseid", "left")->get("reptypeproduct c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			return $cycles;
		} else {
			return array();
		}
	}
	function getAllFilteredProductIssues($products){
		if (count($products) <= 0){
			return array();
		}
		$prodids = "(";
		$delim = "";
		foreach ($products as $pr){
			$prodids .= $delim.$pr->productid;
			$delim = ",";
		}
		$prodids .= ")";
		return $this->db->where("productid in", $prodids, false)->get("productissue")->result();
	}
	function getDeliveryFlag($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$toggle = $this->db->select("visibility")->where("reptypeid", $reptype)->where("visibility", 1)->get("closingdeliveryreptypetoggling");
		if ($toggle->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function getNonObsDeliveryCriteria(){
		$criteria = $this->db->select("criteriaid, criteriatitle")->where("obsolete", 0)->get("closingdeliverycriteria");
		if ($criteria->num_rows() > 0){
			return $criteria->result();
		} else {
			return array();
		}
	}

	function getNonObsPotencyCriteria(){
		$criteria = $this->db->select("criteriaid, criteriatitle")->where("obsolete", 0)->get("closingpotencycriteria");
		if ($criteria->num_rows() > 0){
			return $criteria->result();
		} else {
			return array();
		}
	}

	function getAllFilteredQuestions($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$cycles = $this->db->select("c.cycleid, cycletitle, periodstart, periodend")->where("c.cycleid in", "(SELECT DISTINCT(crt.cycleid) from closingsurveycyclereptype crt where reptypeid = $reptype)", false)->where("obsolete", 0)->get("closingsurveycycle c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			foreach ($cycles as $cycle){
				$cycle->visittype = $this->db->select("visittype")->where("cycleid", $cycle->cycleid)->get("closingsurveycyclevisittype")->row()->visittype;
				$modulecycle = $this->db->select("GROUP_CONCAT(questionid separator ',') as questionid, order")->where("cycleid", $cycle->cycleid)->order_by("order", "asc")->group_by("order")->get("closingsurveycyclequestions")->result();
				$filteraccsegment = $this->db->select("hospitalsegment")->where("cycleid", $cycle->cycleid)->get('closingsurveycycleaccsegments')->result();
				$filterspecialty = $this->db->select("specialtyid")->where("cycleid", $cycle->cycleid)->get('closingsurveycyclespecialty')->result();
				$filterhcpsegment = $this->db->select("segmentid")->where("cycleid", $cycle->cycleid)->get('closingsurveycyclesegment')->result();
				$filterroom = $this->db->select("roomid")->where("cycleid", $cycle->cycleid)->get('closingsurveycycleroom')->result();
				
				$cycle->filterspecialty = "";
				$cycle->filterhcpsegment = "";
				$cycle->filteraccsegment = "";
				$cycle->filterroom = "";
				$cycle->surveycycle = "";
				$survquestions = "";
				$delim = "";
				foreach ($modulecycle as $mc){
				    $survquestions .= $delim . $mc->questionid;
				    $delim = ",";
				}
				$delim = "";
				for ($i = 1; $i < 6; $i++){
					$cycle->surveycycle .= $delim .$i.":". $survquestions;
					$delim = ";";
				}
				$delim = "";
				foreach ($filteraccsegment as $p){
					$cycle->filteraccsegment .= $delim . $p->hospitalsegment;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterspecialty as $p){
					$cycle->filterspecialty .= $delim . $p->specialtyid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterhcpsegment as $p){
					$cycle->filterhcpsegment .= $delim . $p->segmentid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterroom as $p){
					$cycle->filterroom .= $delim . $p->roomid;
					$delim = ",";
				}
			}
			return $cycles;
		} else {
			return array();
		}
	}

	function getAllNonObsQuestions(){
		$criteria = $this->db->select("surveyid, if(surveytype>0, 'FreeText', 'MCQ') as type, surveytext as question, surveychoices as mcqchoices")->where("availability", 1)->get("closingsurvey");
		if ($criteria->num_rows() > 0){
			return $criteria->result();
		} else {
			return array();
		}
	}

	function recordActualVisit($data){
		return $this->db->insert("actualvisit", $data);
	}

	function markVisited($schedid){
		return $this->db->where("schedid", $schedid)->update("representativeschedule", array("visited" => 1));
	}

	function saveImgBase64($sig, $pho){
		return $this->db->insert("base64test", array("base64" => $sig, "base642" => $pho));
	}

	function recordModuleStatistic($data){
		return $this->db->insert("modulestatistics", $data);
	}

	function recordPageStatistic($data){
		return $this->db->insert("pagestatistics", $data);
	}

	function checkExistingVisitData($schedid){
		$res = $this->db->where("schedid", $schedid)->get("actualvisit");
		if ($res->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function checkExistingClosingShareSchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdatashare");
		if ($res->num_rows() > 0){
			return $res->row()->shareid;
		} else {
			return false;
		}
	}

	function checkExistingClosingShareAccData($userid, $accid, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		
		$res = $this->db->get("closingdatashare");
		if ($res->num_rows() > 0){
			return $res->row()->shareid;
		} else {
			return false;
		}
	}

	function checkExistingClosingPotencySchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdatapotency");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}

	function checkExistingClosingPotencyAccData($userid, $accid, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		
		$res = $this->db->get("closingdatapotency");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}
	function insertClosingPotencyEntry($data){
		$b = $this->db->insert("closingdatapotency", $data);
		return $this->db->insert_id();
	}

	function updateClosingPotencyEntry($shareid, $data){
		$b = $this->db->where("dataid", $shareid)->update("closingdatapotency", $data);
		return $b;
	}
	function updateClosingPotencyValues($id, $sharearray){
		$vt = $this->db->where("dataid", $id)->delete("closingdatapotencyvalues");
		if ($vt){
			if (empty($sharearray)){
				return true;
			}
			return $this->db->insert_batch("closingdatapotencyvalues", $sharearray);
		} else {
			return false;
		}
	}



	function checkExistingClosingDeliverySchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdatadelivery");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}

	function checkExistingClosingDeliveryAccData($userid, $accid, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		
		$res = $this->db->get("closingdatadelivery");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}
	function insertClosingDeliveryEntry($data){
		$b = $this->db->insert("closingdatadelivery", $data);
		return $this->db->insert_id();
	}

	function updateClosingDeliveryEntry($shareid, $data){
		$b = $this->db->where("dataid", $shareid)->update("closingdatadelivery", $data);
		return $b;
	}
	function updateClosingDeliveryValues($id, $sharearray){
		$vt = $this->db->where("dataid", $id)->delete("closingdatadeliveryvalues");
		if ($vt){
			if (empty($sharearray)){
				return true;
			}
			return $this->db->insert_batch("closingdatadeliveryvalues", $sharearray);
		} else {
			return false;
		}
	}

	function checkExistingClosingBirthSchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdatabirth");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}

	function checkExistingClosingBirthAccData($userid, $accid, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		
		$res = $this->db->get("closingdatabirth");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}
	function insertClosingBirthEntry($data){
		$b = $this->db->insert("closingdatabirth", $data);
		return $this->db->insert_id();
	}

	function updateClosingBirthEntry($shareid, $data){
		$b = $this->db->where("dataid", $shareid)->update("closingdatabirth", $data);
		return $b;
	}

	function updateClosingShareValues($id, $sharearray){
		$vt = $this->db->where("shareid", $id)->delete("closingdatashareproducts");
		if ($vt){
			if (empty($sharearray)){
				return true;
			}
			return $this->db->insert_batch("closingdatashareproducts", $sharearray);
		} else {
			return false;
		}
	}

	function insertClosingShareEntry($data){
		$b = $this->db->insert("closingdatashare", $data);
		return $this->db->insert_id();
	}

	function updateClosingShareEntry($shareid, $data){
		$b = $this->db->where("shareid", $shareid)->update("closingdatashare", $data);
		return $b;
	}

	function checkExistingClosingITRSchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdataitr");
		if ($res->num_rows() > 0){
			return $res->row()->itrid;
		} else {
			return false;
		}
	}

	function checkExistingClosingITRHCPData($userid, $accid, $hcpid = NULL, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		if (!empty($hcpid)){
			$this->db->where("specid", $hcpid);
		}
		$res = $this->db->get("closingdataitr");
		if ($res->num_rows() > 0){
			return $res->row()->itrid;
		} else {
			return false;
		}
	}

	function updateClosingITRIntentions($id, $itrarray){
		$vt = $this->db->where("itrid", $id)->delete("closingdataitrproducts");
		if ($vt){
			if (empty($itrarray)){
				return true;
			}
			return $this->db->insert_batch("closingdataitrproducts", $itrarray);
		} else {
			return false;
		}
	}

	function insertClosingITREntry($data){
		$b = $this->db->insert("closingdataitr", $data);
		return $this->db->insert_id();
	}

	function updateClosingITREntry($itrid, $data){
		$b = $this->db->where("itrid", $itrid)->update("closingdataitr", $data);
		return $b;
	}

	function checkExistingClosingSurveySchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdatasurvey");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}

	function checkExistingClosingSurveyHCPData($userid, $accid, $hcpid = NULL, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		if (!empty($hcpid)){
			$this->db->where("specid", $hcpid);
		}
		$res = $this->db->get("closingdatasurvey");
		if ($res->num_rows() > 0){
			return $res->row()->dataid;
		} else {
			return false;
		}
	}

	function updateClosingSurveyAnswers($id, $itrarray){
		$vt = $this->db->where("dataid", $id)->delete("closingdatasurveyanswers");
		if ($vt){
			if (empty($itrarray)){
				return true;
			}
			return $this->db->insert_batch("closingdatasurveyanswers", $itrarray);
		} else {
			return false;
		}
	}

	function insertClosingSurveyEntry($data){
		$b = $this->db->insert("closingdatasurvey", $data);
		return $this->db->insert_id();
	}

	function updateClosingSurveyEntry($itrid, $data){
		$b = $this->db->where("dataid", $itrid)->update("closingdatasurvey", $data);
		return $b;
	}

	function getNRName($repid){
		return $this->db->where("repid", $repid)->get("representatives")->row()->repname;
	}

	function fetchManagerMails($repid){
		$emails = $this->db->select("email")->from("cmsuser cu")->join("dmreps dm", "cu.userid = dm.userid")->where("dm.repid", $repid)->get()->result();
		$emarr = array();
		foreach ($emails as $em){
			  $emarr[] = $em->email;
		}
		return $emarr;
	}

	function updatePush($userid, $pushtoken, $pushtype){
		return $this->db->where("repid", $userid)->update("representatives", array("pushid" => $pushtoken, "pushtype" => $pushtype, "lastpushupdated" => date("Y-m-d H:i:s")));
	}

	function doesProductExist($productid){
		return $this->db->where("productid", $productid)->get("product")->num_rows();
	}

	function markDeleted($schedid){
		return $this->db->where("schedid", $schedid)->update("representativeschedule", array("deleted" => 1));
	}

	function checkDetailingSessionExistence($userid, $internalsessionid){
		$sess = $this->db->where("internalsessionid", $internalsessionid)->where("repid", $userid)->get("representativesession");
		if ($sess->num_rows()){
			return $sess->row()->sessionid;
		} else {
			return 0;
		}
	}

	function createRepDetailingSession($sessdata){
		$this->db->insert("representativesession", $sessdata);
		return $this->db->insert_id();
	}

	function updateDetailingSessionModule($moddata, $sessionid){
		$this->db->where("schedid", $sessionid)->delete("modulestatistics");
		$this->db->insert("modulestatistics", $moddata);
		return true;
	}

	function clearDetailingSessionPageData($sessionid){
		$this->db->where("schedid", $sessionid)->delete("pagestatistics");
		return true;
	}

	function insertDetailingSessionPageData($pagedata){
		return $this->db->insert("pagestatistics", $pagedata);
	}

	function checkSurveySessionExistence($userid, $internalsessionid){
		$sess = $this->db->where("internalsessionid", $internalsessionid)->where("repid", $userid)->get("surveysession");
		if ($sess->num_rows()){
			return $sess->row()->sessionid;
		} else {
			return 0;
		}
	}

	function insertSurveySessionQuizData($quizdata){
		$this->db->insert("surveysession", $quizdata);
		return $this->db->insert_id();
	}

	function clearSurveySessionQuizData($sessionid){
		$this->db->where("sessionid", $sessionid)->delete("surveysessionanswers");
		return true;
	}

	function insertSurveySessionAnswerData($answerdata){
		return $this->db->insert("surveysessionanswers", $answerdata);
	}

	private function _crypto_rand_secure($min, $max)
	{
		$range = $max - $min;
		if ($range < 1) return $min;

		$log    = ceil(log($range, 2));
		$bytes  = (int) ($log / 8) + 1;
		$bits   = (int) $log + 1;
		$filter = (int) (1 << $bits) - 1; 

		do {
			$rnd = hexdec(bin2hex(mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM)));
			$rnd = $rnd & $filter; 
		} while ($rnd >= $range);

		return $min + $rnd;
	}

	private function _generate_key($length)
	{
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet); 

		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->_crypto_rand_secure(0, $max)];
		}

		return $token;
	}

	private function _generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
?>