<?php
class API_Accounts_bkup extends CI_Model
{
	/******* PUBLISHER SECTION *******/

	
	function login($username, $password)
	{
		$this->db->select('repid, loginpass');
		$this->db->from('representatives');
		$this->db->where('loginid', $username);

		$query = $this->db->get();

		if ($query->num_rows() && password_verify($password, $query->row()->loginpass)){
			return $query->row()->repid;
		} else {
			return -1;
		}		
	}

	function getInfo($id){
		$this->db->select("r.repid, r.repname, r.address, r.phone, rt.typename, t.territoryname, dc.districtname, c.cityname, p.provinceid, r.xmlnacd as repcode");
		$this->db->where("repid", $id);
		$this->db->from("representatives r");
		$this->db->join("reptype rt", "rt.typeid = r.reptype", "left");
		$this->db->join("territory t", "t.territoryid = r.territory", "left");
		$this->db->join("district dc", "dc.districtid = r.district", "left");
		$this->db->join("city c", "dc.cityid = c.cityid", "left");
		$this->db->join("province p", "p.provinceid = c.provinceid", "left");
		$info = $this->db->get()->row();
		$sched = $this->db->select("schedid, repschedid, type, scheddate, createddate, approved, visited, deleted")->where("deleted", 0)->where("repid", $id)->order_by("schedid", "asc")->get("representativeschedule");
		$pagestats = array();
		$modstats = array();
		if ($sched->num_rows() > 0){
			$info->schedules = $sched->result();
			foreach ($info->schedules as $sch){
				$specs = $this->db->select('sp.specialistname, sp.specid, hs.hospitalname, hs.hospitalid')->where("schedid", $sch->schedid)->join("specialist sp", "sp.specid = sv.specid")->join("hospital hs", "hs.hospitalid = sv.hospitalid")->get("schedulevisit sv")->result();
				$sch->specname = "";
				$sch->specialistid = "";
				$sch->hospitalname = "";
				$sch->hospitalid = "";
				$delim = "";
				foreach ($specs as $sp){
					$sch->specname .= $delim.$sp->specialistname;
					$sch->specialistid .= $delim.$sp->specid;
					$sch->hospitalname .= $delim.$sp->hospitalname;
					$sch->hospitalid .= $delim.$sp->hospitalid;

					$delim = "x;x";
				}
				$sch->starttime = "--:--";
				$sch->endtime = "--:--";
				$sch->duration = 0;
				if ($sch->visited == 1){
					$actualvisit = $this->db->select("visittime, visitend, visitduration")->where("schedid", $sch->schedid)->get("actualvisit");
					if ($actualvisit->num_rows() > 0){
						$actualvisit = $actualvisit->row();
						$sch->starttime = date("H:i", strtotime($actualvisit->visittime));
						$sch->endtime = date("H:i", strtotime($actualvisit->visittime) + $actualvisit->visitduration);
						$sch->duration = $actualvisit->visitduration;
					}
					$pgstat = $this->db->select("'$sch->repschedid' as repschedid, pageid, accesstime as seconds, accessdate, feedback", false)->where("schedid", $sch->schedid)->order_by("accessdate", "asc")->get("pagestatistics")->result();
					$mdstat = $this->db->select("'$sch->repschedid' as repschedid, moduleid, accesstime as seconds, accessdate", false)->where("schedid", $sch->schedid)->order_by("accessdate", "asc")->get("modulestatistics")->result();
					$i = 0;
					foreach ($mdstat as $mds){
						$secs = $mds->seconds;
						for (; $i < count($pgstat); $i++){
							$dur = $pgstat[$i]->seconds;
							$secs -= $dur;
							$pgstat[$i]->moduleid = $mds->moduleid;
							if ($secs <= 0){
								break;
							}
							$pagestats[] = $pgstat[$i];
						}
					}
				}
			}
		} else {
			$info->schedules = array(
			    array(
			        "schedid" => 0,
			        "repschedid" => "DefaultDummy",
			        "type" => "1On1",
			        "scheddate" => "2000-02-01 00:00:00",
			        "createddate" => "2000-01-01 00:00:00",
			        "approved" => 1,
			        "visited" => 1,
			        "deleted" => 0,
			        "specname" => "Dummy Schedule",
			        "specialistid" => "",
			        "hospitalname" => "Dummy Schedule", 
			        "hospitalid" => "",
			        "starttime" => "--:--",
			        "endtime" => "--:--",
			        "duration" => 0
			    )
			);
		}
		$info->timer = $pagestats;
		if (count($pagestats) > 0){
			foreach ($pagestats as $pg){

			}
		}

		return $info;
	}

	function checkExistingSchedule($repid, $repschedid){
		$sched = $this->db->select("schedid, approved, visited")->from("representativeschedule")->where("repid", $repid)->where("repschedid", $repschedid)->get();
		if ($sched->num_rows() > 0){
			if ($sched->row()->visited == 0 && $sched->row()->approved == 0){
				return $sched->row()->schedid;
			} else {
				return -1;
			}
			
		} else {
			return 0;
		}
	}

	function checkScheduleExistence($repid, $repschedid){
		$sched = $this->db->select("schedid, approved, visited")->from("representativeschedule")->where("repid", $repid)->where("repschedid", $repschedid)->get();
		if ($sched->num_rows() > 0){
			return $sched->row()->schedid;
		} else {
			return 0;
		}
	}

	function checkApproved($schedid){
		$b = $this->db->select("approved")->from("representativeschedule")->where("schedid", $schedid)->get()->row()->approved;
		if ($b == 1){
			return true;
		} else {
			return false;
		}
	} 

	function getApprovalStatus($repid){
		$res = $this->db->select("IFNULL(approveddate, '') as approveddate, repschedid")->where("repid", $repid)->where("approved", 1)->where("deleted", 0)->get("representativeschedule");
		if ($res->num_rows()){
			return $res->result();
		} else {
			return array();
		}
	}

	function getCancelStatus($repid){
		$res = $this->db->select("IFNULL(deleteddate, '') as deleteddate, repschedid")->where("repid", $repid)->where("deleted", 1)->get("representativeschedule");
		if ($res->num_rows()){
			return $res->result();
		} else {
			return array();
		}
	}

	function getAssignedSpecialists($repid){
		$this->db->select("CONCAT(rd.hospitalid, '-', rd.specid, '-', hr.roomid) as hsrid, rd.specid as specialistid, rd.hospitalid, hr.roomid, sp.segmentation as hcpsegmentid, sp.specialty as specialtyid, sp.specialistname, h.hospitalname, r.roomname, se.segmentation as hcpsegmentation, spc.specialtyname as specialty, h.address as hospitaladdress, IFNULL(sp.phone, 0) as specialistphone, IFNULL(spv.visits,3) as maxmonthlyvisits");
		$this->db->from("representativedesignation rd");
		$this->db->where("rd.repid", $repid);
		$this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
		$this->db->join("specialist sp", "rd.specid = sp.specid");
		$this->db->join("hospital h", "h.hospitalid = rd.hospitalid");
		$this->db->join("room r", "hr.roomid = r.roomid");
		$this->db->join("segmentation se", "se.segmentid = sp.segmentation");
		$this->db->join("specialty spc", "spc.specialtyid = sp.specialty");
		$this->db->join("specialtyvisit spv", "spv.segmentid = sp.segmentation AND spv.hospitalsegment = h.segmentid", "left");
		$res = $this->db->get();
		if ($res->num_rows() > 0){
			return $res->result();
		} else {
			return array();
		}
	}

	function getAssignedHospitals($repid){
		$this->db->distinct("hospitalid");
		$this->db->select("rd.hospitalid, hospitalname, address, hs.segmentid as accsegmentid, hs.segmentation as accsegmentation");
		$this->db->from("representativedesignation rd");
		$this->db->where("rd.repid", $repid);
		$this->db->join("hospital h", "h.hospitalid = rd.hospitalid");
		$this->db->join("hospitalsegment hs", "h.segmentid = hs.segmentid");
		$res = $this->db->get();
		if ($res->num_rows() > 0){
			$processed = $res->result();
			// foreach ($processed as $row){
			// 	$this->db->distinct("roomid");
			// 	$this->db->select("hr.roomid, r.roomname");
			// 	$this->db->from("representativedesignation rd");
			// 	$this->db->where("rd.repid", $repid);
			// 	$this->db->where("hr.hospitalid", $row->hospitalid);
			// 	$this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
			// 	$this->db->join("room r", "hr.roomid = r.roomid");		
			// 	$row->rooms = $this->db->get()->result();
			// }
			return $processed;
		} else {
			return array();
		}
	}

	function getAssignedRooms($hospitalarray, $repid){
		$roomarr = array();
		foreach ($hospitalarray as $hosp){
			$this->db->distinct("roomid");
			$this->db->select("CONCAT(hr.hospitalid, '-', hr.roomid) as hrid, hr.hospitalid, hr.roomid, r.roomname");
			$this->db->from("representativedesignation rd");
			$this->db->where("rd.repid", $repid);
			$this->db->where("hr.hospitalid", $hosp->hospitalid);
			$this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
			$this->db->join("room r", "hr.roomid = r.roomid");		
			$rooms = $this->db->get()->result();
			foreach ($rooms as $key=>$r){
				$roomarr[] = $r;
			}
		}
		return $roomarr;
	}

	function updateSchedule($data, $hcparray, $schedid){
		$b = $this->db->where("schedid", $schedid)->update("representativeschedule", $data);
		$c = $this->db->where('schedid', $schedid)->delete("schedulevisit");

		if ($b && $c){
			foreach ($hcparray as $hcp){
				if ($hcp->hospitalid == ""){
					continue;
				}
				if ($hcp->specialistid == ""){
					continue;
				}
				$data = array(
					"schedid" => $schedid,
					"specid" => $hcp->specialistid,
					"hospitalid" => $hcp->hospitalid
				);
				$this->db->insert("schedulevisit", $data);
			}
			return true;
		} else {
			return false;
		}
	}

	function createSchedule($data, $hcparray){
		$b = $this->db->insert("representativeschedule", $data);
		$schedid = $this->db->insert_id();
		if ($b){
			foreach ($hcparray as $hcp){
				if ($hcp->hospitalid == ""){
					continue;
				}
				if ($hcp->specialistid == ""){
					continue;
				}
				if (!$this->checkHospitalExistence($hcp->hospitalid)){
					// continue;
				}
				if (!$this->checkSpecialistExistence($hcp->specialistid)){
					// continue;
				}
				$data = array(
					"schedid" => $schedid,
					"specid" => $hcp->specialistid,
					"hospitalid" => $hcp->hospitalid
				);
				$this->db->insert("schedulevisit", $data);
			}
			return true;
		} else {
			return false;
		}
	}

	private function checkHospitalExistence($hospitalid){
		$b = $this->db->where("hospitalid", $hospitalid)->get("hospital");
		if ($b->num_rows()){
			return $hospitalid;
		} else {
			$this->db->insert("hospital", array("hospitalid" => $hospitalid, "hospitalname" => "ADDED HOSPITAL", "xmlaccid" => "ADD-".$hospitalid));
			return $hospitalid;
		}
	}

	private function checkSpecialistExistence($specid){
		$b = $this->db->where("specid", $specid)->get("specialist");
		if ($b->num_rows()){
			return $specid;
		} else {
			$this->db->insert("specialist", array("specid" => $specid, "specialistname" => "ADDED SPECIALIST", "xmlhcpid" => "ADD-".$specid));
			return $specid;
		}
	}

	function storePost($postdata, $userid){
		return $this->db->insert("base64test", array("base64" => json_encode($postdata), "base642" => $userid. " " .date("Y-m-d")));
	}

	function getAllPages(){
		$pages = $this->db->select("cp.pageid, cp.pagetitle, cp.description as pagedescription, cp.pagename as pagename, cp.pagehtmllink as downloadlink, pc.categoryid, pc.categoryname, cp.folderlisting")->join("pagecategory pc", "categoryid = pagecategory")->get("contentpages cp");
		if ($pages->num_rows() > 0){
			$pages = $pages->result();
			foreach ($pages as $page){
				$page->thumbnail = base_url()."assets/thumbs/".$page->downloadlink.".jpg";
				$page->downloadlink = base_url()."assets/pages/".$page->downloadlink;
				if ($page->folderlisting != null){
				    $page->folderlisting = json_decode($page->folderlisting);
				    $links = "";
					$delim = "";
					foreach ($page->folderlisting as $link){
						$links .= $delim.$link;
						$delim = ",";
					}
					$page->folderlisting = $links;
				}
				
				
			}
			return $pages;
		} else {
			return array();
		}
	}

	function getAllPagesAfterDate($date){
		$pages = $this->db->select("cp.pageid, cp.pagetitle, cp.description as pagedescription, cp.pagename as pagename, cp.pagehtmllink as downloadlink, pc.categoryid, pc.categoryname, cp.folderlisting")->where("lastupdated > ", $date)->join("pagecategory pc", "categoryid = pagecategory", "left")->where("obsolete", 0)->get("contentpages cp");
		if ($pages->num_rows() > 0){
			$pages = $pages->result();
			foreach ($pages as $page){
				$page->thumbnail = base_url()."assets/thumbs/".$page->downloadlink.".jpg";
				$page->downloadlink = base_url()."assets/pages/".$page->downloadlink;
				if ($page->folderlisting != null){
				    $page->folderlisting = json_decode($page->folderlisting);
				    $links = "";
					$delim = "";
					foreach ($page->folderlisting as $link){
						$links .= $delim.$link;
						$delim = ",";
					}
					$page->folderlisting = $links;
				}
			}
			return $pages;
		} else {
			return array();
		}
	}

	function getAllHouses(){
		return $this->db->select("houseid, housename")->get("house")->result();
	}

	function getAllGroups(){
		return $this->db->select("groupid, groupname")->get("grouping")->result();
	}

	function getPageCategories(){
		return $this->db->select("categoryid, categoryname")->get("pagecategory")->result();
	}

	function getAllSpecialties(){
		return $this->db->select("specialtyid, specialtyname")->get("specialty")->result();
	}

	function getAllHospitalSegment(){
		return $this->db->select("segmentid as accsegmentid, segmentation as accsegmentation")->get("hospitalsegment")->result();
	}

	function getAllHCPSegment(){
		return $this->db->select("segmentid as hcpsegmentid, segmentation as hcpsegmentation")->get("segmentation")->result();
	}

	function getAllModules(){
		$modules = $this->db->select("cm.moduleid, cm.moduletitle as modulename, houseid, housename, cm.moduledesc as moduledescription")->where("obsolete", 0)->join("house", "house.houseid = cm.modulehouse")->get("contentmodules cm");
		if ($modules->num_rows() > 0){
			$modules = $modules->result();
			foreach ($modules as $mod){
				$mod->grouping = $this->db->select("g.groupid as groupid, groupname as groupname")->where("moduleid", $mod->moduleid)->join("grouping g", "g.groupid = mg.groupid")->get("modulegroup mg")->result();

				$modulepages = $this->db->select("pageid")->order_by("pagenumber", "asc")->where("moduleid", $mod->moduleid)->get("modulepages")->result();
				$mod->pages = array();
				foreach ($modulepages as $p){
					$mod->pages[] = $p->pageid;
				}
				
				$pagestring = "";
				$delim = "";
				foreach ($mod->pages as $pnum){
				    $pagestring .= $delim."'".$pnum."'";
				    $delim = ",";
				}
				$mod->pages = $pagestring;
			}
			return $modules;
		} else{
			return array();
		}
	}

	function getAllFilteredModules($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$modules = $this->db->select("cm.moduleid, cm.moduletitle as modulename, houseid, housename, cm.moduledesc as moduledescription")->where("obsolete", 0)->join("house", "house.houseid = cm.modulehouse")->get("contentmodules cm");
		if ($modules->num_rows() > 0){
			$modules = $modules->result();
			foreach ($modules as $mod){
				$toBeHidden = $this->db->where("reptypeid", $reptype)->where("moduleid", $mod->moduleid)->from("modulereptype")->get();
				$mod->hidden = 1;
				if ($toBeHidden->num_rows()){
					$mod->hidden = 0;
				}

				$mod->grouping = $this->db->select("g.groupid as groupid, groupname as groupname")->where("moduleid", $mod->moduleid)->join("grouping g", "g.groupid = mg.groupid")->get("modulegroup mg")->result();

				$modulepages = $this->db->select("mp.pageid, mp.bookmark")->order_by("pagenumber", "asc")->where("moduleid", $mod->moduleid)->join("contentpages cp", "cp.pageid = mp.pageid")->where("cp.obsolete", 0)->get("modulepages mp")->result();
				$mod->pages = array();
				$mod->bookmarks = array();
				foreach ($modulepages as $p){
					if ($p->bookmark == 1){
						// echo $p->pageid;
						$mod->bookmarks[] = $p->pageid;
					}
					$mod->pages[] = $p->pageid;
				}
				
				$pagestring = "";
				$delim = "";
				foreach ($mod->pages as $pnum){
				    $pagestring .= $delim."'".$pnum."'";
				    $delim = ",";
				}
				$mod->pages = $pagestring;
				$bmarkstring = "";
				$delim = "";
				foreach ($mod->bookmarks as $pnum){
				    $bmarkstring .= $delim."'".$pnum."'";
				    $delim = ",";
				}
				$mod->bookmarks = $bmarkstring;
			}
			return $modules;
		} else{
			return array();
		}
	}

	function getAllCycles(){
		$cycles = $this->db->select("c.cycleid, cycletitle, periodstart, periodend")->get("cycle c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			foreach ($cycles as $cycle){
				$cycle->visittype = $this->db->select("visittype")->where("cycleid", $cycle->cycleid)->get("cyclefiltervisittype")->row()->visittype;
				$modulecycle = $this->db->select("GROUP_CONCAT(moduleid separator ',') as moduleid, visitnumber")->where("cycleid", $cycle->cycleid)->order_by("visitnumber", "asc")->group_by("visitnumber")->get("cyclemodules")->result();
				$filteraccsegment = $this->db->select("hospitalsegment")->where("cycleid", $cycle->cycleid)->get('cyclefilteraccsegment')->result();
				$filterspecialty = $this->db->select("specialtyid")->where("cycleid", $cycle->cycleid)->get('cyclefilterspecialty')->result();
				$filterhcpsegment = $this->db->select("segmentid")->where("cycleid", $cycle->cycleid)->get('cyclefiltersegment')->result();
				$filterroom = $this->db->select("roomid")->where("cycleid", $cycle->cycleid)->get('cyclefilterroom')->result();
				
				$cycle->filterspecialty = "";
				$cycle->filterhcpsegment = "";
				$cycle->filteraccsegment = "";
				$cycle->filterroom = "";
				$cycle->modulecycle = "";
				$delim = "";
				foreach ($modulecycle as $mc){
				    $cycle->modulecycle .= $delim . $mc->visitnumber. ":". $mc->moduleid;
				    $delim = ";";
				}
				$delim = "";
				foreach ($filteraccsegment as $p){
					$cycle->filteraccsegment .= $delim . $p->hospitalsegment;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterspecialty as $p){
					$cycle->filterspecialty .= $delim . $p->specialtyid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterhcpsegment as $p){
					$cycle->filterhcpsegment .= $delim . $p->segmentid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterroom as $p){
					$cycle->filterroom .= $delim . $p->roomid;
					$delim = ",";
				}
			}
			return $cycles;
		} else {
			return array();
		}
	}

	function getAllFilteredCycles($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$cycles = $this->db->select("c.cycleid, cycletitle, periodstart, periodend")->where("c.cycleid in", "(SELECT DISTINCT(crt.cycleid) from cyclefilterreptype crt where reptypeid = $reptype)", false)->where("obsolete", 0)->get("cycle c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			foreach ($cycles as $cycle){
				$cycle->visittype = $this->db->select("visittype")->where("cycleid", $cycle->cycleid)->get("cyclefiltervisittype")->row()->visittype;
				$modulecycle = $this->db->select("GROUP_CONCAT(moduleid separator ',') as moduleid, visitnumber")->where("cycleid", $cycle->cycleid)->order_by("visitnumber", "asc")->group_by("visitnumber")->get("cyclemodules")->result();
				$filteraccsegment = $this->db->select("hospitalsegment")->where("cycleid", $cycle->cycleid)->get('cyclefilteraccsegment')->result();
				$filterspecialty = $this->db->select("specialtyid")->where("cycleid", $cycle->cycleid)->get('cyclefilterspecialty')->result();
				$filterhcpsegment = $this->db->select("segmentid")->where("cycleid", $cycle->cycleid)->get('cyclefiltersegment')->result();
				$filterroom = $this->db->select("roomid")->where("cycleid", $cycle->cycleid)->get('cyclefilterroom')->result();
				
				$cycle->filterspecialty = "";
				$cycle->filterhcpsegment = "";
				$cycle->filteraccsegment = "";
				$cycle->filterroom = "";
				$cycle->modulecycle = "";
				$delim = "";
				foreach ($modulecycle as $mc){
				    $cycle->modulecycle .= $delim . $mc->visitnumber. ":". $mc->moduleid;
				    $delim = ";";
				}
				$delim = "";
				foreach ($filteraccsegment as $p){
					$cycle->filteraccsegment .= $delim . $p->hospitalsegment;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterspecialty as $p){
					$cycle->filterspecialty .= $delim . $p->specialtyid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterhcpsegment as $p){
					$cycle->filterhcpsegment .= $delim . $p->segmentid;
					$delim = ",";
				}
				$delim = "";
				foreach ($filterroom as $p){
					$cycle->filterroom .= $delim . $p->roomid;
					$delim = ",";
				}
			}
			return $cycles;
		} else {
			return array();
		}
	}

	function getAllFilteredProducts($userid){
		$reptype = $this->db->select("reptype")->where("repid", $userid)->get("representatives")->row()->reptype;
		$cycles = $this->db->select("p.productid, productname, danoneflag as isdanoneproduct, housename")->where("c.typeid", $reptype)->join("product p", "p.productid = c.productid")->join("housecategorization hc", "hc.hcid = p.house", "left")->join("house h", "h.houseid = hc.houseid", "left")->get("reptypeproduct c");
		if ($cycles->num_rows() > 0){
			$cycles = $cycles->result();	
			return $cycles;
		} else {
			return array();
		}
	}

	function recordActualVisit($data){
		return $this->db->insert("actualvisit", $data);
	}

	function markVisited($schedid){
		return $this->db->where("schedid", $schedid)->update("representativeschedule", array("visited" => 1));
	}

	function saveImgBase64($sig, $pho){
		return $this->db->insert("base64test", array("base64" => $sig, "base642" => $pho));
	}

	function recordModuleStatistic($data){
		return $this->db->insert("modulestatistics", $data);
	}

	function recordPageStatistic($data){
		return $this->db->insert("pagestatistics", $data);
	}

	function checkExistingVisitData($schedid){
		$res = $this->db->where("schedid", $schedid)->get("actualvisit");
		if ($res->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}

	function checkExistingClosingShareSchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdatashare");
		if ($res->num_rows() > 0){
			return $res->row()->shareid;
		} else {
			return -1;
		}
	}

	function checkExistingClosingShareAccData($userid, $accid, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		
		$res = $this->db->get("closingdatashare");
		if ($res->num_rows() > 0){
			return $res->row()->shareid;
		} else {
			return -1;
		}
	}

	function updateClosingShareValues($id, $sharearray){
		$vt = $this->db->where("shareid", $id)->delete("closingdatashareproducts");
		if ($vt){
			if (empty($sharearray)){
				return true;
			}
			return $this->db->insert_batch("closingdatashareproducts", $sharearray);
		} else {
			return false;
		}
	}

	function insertClosingShareEntry($data){
		$b = $this->db->insert("closingdatashare", $data);
		return $this->db->insert_id();
	}

	function updateClosingShareEntry($shareid, $data){
		$b = $this->db->where("shareid", $shareid)->update("closingdatashare", $data);
		return $b;
	}

	function checkExistingClosingITRSchedule($userid, $schedid){
		$res = $this->db->where("schedid", $schedid)->where("submittedby", $userid)->get("closingdataitr");
		if ($res->num_rows() > 0){
			return $res->row()->itrid;
		} else {
			return -1;
		}
	}

	function checkExistingClosingITRHCPData($userid, $accid, $hcpid = NULL, $foryear, $formonth){
		$this->db->where("submittedby", $userid)->where("hospitalid", $accid)->where('foryear', $foryear)->where("formonth", $formonth);
		if (!empty($hcpid)){
			$this->db->where("specid", $hcpid);
		}
		$res = $this->db->get("closingdataitr");
		if ($res->num_rows() > 0){
			return $res->row()->itrid;
		} else {
			return -1;
		}
	}

	function updateClosingITRIntentions($id, $itrarray){
		$vt = $this->db->where("itrid", $id)->delete("closingdataitrproducts");
		if ($vt){
			if (empty($itrarray)){
				return true;
			}
			return $this->db->insert_batch("closingdataitrproducts", $itrarray);
		} else {
			return false;
		}
	}

	function insertClosingITREntry($data){
		$b = $this->db->insert("closingdataitr", $data);
		return $this->db->insert_id();
	}

	function updateClosingITREntry($itrid, $data){
		$b = $this->db->where("itrid", $itrid)->update("closingdataitr", $data);
		return $b;
	}

	function getNRName($repid){
		return $this->db->where("repid", $repid)->get("representatives")->row()->repname;
	}

	function fetchManagerMails($repid){
		$emails = $this->db->select("email")->from("cmsuser cu")->join("dmreps dm", "cu.userid = dm.userid")->where("dm.repid", $repid)->get()->result();
		$emarr = array();
		foreach ($emails as $em){
			  $emarr[] = $em->email;
		}
		return $emarr;
	}

	function updatePush($userid, $pushtoken, $pushtype){
		return $this->db->where("repid", $userid)->update("representatives", array("pushid" => $pushtoken, "pushtype" => $pushtype));
	}

	function markDeleted($schedid){
		return $this->db->where("schedid", $schedid)->update("representativeschedule", array("deleted" => 1));
	}

	private function _crypto_rand_secure($min, $max)
	{
		$range = $max - $min;
		if ($range < 1) return $min;

		$log    = ceil(log($range, 2));
		$bytes  = (int) ($log / 8) + 1;
		$bits   = (int) $log + 1;
		$filter = (int) (1 << $bits) - 1; 

		do {
			$rnd = hexdec(bin2hex(mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM)));
			$rnd = $rnd & $filter; 
		} while ($rnd >= $range);

		return $min + $rnd;
	}

	private function _generate_key($length)
	{
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet); 

		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->_crypto_rand_secure(0, $max)];
		}

		return $token;
	}

	private function _generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
?>