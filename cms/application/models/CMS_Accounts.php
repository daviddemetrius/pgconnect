<?php
class CMS_Accounts extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}		

	function loginUser(){
		$username = $this->input->post('username');
		$pwd = $this->input->post('pwd');
		$this->db->where('username', $username);
		$this->db->where('accountstatus', 1);
		$this->db->limit(1);
		$query = $this->db->get('cmsuser');

		if ($query->num_rows() > 0) {
			$row = $query->row();
			$hashed = $row->password;
			if (password_verify($pwd, $hashed)){
				if (1){
					$this->load->library('session');
					$logindata = array(
						'user_id' => $row->userid,
						'username' => $username,
						'name' => $row->fullname,
						'tier' => $row->tier,
						'syncp_logged_in' => TRUE
						);
					$this->session->set_userdata($logindata);
					return "success";
				}
				else {
					return "fail";
				}			
			} else {
				return "fail";
			}			
		} else {
			return "fail";
		}
	}

	function getHouses(){
		return $this->db->get("house")->result();
	}

	function getAccSegments(){
		return $this->db->get("hospitalsegment")->result();
	}

	function loginUserTest(){
		$this->db->where('username', "adminR8MW");
		$this->db->where('accountstatus', 1);
		$this->db->limit(1);
		$query = $this->db->get('cmsuser');

		if ($query->num_rows() > 0) {
			$row = $query->row();
			$hashed = $row->password;
			if (1){
				if (1){
					$this->load->library('session');
					$logindata = array(
						'user_id' => $row->userid,
						'username' => $username,
						'name' => $row->fullname,
						'tier' => $row->tier,
						'syncp_logged_in' => TRUE
						);
					$this->session->set_userdata($logindata);
					$hospitals = array();
					if ($this->session->userdata("tier") == 2.5){
						$addlquery = "select hospitalid from territoryassignment where territoryid in(select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))";
						$hospitals = $this->db->query($addlquery)->result();
					} else if ($this->session->userdata("tier") == 2){
						$addlquery = "select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))";
						$hospitals = $this->db->query($addlquery)->result();
					} else if ($this->session->userdata("tier") == 1){
						$addlquery = "select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id").")";
						$hospitals = $this->db->query($addlquery)->result();
					} else if ($this->session->userdata("tier") == 0.5){
						$addlquery = "select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))";
						$hospitals = $this->db->query($addlquery)->result();
						$query2 = "select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id").")";
						$hospitals2 = $this->db->query($query2)->result();
						$hospitals = array_merge($hospitals, $hospitals2);
					}
					$hspids = "(";
					$delim = "";
					foreach ($hospitals as $ids){
						$hspids .= $delim.$ids->hospitalid;
						$delim = ",";
					}
					$hspids .= ")";
					if ($hspids == "()"){
						$hspids = "(0)";
					}
					$this->session->set_userdata("hospitals", $hspids);
					return "success";
				}
				else {
					return "fail";
				}			
			} else {
				return "fail";
			}			
		} else {
			return "fail";
		}
	}

	function initAdministrator(){
		$username = "admin";
		$password = "p&gC0nn3ct!Adm";
		$hashed = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
		echo $password;
		echo $hashed;
		$data = array(
			'username' => $username,
			'password'=> $hashed,
			'tier' => 999,
			'fullname' => "Administrator",
		);
		if ($this->db->insert('cmsuser', $data)){
			return true;
		} else return false;
	}

	function getAllAdminsPaged($page, $limit = 10){
		$this->db->from("cmsuser ei");
		$this->db->where("tier <", $this->session->userdata("tier"));
		if ($this->session->userdata("tier") == 2){
			$this->db->where("userid in", "(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("userid in", "(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")", false);
			$this->db->or_where("userid in", "(select dmid from rmdm where rmid IN (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchAdmins($query, $page, $limit = 10){
		$this->db->from("cmsuser ei");
		$this->db->where("tier <", $this->session->userdata("tier"));
		if ($this->session->userdata("tier") == 2){
			$this->db->where("userid in", "(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("userid in", "(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")", false);
			$this->db->or_where("userid in", "(select dmid from rmdm where rmid IN (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->like("fullname", $query, "after")->or_like("email", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalAdminPages($limit){
		$this->db->from("cmsuser ei");	
		$this->db->where("tier <", $this->session->userdata("tier"));	
		if ($this->session->userdata("tier") == 2){
			$this->db->where("userid in", "(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("userid in", "(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")", false);
			$this->db->or_where("userid in", "(select dmid from rmdm where rmid IN (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllRepsPaged($page, $limit = 10){
		$this->db->from("representatives ei");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		}
		$this->db->join("reptype", "reptype = typeid", "left");
		$this->db->join("territory", "territory = territoryid", "left");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchReps($query, $page, $limit = 10){
		$this->db->from("representatives ei");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		}
		$this->db->join("reptype", "reptype = typeid", "left");
		$this->db->join("territory", "territory = territoryid", "left");
		$this->db->like("repname", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalRepPages($limit){
		$this->db->from("representatives ei");	
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllRepsUnapprovedPaged($page, $limit = 10){
		$this->db->from("representatives ei");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		}
		$this->db->join("reptype", "reptype = typeid", "left");
		$this->db->join("territory", "territory = territoryid", "left");
		$this->db->where("repid in", "(select distinct(repid) as repid from representativeschedule where approved = 0 and visited = 0)", false);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchRepsUnapproved($query, $page, $limit = 10){
		$this->db->from("representatives ei");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		}
		$this->db->join("reptype", "reptype = typeid", "left");
		$this->db->join("territory", "territory = territoryid", "left");
		$this->db->where("repid in", "(select distinct(repid) as repid from representativeschedule where approved = 0 and visited = 0)", false);
		$this->db->like("fullname", $query, "after")->or_like("email", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalRepUnapprovedPages($limit){
		$this->db->from("representatives ei");	
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("repid in", "(select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("repid in", "(select repid from dmreps where userid = ".$this->session->userdata("user_id").")", false);
		}
		$this->db->where("repid in", "(select distinct(repid) as repid from representativeschedule where approved = 0 and visited = 0)", false);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewAccount($data){
		$b = $this->db->insert("cmsuser", $data);
		$id = $this->db->insert_id();
		
		$href = site_url("cms/accounts/admin/$id");
		
		$this->logAccountActivity("Added New Admin Account on <a href='$href'>ID #$id</a> named ".$data['fullname']);
		
		if ($b){
			return "success";
		} else {
			return "fail";
		}
	}

	function createNewRep($data){
		$b = $this->db->insert("representatives", $data);
		$id = $this->db->insert_id();
		
		$href = site_url("cms/accounts/reps/$id");
		
		$this->logAccountActivity("Added New Representative Account on <a href='$href'>ID #$id</a> named ".$data['repname']);

		if ($b){
			return "success";
		} else {
			return "fail";
		}
	}

	function getRepTypes(){
		return $this->db->get("reptype")->result();
	}

	function getAccountInfo($id){
		$activityLog = $this->db->from("cmsuseractivity")->join("cmsactivitytype", "typeid = activitytype")->where("userid", $id)->order_by("datelogged", "desc")->get()->result();
		$userinfo = $this->db->from("cmsuser")->where("userid", $id)->get()->row();
		$userinfo->activity = $activityLog;
		// $userinfo->reps = $this->db->from("dmreps dr")->where("userid", $id)->join("representatives rp", "rp.repid = dr.repid")->join("reptype", "reptype = typeid", "left")->join("territory", "territory = territoryid", "left")->join("district d", "district = d.districtid", "left")->join("city c", "c.cityid = d.cityid", "left")->join("province p", "p.provinceid = c.provinceid", "left")->get()->result();
		// $userinfo->dms = $this->db->from("rmdm")->where("rmid", $id)->join("cmsuser", "dmid = userid")->get()->result();
		return $userinfo;
	}

	function getRepInfo($id){
		$repinfo = $this->db->from("representatives rp")->where("repid", $id)->get()->row();
		$ltm = date("n", strtotime("-3 months"));
		// echo $ltm;
		return $repinfo;
	}

	function updateAccount($data, $id){
		$href = site_url("cms/accounts/admin/$id");
		$this->logAccountActivity("Updated Account for <a href='$href'>ID #$id</a>");
		if(empty($_FILES['userimage']['name'])){
		} else {
			$config['upload_path']          = './images/uploads/agents/';
	                $config['allowed_types']        = 'gif|jpg|png'; 
	                $config['file_name']            = $id.'.jpg'; 
	                $config['overwrite']            = true; 
			$this->load->library('upload', $config);
	                $this->upload->do_upload('userimage');
		}
		return $this->db->where("userid", $id)->update("cmsuser", $data);
	}

	function updateRep($data, $id){
		$href = site_url("cms/accounts/reps/$id");
		$this->logAccountActivity("Updated Rep Info for <a href='$href'>ID #$id</a>");
		
		return $this->db->where("repid", $id)->update("representatives", $data);
	}

	function logAccountActivity($log){
		$data = array(
			"userid" => $this->session->userdata("user_id"),
			"log" => $log,
			"datelogged" => date("Y-m-d H:i:s"),
			"activitytype" => 1
		);
		return $this->db->insert("cmsuseractivity", $data);
	}

	function deactivateAccount($id){
		$data = array(
			"accountstatus" => 0
		);
		return $this->db->where("userid", $id)->update("cmsuser", $data);
	}

	function reactivateAccount($id){
		$data = array(
			"accountstatus" => 1
		);
		return $this->db->where("userid", $id)->update("cmsuser", $data);
	}

	function deactivateRep($id){
		$r = $this->db->where("createdby", $this->session->userdata("user_id"))->where("repid", $id)->get('representatives');
		if (!$r->num_rows()){
			return "You are not allowed to do this operation";
		}
		
		$data = array(
			"obsolete" => 1
		);
		$this->db->where("repid", $id)->update("representatives", $data);
		return $r->row()->associatedaccount;
	}

	function reactivateRep($id){
		$r = $this->db->where("createdby", $this->session->userdata("user_id"))->where("repid", $id)->get('representatives');
		if (!$r->num_rows()){
			return "You are not allowed to do this operation";
		}

		$data = array(
			"obsolete" => 0
		);
		$this->db->where("repid", $id)->update("representatives", $data);
		return $r->row()->associatedaccount;
	}

	public function getTier($id){
		return $this->db->where("userid", $id)->get("cmsuser")->row()->tier;
	}

	function migrateActivity($oldUrl, $newUrl){
		$data = $this->db->get("cmsuseractivity")->result();
		$this->db->delete("cmsuseractivity");
		foreach ($data as $log){
			$log->description = str_replace($oldUrl, $newUrl, $log->description);
			$arr = array(
				"userid" => $log->employeeid,
				"log" => $log->description,
				"datelogged" => $log->datelogged,
				"activitytype" => $log->activitytype
			);
			$this->db->insert("cmsuseractivity", $arr);
		}
	}

	function migrateLogs($oldUrl, $newUrl){
		$data = $this->db->get("contentpages")->result();
		foreach ($data as $log){
			var_dump(json_decode($log->folderlisting));
			// break;
			$log->folderlisting = str_replace($oldUrl, $newUrl, $log->folderlisting);
			$arr = array(
				"folderlisting" => $log->folderlisting
			);
			// $this->db->where("pageid", $log->pageid)->update("contentpages", $arr);
			// break;
		}
	}

	function approveSchedules($idArray){
		$this->db->where_in("schedid", $idArray);
		$b = $this->db->update("representativeschedule", array("approved" => 1, "approvedby" => $this->session->userdata("user_id"), "approveddate" => date("Y-m-d H:i:s")));
		if ($b){
			return "success";
		} else {
			return "An error has occured. Please contact an administrator";
		}
	}

	function deleteSchedules($idArray){
		$this->db->where_in("schedid", $idArray);
		$b = $this->db->update("representativeschedule", array("deleted" => 1, "deletedby" => $this->session->userdata("user_id"), "deleteddate" => date("Y-m-d H:i:s")));
		if ($b){
			return "success";
		} else {
			return "An error has occured. Please contact an administrator";
		}
	}

	function getSessionData($sessionid){
		$visit = new stdClass();
		$v = $this->db->where("sessionid", $sessionid)->get("representativesession");
		if ($v->num_rows()){
			$visit = $v->row();
			$visit->moduleStatistics = $this->db->where("schedid", $sessionid)->join("contentmodules cm", "cm.moduleid = ms.moduleid")->get("modulestatistics ms")->result();
			$visit->pageStatistics = $this->db->where("schedid", $sessionid)->join("contentpages cp", "cp.pageid = ps.pageid")->get("pagestatistics ps")->result();
			return $visit;
		} else {
			return false;
		}
	}

	function getSurveyData($sessionid){
		$visit = new stdClass();
		$v = $this->db->where("sessionid", $sessionid)->get("surveysession");
		if ($v->num_rows()){
			$visit = $v->row();
			$visit->answers = $this->db->where("sessionid", $sessionid)->get("surveysessionanswers ms")->result();
			return $visit;
		} else {
			return false;
		}		
	}

	function getVisitData($scheduleid){
		$visit = new stdClass();
		$v = $this->db->where("schedid", $scheduleid)->get("actualvisit");

		if ($v->num_rows()){
			$visit = $v->row();
			$visit->moduleStatistics = $this->db->where("schedid", $scheduleid)->join("contentmodules cm", "cm.moduleid = ms.moduleid")->get("modulestatistics ms")->result();
			$visit->pageStatistics = $this->db->where("schedid", $scheduleid)->join("contentpages cp", "cp.pageid = ps.pageid")->get("pagestatistics ps")->result();
			$visit->originalData = $this->db->where('schedid', $scheduleid)->get("representativeschedule")->row();
			$visit->closingBirth = $this->db->where("schedid", $scheduleid)->get("closingdatabirth")->result();
			$visit->closingDelivery = $this->db->where("schedid", $scheduleid)->get("closingdatadelivery")->result();
			foreach ($visit->closingDelivery as $dl){
				$dl->values = $this->db->where("dataid", $dl->dataid)->join("closingdeliverycriteria cdc", "cdc.criteriaid = cdv.criteriaid")->get("closingdatadeliveryvalues cdv")->result();
			}
			$visit->closingSurvey = $this->db->where("schedid", $scheduleid)->get("closingdatasurvey")->result();
			foreach ($visit->closingSurvey as $dl){
				$dl->values = $this->db->where("dataid", $dl->dataid)->join("closingsurvey cdc", "cdc.surveyid = cdv.surveyid")->get("closingdatasurveyanswers cdv")->result();
			}
			$visit->closingItr = $this->db->where("schedid", $scheduleid)->get("closingdataitr")->result();
			foreach ($visit->closingItr as $dl){
				$dl->values = $this->db->where("itrid", $dl->itrid)->join("product cdc", "cdc.productid = cdv.productid")->get("closingdataitrproducts cdv")->result();
			}
			$visit->closingShare = $this->db->where("schedid", $scheduleid)->get("closingdatashare")->result();
			foreach ($visit->closingShare as $dl){
				$dl->values = $this->db->where("shareid", $dl->shareid)->join("product cdc", "cdc.productid = cdv.productid")->get("closingdatashareproducts cdv")->result();
			}
			$visit->closingPotency = $this->db->where("schedid", $scheduleid)->get("closingdatapotency")->result();
			foreach ($visit->closingPotency as $dl){
				$dl->values = $this->db->where("dataid", $dl->dataid)->join("closingpotencycriteria cdc", "cdc.criteriaid = cdv.criteriaid")->get("closingdatapotencyvalues cdv")->result();
			}
			return $visit;
		} else {
			return false;
		}
	}

	function repAssignSpec($data){
		$res = $this->db->get_where("representativedesignation", $data);
		if ($res->num_rows() > 0){
			return "Rep ini sudah diassign ke HCP di rumah sakit yang sama.";
		} else {
			$b = $this->db->insert("representativedesignation", $data);
			if ($b){
				return "success";
			} else {
				return "An error has occured. Please contact an administrator.";
			}
		}	
	}

	function deleteAssignment($repid, $specid, $hospitalid){
		$b = $this->db->where("repid", $repid)->where("specid", $specid)->where("hospitalid", $hospitalid)->delete("representativedesignation");
		if ($b){
			return "success";
		} else {
			return "An error has occured. Please contact an administrator.";
		}
	}

	function getRepsForSearch($dmid){
		return array();
	}

	function getDMForSearch($dmid){
		return array();
	}

	function getRMForSearch($dmid){
		return array();
	}

	function addRepToDM($repid, $dmid){
		$d = $this->db->insert("dmreps", array("userid" => $dmid, "repid" => $repid));
		if ($d){
			return "success";
		} else {
			return "Terjadi kesalahan saat penginputan data. Tolong cek kembali apakah Rep sudah diassign ke DM ini atau kontak administrator";
		}
	}

	function addDMToRM($dmid, $rmid){
		$d = $this->db->insert("rmdm", array("dmid" => $dmid, "rmid" => $rmid));
		if ($d){
			return "success";
		} else {
			return "Terjadi kesalahan saat penginputan data. Tolong cek kembali apakah DM sudah diassign ke RM ini atau kontak administrator";
		}
	}

	function deleteRepFromDM($repid, $dmid){
		$d = $this->db->where("userid", $dmid)->where("repid", $repid)->delete("dmreps");
		if ($d){
			return "success";
		} else {
			return "Terjadi kesalahan saat penghapusan data. Mohon kontak administrator";
		}
	}

	function deleteDMFromRM($dmid, $rmid){
		$d = $this->db->where("dmid", $dmid)->where("rmid", $rmid)->delete("rmdm");
		if ($d){
			return "success";
		} else {
			return "Terjadi kesalahan saat penghapusan data. Mohon kontak administrator";
		}
	}

	function getAllRepTypesPaged($page, $limit = 10){
		$this->db->select("typename, typeid as id");
		$this->db->from("reptype ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchRepTypes($query, $page, $limit = 10){
		$this->db->select("typename, typeid as id");
		$this->db->from("reptype ei");
		$this->db->like("typename", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalRepTypePages($limit){
		$this->db->from("reptype ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewRepType($data, $houses, $accsegs, $modules){
		$b = $this->db->insert("reptype", $data);
		if ($b){
			$reptypeid = $this->db->insert_id();
			$this->updateRepTypeHouseFilter($reptypeid, $houses);
			$this->updateRepTypeSegmentFilter($reptypeid, $accsegs);
			$this->updateRepTypeModuleFilter($reptypeid, $modules);
		}
		return $b;
	}

	function updateRepTypeHouseFilter($id, $houses){
		$vt = $this->db->where("typeid", $id)->delete("reptypehouse");
		if ($vt){
			if (empty($houses)){
				return true;
			}
			$housecatarr = array();
			foreach ($houses as $category){
				$housecatarr[] = array("typeid" => $id, "houseid" => $category);
			}
			return $this->db->insert_batch("reptypehouse", $housecatarr);
		} else {
			return false;
		}
	}

	function updateRepTypeSegmentFilter($id, $accsegs){
		$vt = $this->db->where("typeid", $id)->delete("reptypeaccseg");
		if ($vt){
			if (empty($accsegs)){
				return true;
			}
			$productarr = array();
			foreach ($accsegs as $product){
				$productarr[] = array("typeid" => $id, "segmentid" => $product);
			}
			return $this->db->insert_batch("reptypeaccseg", $productarr);
		} else {
			return false;
		}
	}

	function updateRepTypeModuleFilter($id, $modules){
		$vt = $this->db->where("reptypeid", $id)->delete("modulereptype");
		if ($vt){
			if (empty($modules)){
				return true;
			}
			$productarr = array();
			foreach ($modules as $product){
				$productarr[] = array("reptypeid" => $id, "moduleid" => $product);
			}
			return $this->db->insert_batch("modulereptype", $productarr);
		} else {
			return false;
		}
	}

	function getRepTypeDetail($id){
		$b = $this->db->where("typeid", $id)->get("reptype")->row();
		$b->houses = $this->db->where("typeid", $id)->join("house hc", "hc.houseid = reptypehouse.houseid")->get('reptypehouse')->result();
		$b->accsegs = $this->db->where("typeid", $id)->join("hospitalsegment hc", "hc.segmentid = reptypeaccseg.segmentid")->get('reptypeaccseg')->result();
		return $b;
	}

	function updateRepType($data, $id, $houses, $accsegs, $modules){
		$b = $this->db->where("typeid",  $id)->update("reptype", $data);
		if ($b){
			$this->updateRepTypeHouseFilter($id, $houses);
			$this->updateRepTypeSegmentFilter($id, $accsegs);
			$this->updateRepTypeModuleFilter($reptypeid, $modules);
		}
		return $b;
	}

	function sendMessage($msg, $title){
		$this->db->query("INSERT into representativeinbox (repid, message, title) select repid, ".$this->db->escape($msg).", ".$this->db->escape($title)." from representatives");
		$this->db->insert("messagehistory", array("message" => $msg, "title" => $title));
		return true;
		// sendquiz
		// sendpush
	}

	function getMessageHistory(){
		return $this->db->limit(30)->order_by("datesent", "desc")->get("messagehistory")->result();
	}

	function getCities(){
		return $this->db->join("province p", "p.provinceid = c.provinceid")->get("city c")->result();
	}
	function getAvailablePushIDs(){
		$res = $this->db->where("pushid IS NOT NULL", null, false)->where("obsolete", 0)->get("representatives")->result();
		$pushid = array();
		foreach ($res as $row){
			$pushid[] = $row->pushid;
		}
		return $pushid;
	}
	function getAvailableModules(){
		return $this->db->where("obsolete", 0)->where("periodend >", date("Y-m-d H:i:s"))->get("contentmodules")->result();
	}
}

?>