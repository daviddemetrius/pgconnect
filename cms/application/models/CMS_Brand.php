<?php
class CMS_Brand extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function getAllHousesPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("housename, houseid");
		$this->db->from("house ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchHouses($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("housename, houseid");
		$this->db->from("house ei");
		$this->db->like("housename", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalHousePages($limit){
		$this->db->from("house ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchHousePages($limit){
		$this->db->from("house ei");		
		$this->db->like("housename", $query);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getCats(){
		return $this->db->get("housecategory")->result();
	}

	function createNewCats($data){
		$b = $this->db->insert("housecategory", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getCatsDetails($id){
		$result = $this->db->where("categoryid", $id)->get("housecategory")->row();
		return $result;
	}

	function editCats($data, $id){
		return $this->db->where("categoryid", $id)->update("housecategory", $data);
	}

	function getAllHouseCats(){
		$this->db->select("categoryid as id, '' as value, categoryname as label");
		$this->db->select("categoryname, categoryid");
		$this->db->from("housecategory ei");
		$this->db->order_by("categoryname", "asc");
		$res = $this->db->get()->result();
		$arr = array();
		foreach ($res as $row){
			$arr[$row->id] = $row->label;
		}

		return $arr;
	}

	function getAllHouseCatsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("categoryname, categoryid");
		$this->db->from("housecategory ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchHouseCats($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("categoryname, categoryid");
		$this->db->from("housecategory ei");
		$this->db->like("categoryname", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchHouseCatPages($limit, $query){
		$this->db->from("housecategory ei");	
		$this->db->like("categoryname", $query);	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalHouseCatPages($limit){
		$this->db->from("housecategory ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getIssue(){
		return $this->db->get("healthissues")->result();
	}

	function createNewIssue($data){
		$b = $this->db->insert("healthissues", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getIssueDetails($id){
		$result = $this->db->where("issueid", $id)->get("healthissues")->row();
		$result->products = $this->db->where("issueid", $id)->join("product p", "p.productid = pi.productid")->join("housecategorization h", "h.hcid = p.house", "left")->join("house ho", "ho.houseid = h.houseid", "left")->join("housecategory hc", "hc.categoryid = h.categoryid", "left")->get("productissue pi")->result();
		return $result;
	}

	function editIssue($data, $id){
		return $this->db->where("issueid", $id)->update("healthissues", $data);
	}

	function getAllIssues(){
		return $this->db->order_by("issuename", "asc")->get("healthissues")->result();
	}

	function getAllDDIssues(){
		$res = $this->db->select("issueid as id, issuename as value, issuename as label")->get("healthissues")->result();
		$arr = array();
		foreach ($res as $row){
			$arr[$row->id] = $row->value;
		}
		return $arr;
	}

	function getAllIssuesPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("issuename, issueid");
		$this->db->from("healthissues ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchIssues($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("issuename, issueid");
		$this->db->from("healthissues ei");
		$this->db->like("issuename", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchIssuePages($limit, $query){
		$this->db->from("healthissues ei");	
		$this->db->like("issuename", $query);	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalIssuePages($limit){
		$this->db->from("healthissues ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}


	function getProducts(){
		return $this->db->get("product")->result();
	}

	function createNewProducts($data){
		$b = $this->db->insert("product", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getProductsDetails($id){
		$result = $this->db->select("p.*, hcz.hcid, h.houseid, h.housename, hc.categoryid, hc.categoryname")->where("productid", $id)->join("housecategorization hcz", "hcz.hcid = p.house", "left")->join("house h", "h.houseid = hcz.houseid", "left")->join("housecategory hc", "hc.categoryid = hcz.categoryid", "left")->get("product p")->row();
		$result->issues = $this->db->where("productid", $result->productid)->join("healthissues hi", "hi.issueid = pi.issueid")->get('productissue pi')->result();
		return $result;
	}

	function editProducts($data, $id){
		return $this->db->where("productid", $id)->update("product", $data);
	}

	function getAllProductsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("productname, IF(danoneflag = 1, '<span class=\"greent\">Danone Product</span>', '<span class=\"oranget\">Competitor Product</span>') as producttype, CONCAT(housename ,' - ', categoryname), productid, obsolete");
		$this->db->from("product ei");
		$this->db->join("housecategorization h", "h.hcid = ei.house", "left")->join("house ho", "ho.houseid = h.houseid", "left")->join("housecategory hc", "hc.categoryid = h.categoryid", "left");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchProducts($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("productname, IF(danoneflag = 1, '<span class=\"greent\">Danone Product</span>', '<span class=\"oranget\">Competitor Product</span>') as producttype, CONCAT(housename ,' - ', categoryname) as housenaming, productid, obsolete");
		$this->db->from("product ei");
		$this->db->join("housecategorization h", "h.hcid = ei.house", "left")->join("house ho", "ho.houseid = h.houseid", "left")->join("housecategory hc", "hc.categoryid = h.categoryid", "left");
		$this->db->like("productname", $query)->or_like("housename", $query)->or_like("categoryname", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalProductPages($limit){
		$this->db->from("product ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchProductPages($limit, $query){
		$this->db->from("product ei");		
		$this->db->join("housecategorization h", "h.hcid = ei.house", "left")->join("house ho", "ho.houseid = h.houseid", "left")->join("housecategory hc", "hc.categoryid = h.categoryid", "left");
		$this->db->like("productname", $query);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewHouse($data){
		$b = $this->db->insert("house", $data);
		if ($b){
			$id = $this->db->insert_id();
			if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
			else {
				if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

				} else {
					$config['upload_path']          = './assets/houses/';
			        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
			        $config['file_name']            = $id.'.png'; 
			        $config['overwrite']            = true; 
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
			        $c = $this->upload->do_upload('userfile');
			        if (!$c){
			            $error = array('error' => $this->upload->display_errors());
			        }
				}
			}
			return "success";
		} else {
			return false;
		}
	}

	function getHouseDetails($id){
		$result = $this->db->where("houseid", $id)->get("house")->row();
		// $result->categories = $this->db->where("houseid", $id)->join("housecategory hoc", "hoc.categoryid = hc.categoryid")->get("housecategorization hc")->result();
		return $result;
	}
	function getHouseCategories(){
		return $this->db->get("housecategory")->result();
	}

	function editHouse($data, $id){
		if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/houses/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('userfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}
		return $this->db->where("houseid", $id)->update("house", $data);
	}

	function addHouseCategorization($houseid, $catid){
		return $this->db->insert("housecategorization", array("categoryid" => $catid, "houseid" =>$houseid ));
	}

	function addProductIssue($productid, $issueid){
		return $this->db->insert("productissue", array("issueid" => $issueid, "productid" => $productid));
	}

	function deleteHouseCategorization($hcid){
		return $this->db->where("hcid", $hcid)->delete("housecategorization");
	}

	function deleteProductIssue($issueid, $productid){
		return $this->db->where("issueid", $issueid)->where("productid", $productid)->delete("productissue");
	}

	function getAllDDHouseCategories(){
		return $this->db->select("hcid as id, CONCAT(housename, ' - ', categoryname) as value")->join("house h", "h.houseid = hc.houseid")->join("housecategory hoc", "hoc.categoryid = hc.categoryid")->get("housecategorization hc")->result();
	}

	function deactivateProduct($id){
		$data = array(
			"obsolete" => 1
		);
		return $this->db->where("productid", $id)->update("product", $data);
	}

	function reactivateProduct($id){
		$data = array(
			"obsolete" => 0
		);
		return $this->db->where("productid", $id)->update("product", $data);
	}
}
?>