<?php
class CMS_Closing extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function getAllSurveysPaged($page, $limit = 10){
		$this->db->from("closingsurvey ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchSurveys($query, $page, $limit = 10){
		$this->db->from("closingsurvey ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->like("surveytext", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSurveyPages($limit){
		$this->db->from("closingsurvey ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewSurvey($data, $reptypeid){
		$b = $this->db->insert("closingsurvey", $data);
		if ($b){
			$surveyid = $this->db->insert_id();
			$this->updateSurveyRepTypes($surveyid, $reptypeid);
		}
		return $b;
	}

	function updateSurveyRepTypes($id, $reptypeArray){
		$vt = $this->db->where("surveyid", $id)->delete("closingsurveyreptype");
		if ($vt){
			if (empty($reptypeArray)){
				return true;
			}
			$reparr = array();
			foreach ($reptypeArray as $reptype){
				$reparr[] = array("surveyid" => $id, "typeid" => $reptype);
			}
			return $this->db->insert_batch("closingsurveyreptype", $reparr);
		} else {
			return false;
		}
	}

	function getSurveyDetails($id){
		$b = $this->db->where("surveyid", $id)->get("closingsurvey")->row();
		$b->reptypes = $this->db->where("surveyid", $id)->join("reptype", "reptype.typeid = closingsurveyreptype.typeid")->get("closingsurveyreptype")->result();
		return $b;
	}

	function editSurvey($id, $data, $reptypes){
		$b = $this->db->where("surveyid",  $id)->update("closingsurvey", $data);
		if ($b){
			$this->updateSurveyRepTypes($id, $reptypes);
		}
		return $b;
	}

	function getAllPotenciesPaged($page, $limit = 10){
		$this->db->from("closingpotencycriteria ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchPotency($query, $page, $limit = 10){
		$this->db->from("closingpotencycriteria ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->like("criteriatitle", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalPotencyPages($limit){
		$this->db->from("closingpotencycriteria ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllDeliveriesPaged($page, $limit = 10){
		$this->db->from("closingdeliverycriteria ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchDelivery($query, $page, $limit = 10){
		$this->db->from("closingdeliverycriteria ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->like("criteriatitle", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalDeliveryPages($limit){
		$this->db->from("closingdeliverycriteria ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function addDeliveryCriteria($criteria){
		$res = $this->db->where("criteriatitle", $criteria)->get("closingdeliverycriteria");
		if ($res->num_rows()){
			return "Criteria with the same name already exists.";
		} else {
			$s = $this->db->insert("closingdeliverycriteria", array("criteriatitle" => $criteria));
			if ($s){
				return "success";
			} else {
				return "Error occured during criteria addition. Please try again later.";
			}
		}
	}

	function editDeliveryCriteria($criteria, $obs, $id){
		$res = $this->db->where("criteriatitle", $criteria)->where("criteriaid !=", $id)->get("closingdeliverycriteria");
		if ($res->num_rows()){
			return "Criteria with the same name already exists.";
		} else {
			$s = $this->db->where('criteriaid', $id)->update("closingdeliverycriteria", array("criteriatitle" => $criteria, "obsolete" => $obs));
			if ($s){
				return "";
			} else {
				return "Error occured during criteria editing. Please try again later.";
			}
		}
	}

	function addPotencyCriteria($criteria){
		$res = $this->db->where("criteriatitle", $criteria)->get("closingpotencycriteria");
		if ($res->num_rows()){
			return "Criteria with the same name already exists.";
		} else {
			$s = $this->db->insert("closingpotencycriteria", array("criteriatitle" => $criteria));
			if ($s){
				return "success";
			} else {
				return "Error occured during criteria addition. Please try again later.";
			}
		}
	}

	function editPotencyCriteria($criteria, $obs, $id){
		$res = $this->db->where("criteriatitle", $criteria)->where("criteriaid !=", $id)->get("closingpotencycriteria");
		if ($res->num_rows()){
			return "Criteria with the same name already exists.";
		} else {
			$s = $this->db->where('criteriaid', $id)->update("closingpotencycriteria", array("criteriatitle" => $criteria, "obsolete" => $obs));
			if ($s){
				return "";
			} else {
				return "Error occured during criteria editing. Please try again later.";
			}
		}
	}

	function getRepTypeVisibility(){
		return $this->db->join("closingdeliveryreptypetoggling rt", "rt.reptypeid = r.typeid", "left")->order_by("typeid", "asc")->get("reptype r")->result();
	}

	function editVisibility($reptypeid, $visibility){
		return $this->db->query("INSERT into closingdeliveryreptypetoggling values($reptypeid, $visibility) on duplicate key update visibility = $visibility");
	}

	function getAllCyclesPaged($page, $limit = 10){
		$this->db->from("closingsurveycycle ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchCycles($query, $page, $limit = 10){
		$this->db->from("closingsurveycycle ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		$this->db->like("cycletitle", $query, "after")->or_like("periodstart", $query, "after")->or_like("periodend", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalCyclePages($limit){
		$this->db->from("closingsurveycycle ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewCycle($data, $reptypes, $accsegments, $hcpsegments, $specs, $rooms, $cyclevisittype){
		$b = $this->db->insert("closingsurveycycle", $data);
		if ($b){
			$cycleid = $this->db->insert_id();
			$this->updateCycleRepTypes($cycleid, $reptypes);
			$this->updateCycleVisitType($cycleid, $cyclevisittype);
			return "success";
		} else {
			return "error";
		}
	}

	function editCycle($cycleid ,$data, $reptypes, $cyclevisittype, $accsegments, $hcpsegments, $specs, $rooms){
		$b = $this->db->where("cycleid", $cycleid)->update("closingsurveycycle", $data);
		if ($b){
			$this->updateCycleRepTypes($cycleid, $reptypes);
			$this->updateCycleVisitType($cycleid, $cyclevisittype);
			return "success";
		} else {
			return "error";
		}
	}

	function updateCycleRepTypes($id, $reptypeArray){
		$vt = $this->db->where("cycleid", $id)->delete("closingsurveycyclereptype");
		if ($vt){
			if (empty($reptypeArray)){
				return true;
			}
			$reparr = array();
			foreach ($reptypeArray as $reptype){
				$reparr[] = array("cycleid" => $id, "reptypeid" => $reptype);
			}
			return $this->db->insert_batch("closingsurveycyclereptype", $reparr);
		} else {
			return false;
		}
	}

	function updateCycleVisitType($id, $visittype){
		$vt = $this->db->where("cycleid", $id)->delete("closingsurveycyclevisittype");
		if ($vt){
			return $this->db->insert("closingsurveycyclevisittype", array("cycleid" => $id, "visittype" => $visittype));
		} else {
			return false;
		}
	}

	function updateCycleACCSegments($id, $segmentArray){
		$vt = $this->db->where("cycleid", $id)->delete("closingsurveycycleaccsegments");
		if ($vt){
			if (empty($segmentArray)){
				return true;
			}
			$segarr = array();
			foreach ($segmentArray as $segmentid){
				$segarr[] = array("cycleid" => $id, "hospitalsegment" => $segmentid);
			}
			return $this->db->insert_batch("closingsurveycycleaccsegments", $segarr);
		} else {
			return false;
		}
	}

	function updateCycleRooms($id, $roomArray){
		$vt = $this->db->where("cycleid", $id)->delete("closingsurveycycleroom");
		if ($vt){
			if (empty($roomArray)){
				return true;
			}
			$roomarr = array();
			foreach ($roomArray as $roomid){
				$roomarr[] = array("cycleid" => $id, "roomid" => $roomid);
			}
			return $this->db->insert_batch("closingsurveycycleroom", $roomarr);
		} else {
			return false;
		}
	}

	function updateCycleHCPSegments($id, $segmentArray){
		$vt = $this->db->where("cycleid", $id)->delete("closingsurveycyclesegment");
		if ($vt){
			if (empty($segmentArray)){
				return true;
			}
			$segarr = array();
			foreach ($segmentArray as $segmentid){
				$segarr[] = array("cycleid" => $id, "segmentid" => $segmentid);
			}
			return $this->db->insert_batch("closingsurveycyclesegment", $segarr);
		} else {
			return false;
		}

	}

	function updateCycleSpecialty($id, $specialtyArray){
		$vt = $this->db->where("cycleid", $id)->delete("closingsurveycyclespecialty");
		if ($vt){
			if (empty($specialtyArray)){
				return true;
				$vt = $this->db->where("cycleid", $id)->get('cyclefiltervisittype');
		if ($vt->num_rows() > 0){
			$cycle->visittype = $vt->row()->visittype;
		} else {
			$cycle->visittype = "Both";
		}
			}
			$specarr = array();
			foreach ($specialtyArray as $specid){
				$cycle->accsegments = $this->db->where("cycleid", $id)->join('hospitalsegment ac', 'f.hospitalsegment = ac.segmentid')->get('cyclefilteraccsegment f')->result();
		$cycle->hcpsegments = $this->db->where("cycleid", $id)->join('segmentation ac', 'f.segmentid = ac.segmentid')->get('cyclefiltersegment f')->result();
		$cycle->reptypes = $this->db->where("cycleid", $id)->join('reptype', 'f.reptypeid = typeid')->get('cyclefilterreptype f')->result();
		$cycle->rooms = $this->db->where("cycleid", $id)->join('room', 'f.roomid = room.roomid')->get('cyclefilterroom f')->result();
		$cycle->specialties = $this->db->where("cycleid", $id)->join('specialty ac', 'f.specialtyid = ac.specialtyid')->get("cyclefilterspecialty f")->result();
				$specarr[] = array("cycleid" => $id, "specialtyid" => $specid);
			}
			return $this->db->insert_batch("closingsurveycyclespecialty", $specarr);
		} else {
			return false;
		}
	}

	function getAllSurveysForSearch(){
		return $this->db->select("surveyid as id, '' as value, surveytext as label")->get("closingsurvey")->result();
	}

	function getCycleDetail($id){
		$cycle = $this->db->where("cycleid", $id)->get("closingsurveycycle")->row();
		$vt = $this->db->where("cycleid", $id)->get('closingsurveycyclevisittype');
		if ($vt->num_rows() > 0){
			$cycle->visittype = $vt->row()->visittype;
		} else {
			$cycle->visittype = "Both";
		}
		$cycle->reptypes = $this->db->where("cycleid", $id)->join('reptype', 'f.reptypeid = typeid')->get('closingsurveycyclereptype f')->result();
		$cycle->questions = $this->db->where("cycleid", $id)->order_by("order", "asc")->join("closingsurvey", "closingsurvey.surveyid = questionid")->get('closingsurveycyclequestions')->result();
		
		$cycle->reptypes = $this->db->where("cycleid", $id)->join('reptype', 'f.reptypeid = typeid')->get('closingsurveycyclereptype f')->result();
		
		return $cycle;
	}

	function eraseQuestions($moduleid){
		return $this->db->where("cycleid", $moduleid)->delete("closingsurveycyclequestions");
	}

	function addModuleQuestion($data){
		return $this->db->insert("closingsurveycyclequestions", $data);
	}

	function getAllDanoneProduct(){
		return $this->db->where("danoneflag", 1)->get("product")->result();
	}

	function getAllProducts(){
		return $this->db->get("product")->result();
	}

	function searchAcc($q){
		$exp = explode(" ", $q);
		$str = "";
		$delim = "";
		foreach ($exp as $e){
			$str .= "$delim (xmlaccid like '%$e%' or hospitalname like '%$e%' or territoryname like '%$e%')";
			$delim = "and ";
		}
		$addlquery = "";
		if ($this->session->userdata("tier") == 2.5){
			$addlquery = "hospitalid in".$this->session->userdata("hospitals")." and";
		} else if ($this->session->userdata("tier") == 2){
			$addlquery = "hospitalid in".$this->session->userdata("hospitals")." and";
		} else if ($this->session->userdata("tier") == 1){
			$addlquery = "hospitalid in".$this->session->userdata("hospitals")." and";
		} else if ($this->session->userdata("tier") == 0.5){
			$addlquery = "hospitalid in".$this->session->userdata("hospitals")." and";
			// $addlquery = "hospitalid in (select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))";
		}
		return $this->db->query("SELECT hospitalid as data, concat(xmlaccid, ' - ', territoryname, ' - ', hospitalname) as value from hospital join territory on territory = territoryid where $addlquery ($str) limit 10")->result();
		// return $this->db->select("hospitalid as data, concat(xmlaccid, ' - ', hospitalname) as value")->group_start()->where("hospitalid >", 45348)->group_start()->like("xmlaccid", $q)->or_like("hospitalname", $q)->group_end()->group_end()->limit(10)->get("hospital")->result();
	}

	function searchHcp($q, $hospitalid = ""){
		$exp = explode(" ", $q);
		$str = "";
		$delim = "";
		foreach ($exp as $e){
			$str .= "$delim (xmlhcpid like '%$e%' or specialistname like '%$e%')";
			$delim = "and ";
		}
		return $this->db->query("SELECT specid as data, concat(xmlhcpid, ' - ', specialistname) as value from specialist where specid in (SELECT specid from hospitalrooms where hospitalid = $hospitalid) and (xmlhcpid like '%$q%'or specialistname like '%$q%')limit 10")->result();
		// return $this->db->select("hospitalid as data, concat(xmlaccid, ' - ', hospitalname) as value")->group_start()->where("hospitalid >", 45348)->group_start()->like("xmlaccid", $q)->or_like("hospitalname", $q)->group_end()->group_end()->limit(10)->get("hospital")->result();
	}

	function getITRDataACCOnly($foryear, $formonth, $accid){
		$this->db->from("closingdataitr");
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("hospitalid", $accid);
		$this->db->where("specid is null", NULL, false);
		$this->db->where("submittedby is null", NULL, false);
		$this->db->where("schedid is null", NULL, false);
		$r = $this->db->get();
		if ($r->num_rows()){
			$this->db->select("product.*, intention");
			$this->db->from("product");
			$this->db->join("closingdataitrproducts", "product.productid = closingdataitrproducts.productid and itrid = ".$r->row()->itrid, "left");
			$this->db->where("danoneflag", 1);
			$this->db->where("obsolete", 0);
			$b = $this->db->get()->result();
			return $b;
		} else {
			return $this->db->select("product.*, 0 as intention")->where("danoneflag", 1)->where("obsolete", 0)->get("product")->result();
		}
	}

	function getShareDataACCOnly($foryear, $formonth, $accid){
		$this->db->from("closingdatashare");
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("hospitalid", $accid);
		$this->db->where("submittedby is null", NULL, false);
		$this->db->where("schedid is null", NULL, false);
		$r = $this->db->get();
		if ($r->num_rows()){
			$this->db->select("product.*, birth as share, peds as category");
			$this->db->from("product");
			$this->db->join("closingdatashareproducts", "product.productid = closingdatashareproducts.productid and shareid = ".$r->row()->shareid, "left");
			$this->db->where("obsolete", 0);
			$b = $this->db->get()->result();
			return $b;
		} else {
			return $this->db->select("product.*, 0 as share, 0 as category")->where("obsolete", 0)->get("product")->result();
		}
	}

	function getITRDataACCHCPOnly($foryear, $formonth, $accid, $hcpid){
		$this->db->from("closingdataitr");
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("hospitalid", $accid);
		$this->db->where("specid", $hcpid);
		$this->db->where("submittedby is null", NULL, false);
		$this->db->where("schedid is null", NULL, false);
		$r = $this->db->get();
		if ($r->num_rows()){
			$this->db->select("product.*, intention");
			$this->db->from("product");
			$this->db->join("closingdataitrproducts", "product.productid = closingdataitrproducts.productid and itrid = ".$r->row()->itrid, "left");
			$this->db->where("danoneflag", 1);
			$this->db->where("obsolete", 0);
			return $this->db->get()->result();
		} else {
			$this->db->where("obsolete", 0);
			return $this->db->select("product.*, 0 as intention")->where("danoneflag", 1)->get("product")->result();
		}

	}

	function saveShareDataACCOnly($foryear, $formonth, $accid, $productlist){
		$this->db->from("closingdatashare");
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("hospitalid", $accid);
		$this->db->where("submittedby is null", NULL, false);
		$this->db->where("schedid is null", NULL, false);
		$r = $this->db->get();
		if ($r->num_rows()){
			//update
			$shareid = $r->row()->shareid;
		} else {
			$this->db->insert("closingdatashare", array("foryear"=>$foryear, "formonth"=>$formonth, "hospitalid"=>$accid, "submitteddate"=>date("Y-m-d H:i:s"), "lastupdated"=>date("Y-m-d H:i:s")));
			$shareid = $this->db->insert_id();
		}
		$this->updateClosingShareData($shareid, $productlist);
	}

	function saveITRDataACCOnly($foryear, $formonth, $accid, $productlist){
		$this->db->from("closingdataitr");
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("hospitalid", $accid);
		$this->db->where("specid is null", NULL, false);
		$this->db->where("submittedby is null", NULL, false);
		$this->db->where("schedid is null", NULL, false);
		$r = $this->db->get();
		if ($r->num_rows()){
			//update
			$itrid = $r->row()->itrid;
		} else {
			$this->db->insert("closingdataitr", array("foryear"=>$foryear, "formonth"=>$formonth, "hospitalid"=>$accid, "submitteddate"=>date("Y-m-d H:i:s"), "lastupdated"=>date("Y-m-d H:i:s")));
			$itrid = $this->db->insert_id();
		}
		$this->updateClosingITRIntentions($itrid, $productlist);
	}

	function saveITRDataACCHCPOnly($foryear, $formonth, $accid, $hcpid, $productlist){
		$this->db->from("closingdataitr");
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("hospitalid", $accid);
		$this->db->where("specid", $hcpid);
		$this->db->where("submittedby is null", NULL, false);
		$this->db->where("schedid is null", NULL, false);
		$r = $this->db->get();
		if ($r->num_rows()){
			$itrid = $r->row()->itrid;
		} else {
			$this->db->insert("closingdataitr", array("foryear"=>$foryear, "formonth"=>$formonth, "hospitalid"=>$accid, "submitteddate"=>date("Y-m-d H:i:s"), "specid"=>$hcpid, "lastupdated"=>date("Y-m-d H:i:s")));
			$itrid = $this->db->insert_id();
		}
		$this->updateClosingITRIntentions($itrid, $productlist);
	}

	function updateClosingITRIntentions($id, $itrarray){
		$vt = $this->db->where("itrid", $id)->delete("closingdataitrproducts");
		if ($vt){
			if (empty($itrarray)){
				return true;
			}
			$newarr = array();
			foreach ($itrarray as $arr){
				$arr['itrid'] = $id;
				$newarr[] = $arr;
			}
			// var_dump($itrarray);
			return $this->db->insert_batch("closingdataitrproducts", $newarr);
		} else {
			return false;
		}
	}

	function updateClosingShareData($id, $sharearray){
		$vt = $this->db->where("shareid", $id)->delete("closingdatashareproducts");
		if ($vt){
			if (empty($sharearray)){
				return true;
			}
			$newarr = array();
			foreach ($sharearray as $arr){
				$arr['shareid'] = $id;
				$newarr[] = $arr;
			}
			// var_dump($newarr);
			return $this->db->insert_batch("closingdatashareproducts", $newarr);
		} else {
			return false;
		}
	}
}
?>