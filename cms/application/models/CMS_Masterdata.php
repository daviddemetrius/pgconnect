<?php
class CMS_Masterdata extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function getAllHospitals(){
		return $this->db->order_by("branch", "asc")->get('hospital')->result();
	}

	function getAllPromos(){
		return $this->db->get('promotions')->result();
	}

	function getHospitalsForSearch(){
		return $this->db->select("hospitalid as id, '' as value, hospitalname as label")->get("hospital")->result();
	}

	function getHospitalsForSearchForRep($repid){
		$res = $this->db->select("hospitalid as id, '' as value, hospitalname as label")->where("hospital.territory IN", "(SELECT territory from representatives where repid = $repid)", false)->get("hospital")->result();
		$r = array();

		foreach ($res as $row){
			$r[$row->id] = $row->label;
		}
		return $r;
	}	

	function getSpecsForSearch($id){
		$res = $this->db->select("hr.specid as id, '' as value, specialistname as label")->join("specialist sp", "sp.specid = hr.specid")->where("hr.hospitalid", $id)->get("hospitalrooms hr")->result();

		$r = array();

		foreach ($res as $row){
			$r[$row->id] = $row->label;
		}
		return $r;
	}

	function getSpecsForSearchTwo($id){
		$res = $this->db->select("hr.specid as id, '' as value, specialistname as label")->join("specialist sp", "sp.specid = hr.specid")->where("hr.hospitalid", $id)->get("hospitalrooms hr")->result();

		return $res;
	}

	function getAllHospitalsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("hospitalid as id, region, distributor, branch, customercode, hospitalname, obsolete");
		$this->db->from("hospital ei");
		// $this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		// $this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		// $this->db->join("district d", "d.districtid = ei.district", "left");
		// $this->db->join("city c", "c.cityid = city", "left");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchHospitals($query, $page, $limit = 10, $sorting = FALSE, $selection = ""){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("hospitalid as id, region, distributor, branch, customercode, hospitalname, obsolete");
		$this->db->from("hospital ei");
		// $this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		// $this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		// $this->db->join("district d", "d.districtid = ei.district");
		// $this->db->join("city c", "c.cityid = city", "left");
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("hospitalname", $query)->or_like("address", $query)->or_like("phone", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("hospitalname", $query)->or_like("address", $query)->or_like("phone", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("hospitalname", $query)->or_like("address", $query)->or_like("phone", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else {
			$this->db->like($selection, $query);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalHospitalPages($limit){
		$this->db->from("hospital ei");		
		// $this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		// $this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchHospitalPages($limit, $query, $selection = ""){
		$this->db->from("hospital ei");		
		// $this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		// $this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		// $this->db->join("district d", "d.districtid = ei.district");
		// $this->db->join("city c", "c.cityid = city", "left");
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("hospitalname", $query)->or_like("address", $query)->or_like("phone", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("hospitalname", $query)->or_like("address", $query)->or_like("phone", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("hospitalname", $query)->or_like("address", $query)->or_like("phone", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else {
			$this->db->like($selection, $query);
		}
		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllPromosPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->from("promotions ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchPromos($query, $page, $limit = 10, $sorting = FALSE, $selection = ""){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->from("promotions ei");
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("promoname", $query)->or_like("promoend", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("promoname", $query)->or_like("promoend", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("promoname", $query)->or_like("promoend", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else {
			$this->db->like($selection, $query);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalPromoPages($limit){
		$this->db->from("promotions ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchPromoPages($limit, $query, $selection = ""){
		$this->db->from("promotions ei");
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("promoname", $query)->or_like("promoend", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("promoname", $query)->or_like("promoend", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("promoname", $query)->or_like("promoend", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else {
			$this->db->like($selection, $query);
		}
		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllUnassignedHospitalsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("hospitalid as id, hospitalname, typename, segmentation, obsolete");
		$this->db->from("hospital ei");
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->group_start();
		$this->db->where("hospitalid not in ", "(select hospitalid from hospitalterritory)", false);
		$this->db->group_end();
		// $this->db->join("district d", "d.districtid = ei.district", "left");
		$this->db->join("city c", "c.cityid = city", "left");
		// $this->db->like("hospitalname", "cyber");
		$this->db->limit($limit, $limit * ($page-1));
		$r = $this->db->get()->result();
		// var_dump($this->db->last_query());
		return $r;
	}

	function searchUnassignedHospitals($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("hospitalid as id, hospitalname, typename, segmentation,  obsolete");
		$this->db->from("hospital ei");
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->where("hospitalid not in ", "(select hospitalid from hospitalterritory)", false);
		// $this->db->join("district d", "d.districtid = ei.district");
		$this->db->join("city c", "c.cityid = city", "left");
		$this->db->group_start();
		$this->db->like("hospitalname", $query)->or_like("typename", $query)->or_like("segmentation", $query)->or_like("xmlaccid", $query);
		$this->db->group_end();
		$r = $this->db->get()->result();
		// var_dump($this->db->last_query());
		return $r;
	}

	function getTotalUnassignedHospitalPages($limit){
		$this->db->from("hospital ei");		
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->where("hospitalid not in ", "(select hospitalid from hospitalterritory)", false);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchUnassignedHospitalPages($limit, $query){
		$this->db->from("hospital ei");		
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		// $this->db->join("district d", "d.districtid = ei.district");
		$this->db->join("city c", "c.cityid = city", "left");
		$this->db->group_start();
		$this->db->like("hospitalname", $query)->or_like("typename", $query)->or_like("segmentation", $query)->or_like("xmlaccid", $query);
		$this->db->group_end();
		$this->db->where("hospitalid not in ", "(select hospitalid from hospitalterritory)", false);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllUnapprovedHospitalsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("hospitalid as id, hospitalname, typename, segmentation,  obsolete");
		$this->db->from("hospital ei");
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->where("approved", 0);
		// $this->db->join("district d", "d.districtid = ei.district", "left");
		$this->db->join("city c", "c.cityid = city", "left");
		$this->db->limit($limit, $limit * ($page-1));

		$r = $this->db->get()->result();

		foreach ($r as $row){
			$row->assignedhcp = $this->db->query("SELECT count(*) as cnt from hospitalrooms where hospitalid = $row->id")->row()->cnt;
			$row->assignedtm = $this->db->query("SELECT count(*) as cnt from hospitalterritory where hospitalid = $row->id")->row()->cnt;
			$row->assignedtma = $this->db->query("SELECT count(*) as cnt from territoryassignment where hospitalid = $row->id")->row()->cnt;
		}
		return $r;
	}

	function searchUnapprovedHospitals($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("hospitalid as id, hospitalname, typename, segmentation,  obsolete");
		$this->db->from("hospital ei");
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->where("approved", 0);
		// $this->db->join("district d", "d.districtid = ei.district");
		$this->db->join("city c", "c.cityid = city", "left");
		$this->db->like("hospitalname", $query)->or_like("typename", $query)->or_like("segmentation", $query)->or_like("xmlaccid", $query);
		$this->db->limit($limit, $limit * ($page-1));
		$r = $this->db->get()->result();
		foreach ($r as $row){
			$row->assignedhcp = $this->db->query("SELECT count(*) as cnt from hospitalrooms where hospitalid = $row->hospitalid")->row()->cnt;
			$row->assignedtm = $this->db->query("SELECT count(*) as cnt from hospitalterritory where hospitalid = $row->hospitalid")->row()->cnt;
			$row->assignedtma = $this->db->query("SELECT count(*) as cnt from territoryassignment where hospitalid = $row->hospitalid")->row()->cnt;
		}
		return $r;
	}

	function getTotalUnapprovedHospitalPages($limit){
		$this->db->from("hospital ei");		
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();

			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->where("approved", 0);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchUnapprovedHospitalPages($limit, $query){
		$this->db->from("hospital ei");		
		if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("hospitalid in", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 2){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);
			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 1){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->group_start();
			$this->db->where("hospitalid in ", $this->session->userdata("hospitals"), false);

			$this->db->or_where("ei.createdby", $this->session->userdata("user_id"));
			$this->db->group_end();
			// $this->db->or_where("hospitalid in ", "(select hospitalid from territoryassignment where territoryid in (select territory from dmreps join representatives on representatives.repid = dmreps.repid where userid = ".$this->session->userdata("user_id")."))", false);
		}
		$this->db->join("hospitaltype ht", "ht.typeid = ei.hospitaltype");
		$this->db->join("hospitalsegment hs", "hs.segmentid = ei.segmentid");
		// $this->db->join("district d", "d.districtid = ei.district");
		$this->db->join("city c", "c.cityid = city", "left");
		$this->db->like("hospitalname", $query)->or_like("typename", $query)->or_like("segmentation", $query)->or_like("xmlaccid", $query);
		$this->db->where("approved", 0);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getHospitalTypes(){
		return $this->db->get("hospitaltype")->result();
	}

	function createNewHospital($data, $promos){
		$b = $this->db->insert("hospital", $data);
		if ($b){
			$id = $this->db->insert_id();
			if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
			else {
				if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

				} else {
					$config['upload_path']          = './assets/accounts/';
			        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
			        $config['file_name']            = $id.'.png'; 
			        $config['overwrite']            = true; 
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
			        $c = $this->upload->do_upload('userfile');
			        if (!$c){
			            $error = array('error' => $this->upload->display_errors());
			        }
				}
			}
		}
		$hospitalid = $this->db->insert_id();
		$this->updateAccountPromos($hospitalid, $promos);
		return $b;
	}

	function updateAccountPromos($id, $promoArray){
		$vt = $this->db->where("hospitalid", $id)->delete("promoaccounts");
		if ($vt){
			if (empty($promoArray)){
				return true;
			}
			$reparr = array();
			foreach ($promoArray as $reptype){
				$reparr[] = array("hospitalid" => $id, "promoid" => $reptype);
			}
			return $this->db->insert_batch("promoaccounts", $reparr);
		} else {
			return false;
		}
	}

	function updatePromoAccounts($id, $hospitalArray){
		$vt = $this->db->where("promoid", $id)->delete("promoaccounts");
		if ($vt){
			if (empty($hospitalArray)){
				return true;
			}
			$reparr = array();
			foreach ($hospitalArray as $reptype){
				$reparr[] = array("promoid" => $id, "hospitalid" => $reptype);
			}
			return $this->db->insert_batch("promoaccounts", $reparr);
		} else {
			return false;
		}
	}

	function createNewChannel($data){
		return $this->db->insert("hospitaltype", $data);
	}
	function createNewPromo($data, $promoid){
		$b = $this->db->insert("promotions", $data);
		if ($b){
			$id = $this->db->insert_id();
			if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
			else {
				if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

				} else {
					$config['upload_path']          = './assets/promos/';
			        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
			        $config['file_name']            = $id.$data['promonumber'].'.png'; 
			        $config['overwrite']            = true; 
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
			        $c = $this->upload->do_upload('userfile');
			        if (!$c){
			            $error = array('error' => $this->upload->display_errors());
			        }
				}
			}
		}
		$promoid = $this->db->insert_id();
		$this->updatePromoAccounts($promoid, $hospitalid);
		return $b;
	}

	function getHospitalDetails($id){
		$hospdtl = $this->db->select("hospital.*")->where("hospitalid", $id)->get("hospital");
		if (!$hospdtl->num_rows()){
			return false;
		}
		$hospdtl = $hospdtl->row();
		$hospdtl->promos = $this->db->where("hospitalid", $id)->get("promoaccounts")->result();
		// $hospdtl->specialists = $this->db->from("hospitalrooms hr")->join("specialist sp", "sp.specid = hr.specid")->join("room r", "r.roomid = hr.roomid")->where("hr.hospitalid", $hospdtl->hospitalid)->order_by("sp.specialistname", "asc")->get()->result();
		// $hospdtl->territories = $this->db->from("hospitalterritory ht")->join("territory t", "t.territoryid = ht.territory")->join("district d", "d.districtid = t.districtid", "left")->where("ht.hospitalid", $id)->get()->result();
		return $hospdtl;
	}

	function getPromoDetails($id){
		$hospdtl = $this->db->where("promoid", $id)->get("promotions");
		if (!$hospdtl->num_rows()){
			return false;
		}
		$hospdtl = $hospdtl->row();
		$hospdtl->hospitals = $this->db->where("promoid", $id)->get("promoaccounts")->result();
		// $hospdtl->specialists = $this->db->from("hospitalrooms hr")->join("specialist sp", "sp.specid = hr.specid")->join("room r", "r.roomid = hr.roomid")->where("hr.hospitalid", $hospdtl->hospitalid)->order_by("sp.specialistname", "asc")->get()->result();
		// $hospdtl->territories = $this->db->from("hospitalterritory ht")->join("territory t", "t.territoryid = ht.territory")->join("district d", "d.districtid = t.districtid", "left")->where("ht.hospitalid", $id)->get()->result();
		return $hospdtl;
	}
	function getPromoCodes($id){
		return $this->db->where("promoid", $id)->join("hospital", "hospital.hospitalid = customers.hospitalid", "left")->select("customers.*, hospitalname")->order_by("dateregged", "desc")->get("customers")->result();
	}
	function getPromoCodesByHospital($id){
		return $this->db->where("hospitalid", $id)->join("promotions", "promotions.promoid = customers.promoid", "left")->select("customers.*, promoname")->order_by("dateregged", "desc")->get("customers")->result();
	}

	function getApotekReps($hospitalid){
		return $this->db->where("associatedaccount", $hospitalid)->get("representatives")->result();
	}
	function getApotekPromos($hospitalid){
		return $this->db->where("associatedaccount", $hospitalid)->get("promotions")->result();
	}
	function getApotekEvents($hospitalid){
		return $this->db->where("hospitalid", $hospitalid)->get("hospitaleventbanners")->result();
	}

	function editHospital($data, $id, $promos){
		if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/accounts/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('userfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}
		$this->updateAccountPromos($id, $promos);
		return $this->db->where("hospitalid", $id)->update("hospital", $data);
	}

	function editPromo($data, $id, $hospitals){
		if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

			} else {
				$promonumber = $this->db->where("promoid", $id)->get("promotions")->row()->promonumber;
				$config['upload_path']          = './assets/promos/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.$promonumber.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('userfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}
		$this->updatePromoAccounts($id, $hospitals);
		return $this->db->where("promoid", $id)->update("promotions", $data);
	}

	function createNewSpecialist($data){
		return $this->db->insert("specialist", $data);
	}

	function editEvent($data, $id){
		$this->db->where("eventid", $id)->update("event", $data);
		if (!isset($_FILES['infofile']) || $_FILES['infofile']['error'] == UPLOAD_ERR_NO_FILE){
			echo "No File";
		} 
		else {
			if(!file_exists($_FILES['infofile']['tmp_name']) || !is_uploaded_file($_FILES['infofile']['tmp_name'])){
				echo json_encode($_FILES['infofile']);
			} else {
				echo "Uploading";
				$config['upload_path']          = './assets/eventdoc/';
		        $config['allowed_types']        = '*'; 
		        $config['overwrite']            = false; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('infofile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		            echo json_encode($_FILES['infofile']);
		            echo json_encode($error);
		        } else {
		        	$filename = $this->upload->data("file_name");
		        	echo "$filename";
		        	$this->db->where("eventid", $id)->update("event", array("docfile" => $filename));
		        }
			}
		}

		if (!isset($_FILES['bannerfile']) || $_FILES['bannerfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['bannerfile']['tmp_name']) || !is_uploaded_file($_FILES['bannerfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/eventb/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('bannerfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}

		if (!isset($_FILES['thumbfile']) || $_FILES['thumbfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['thumbfile']['tmp_name']) || !is_uploaded_file($_FILES['thumbfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/eventf/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('thumbfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}

		return true;
	}

	function createNewEvent($data){
		$this->db->insert("event", $data);
		$id = $this->db->insert_id();

		if (!isset($_FILES['infofile']) || $_FILES['infofile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['infofile']['tmp_name']) || !is_uploaded_file($_FILES['infofile']['tmp_name'])){

			} else {
				echo "Uploading";
				$config['upload_path']          = './assets/eventdoc/';
		        $config['allowed_types']        = '*'; 
		        $config['overwrite']            = false; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('infofile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		            echo json_encode($_FILES['infofile']);
		            echo json_encode($error);
		        } else {
		        	$filename = $this->upload->data("file_name");
		        	echo "$filename";
		        	$this->db->where("eventid", $id)->update("event", array("docfile" => $filename));
		        }
			}
		}

		if (!isset($_FILES['bannerfile']) || $_FILES['bannerfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['bannerfile']['tmp_name']) || !is_uploaded_file($_FILES['bannerfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/eventb/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('bannerfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}

		if (!isset($_FILES['thumbfile']) || $_FILES['thumbfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['thumbfile']['tmp_name']) || !is_uploaded_file($_FILES['thumbfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/eventf/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('thumbfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}

		return true;
	}

	function getSpecialistDetails($id){
		$r = $this->db->select("specialist.*")->where("specid", $id)->get("specialist")->row();
		
		return $r;
	}

	function editSpecialist($data, $id){
		$r = $this->db->where("xmlhcpid", $data['xmlhcpid'])->where("specid !=", $id)->get("specialist");
		if ($r->num_rows()){
			return false;
		}
		return $this->db->where("specid", $id)->update("specialist", $data);
	}

	function getRooms(){
		return $this->db->get("room")->result();
	}

	function createNewRooms($data){
		$b = $this->db->insert("room", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getRoomDetails($id){
		$result = $this->db->where("roomid", $id)->get("room")->row();
		return $result;
	}

	function editRoom($data, $id){
		return $this->db->where("roomid", $id)->update("room", $data);
	}

	function getAllRoomsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("roomname, roomid");
		$this->db->from("room ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchRooms($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("roomname, roomid");
		$this->db->from("room ei");
		$this->db->like("roomname", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalRoomPages($limit){
		$this->db->from("room ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchRoomPages($limit, $query){
		$this->db->from("room ei");	
		$this->db->like("roomname", $query);	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getSpecialties(){
		return $this->db->get("specialty")->result();
	}

	function createNewSpecialties($data){
		$b = $this->db->insert("specialty", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getSpecialtyDetails($id){
		$result = $this->db->where("specialtyid", $id)->get("specialty")->row();
		return $result;
	}

	function editSpecialty($data, $id){
		return $this->db->where("specialtyid", $id)->update("specialty", $data);
	}

	function editAccSegment($data, $id){
		return $this->db->where("segmentid", $id)->update("hospitalsegment", $data);	
	}

	function editAccChannel($data, $id){
		return $this->db->where("typeid", $id)->update("hospitaltype", $data);	
	}

	function editHcpSegment($data, $id){
		return $this->db->where("segmentid", $id)->update("segmentation", $data);	
	}

	function getAllSpecialtysPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("specialtyname, specialtyid");
		$this->db->from("specialty ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchSpecialtys($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("specialtyname, specialtyid");
		$this->db->from("specialty ei");
		$this->db->like("specialtyname", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSpecialtyPages($limit){
		$this->db->from("specialty ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchSpecialtyPages($limit, $query){
		$this->db->from("specialty ei");	
		$this->db->like("specialtyname", $query, "after");	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewSegments($data){
		$b = $this->db->insert("segmentation", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getAllSegmentsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("segmentation, segmentid");
		$this->db->from("segmentation ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchSegments($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("segmentation, segmentid");
		$this->db->from("segmentation ei");
		$this->db->like("segmentation", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSegmentPages($limit){
		$this->db->from("segmentation ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchSegmentPages($limit, $query){
		$this->db->from("segmentation ei");	
		$this->db->like("segmentation", $query);	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewAccseg($data){
		$b = $this->db->insert("hospitalsegment", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}


	function getAllACCSegmentsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("segmentation, segmentid");
		$this->db->from("hospitalsegment ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchACCSegments($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("segmentation, segmentid");
		$this->db->from("hospitalsegment ei");
		$this->db->like("segmentation", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalACCSegmentPages($limit){
		$this->db->from("hospitalsegment ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchACCSegmentPages($limit, $query){
		$this->db->from("hospitalsegment ei");	
		$this->db->like("typename", $query);	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}


	function getAllACCChannelsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("typename, typeid");
		$this->db->from("hospitaltype ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchACCChannels($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("typename, typeid");
		$this->db->from("hospitaltype ei");
		$this->db->like("typename", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalACCChannelPages($limit){
		$this->db->from("hospitaltype ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchACCChannelPages($limit, $query){
		$this->db->from("hospitaltype ei");	
		$this->db->like("typename", $query);	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllHCPsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("specid as id, xmlhcpid, specialistname, specialty, institution, phone, obsolete");
		$this->db->from("specialist ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchHCPs($query, $page, $limit = 10, $sorting = FALSE, $selection = ""){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("specid as id, xmlhcpid, specialistname, specialty, institution, phone, obsolete");
		$this->db->from("specialist ei");
		
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("specialistname", $query, "after")->or_like("specialty", $query, "after")->or_like("institution", $query, "after")->or_like("xmlhcpid", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("specialistname", $query, "after")->or_like("specialty", $query, "after")->or_like("institution", $query, "after")->or_like("xmlhcpid", $query);
			$this->db->group_end();
			$this->db->where('obsolete', 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("specialistname", $query, "after")->or_like("specialty", $query, "after")->or_like("institution", $query, "after")->or_like("xmlhcpid", $query);
			$this->db->group_end();
			$this->db->where('obsolete', 0);			
		} else {
			$this->db->like($selection, $query);
		}
		
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalHCPPages($limit){
		$this->db->from("specialist ei");	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchHCPPages($limit, $query, $selection = ""){
		$this->db->from("specialist ei");	
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("specialistname", $query, "after")->or_like("specialty", $query, "after")->or_like("institution", $query, "after")->or_like("xmlhcpid", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("specialistname", $query, "after")->or_like("specialty", $query, "after")->or_like("institution", $query, "after")->or_like("xmlhcpid", $query);
			$this->db->group_end();
			$this->db->where('obsolete', 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("specialistname", $query, "after")->or_like("specialty", $query, "after")->or_like("institution", $query, "after")->or_like("xmlhcpid", $query);
			$this->db->group_end();
			$this->db->where('obsolete', 0);			
		} else {
			$this->db->like($selection, $query);
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllEventsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("eventid as id, eventtitle, datestart, dateend, goingcount");
		$this->db->from("event ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchEvents($query, $page, $limit = 10, $sorting = FALSE, $selection = ""){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("eventid as id, eventtitle, datestart, dateend, goingcount");
		$this->db->from("event ei");
		
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("eventtitle", $query, "after");
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("eventtitle", $query, "after");
			$this->db->group_end();
			$this->db->where('obsolete', 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("eventtitle", $query, "after");
			$this->db->group_end();
			$this->db->where('obsolete', 0);			
		} else {
			$this->db->like($selection, $query);
		}
		
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getEventDetails($eventid){
		return $this->db->where('eventid', $eventid)->get("event")->row();
	}

	function getEventAttendees($eventid){
		return $this->db->where('eventid', $eventid)->join("specialist sp", "sp.specid = ea.specid")->get("eventattendance ea")->result();
	}

	function getTotalEventPages($limit){
		$this->db->from("event ei");	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchEventPages($limit, $query, $selection = ""){
		$this->db->from("event ei");	
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("eventtitle", $query, "after");
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("eventtitle", $query, "after");
			$this->db->group_end();
			$this->db->where('obsolete', 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("eventtitle", $query, "after");
			$this->db->group_end();
			$this->db->where('obsolete', 0);			
		} else {
			$this->db->like($selection, $query);
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getHCPSegmentInfo($id){
		$segmentdtl = $this->db->where("segmentid", $id)->get("segmentation")->row();
		$segmentdtl->visitmap = $this->db->select("hs.segmentid as segmentid, hs.segmentation as segment")->get("hospitalsegment hs")->result();
		foreach ($segmentdtl->visitmap as $segment){
			$v = $this->db->select("visits")->where("hospitalsegment", $segment->segmentid)->where("segmentid", $id)->get("specialtyvisit");
			if ($v->num_rows() > 0){
				$v = $v->row()->visits;
			} else {
				$v = 0;
			}
			$segment->visits = $v;
		}
		// var_dump($segmentdtl);
		return $segmentdtl;
	}

	function getACCSegmentInfo($id){
		$segmentdtl = $this->db->where("segmentid", $id)->get("hospitalsegment")->row();
		$segmentdtl->visitmap = $this->db->select("s.segmentation as segment, s.segmentid as segmentid")->get("segmentation s")->result();
		foreach ($segmentdtl->visitmap as $segment){
			$v = $this->db->select("visits")->where("segmentid", $segment->segmentid)->where("hospitalsegment", $id)->get("specialtyvisit");
			if ($v->num_rows() > 0){
				$v = $v->row()->visits;
			} else {
				$v = 0;
			}
			$segment->visits = $v;
		}
		// var_dump($segmentdtl);
		return $segmentdtl;
	}

	function getACCChannelInfo($id){
		$segmentdtl = $this->db->where("typeid", $id)->get("hospitaltype")->row();
		
		// var_dump($segmentdtl);
		return $segmentdtl;
	}

	function editVisit($data, $hcpid, $accid){
		return $this->db->query("INSERT INTO specialtyvisit values($accid, $hcpid, ".$data['visits'].") ON DUPLICATE KEY UPDATE visits = ".$data['visits']);
	}

	function getAccSegments(){
		return $this->db->get("hospitalsegment")->result();
	}

	function getHcpSegments(){
		return $this->db->get("segmentation")->result();
	}

	function HCgetRedundantACCEntries(){
		return $this->db->query("SELECT xmlaccid, territory, hospital.hospitalid FROM hospital WHERE xmlaccid IN (SELECT xmlaccid AS hospitals FROM hospital GROUP BY xmlaccid HAVING COUNT(*) > 1) ORDER BY xmlaccid ASC, territory ASC")->result();
	}

	function HCinsertIntoHospitalTerritory($values){
		return $this->db->query("INSERT ignore into hospitalterritory values $values");
	}

	function HCupdateClosingBirth($values, $newvalue){
		return $this->db->query("UPDATE ignore closingdatabirth set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateClosingDelivery($values, $newvalue){
		return $this->db->query("UPDATE ignore closingdatadelivery set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateClosingITR($values, $newvalue){
		return $this->db->query("UPDATE ignore closingdataitr set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateClosingPotency($values, $newvalue){
		return $this->db->query("UPDATE ignore closingdatapotency set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateClosingShare($values, $newvalue){
		return $this->db->query("UPDATE ignore closingdatashare set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateClosingSurvey($values, $newvalue){
		return $this->db->query("UPDATE ignore closingdatasurvey set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateHospitalRooms($values, $newvalue){
		return $this->db->query("UPDATE ignore hospitalrooms set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateRepDesignation($values, $newvalue){
		return $this->db->query("UPDATE ignore representativedesignation set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCupdateScheduleVisit($values, $newvalue){
		return $this->db->query("UPDATE ignore schedulevisit set hospitalid = $newvalue where hospitalid in $values");
	}

	function HCdeleteHospitalEntries($values){
		return $this->db->query("DELETE from hospital where hospitalid in $values");
	}

	function HCgetAssignmentData(){
		// $this->db->select("rd.specid as specialistid, rd.hospitalid, hr.roomid, rp.territory");
		// $this->db->from("representativedesignation rd");
		// $this->db->join("representatives rp", "rp.repid = rd.repid");
		// $this->db->where("rp.repid", 900);
		// $this->db->join("hospitalrooms hr", "hr.specid = rd.specid AND hr.hospitalid =  rd.hospitalid");
		// $res = $this->db->get()->result();
		// echo $this->db->last_query();
		// echo json_encode($res);
		$this->db->query("INSERT IGNORE INTO territoryassignment (specid, hospitalid, roomid, territoryid) SELECT `rd`.`specid` as `specialistid`, `rd`.`hospitalid`, `hr`.`roomid`, `rp`.`territory` FROM `representativedesignation` `rd` JOIN `representatives` `rp` ON `rp`.`repid` = `rd`.`repid` JOIN `hospitalrooms` `hr` ON `hr`.`specid` = `rd`.`specid` AND `hr`.`hospitalid` = `rd`.`hospitalid`");
	}



	function searchHcp($q){
		$exp = explode(" ", $q);
		$str = "";
		$delim = "";
		foreach ($exp as $e){
			$str .= "$delim (xmlhcpid like '%$e%' or specialistname like '%$e%' or cityname like '%$e%' or specialtyname like '%$e%')";
			$delim = " and ";

		}
		// echo $str;
		$b = $this->db->query("SELECT specid as data, concat(xmlhcpid, ' - ', cityname, ' - ', specialtyname, ' - ', specialistname) as value from specialist left join city on city = cityid left join specialty on specialty = specialtyid where $str limit 10")->result();
		// echo $this->db->last_query();
		return $b;
		// return $this->db->select("hospitalid as data, concat(xmlaccid, ' - ', hospitalname) as value")->group_start()->where("hospitalid >", 45348)->group_start()->like("xmlaccid", $q)->or_like("hospitalname", $q)->group_end()->group_end()->limit(10)->get("hospital")->result();
	}

	function addHospitalRoom($specid, $roomid, $hospitalid){
		$r = $this->db->where("specid", $specid)->where("roomid", $roomid)->where("hospitalid", $hospitalid)->get("hospitalrooms");
		if ($r->num_rows()){
			return "exist";
		} else {
			return $this->db->insert('hospitalrooms', array("specid"=>$specid, "roomid"=>$roomid, "hospitalid"=>$hospitalid));
		}
	}

	function editHospitalRoom($specid, $roomid, $hospitalid, $oldroomid){
		$r = $this->db->where("specid", $specid)->where("roomid", $roomid)->where("hospitalid", $hospitalid)->get("hospitalrooms");
		if ($r->num_rows()){
			return "exist";
		} else {
			return $this->db->where("specid", $specid)->where("hospitalid", $hospitalid)->where("roomid", $oldroomid)->update('hospitalrooms', array("specid"=>$specid, "roomid"=>$roomid, "hospitalid"=>$hospitalid));
		}
	}

	function deleteHospitalRoom($specid, $roomid, $hospitalid){
		$r = $this->db->where("specid", $specid)->where("roomid", $roomid)->where("hospitalid", $hospitalid)->delete("hospitalrooms");
		if ($r){
			$r = $this->db->where("specid", $specid)->where("roomid", $roomid)->where('hospitalid', $hospitalid)->delete("territoryassignment");
			return "success";
		} else {
			return "Failed to delete assignment. Please try again later";
		}
	}

	function addHospitalTerritory($territoryid, $hospitalid){
		$r = $this->db->where("territory", $territoryid)->where("hospitalid", $hospitalid)->get("hospitalterritory");
		if ($r->num_rows()){
			return "exist";
		} else {
			return $this->db->insert('hospitalterritory', array("territory"=>$territoryid, "hospitalid"=>$hospitalid));
		}
	}

	function deleteHospitalTerritory($territoryid, $hospitalid){
		$r = $this->db->where("territory", $territoryid)->where("hospitalid", $hospitalid)->delete("hospitalterritory");
		if ($r){
			$this->db->where("territoryid", $territoryid)->where("hospitalid", $hospitalid)->delete("territoryassignment");
			return "success";
		} else {
			return "Failed to delete assignment. Please try again later";
		}
	}


	function deactivateAccount($id){
		$data = array(
			"obsolete" => 1
		);
		return $this->db->where("hospitalid", $id)->update("hospital", $data);
	}

	function reactivateAccount($id){
		$data = array(
			"obsolete" => 0
		);
		return $this->db->where("hospitalid", $id)->update("hospital", $data);
	}

	function approveAccount($id){
		$data = array(
			"approved" => 1
		);
		return $this->db->where("hospitalid", $id)->update("hospital", $data);
	}
	function rejectAccount($id){
		
		return $this->db->where("hospitalid", $id)->delete("hospital");
	}

	function deactivateHCP($id){
		$data = array(
			"obsolete" => 1
		);
		return $this->db->where("specid", $id)->update("specialist", $data);
	}

	function reactivateHCP($id){
		$data = array(
			"obsolete" => 0
		);
		return $this->db->where("specid", $id)->update("specialist", $data);
	}

	function approveHCP($id){
		$data = array(
			"approved" => 1
		);
		return $this->db->where("specid", $id)->update("specialist", $data);
	}

	function rejectHCP($id){
		return $this->db->where("specid", $id)->delete("specialist");
	}
}
?>