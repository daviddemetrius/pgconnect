<?php
class CMS_Modules extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function getAllCyclesPaged($page, $limit = 10, $sorting = FALSE){
		$this->db->from("cycle ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchCycles($query, $page, $limit = 10, $sorting = FALSE){
		$this->db->from("cycle ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->like("cycletitle", $query)->or_like("periodstart", $query, "after")->or_like("periodend", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchCyclePages($limit, $query){
		$this->db->from("cycle ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$this->db->like("cycletitle", $query)->or_like("periodstart", $query, "after")->or_like("periodend", $query, "after");
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalCyclePages($limit){
		$this->db->from("cycle ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllPagesPaged($page, $limit = 10, $sorting = FALSE){
		$this->db->from("contentpages ei");
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->join("pagecategory", "pagecategory = categoryid");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchPages($query, $page, $limit = 10, $sorting = FALSE, $selection = ""){
		$this->db->from("contentpages ei");
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->join("pagecategory", "pagecategory = categoryid");
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("pagetitle", $query)->or_like("description", $query)->or_like("categoryname", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
		$this->db->like("pagetitle", $query)->or_like("description", $query)->or_like("categoryname", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
		$this->db->like("pagetitle", $query)->or_like("description", $query)->or_like("categoryname", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else {
			$this->db->like($selection, $query);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchPagePages($limit, $query, $selection = ""){
		$this->db->from("contentpages ei");	
		$this->db->join("pagecategory", "pagecategory = categoryid");
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("pagetitle", $query)->or_like("description", $query)->or_like("categoryname", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
		$this->db->like("pagetitle", $query)->or_like("description", $query)->or_like("categoryname", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
		$this->db->like("pagetitle", $query)->or_like("description", $query)->or_like("categoryname", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else {
			$this->db->like($selection, $query);
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalPagePages($limit){
		$this->db->from("contentpages ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getHouses(){
		return $this->db->get("house")->result();
	}

	function getHospitals(){
		return $this->db->get("hospital")->result();
	}

	function getModuleGroups(){
		return $this->db->get("grouping")->result();
	}

	function getAllPageCategorysPaged($page, $limit = 10, $sorting = FALSE){
		$this->db->select("categoryid as id, categoryname");
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->from("pagecategory ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchPageCategorys($query, $page, $limit = 10, $sorting = FALSE){
		$this->db->select("categoryid as id, categoryname");
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->from("pagecategory ei");
		$this->db->like("categoryname", $query, "after");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchPageCategoryPages($limit, $query){
		$this->db->from("pagecategory ei");	
		$this->db->like("categoryname", $query, "after");	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalPageCategoryPages($limit){
		$this->db->from("pagecategory ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllModuleGroupsPaged($page, $limit = 10, $sorting = FALSE){
		$this->db->select("groupid as id, groupname");
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->from("grouping ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchModuleGroups($query, $page, $limit = 10, $sorting = FALSE){
		$this->db->select("groupid as id, groupname");
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->from("grouping ei");
		$this->db->like("groupname", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchModuleGroupPages($limit, $query){
		$this->db->from("grouping ei");		
		$this->db->like("groupname", $query);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalModuleGroupPages($limit){
		$this->db->from("grouping ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllModulesPaged($page, $limit = 10, $sorting = FALSE){
		$this->db->from("contentmodules ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		if ($sorting){
			$this->db->order_by($sorting);
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchModules($query, $page, $limit = 10, $sorting = FALSE, $selection = ""){
		$this->db->from("contentmodules ei");
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}
		if ($sorting){
			$this->db->order_by($sorting);
		}
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else if ($selection == "general"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();	
			$this->db->where("generaltype", 1);		
		} else if ($selection == "nongeneral"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();	
			$this->db->where("generaltype", 0);		
		} else {
			$this->db->like($selection, $query);
		}
		
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalSearchModulePages($limit, $query, $selection = ""){
		$this->db->from("contentmodules ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		
		if ($selection == ""){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();
		} else if ($selection == "inactive"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 1);			
		} else if ($selection == "active"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();
			$this->db->where("obsolete", 0);			
		} else if ($selection == "general"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();	
			$this->db->where("generaltype", 1);		
		} else if ($selection == "nongeneral"){
			$this->db->group_start();
			$this->db->like("moduletitle", $query)->or_like("moduledesc", $query);
			$this->db->group_end();	
			$this->db->where("generaltype", 0);		
		} else {
			$this->db->like($selection, $query);
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalModulePages($limit){
		$this->db->from("contentmodules ei");	
		if ($this->session->userdata("tier") <= 2){
			$this->db->where("obsolete", 0);
		}	
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getAllPageCategories(){
		return $this->db->get("pagecategory")->result();
	}

	function createNewPage($data){
		$this->db->insert("contentpages", $data);
		return $this->db->insert_id();
	}

	function createNewModule($data, $reptypeid, $accounts, $modcats){
		$b = $this->db->insert("contentmodules", $data);
		if ($b){
			$moduleid = $this->db->insert_id();
			$this->updateModuleRepTypes($moduleid, $reptypeid);
			$this->updateModuleAccounts($moduleid, $accounts);
			$this->updateModuleGroups($moduleid, $modcats);
		}
		return $b;
	}

	function updateModuleRepTypes($id, $reptypeArray){
		$vt = $this->db->where("moduleid", $id)->delete("modulereptype");
		if ($vt){
			if (empty($reptypeArray)){
				return true;
			}
			$reparr = array();
			foreach ($reptypeArray as $reptype){
				$reparr[] = array("moduleid" => $id, "reptypeid" => $reptype);
			}
			return $this->db->insert_batch("modulereptype", $reparr);
		} else {
			return false;
		}
	}

	function updateModuleAccounts($id, $accounts){
		$vt = $this->db->where("moduleid", $id)->delete("moduleaccounts");
		if ($vt){
			if (empty($accounts)){
				return true;
			}
			$reparr = array();
			foreach ($accounts as $reptype){
				$reparr[] = array("moduleid" => $id, "hospitalid" => $reptype);
			}
			return $this->db->insert_batch("moduleaccounts", $reparr);
		} else {
			return false;
		}
	}

	function updateModuleGroups($id, $modcats){
		$vt = $this->db->where("moduleid", $id)->delete("modulegroup");
		if ($vt){
			if (empty($modcats)){
				return true;
			}
			$reparr = array();
			foreach ($modcats as $reptype){
				$reparr[] = array("moduleid" => $id, "groupid" => $reptype);
			}
			return $this->db->insert_batch("modulegroup", $reparr);
		} else {
			return false;
		}
	}

	function getPageInfo($id){
		$row = $this->db->where("pageid", $id)->join("pagecategory", "pagecategory = categoryid", "left")->get("contentpages")->row();
		$row->modules = $this->db->where("pageid", $id)->join("contentmodules cm", "cm.moduleid = mp.moduleid")->get("modulepages mp")->result();
		return $row;
	}

	function getModuleInfo($id){
		$row = $this->db->where("moduleid", $id)->get("contentmodules")->row();
		$row->groups = $this->db->where("moduleid", $id)->join("grouping g", "g.groupid = mg.groupid")->get("modulegroup mg")->result();
		$row->pages = $this->db->where("moduleid", $id)->join("contentpages cp", "cp.pageid = mp.pageid")->order_by("mp.pagenumber", "asc")->get("modulepages mp")->result();
		$row->reptypes = $this->db->where("moduleid", $id)->join('reptype', 'f.reptypeid = typeid')->get('modulereptype f')->result();
		$row->accounts = $this->db->where("moduleid", $id)->join('hospital h', 'h.hospitalid = f.hospitalid')->get('moduleaccounts f')->result();
		$row->quiz = $this->db->where("moduleid", $id)->order_by("availability", "desc")->get("modulequiz")->result();
		return $row;
	}

	function deactivateQuiz($quizid, $moduleid){
		return $this->db->where("quizid", $quizid)->where("moduleid", $moduleid)->update("modulequiz", array("availability" => 0));
	}

	function activateQuiz($quizid, $moduleid){
		return $this->db->where("quizid", $quizid)->where("moduleid", $moduleid)->update("modulequiz", array("availability" => 1));
	}

	function createNewQuiz($data){
		$b = $this->db->insert("modulequiz", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function updatePage($data, $id){
		return $this->db->where("pageid",  $id)->update("contentpages", $data);
	}

	function updateModule($data, $id, $reptypes, $accounts, $modcats){
		$b = $this->db->where("moduleid",  $id)->update("contentmodules", $data);
		if ($b){
			$this->updateModuleRepTypes($id, $reptypes);
			$this->updateModuleAccounts($id, $accounts);
			$this->updateModuleGroups($id, $modcats);
		}
		return $b;
	}

	function getAllPagesForSearch(){
		return $this->db->select("pageid as id, pageid as link, '' as value, pagetitle as label")->where("obsolete", 0)->get("contentpages")->result();
	}

	function getAllGroupsForSearch(){
		return $this->db->select("groupid as id, '' as value, groupname as label")->get("grouping")->result();
	}

	function getModulesForSearch(){
		return $this->db->select("moduleid as id, '' as value, concat(housename, ' - ', moduletitle) as label")->where("obsolete", 0)->join("house", "houseid = modulehouse")->get("contentmodules")->result();
	}

	function erasePages($moduleid){
		return $this->db->where("moduleid", $moduleid)->delete("modulepages");
	}

	function addModulePage($data){
		return $this->db->insert("modulepages", $data);
	}

	function getModulePagesForPreview($id){
		return $this->db->select("pagename")->where("mp.moduleid", $id)->join("contentpages cp", "cp.pageid = mp.pageid")->get("modulepages mp")->result();
	}

	function createNewModuleGroup($data){
		$b = $this->db->insert("grouping", $data);
		if ($b){
			$id = $this->db->insert_id();
			if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
			else {
				if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

				} else {
					$config['upload_path']          = './assets/modcats/';
			        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
			        $config['file_name']            = $id.'.png'; 
			        $config['overwrite']            = true; 
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
			        $c = $this->upload->do_upload('userfile');
			        if (!$c){
			            $error = array('error' => $this->upload->display_errors());
			        }
				}
			}
		}
		return $b;
	}

	function createNewPageCategory($data){
		return $this->db->insert("pagecategory", $data);
	}

	function getGroupInfo($id){
		$group = $this->db->where("groupid", $id)->get("grouping")->row();
		$group->modules = $this->db->where("groupid", $id)->join("contentmodules cm", "mg.moduleid = cm.moduleid")->join("house", "houseid = modulehouse")->get("modulegroup mg")->result();
		return $group;
	}

	function updateModuleGroup($data, $id){
		if (!isset($_FILES['userfile']) || $_FILES['userfile']['error'] == UPLOAD_ERR_NO_FILE){} 
		else {
			if(!file_exists($_FILES['userfile']['tmp_name']) || !is_uploaded_file($_FILES['userfile']['tmp_name'])){

			} else {
				$config['upload_path']          = './assets/modcats/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg'; 
		        $config['file_name']            = $id.'.png'; 
		        $config['overwrite']            = true; 
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		        $c = $this->upload->do_upload('userfile');
		        if (!$c){
		            $error = array('error' => $this->upload->display_errors());
		        }
			}
		}
		
		return $this->db->where("groupid", $id)->update("grouping", $data);
	}

	function addModuleToGroup($moduleid, $groupid){
		$b = $this->db->insert("modulegroup", array("moduleid" => $moduleid, "groupid" => $groupid));
		if ($b){
			return "success";
		} else {
			return "An error has occured during data insertion. Please contact an administrator.";
		}
	}

	function deleteGroupFromModule($moduleid, $groupid){
		$b = $this->db->where("moduleid", $moduleid)->where("groupid", $groupid)->delete("modulegroup");
		if ($b){
			return "success";
		} else {
			return "An error has occured during data deletion. Please contact an administrator.";
		}
	}

	function addModuleToCycle($moduleid, $cycleid, $visitnumber){
		$b = $this->db->insert("cyclemodules", array("moduleid" => $moduleid, "cycleid" => $cycleid, "visitnumber" => $visitnumber));
		if ($b){
			return "success";
		} else {
			return "An error has occured during data insertion. Please contact an administrator.";
		}
	}

	function deleteModuleFromCycle($moduleid, $cycleid, $visitnumber){
		$b = $this->db->where("moduleid", $moduleid)->where("cycleid", $cycleid)->where("visitnumber", $visitnumber)->delete("cyclemodules");
		if ($b){
			return "success";
		} else {
			return "An error has occured during data deletion. Please contact an administrator.";
		}
	}

	function getHCPSegments(){
		return $this->db->select("segmentid, segmentation")->get("segmentation")->result();
	}

	function getACCSegments(){
		return $this->db->select("segmentid, segmentation")->get("hospitalsegment")->result();
	}

	function getRepTypes(){
		return $this->db->select("typeid, typename")->get('reptype')->result();
	}

	function getHCPSpecialties(){
		return $this->db->select('specialtyid, specialtyname')->get("specialty")->result();
	}

	function getAllRooms(){
		return $this->db->select('roomid, roomname')->get("room")->result();
	}

	function getCycleDetail($id){
		$cycle = $this->db->where("cycleid", $id)->get("cycle")->row();
		$vt = $this->db->where("cycleid", $id)->get('cyclefiltervisittype');
		if ($vt->num_rows() > 0){
			$cycle->visittype = $vt->row()->visittype;
		} else {
			$cycle->visittype = "Both";
		}
		$cycle->accsegments = $this->db->where("cycleid", $id)->join('hospitalsegment ac', 'f.hospitalsegment = ac.segmentid')->get('cyclefilteraccsegment f')->result();
		$cycle->hcpsegments = $this->db->where("cycleid", $id)->join('segmentation ac', 'f.segmentid = ac.segmentid')->get('cyclefiltersegment f')->result();
		$cycle->reptypes = $this->db->where("cycleid", $id)->join('reptype', 'f.reptypeid = typeid')->get('cyclefilterreptype f')->result();
		$cycle->rooms = $this->db->where("cycleid", $id)->join('room', 'f.roomid = room.roomid')->get('cyclefilterroom f')->result();
		$cycle->specialties = $this->db->where("cycleid", $id)->join('specialty ac', 'f.specialtyid = ac.specialtyid')->get("cyclefilterspecialty f")->result();
		$cycle->modules = $this->db->where("cycleid", $id)->order_by("visitnumber", "asc")->join("contentmodules", "contentmodules.moduleid = cyclemodules.moduleid")->join("house h", "h.houseid = contentmodules.modulehouse")->get('cyclemodules')->result();
		return $cycle;
	}

	function createNewCycle($data, $visittype, $reptypes, $accsegments, $hcpsegments, $specs, $rooms){
		$b = $this->db->insert("cycle", $data);
		if ($b){
			$cycleid = $this->db->insert_id();
			$this->updateCycleVisitType($cycleid, $visittype);
			$this->updateCycleACCSegments($cycleid, $accsegments);
			$this->updateCycleHCPSegments($cycleid, $hcpsegments);
			$this->updateCycleSpecialty($cycleid, $specs);
			$this->updateCycleRepTypes($cycleid, $reptypes);
			$this->updateCycleRooms($cycleid, $rooms);
			return "success";
		} else {
			return "error";
		}
	}

	function editCycle($cycleid ,$data, $visittype, $reptypes, $accsegments, $hcpsegments, $specs, $rooms){
		$b = $this->db->where("cycleid", $cycleid)->update("cycle", $data);
		if ($b){
			$this->updateCycleVisitType($cycleid, $visittype);
			$this->updateCycleACCSegments($cycleid, $accsegments);
			$this->updateCycleHCPSegments($cycleid, $hcpsegments);
			$this->updateCycleSpecialty($cycleid, $specs);
			$this->updateCycleRepTypes($cycleid, $reptypes);
			$this->updateCycleRooms($cycleid, $rooms);
			return "success";
		} else {
			return "error";
		}
	}

	function updateCycleVisitType($id, $visittype){
		$vt = $this->db->where("cycleid", $id)->delete("cyclefiltervisittype");
		if ($vt){
			return $this->db->insert("cyclefiltervisittype", array("cycleid" => $id, "visittype" => $visittype));
		} else {
			return false;
		}
	}

	function updateCycleRepTypes($id, $reptypeArray){
		$vt = $this->db->where("cycleid", $id)->delete("cyclefilterreptype");
		if ($vt){
			if (empty($reptypeArray)){
				return true;
			}
			$reparr = array();
			foreach ($reptypeArray as $reptype){
				$reparr[] = array("cycleid" => $id, "reptypeid" => $reptype);
			}
			return $this->db->insert_batch("cyclefilterreptype", $reparr);
		} else {
			return false;
		}
	}

	function updateCycleACCSegments($id, $segmentArray){
		$vt = $this->db->where("cycleid", $id)->delete("cyclefilteraccsegment");
		if ($vt){
			if (empty($segmentArray)){
				return true;
			}
			$segarr = array();
			foreach ($segmentArray as $segmentid){
				$segarr[] = array("cycleid" => $id, "hospitalsegment" => $segmentid);
			}
			return $this->db->insert_batch("cyclefilteraccsegment", $segarr);
		} else {
			return false;
		}

	}

	function updateCycleRooms($id, $roomArray){
		$vt = $this->db->where("cycleid", $id)->delete("cyclefilterroom");
		if ($vt){
			if (empty($roomArray)){
				return true;
			}
			$roomarr = array();
			foreach ($roomArray as $roomid){
				$roomarr[] = array("cycleid" => $id, "roomid" => $roomid);
			}
			return $this->db->insert_batch("cyclefilterroom", $roomarr);
		} else {
			return false;
		}

	}

	function updateCycleHCPSegments($id, $segmentArray){
		$vt = $this->db->where("cycleid", $id)->delete("cyclefiltersegment");
		if ($vt){
			if (empty($segmentArray)){
				return true;
			}
			$segarr = array();
			foreach ($segmentArray as $segmentid){
				$segarr[] = array("cycleid" => $id, "segmentid" => $segmentid);
			}
			return $this->db->insert_batch("cyclefiltersegment", $segarr);
		} else {
			return false;
		}

	}

	function updateCycleSpecialty($id, $specialtyArray){
		$vt = $this->db->where("cycleid", $id)->delete("cyclefilterspecialty");
		if ($vt){
			if (empty($specialtyArray)){
				return true;
			}
			$specarr = array();
			foreach ($specialtyArray as $specid){
				$specarr[] = array("cycleid" => $id, "specialtyid" => $specid);
			}
			return $this->db->insert_batch("cyclefilterspecialty", $specarr);
		} else {
			return false;
		}
	}

	function togglePageObsolete($pageid){
		$pageid = $this->db->escape($pageid);
		$dt = date("Y-m-d H:i:s");
		$b = $this->db->query("UPDATE contentpages set obsolete = 1 - obsolete, lastupdated = '$dt' where pageid = $pageid");
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function toggleModuleObsolete($pageid){
		$pageid = $this->db->escape($pageid);
		$b = $this->db->query("UPDATE contentmodules set obsolete = 1 - obsolete where moduleid = $pageid");
		if ($b){
			return "success";
		} else {
			return false;
		}
	}
}
?>