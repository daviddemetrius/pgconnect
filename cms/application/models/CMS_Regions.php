<?php
class CMS_Regions extends CI_Model {
	function __construct()
	{
		parent::__construct();

	}

	function getTerritories(){
		return $this->db->get("territory")->result();
	}

	function getTerritorys(){
		return $this->db->get("territory")->result();
	}

	function getDistrictsForSearch(){
		return $this->db->select("districtid as id, '' as value, CONCAT(districtname) as label")->get("district d")->result();
	}

	function getAllTerritorysPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("territoryname, territoryid, masterid");
		$this->db->from("territory ei");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 0.5){ 
			$this->db->group_start();
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->or_where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->group_end();
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchTerritorys($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("territoryname, territoryid, masterid");
		$this->db->from("territory ei");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 0.5){ 
			$this->db->group_start();
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->or_where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->group_end();
		}
		$this->db->like("territoryname", $query)->or_like("masterid", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalTerritoryPages($limit){
		$this->db->from("territory ei");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 0.5){ 
			$this->db->group_start();
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->or_where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->group_end();
		}		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalTerritorysPages($limit){
		$this->db->from("territory ei");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 0.5){ 
			$this->db->group_start();
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->or_where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->group_end();
		}		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchTerritorysPages($limit, $query){
		$this->db->from("territory ei");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 0.5){ 
			$this->db->group_start();
			$this->db->where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->or_where("territoryid in", "(select territory from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->group_end();
		}		
		$this->db->like("territoryname", $query, "after");
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function createNewTerritorys($data){
		$b = $this->db->insert("territory", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getTerritoryDetails($id){
		$result = $this->db->select("territory.*, district.districtname")->where("territoryid", $id)->join("district", "district.districtid = territory.districtid", "left")->get("territory")->row();
		$result->specialists = $this->db->from("territoryassignment ta")->where("ta.territoryid", $id)->join("hospital h", "h.hospitalid = ta.hospitalid")->join("specialist sp", "sp.specid = ta.specid")->join("room r", "r.roomid = ta.roomid")->order_by("h.hospitalname", "asc")->get()->result();
		$result->accounts = $this->db->from("hospitalterritory ht")->where("ht.territory", $id)->join("hospital h", "h.hospitalid = ht.hospitalid")->order_by("h.hospitalname", "asc")->get()->result();
		$result->assignedto = $this->db->where("territory", $id)->get("representatives")->result();
		return $result;
	}

	function editTerritory($data, $id){
		$r = $this->db->where("masterid", $data['masterid'])->where("territoryid !=", $id)->get("territory");
		if ($r->num_rows()){
			return false;
		}
		return $this->db->where("territoryid", $id)->update("territory", $data);
	}

	function getProvinces(){
		return $this->db->get("province")->result();
	}

	function createNewProvinces($data){
		$b = $this->db->insert("province", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getProvinceDetails($id){
		$result = $this->db->where("provinceid", $id)->get("province")->row();
		return $result;
	}

	function editProvince($data, $id){
		return $this->db->where("provinceid", $id)->update("province", $data);
	}

	function getAllProvincesPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("provincename, provinceid");
		$this->db->from("province ei");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchProvinces($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("provincename, provinceid");
		$this->db->from("province ei");
		$this->db->like("provincename", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalProvincePages($limit){
		$this->db->from("province ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchProvincePages($limit, $query){
		$this->db->from("province ei");		
		$this->db->like("provincename", $query);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	
	function getCitys(){
		return $this->db->order_by("cityname", "asc")->get("city")->result();
	}

	function createNewCitys($data){
		$c = $this->db->where("cityid", $data['cityid'])->get('city');
		if ($c->num_rows()){
			return false;
		}
		$b = $this->db->insert("city", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getCityDetails($id){
		$result = $this->db->select("city.*, provincename")->where("cityid", $id)->join("province", "city.provinceid = province.provinceid")->get("city")->row();
		return $result;
	}

	function editCity($data, $id){
		$r = $this->db->where("cityid", $data['cityid'])->where("cityid !=", $id)->get("territory");
		if ($r->num_rows()){
			return false;
		}
		return $this->db->where("cityid", $id)->update("city", $data);
	}

	function getAllCitysPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("cityname, provincename, cityid");
		$this->db->from("city ei");
		$this->db->join("province pi", "pi.provinceid = ei.provinceid");
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchCitys($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("cityname, provincename, cityid");
		$this->db->from("city ei");
		$this->db->join("province pi", "pi.provinceid = ei.provinceid");
		$this->db->like("cityname", $query);
		$this->db->or_like("provincename", $query);
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalCityPages($limit){
		$this->db->from("city ei");		
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchCityPages($limit, $query){
		$this->db->from("city ei");		
		$this->db->join("province pi", "pi.provinceid = ei.provinceid");
		$this->db->like("cityname", $query);
		$this->db->like("provincename", $query);
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getDistrict(){
		return $this->db->get("district")->result();
	}

	function createNewDistrict($data){
		$b = $this->db->insert("district", $data);
		if ($b){
			return "success";
		} else {
			return false;
		}
	}

	function getDistrictDetails($id){
		$result = $this->db->where("districtid", $id)->get("district")->row();
		return $result;
	}

	function editDistrict($data, $id){
		return $this->db->where("districtid", $id)->update("district", $data);
	}

	function getAllDistrictsPaged($page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("districtname, districtid");
		$this->db->from("district ei");
		$this->db->join("city ci", "ci.cityid = ei.cityid", "left");
		$this->db->join("province pi", "pi.provinceid = ci.provinceid", "left");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->group_end();
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function searchDistricts($query, $page, $limit = 10, $sorting = FALSE){
		if ($sorting) $this->db->order_by($sorting);
		$this->db->select("districtname, districtid");
		$this->db->from("district ei");
		$this->db->join("city ci", "ci.cityid = ei.cityid", "left");
		$this->db->join("province pi", "pi.provinceid = ci.provinceid", "left");
		$this->db->like("cityname", $query, "after");
		$this->db->like("provincename", $query, "after");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->group_end();
		}
		$this->db->limit($limit, $limit * ($page-1));
		return $this->db->get()->result();
	}

	function getTotalDistrictPages($limit){
		$this->db->from("district ei");	
		if ($this->session->userdata("tier") == 2){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->group_end();
		}
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function getTotalSearchDistrictPages($limit, $query){
		$this->db->from("district ei");	
		$this->db->join("city ci", "ci.cityid = ei.cityid", "left");
		$this->db->join("province pi", "pi.provinceid = ci.provinceid", "left");
		$this->db->like("cityname", $query, "after");
		$this->db->like("provincename", $query, "after");
		if ($this->session->userdata("tier") == 2){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 1){ 
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))))", false);
		} else if ($this->session->userdata("tier") == 2.5){
			$this->db->group_start();
			$this->db->where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("districtid in", "(select district from representatives where repid in (select repid from dmreps where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
			$this->db->group_end();
		}			
		$c = $this->db->count_all_results();
		return ceil($c/$limit);
	}

	function deleteTerritoryAssignment($territoryid, $specid, $roomid, $hospitalid){
		$b = $this->db->where("hospitalid", $hospitalid)->where("specid", $specid)->where('roomid', $roomid)->where("territoryid", $territoryid)->delete("territoryassignment");
		if ($b){
			return "success";
		} else {
			return "Failed to delete assignment. Please try again later";
		}
	}

	function addTerritoryAssignment($territoryid, $specid, $roomid, $hospitalid){
		$b = $this->db->where("hospitalid", $hospitalid)->where("specid", $specid)->where('roomid', $roomid)->where("territoryid", $territoryid)->get("territoryassignment");
		if ($b->num_rows()){
			return "Failed to add assignment. That assignment already exists";
		} else {
			$b = $this->db->insert("territoryassignment", array("hospitalid" => $hospitalid, "territoryid" => $territoryid, "specid" => $specid, "roomid"=>$roomid));
			if ($b){
				return "success";
			} else {
				return "Failed to add assignment. Please try again later";
			}
			
		}
	}

	function refreshTerritoryAssignment($territoryid, $hospitalid){
		$b = $this->db->where("hospitalid", $hospitalid)->where("territoryid", $territoryid)->delete('territoryassignment');
		if ($b){
			$b = $this->db->query("INSERT into territoryassignment (territoryid, hospitalid, specid, roomid) SELECT $territoryid, hospitalid, specid, roomid from hospitalrooms where hospitalid = $hospitalid");
			if ($b){
				return "success";
			} else {
				return "Failed to refresh assignment. Please try again later";
			}
			
		} else {
			return "Failed to delete assignment while refreshing. Please try again later";
		}
	}

	function searchAcc($q, $territoryid){
		$exp = explode(" ", $q);
		$str = "";
		$delim = "";
		foreach ($exp as $e){
			$str .= "$delim (xmlaccid like '%$e%' or hospitalname like '%$e%')";
			$delim = "and ";
		}
		return $this->db->query("SELECT hospitalid as data, concat(xmlaccid, ' - ', hospitalname) as value from hospital where hospitalid in (SELECT hospitalid from hospitalterritory where territory = $territoryid) and ($str) limit 10")->result();
		// return $this->db->select("hospitalid as data, concat(xmlaccid, ' - ', hospitalname) as value")->group_start()->where("hospitalid >", 45348)->group_start()->like("xmlaccid", $q)->or_like("hospitalname", $q)->group_end()->group_end()->limit(10)->get("hospital")->result();
	}

	function searchHcp($q, $hospitalid = ""){
		$exp = explode(" ", $q);
		$str = "";
		$delim = "";
		foreach ($exp as $e){
			$str .= "$delim (xmlhcpid like '%$e%' or specialistname like '%$e%')";
			$delim = "and ";
		}
		return $this->db->query("SELECT sp.specid as data, r.roomid as roomid, concat(xmlhcpid, ' - ', roomname, ' - ', specialistname) as value from specialist sp JOIN hospitalrooms hr ON hr.specid = sp.specid and hr.hospitalid = $hospitalid JOIN room r on r.roomid = hr.roomid where (xmlhcpid like '%$q%'or specialistname like '%$q%')limit 10")->result();
	}
}
?>