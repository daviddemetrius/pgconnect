<?php
class CMS_Statistics extends CI_Model {
	function __construct()
	{
		parent::__construct();
	}

	function getModuleGroupSessionCount($month, $year){
		$sql = "SELECT COUNT(*) as count, mg.groupid, gr.groupname FROM modulestatistics ms JOIN modulegroup mg ON mg.moduleid = ms.moduleid JOIN grouping gr ON gr.groupid = mg.groupid WHERE YEAR(ms.accessdate) = $year AND MONTH(ms.accessdate) = $month GROUP BY mg.groupid order by count(*) desc limit 5";
		return $this->db->query($sql)->result();
	}

	function getActivePromoCount($month, $year){
		$currentdt = date("$year-$month-d 00:00:00");
		$sql = "SELECT COUNT(*) as count FROM contentmodules WHERE periodstart <= '$currentdt' AND periodend > '$currentdt' AND obsolete = 0 AND moduleid IN (SELECT moduleid FROM modulegroup where groupid IN (SELECT groupid from grouping WHERE groupname = 'Promo' or groupname = 'Promosi'))";
		return $this->db->query($sql)->row()->count;
	}

	function getOngoingPromoAccounts($month, $year){
		$currentdt = date("$year-$month-d 00:00:00");
		$sql = "SELECT h.hospitalid, h.hospitalname, SUM(IF(promomod.obsolete = 1, 1, IF(promomod.periodstart > '$currentdt', 1, IF(promomod.periodend <= '$currentdt', 1, 0)))) as inactive, COUNT(*) as total FROM (SELECT moduleid, periodstart, periodend, obsolete FROM contentmodules WHERE moduleid IN (SELECT moduleid FROM modulegroup where groupid IN (SELECT groupid from grouping WHERE groupname = 'Promo' or groupname = 'Promosi'))) promomod JOIN moduleaccounts ma ON ma.moduleid = promomod.moduleid JOIN hospital h ON h.hospitalid = ma.hospitalid GROUP BY h.hospitalid";

		return $this->db->query($sql)->result();
	}

	function getUsersPerType($month, $year){
		$sql = "SELECT SUM(IF(repid IS NOT NULL, 1, 0)) AS count, rt.typeid, rt.typename FROM reptype rt LEFT JOIN (SELECT repid, reptype FROM representatives WHERE YEAR(created) <= $year AND MONTH(created) <= $month) rp ON rp.reptype = rt.typeid GROUP BY rt.typeid";
		return $this->db->query($sql)->result();
	}

	function getLoggedInUsersPerType($month, $year){
		$sql = "SELECT SUM(IF(repid IS NOT NULL, 1, 0)) AS count, rt.typeid, rt.typename FROM reptype rt LEFT JOIN (SELECT repid, reptype FROM representatives WHERE YEAR(created) <= $year AND MONTH(created) <= $month AND lastpushupdated is not null) rp ON rp.reptype = rt.typeid GROUP BY rt.typeid";
		return $this->db->query($sql)->result();
	}

	function getActiveUsersPerType($month, $year){
		$sql = "SELECT SUM(IF(repid IS NOT NULL, 1, 0)) AS count, rt.typeid, rt.typename FROM reptype rt LEFT JOIN (SELECT repid, reptype FROM representatives WHERE YEAR(created) <= $year AND MONTH(created) <= $month AND (repid IN (SELECT DISTINCT(repid) FROM representativesession WHERE YEAR(sessiondate) = $year AND MONTH(sessiondate) = $month) OR repid IN (SELECT DISTINCT(repid) FROM surveysession WHERE YEAR(sessiondate) = $year AND MONTH(sessiondate) = $month))) rp ON rp.reptype = rt.typeid GROUP BY rt.typeid";
		return $this->db->query($sql)->result();
	}

	function getFrequentModuleAccessPerType($month, $year){
		$sql = "SELECT rt.typeid, rt.typename, stats.moduleid, cm.moduletitle, MAX(modulefreq) AS topfrequency FROM reptype rt LEFT JOIN (SELECT reptype, moduleid, COUNT(*) AS modulefreq FROM representativesession rs JOIN modulestatistics ms ON ms.schedid = rs.sessionid JOIN representatives rp ON rp.repid = rs.repid WHERE MONTH(rs.sessiondate) = $month AND YEAR(rs.sessiondate) = $year GROUP BY reptype, moduleid) stats ON rt.typeid = stats.reptype LEFT JOIN contentmodules cm ON cm.moduleid = stats.moduleid GROUP BY rt.typeid";
		return $this->db->query($sql)->result();
	}

	function getUserActivityRank($month, $year){
		$sql = "SELECT rp.repid, rp.repname, rt.typename, accumulatedtime, quizcount FROM representatives rp LEFT JOIN (SELECT SUM(totalduration) AS accumulatedtime, repid FROM representativesession WHERE YEAR(sessiondate) = $year AND MONTH(sessiondate) = $month GROUP BY repid ) stats ON stats.repid = rp.repid LEFT JOIN (SELECT COUNT(*) AS quizcount, repid FROM surveysession WHERE YEAR(sessiondate) = $year AND MONTH(sessiondate) = $month GROUP BY repid ) quiz ON quiz.repid = rp.repid JOIN reptype rt ON rt.typeid = rp.reptype ORDER BY accumulatedtime DESC, quizcount DESC";
		return $this->db->query($sql)->result();
	}

	function getQuizTakersCount($month, $year){
		$currentdt = date("$year-$month-d 00:00:00");
		$sql = "SELECT cycletitle, cycleid, frequency, reptype FROM closingsurveycycle csc LEFT JOIN (SELECT COUNT(*) AS frequency, quizcycleid, reptype FROM surveysession ss JOIN representatives rp ON ss.repid = rp.repid GROUP BY reptype, quizcycleid) stats ON stats.quizcycleid = csc.cycleid WHERE csc.periodstart <= '$currentdt' AND csc.periodend >= '$currentdt' AND obsolete = 0";
		return $this->db->query($sql)->result();
	}

	function getModuleTopHits($month, $year){
		$sql = "SELECT DATE(accessdate) AS dateaccess, ms.moduleid, SUM(accesstime) AS totalaccess, COUNT(*) AS totalsession, cm.moduletitle FROM modulestatistics ms JOIN contentmodules cm ON cm.moduleid = ms.moduleid WHERE YEAR(accessdate) = $year AND MONTH(accessdate) = $month GROUP BY ms.moduleid, DATE(accessdate) ORDER BY totalsession DESC, totalaccess DESC LIMIT 10;";
		return $this->db->query($sql)->result();
	}

	function getAllDanoneProductITR($month, $year){
		$addlsql = "";
		if ($this->session->userdata("tier") <= 2.5){
			$addlsql = "AND hos.hospitalid in ".$this->session->userdata("hospitals")."";
		}
		
		$sql = "SELECT product.*, h.*, ho.*, hc.*, itrcount FROM product LEFT JOIN (SELECT productid, COUNT(distinct xmlaccid) AS itrcount FROM closingdataitr ci JOIN closingdataitrproducts cip on ci.itrid = cip.itrid JOIN hospital hos on hos.hospitalid = ci.hospitalid WHERE foryear = $year $addlsql AND formonth = $month AND intention = 1 GROUP BY productid) lj ON lj.productid = product.productid LEFT JOIN housecategorization h ON h.hcid = product.house LEFT JOIN house ho ON ho.houseid = h.houseid LEFT JOIN housecategory hc ON hc.categoryid = h.categoryid WHERE danoneflag = 1 and obsolete = 0";
		return $this->db->query($sql)->result();
	}

	function getAllProductsShare($month, $year){
		$addlsql = "";
		if ($this->session->userdata("tier") <= 2.5){
			$addlsql = "AND cs.hospitalid in ".$this->session->userdata("hospitals")."";
		}
		$sql = "SELECT product.*, h.*, ho.*, hc.*, birthcount, pedscount FROM product LEFT JOIN (SELECT productid, SUM(birth) AS birthcount, SUM(peds) as pedscount FROM closingdatashare cs JOIN closingdatashareproducts csp on cs.shareid = csp.shareid WHERE foryear = $year $addlsql AND formonth = $month GROUP BY productid) lj ON lj.productid = product.productid LEFT JOIN housecategorization h ON h.hcid = product.house LEFT JOIN house ho ON ho.houseid = h.houseid LEFT JOIN housecategory hc ON hc.categoryid = h.categoryid where obsolete = 0";
		return $this->db->query($sql)->result();
	}

	function getProductITRACC($foryear, $formonth, $productid){
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->where("itrid in", "(select itrid from closingdataitrproducts where productid = $productid and intention = 1)", false);
		$this->db->join("hospital h", "h.hospitalid = i.hospitalid");
		$this->db->join("hospitalsegment hs", "hs.segmentid = h.segmentid");
		$this->db->join("territory t", "territoryid = territory");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("h.hospitalid in", $this->session->userdata("hospitals"), false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("h.hospitalid in ", $this->session->userdata("hospitals"), false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("h.hospitalid in ", $this->session->userdata("hospitals"), false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("h.hospitalid in ", $this->session->userdata("hospitals"), false);
		}
		// $this->db->group_by("i.hospitalid");
		return $this->db->get("closingdataitr i")->result();
	}

	function getProductShareACC($foryear, $formonth, $productid){
		$this->db->where("foryear", $foryear);
		$this->db->where("formonth", $formonth);
		$this->db->join("closingdatashareproducts csp", "csp.shareid = i.shareid");
		$this->db->where("productid", $productid);
		$this->db->group_start();
		$this->db->where("birth > ", 0);
		$this->db->or_where("peds > ", 0);
		$this->db->group_end();
		$this->db->join("hospital h", "h.hospitalid = i.hospitalid");
		$this->db->join("hospitalsegment hs", "hs.segmentid = h.segmentid");
		$this->db->join("territory t", "territoryid = territory");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("h.hospitalid in", $this->session->userdata("hospitals"), false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("h.hospitalid in ", $this->session->userdata("hospitals"), false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("h.hospitalid in ", $this->session->userdata("hospitals"), false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("h.hospitalid in ", $this->session->userdata("hospitals"), false);
		}
		return $this->db->get("closingdatashare i")->result();
	}

	function getUploaderInfo($uploaderid){
		$this->db->where("repid", $uploaderid);
		$rep = $this->db->get("representatives")->row();
		return $rep->repname." - ".$rep->xmlnacd;
	}

	function rsGetAccCoverage($territoryid){
		return $this->db->query("SELECT hsm.segmentation as title, count(*) as value from hospital h join hospitalsegment hsm on h.segmentid = hsm.segmentid where h.hospitalid in (select tsm.hospitalid from territoryassignment tsm where tsm.territoryid = $territoryid) group by h.segmentid ")->result();
	}

	function rsGetHcpCoverage($territoryid){
		return $this->db->query("SELECT hsm.specialtyname as title, count(*) as value from specialist h join specialty hsm on h.specialty = hsm.specialtyid where h.specid in (select tsm.specid from territoryassignment tsm where tsm.territoryid = $territoryid) group by h.specialty ")->result();
	}

	function rsGetPlanEffectiveness($repid, $month = null, $year = null){
		$this->db->from("representativeschedule rs");
		$this->db->join("actualvisit av", "rs.schedid = av.schedid", "left");
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}
		$this->db->where("repid", $repid);
		$this->db->select("SUM(if(rs.approved = 1, if(rs.deleted = 0, 1, 0), 0)) as approvedcount, SUM(if(rs.deleted = 1, 1, 0)) as deletedcount, SUM(if(rs.deleted = 1, 0, if(rs.visited = 1, if(month(rs.createddate) >= month(rs.scheddate), if(rs.approved = 0, 1, 0), 0), 0))) as adhoccount, SUM(if(rs.deleted = 1, 0, if(rs.approved = 0, 0, if(rs.visited = 1, if(date(rs.scheddate) != date(av.visittime), 1, 0), 0)))) as rescheduled, SUM(if(rs.visited = 0, if(rs.deleted = 0, 1, 0), 0)) as missed, SUM(if(rs.approved = 0, if(rs.visited = 0, 1, 0), 0)) as unapproved, SUM(if(rs.approved = 1, if(rs.deleted = 0, if(rs.visited = 1, 1, 0), 0), 0)) as approvedvisitcount, SUM(if(rs.visited = 1, if(rs.deleted = 0, 1, 0), 0)) as totalvisitcount", false);
		$r = $this->db->get()->row();
		// echo $this->db->last_query();
		// var_dump($r);
		return $r;
	}

	function rsGetModuleStatistics($repid, $month = null, $year = null){
		$this->db->from("representativeschedule rs");
		$this->db->join("modulestatistics ms", "rs.schedid = ms.schedid");
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}
		$this->db->group_by("ms.moduleid");
		$this->db->where("repid", $repid);
		$this->db->select("ms.moduleid, moduletitle, SUM(if(accesstime >= 5, accesstime, 0)) as totalduration, SUM(if(accesstime >= 5, 1, 0)) as frequency", false);
		$this->db->join("contentmodules cm", "cm.moduleid = ms.moduleid");
		$r = $this->db->get()->result();
		// echo json_encode($r);
		return $r;
	}

	function rsGetAccReach($repid, $month = null, $year = null, $territoryid){
		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(hospitalid)) as hospcount");
		$r = $this->db->get()->row()->hospcount;

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		$this->db->where("visited", 1);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(hospitalid)) as hospcount");
		$s = $this->db->get()->row()->hospcount;

		$t = $this->db->query("SELECT count(distinct(tsm.hospitalid)) as hospcount from territoryassignment tsm where tsm.territoryid = $territoryid")->row()->hospcount;
		// var_dump($t);
		return array("covered" => $t, "planned" => $r, "visited" => $s);
	}

	function rsGetPDHcpReach($repid, $month = null, $year = null, $territoryid){
		$t = $this->db->query("SELECT count(distinct(tsm.specid)) as hospcount from territoryassignment tsm join specialist sp on sp.specid = tsm.specid and sp.specialty=1 where tsm.territoryid = $territoryid")->row()->hospcount;

		// var_dump($t);
		if ($t == 0){
			return array("covered" => $t, "planned" => 0, "visited" => 0);
		}

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
		$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 1");
		$r = $this->db->get()->row()->hospcount;

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		$this->db->where("visited", 1);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
		$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 1");
		$s = $this->db->get()->row()->hospcount;

		// var_dump($t);
		return array("covered" => $t, "planned" => $r, "visited" => $s);
	}

	function rsGetMWHcpReach($repid, $month = null, $year = null, $territoryid){
		$t = $this->db->query("SELECT count(distinct(tsm.specid)) as hospcount from territoryassignment tsm join specialist sp on sp.specid = tsm.specid and sp.specialty=2 where tsm.territoryid = $territoryid")->row()->hospcount;

		// var_dump($t);
		if ($t == 0){
			return array("covered" => $t, "planned" => 0, "visited" => 0);
		}

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
		$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 2");
		$r = $this->db->get()->row()->hospcount;

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		$this->db->where("visited", 1);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
		$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 2");
		$s = $this->db->get()->row()->hospcount;

		// var_dump($t);
		return array("covered" => $t, "planned" => $r, "visited" => $s);
	}

	function rsGetOTHcpReach($repid, $month = null, $year = null, $territoryid){
		$t = $this->db->query("SELECT count(distinct(tsm.specid)) as hospcount from territoryassignment tsm join specialist sp on sp.specid = tsm.specid and sp.specialty not in (2,1) where tsm.territoryid = $territoryid")->row()->hospcount;

		// var_dump($t);
		if ($t == 0){
			return array("covered" => $t, "planned" => 0, "visited" => 0);
		}

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
		$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty  not in (2,1)");
		$r = $this->db->get()->row()->hospcount;

		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		$this->db->where("visited", 1);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
		$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty not in (2,1)");
		$s = $this->db->get()->row()->hospcount;

		// var_dump($t);
		return array("covered" => $t, "planned" => $r, "visited" => $s);
	}

	function rsGet1on1Data($repid, $month = null, $year = null){
		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->where("type", "1On1");
		$this->db->where("visited", 1);
		$this->db->where("deleted", 0);
		$this->db->select("COUNT(DISTINCT(ms.schedid)) as schedules, COUNT(DISTINCT(specid)) as specialists");

		return $this->db->get()->row();
	}

	function rsGetGroupData($repid, $month = null, $year = null){
		$this->db->from("representativeschedule rs");
		$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}		
		$this->db->where("type", "Group");
		$this->db->where("visited", 1);
		$this->db->where("deleted", 0);
		$this->db->select("COUNT(DISTINCT(ms.schedid)) as schedules, COUNT(DISTINCT(specid)) as specialists");

		return $this->db->get()->row();
	}

	function rsGetFullDays($repid, $month = null, $year = null){
		$this->db->from("representativeschedule rs");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}
		$this->db->where("visited", 1);
		$this->db->where("deleted", 0);		
		$this->db->select("date(scheddate) as fulldays", false);
		$this->db->having("count(*) >", 6);
		$this->db->group_by("DATE(scheddate)");
		return $this->db->count_all_results();
	}

	function rsGetHalfDays($repid, $month = null, $year = null){
		$this->db->from("representativeschedule rs");
		$this->db->where("repid", $repid);
		
		if ($month != null && $year != null){
			$startmo = sprintf("%02d", $month);
			$starty = $year;
			$startdate = $starty."-".$startmo."-01 00:00:00";
			$nextmo = sprintf("%02d", ($month+1));
			$nexty = $year;
			if ($month == 12){
				$nextmo = "01";
				$nexty = $year+1;
			}
			$nextdate = $nexty."-".$nextmo."-01 00:00:00";
			$this->db->where("scheddate >=", $startdate);
			$this->db->where("scheddate <", $nextdate);
		}
		$this->db->where("visited", 1);
		$this->db->where("deleted", 0);		
		$this->db->select("date(scheddate) as fulldays", false);
		$this->db->having("count(*) <=", 6);
		$this->db->group_by("DATE(scheddate)");
		return $this->db->count_all_results();
	}

	function getAllRepsUnder(){
		$this->db->from("representatives ei");
		if ($this->session->userdata("tier") == 2.5){
			$this->db->where("ei.repid in", "(select dre.repid from dmreps dre where userid in (select dmid from rmdm where rmid in(select dmid from rmdm where rmid = ".$this->session->userdata("user_id").")))", false);
		} else if ($this->session->userdata("tier") == 2){
			$this->db->where("ei.repid in", "(select dre.repid from dmreps dre where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
		} else if ($this->session->userdata("tier") == 1){
			$this->db->where("ei.repid in", "(select dre.repid from dmreps dre where userid = ".$this->session->userdata("user_id").")", false);
		} else if ($this->session->userdata("tier") == 0.5){
			$this->db->where("ei.repid in", "(select dre.repid from dmreps dre where userid in (select dmid from rmdm where rmid = ".$this->session->userdata("user_id")."))", false);
			$this->db->or_where("ei.repid in", "(select dre.repid from dmreps dre where userid = ".$this->session->userdata("user_id").")", false);
		}
		$this->db->join("dmreps dr", "ei.repid = dr.repid");
		$this->db->order_by("dr.userid");
		$this->db->join("cmsuser cu", "cu.userid = dr.userid");
		$this->db->join("reptype", "reptype = typeid", "left");
		$this->db->join("territory", "territory = territoryid", "left");
		return $this->db->get()->result();
	}


	function dmGetAccCoverage($territoryid){
		return $this->db->query("SELECT ntb.territoryid, t.masterid, h.segmentid, hsm.segmentation as title, count(distinct(h.hospitalid)) as value from hospital h join hospitalsegment hsm on h.segmentid = hsm.segmentid join (select tsm.hospitalid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.hospitalid = h.hospitalid join territory t on t.territoryid = ntb.territoryid group by ntb.territoryid, h.segmentid order by masterid")->result();
	}

	function dmGetHcpCoverage($territoryid){
		//$r = $this->db->query("SELECT * from specialty hsm left join (SELECT h.specialty, count(distinct(h.specid)) as value from specialist h join (select tsm.specid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.specid = h.specid group by h.specialty) hsl on hsl.specialty = hsm.specialtyid order by hsm.specialtyid")->result();
		$r = $this->db->query("SELECT ntb.territoryid, t.masterid, h.specialty, hsm.specialtyname as title, count(distinct(h.specid)) as value from specialist h join specialty hsm on h.specialty = hsm.specialtyid join (select tsm.specid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.specid = h.specid join territory t on t.territoryid = ntb.territoryid group by ntb.territoryid, h.specialty order by masterid")->result();
		// echo $this->db->last_query();
		return $r;
	}


	function dmGetUniqueAccCoverage($territoryid){
		$r = $this->db->query("SELECT * from hospitalsegment hsm left join (SELECT h.segmentid, count(distinct(h.hospitalid)) as value from hospital h join (select tsm.hospitalid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.hospitalid = h.hospitalid group by h.segmentid) hsl on hsl.segmentid = hsm.segmentid order by hsm.segmentid")->result();
		//$r = $this->db->query("SELECT h.segmentid, hsm.segmentation as title, count(distinct(h.hospitalid)) as value from hospital h join hospitalsegment hsm on h.segmentid = hsm.segmentid join (select tsm.hospitalid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.hospitalid = h.hospitalid group by h.segmentid order by hsm.segmentid")->result();
		return $r;
	}

	function dmGetUniqueHcpCoverage($territoryid){
		$r = $this->db->query("SELECT * from specialty hsm left join (SELECT h.specialty, count(distinct(h.specid)) as value from specialist h join (select tsm.specid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.specid = h.specid group by h.specialty) hsl on hsl.specialty = hsm.specialtyid order by hsm.specialtyid")->result();
		//$r = $this->db->query("SELECT h.specialty, hsm.specialtyname as title, count(distinct(h.specid)) as value from specialist h join specialty hsm on h.specialty = hsm.specialtyid join (select tsm.specid, tsm.territoryid from territoryassignment tsm where tsm.territoryid in $territoryid) ntb on ntb.specid = h.specid group by h.specialty order by hsm.specialtyid")->result();
		// echo $this->db->last_query();
		return $r;
	}

	function dmGetPlanEffectiveness($repArray, $month = null, $year = null){
		foreach ($repArray as $r){
			$this->db->from("representativeschedule rs");
			$this->db->join("actualvisit av", "rs.schedid = av.schedid", "left");
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}
			$this->db->where("repid", $r->repid);
			$this->db->select("SUM(if(rs.approved = 1, if(rs.deleted = 0, 1, 0), 0)) as approvedcount, SUM(if(rs.deleted = 1, 1, 0)) as deletedcount, SUM(if(rs.deleted = 1, 0, if(rs.visited = 1, if(month(rs.createddate) >= month(rs.scheddate), if(rs.approved = 0, 1, 0), 0), 0))) as adhoccount, SUM(if(rs.deleted = 1, 0, if(rs.approved = 0, 0, if(rs.visited = 1, if(date(rs.scheddate) != date(av.visittime), 1, 0), 0)))) as rescheduled, SUM(if(rs.visited = 0, if(rs.deleted = 0, 1, 0), 0)) as missed, SUM(if(rs.approved = 0, if(rs.visited = 0, 1, 0), 0)) as unapproved, SUM(if(rs.approved = 1, if(rs.deleted = 0, if(rs.visited = 1, 1, 0), 0), 0)) as approvedvisitcount, SUM(if(rs.approved = 0, if(rs.deleted = 0, if(rs.visited = 1, 1, 0), 0), 0)) as unapprovedvisitcount, SUM(if(rs.visited = 1, if(rs.deleted = 0, 1, 0), 0)) as totalvisitcount", false);
			$r->effectiveness = $this->db->get()->row();
		}
		// echo $this->db->last_query();
		// var_dump($r);
		return $repArray;
	}


	function dmGetAccReach($repArray, $month = null, $year = null){
		$ra = $repArray;
		foreach ($ra as $r){
			$repid = $r->repid;
			$territoryid = $r->territory;
			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(hospitalid)) as hospcount");
			$hc = $this->db->get()->row()->hospcount;

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			$this->db->where("visited", 1);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(hospitalid)) as hospcount");
			$s = $this->db->get()->row()->hospcount;

			$t = $this->db->query("SELECT count(distinct(tsm.hospitalid)) as hospcount from territoryassignment tsm where tsm.territoryid = $territoryid")->row()->hospcount;
			// var_dump($t);
			$r->reach = array("covered" => $t, "planned" => $hc, "visited" => $s);
		}
		return $ra;
	}

	function dmGetPDHcpReach($repArray, $month = null, $year = null){
		$ra = $repArray;
		foreach ($ra as $r){
			$repid = $r->repid;
			$territoryid = $r->territory;
			$t = $this->db->query("SELECT count(distinct(tsm.specid)) as hospcount from territoryassignment tsm join specialist sp on sp.specid = tsm.specid and sp.specialty=1 where tsm.territoryid = $territoryid")->row()->hospcount;

			// var_dump($t);
			if ($t == 0){
				$r->pdhcpreach = array("covered" => $t, "planned" => 0, "visited" => 0);
				continue;
			}

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
			$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 1");
			$hc = $this->db->get()->row()->hospcount;

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			$this->db->where("visited", 1);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
			$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 1");
			$s = $this->db->get()->row()->hospcount;

			// var_dump($t);
			$r->pdhcpreach = array("covered" => $t, "planned" => $hc, "visited" => $s);
		}
		return $ra;
	}

	function dmGetMWHcpReach($repArray, $month = null, $year = null){
		$ra = $repArray;
		foreach ($ra as $r){
			$repid = $r->repid;
			$territoryid = $r->territory;
			$t = $this->db->query("SELECT count(distinct(tsm.specid)) as hospcount from territoryassignment tsm join specialist sp on sp.specid = tsm.specid and sp.specialty=2 where tsm.territoryid = $territoryid")->row()->hospcount;

			// var_dump($t);
			if ($t == 0){
				$r->mwhcpreach = array("covered" => $t, "planned" => 0, "visited" => 0);
				continue;
			}

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
			$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 2");
			$hc = $this->db->get()->row()->hospcount;

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			$this->db->where("visited", 1);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
			$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty = 2");
			$s = $this->db->get()->row()->hospcount;

			// var_dump($t);
			$r->mwhcpreach = array("covered" => $t, "planned" => $hc, "visited" => $s);
		}
		return $ra;
	}

	function dmGetOTHcpReach($repArray, $month = null, $year = null){
		$ra = $repArray;
		foreach ($ra as $r){
			$repid = $r->repid;
			$territoryid = $r->territory;
			$t = $this->db->query("SELECT count(distinct(tsm.specid)) as hospcount from territoryassignment tsm join specialist sp on sp.specid = tsm.specid and sp.specialty not in (2,1) where tsm.territoryid = $territoryid")->row()->hospcount;

			// var_dump($t);
			if ($t == 0){
				$r->othcpreach = array("covered" => $t, "planned" => 0, "visited" => 0);
				continue;
			}

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
			$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty  not in (2,1)");
			$hc = $this->db->get()->row()->hospcount;

			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			$this->db->where("visited", 1);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->select("COUNT(DISTINCT(ms.specid)) as hospcount");
			$this->db->join("specialist sp", "sp.specid = ms.specid and sp.specialty not in (2,1)");
			$s = $this->db->get()->row()->hospcount;

			$r->othcpreach = array("covered" => $t, "planned" => $hc, "visited" => $s);
		}
		return $ra;
	}


	function dmGet1on1Data($repArray, $month = null, $year = null){
		foreach ($repArray as $rep){
			$repid = $rep->repid;
			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->where("type", "1On1");
			$this->db->where("visited", 1);
			$this->db->where("deleted", 0);
			$this->db->select("COUNT(DISTINCT(ms.schedid)) as schedules, COUNT(DISTINCT(specid)) as specialists");

			$rep->data1on1 = $this->db->get()->row();
		}
		return $repArray;
	}

	function dmGetGroupData($repArray, $month = null, $year = null){
		foreach ($repArray as $rep){
			$repid = $rep->repid;
			$this->db->from("representativeschedule rs");
			$this->db->join("schedulevisit ms", "rs.schedid = ms.schedid");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}		
			$this->db->where("type", "Group");
			$this->db->where("visited", 1);
			$this->db->where("deleted", 0);
			$this->db->select("COUNT(DISTINCT(ms.schedid)) as schedules, COUNT(DISTINCT(specid)) as specialists");

			$rep->datagroup = $this->db->get()->row();
		}
		return $repArray;
	}

	function dmGetFullDays($repArray, $month = null, $year = null){
		foreach ($repArray as $rep){
			$repid = $rep->repid;
			$this->db->from("representativeschedule rs");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}
			$this->db->where("visited", 1);
			$this->db->where("deleted", 0);		
			$this->db->select("date(scheddate) as fulldays", false);
			$this->db->having("count(*) >", 6);
			$this->db->group_by("DATE(scheddate)");
			$rep->fulld = $this->db->count_all_results();
		}
		return $repArray;
	}

	function dmGetHalfDays($repArray, $month = null, $year = null){
		foreach ($repArray as $rep){
			$repid = $rep->repid;
			$this->db->from("representativeschedule rs");
			$this->db->where("repid", $repid);
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}
			$this->db->where("visited", 1);
			$this->db->where("deleted", 0);		
			$this->db->select("date(scheddate) as fulldays", false);
			$this->db->having("count(*) <=", 6);
			$this->db->group_by("DATE(scheddate)");
			$rep->halfd = $this->db->count_all_results();
		}
		return $repArray;
	}

	function dmGetModuleStatistics($repArray, $month = null, $year = null){
		foreach ($repArray as $rep){
			$repid = $rep->repid;
			$this->db->from("representativeschedule rs");
			$this->db->join("modulestatistics ms", "rs.schedid = ms.schedid");
			
			if ($month != null && $year != null){
				$startmo = sprintf("%02d", $month);
				$starty = $year;
				$startdate = $starty."-".$startmo."-01 00:00:00";
				$nextmo = sprintf("%02d", ($month+1));
				$nexty = $year;
				if ($month == 12){
					$nextmo = "01";
					$nexty = $year+1;
				}
				$nextdate = $nexty."-".$nextmo."-01 00:00:00";
				$this->db->where("scheddate >=", $startdate);
				$this->db->where("scheddate <", $nextdate);
			}
			$this->db->group_by("ms.moduleid");
			$this->db->where("repid", $repid);
			$this->db->select("ms.moduleid, moduletitle, SUM(if(accesstime >= 5, accesstime, 0)) as totalduration, SUM(if(accesstime >= 5, 1, 0)) as frequency", false);
			$this->db->join("contentmodules cm", "cm.moduleid = ms.moduleid");
			$r = $this->db->get()->result();
			// echo json_encode($r);
			// return $r;
			$rep->modulestat = $r;
		}
		return $repArray;
	}
}