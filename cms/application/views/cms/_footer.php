<!--footer start-->
<footer class="site-footer">
  <div class="text-center">
    <?php echo date('Y'); ?> &copy; Imaxxo
   <a href="#" class="go-top">
    <i class="icon-angle-up"></i>
  </a>
</div>
</footer>
<!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/respond.min.js" ></script>
<script src="<?php echo base_url(); ?>js/sweetalert2.all.min.js" ></script>
<!--common script for all pages-->
<script src="<?php echo base_url(); ?>js/common-scripts.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  <?php
  if (isset($SwalMsg)) {
    if (!isset($SwalType)){
      $SwalType = "success";
    }
    if (!isset($SwalTitle)){
      $SwalTitle = $this->config->item("SweetAlertHeader");
    }
    echo 'swal("'.$SwalTitle.'", "'.$SwalMsg.'", "'.$SwalType.'");';
  }
  ?>
});

function defaultppagent(e){
  $(e).attr("src", "<?=base_url()?>/images/img-profile-default.png")
}

function defaultppproperty(e){
  $(e).attr("src", "<?=base_url()?>/images/img-property-default.png")
}

function defaultppsupplier(e){
  $(e).attr("src", "<?=base_url()?>/images/img-supplier-default.png")
}
</script>

</body>
</html>
