<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png">

    <title><?php echo $this->config->item("CMSHeaderPrefix");
      if (isset($HeaderSuffix)){
        echo " | ".$HeaderSuffix;
      }
      ?>
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>css/googleapisfont.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/chartist.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/MAIN.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/sweetalert2.css" rel="stylesheet">
  </head>

  <body>
<?php
		$this->session->set_userdata('referred_from', current_url()); //for redirect
?>
  <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
          <div class="sidebar-toggle-box">
              <div data-original-title="Toggle Navigation" data-placement="right" class="glyphicon glyphicon-th tooltips pointer" onclick='togglenav()'></div>
          </div>
          <!--logo start-->
          <a href="<?=site_url('cms/dashboard');?>" class="logo"><img src='<?=base_url()."/images/logo.png"?>'/></a>
          <!--logo end-->
          
          <div class="top-nav ">
              <ul class="nav pull-right top-menu">
              	 <li><a href="<?php echo site_url('cms/accounts/admin/'.$this->session->userdata("user_id")); ?>"><i class="glyphicon glyphicon-user"></i> <span class='hidden-xs'><?php echo $this->session->userdata("name");?></span></a></li>
                 <li><a href="<?php echo site_url('cms/dashboard/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i> <span class='hidden-xs'>Logout</span></a></li>
                 
              </ul>
          </div>
      </header>
      <!--header end-->