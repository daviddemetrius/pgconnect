<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <li>
        <a class="s_HmBrd" href="<?php echo site_url("cms");?>">
          <i class="glyphicon glyphicon-home right-sm"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <?php if (in_array($this->session->userdata("tier"), array(999, 3))){ ?>
      <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#userdropdown" class="userdropdown collapsed" aria-expanded="false"><i class="glyphicon glyphicon-user right-sm"></i>Users</a>
        <ul id="userdropdown" class="collapse" aria-expanded="false">
          <?php
          if (in_array($this->session->userdata("tier"), array(999))){
          ?>
          <li>
            <a class="ad_admin" href="<?php echo site_url('cms/accounts/admin'); ?>"><i class="glyphicon glyphicon-user right-sm"></i>Administrators</a>
          </li>
          <?php /*
          <li class='hidden'>
            <a class="ad_reps" href="<?php echo site_url('cms/accounts/reps'); ?>"><i class="glyphicon glyphicon-th-large right-sm"></i>Representatives</a>
          </li>
          <li class='hidden'>
            <a class="ad_rept" href="<?php echo site_url('cms/accounts/reptype'); ?>"><i class="glyphicon glyphicon-th-list right-sm"></i>Rep Type</a>
          </li>
          <?php */} ?>
          <?php
          if (false){//in_array($this->session->userdata("tier"), array(999, 3))){
          ?>
          <li>
            <a class="ad_messager" href="<?php echo site_url('cms/accounts/messaging'); ?>"><i class="glyphicon glyphicon-envelope right-sm"></i>Messaging</a>
          </li>
          <?php
          }
          ?>
        </ul>
      </li>
      <?php } ?>
      <?php //if (in_array($this->session->userdata("tier"), array(999))){ ?>
      <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#datadropdown" class="datadropdown collapsed" aria-expanded="false"><i class="glyphicon glyphicon-file right-sm"></i>Master Data</a>
        <ul id="datadropdown" class="collapse" aria-expanded="false">
          <li>
            <a class="dd_evts" href="<?php echo site_url('cms/masterdata/events') ?>"><i class="glyphicon glyphicon-file right-sm"></i>Events</a>
          </li>
          <li>
            <a class="dd_docs" href="<?php echo site_url('cms/masterdata/specialists'); ?>"><i class="glyphicon glyphicon-user right-sm"></i>Doctors</a>
          </li>
          <li class='hidden'>
            <a class="dd_hchannel" href="<?php echo site_url('cms/masterdata/accchannel'); ?>"><i class="glyphicon glyphicon-align-center right-sm"></i>ACC Channels</a>
          </li>
        </ul>
      </li>
      <?php //} ?>
      <?php
      if (false){//in_array($this->session->userdata("tier"), array(999))){
      ?>
      <li class='hidden'>
        <a href="javascript:;" data-toggle="collapse" data-target="#branddropdown" class="branddropdown collapsed" aria-expanded="false"><i class="glyphicon glyphicon-picture right-sm"></i>Brand</a>
        <ul id="branddropdown" class="collapse" aria-expanded="false">
          <li>
            <a class="bd_house" href="<?php echo site_url('cms/brand/house'); ?>"><i class="glyphicon glyphicon-home right-sm"></i>House</a>
          </li>
        </ul>
      </li>
      <li class='hidden'>
        <a href="javascript:;" data-toggle="collapse" data-target="#territorydropdown" class="territorydropdown collapsed" aria-expanded="false"><i class="glyphicon glyphicon-globe right-sm"></i>Region</a>
        <ul id="territorydropdown" class="collapse" aria-expanded="false">
          <li>
            <a class="td_prov" href="<?php echo site_url('cms/regions/province'); ?>"><i class="glyphicon glyphicon-map-marker right-sm"></i>Region</a>
          </li>
          <li>
            <a class="td_city" href="<?php echo site_url('cms/regions/city'); ?>"><i class="glyphicon glyphicon-pushpin right-sm"></i>Area</a>
          </li>
        </ul>
      </li>
      <?php } ?>
    <?php if (false){//in_array($this->session->userdata("tier"), array(999, 2, 3))){ ?>
      <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#moduledropdown" class="moduledropdown collapsed" aria-expanded="false"><i class="glyphicon glyphicon-briefcase right-sm"></i>Modules</a>
        <ul id="moduledropdown" class="collapse" aria-expanded="false">
          <?php
          if (in_array($this->session->userdata("tier"), array(999, 2))){
          ?>
          <li>
            <a class="md_pages" href="<?php echo site_url('cms/modules/pages'); ?>"><i class="glyphicon glyphicon-file right-sm"></i>Pages</a>
          </li>
          <?php } ?>
          <?php if (in_array($this->session->userdata("tier"), array(999, 3))){ ?>
          <li>
            <a class="md_mods" href="<?php echo site_url('cms/modules/'); ?>"><i class="glyphicon glyphicon-briefcase right-sm"></i>Modules</a>
          </li>
          <?php } ?>
          <?php
          if (in_array($this->session->userdata("tier"), array(999))){
          ?>
          <li>
            <a class="md_cat" href="<?php echo site_url('cms/modules/category'); ?>"><i class="glyphicon glyphicon-link right-sm"></i>Page Category</a>
          </li>
          <li>
            <a class="md_grp" href="<?php echo site_url('cms/modules/group'); ?>"><i class="glyphicon glyphicon-link right-sm"></i>Module Group</a>
          </li>
          <?php
          }
          ?>
        </ul>
      </li>
      <?php } ?>
      <?php if (false){//in_array($this->session->userdata("tier"), array(999, 3))){ ?>
      <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#closingdropdown" class="closingdropdown collapsed" aria-expanded="false"><i class="glyphicon glyphicon-book right-sm"></i>Quiz</a>
        <ul id="closingdropdown" class="collapse" aria-expanded="false">
          <li>
            <a class="cd_survey" href="<?php echo site_url('cms/quiz/survey'); ?>"><i class="glyphicon glyphicon-list-alt right-sm"></i>Questions</a>
          </li>
          <li>
            <a class="cd_cycle" href="<?php echo site_url('cms/quiz/cycles'); ?>"><i class="glyphicon glyphicon-refresh right-sm"></i>Sets</a>
          </li>
        </ul>
      </li>
      <?php } /*?>
      <?php
      //if ($this->session->userdata("tier") >= 2 || $this->session->userdata("tier") == 0.5){
      ?>
      <li class='hidden'>
        <a href="javascript:;" data-toggle="collapse" data-target="#statdropdown" class="statdropdown collapsed hidden" aria-expanded="false"><i class="glyphicon glyphicon-stats right-sm"></i>Statistics</a>
        <ul id="statdropdown" class="collapse" aria-expanded="false">
          <li>
            <a class="sd_pitr" href="<?php echo site_url('cms/statistics/productitr'); ?>"><i class="glyphicon glyphicon-thumbs-up right-sm"></i>Product ITR</a>
          </li>
          <li>
            <a class="sd_pshare" href="<?php echo site_url('cms/statistics/productshare'); ?>"><i class="glyphicon glyphicon-tasks right-sm"></i>Product Share</a>
          </li>
          <li>
            <a class="sd_mod" href="<?php echo site_url('cms/statistics/moduledata'); ?>"><i class="glyphicon glyphicon-briefcase right-sm"></i>Module Data</a>
          </li>
          <!-- <li>
            <a class="sd_cov" href="<?php echo site_url('cms/statistics/coverage'); ?>"><i class="glyphicon glyphicon-picture right-sm"></i>Coverage Data</a>
          </li>
          <li>
            <a class="sd_visit" href="<?php echo site_url('cms/statistics/visit'); ?>"><i class="glyphicon glyphicon-plane right-sm"></i>Visit Details</a>
          </li> -->

        </ul>
      </li>
      <?php
    */
    ?>      
      <!--multi level menu end-->
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  <?php if (isset($dropdown)) { ?>
    setTimeout(function () {
      $(".<?php echo $dropdown ?>").removeClass("collapsed").addClass("active");
      $("#<?php echo $dropdown ?>").addClass("in");
    }, 50)
  <?php
  }
  ?>
  $(".<?php echo $datatype ?>").addClass("active");
  
});
function togglenav(){
    if ($("#sidebar").hasClass("navslideout")){
      $("#sidebar").removeClass("navslideout");
      $("#main-content").removeClass("contentslideout");
    } else {
      $("#sidebar").addClass("navslideout");
      $("#main-content").addClass("contentslideout");
    }
    
  }
</script>