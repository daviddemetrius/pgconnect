<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-align-center right-sm"></i>Add New ACC Channel
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/accchannel")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type ACC Channel Name" name="channel" value="<?php echo set_value('channel'); ?>"/>
                <span>ACC Channel Name</span>
              </label>
              <button class='form-control btn btn-primary'>Add ACC Channel</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	