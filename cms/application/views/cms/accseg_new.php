<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-align-center right-sm"></i>Add New ACC Segment
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/accsegments")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type ACC Segment Name" name="segmentation" max="2" value="<?php echo set_value('segmentation'); ?>"/>
                <span>ACC Segment Name</span>
              </label>
              <button class='form-control btn btn-primary'>Add ACC Segment</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	