<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-th-large right-sm"></i>Add New House Category
                <div class='tableheaderback'><a href="<?=site_url("cms/brand/category")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Category Name" name="categoryname" max="2" value="<?php echo set_value('categoryname'); ?>"/>
                <span>House Category Name</span>
              </label>

              <button class='form-control btn btn-primary'>Add House Category</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	