<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-globe right-sm"></i>Add New Area
                <div class='tableheaderback'><a href="<?=site_url("cms/regions/city")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Area Name" name="cityname" max="2" value="<?php echo set_value('cityname'); ?>"/>
                <span>Area Name</span>
              </label>
              <label class="has-float-label">
                <select name="provinceselect">
                  <?php
                  foreach ($provinces as $countrydd){
                    if (set_value("provinceselect") == $countrydd->provinceid){
                      echo "<option value='$countrydd->provinceid' selected>$countrydd->provincename - $countrydd->provinceid</option>";
                    } else {
                      echo "<option value='$countrydd->provinceid'>$countrydd->provincename - $countrydd->provinceid</option>";
                    }
                  }
                  ?>
                </select>
                <span>Region</span>
              </label>
              <label class="has-float-label">
                <input type="number" placeholder="Type Area ID" name="cityid" value="<?php echo set_value('cityid'); ?>"/>
                <span>Area ID</span>
              </label>

              <button class='form-control btn btn-primary'>Add Area</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	