<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
<section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-refresh right-sm"></i> Cycle Detail <?=$cycledata->obsolete==1?" - <span class='oranget'>Inactive</span>": ""?>
      <div class='tableheaderback'><a href="<?=site_url("cms/modules/cycles")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-1'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h1><?=$cycledata->cycletitle?></h1>
            </div>
            <div class='detailcontent'>
              Valid From <b><?=date('j F Y', strtotime($cycledata->periodstart))?></b> until <b><?=date('j F Y', strtotime($cycledata->periodend))?></b>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-plane'></i> <?=$cycledata->visittype=="Both"?"1On1 & Group":$cycledata->visittype?>
            </div>
            <div class='detailcontent'>
              <?php
              $reptypearr = array();
              $reptypestr = "";
              $delim = "";
              foreach ($cycledata->reptypes as $rept){
                $reptypearr[] = $rept->reptypeid;
                $reptypestr .= $delim.$rept->typename;
                $delim = ", ";
              }
              ?>
              <i class='glyphicon glyphicon-th-list'></i> <?=$reptypestr?>
            </div>
            <div class='detailcontent'>
              <?php
              $accsegarr = array();
              $accsegstr = "";
              $delim = "";
              foreach ($cycledata->accsegments as $rept){
                $accsegarr[] = $rept->segmentid;
                $accsegstr .= $delim.$rept->segmentation;
                $delim = ", ";
              }
              ?>
              <i class='glyphicon glyphicon-home'></i> <?=$accsegstr?>
            </div>
            <div class='detailcontent'>
              <?php
              $hcpsegarr = array();
              $hcpsegstr = "";
              $delim = "";
              foreach ($cycledata->hcpsegments as $rept){
                $hcpsegarr[] = $rept->segmentid;
                $hcpsegstr .= $delim.$rept->segmentation;
                $delim = ", ";
              }
              ?>
              <i class='glyphicon glyphicon-align-center'></i> <?=$hcpsegstr?>
            </div>
            <div class='detailcontent'>
              <?php
              $specarr = array();
              $specstr = "";
              $delim = "";
              foreach ($cycledata->specialties as $rept){
                $specarr[] = $rept->specialtyid;
                $specstr .= $delim.$rept->specialtyname;
                $delim = ", ";
              }
              ?>
              <i class='glyphicon glyphicon-cutlery'></i> <?=$specstr?>
            </div>
            <div class='detailcontent'>
              <?php
              $roomarr = array();
              $roomstr = "";
              $delim = "";
              foreach ($cycledata->rooms as $room){
                $roomarr[] = $room->roomid;
                $roomstr .= $delim.$room->roomname;
                $delim = ", ";
              }
              ?>
              <i class='glyphicon glyphicon-stop'></i> <?=$roomstr?>
            </div>
          </div>
          <div class='col-md-1'>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
      <?php if ($this->session->userdata("tier") > 2.5) { ?>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      <?php } ?>
      </div>
    </div>
    <?php if ($this->session->userdata("tier") > 2.5) { ?>
    <form method="POST" action="<?=site_url('cms/modules/editCycle')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
        <input  type='hidden' value='<?=$cycledata->cycleid?>' name='id'>
        <label class="has-float-label">
          <input type="text" placeholder="Title" name="cycletitle" value="<?php echo $cycledata->cycletitle; ?>"/>
          <span>Cycle Title</span>
        </label>
        <label class="has-float-label">
          <input type="date" placeholder="Start" name="periodstart" value="<?php echo date('Y-m-d', strtotime($cycledata->periodstart)); ?>"/>
          <span>Cycle Start</span>
        </label>
        <label class="has-float-label">
          <input type="date" placeholder="End" name="periodend" value="<?php echo date('Y-m-d', strtotime($cycledata->periodend)); ?>"/>
          <span>Cycle End</span>
        </label>
        <label class="has-float-label">
          <select name="cyclevisittype">
            <?php
            $types = array("Both", "1On1", "Group");
            foreach ($types as $cat){
              if ($cat == $cycledata->cyclevisittype){
                echo "<option value='$cat' selected>$cat</option>";
              } else {
                echo "<option value='$cat'>$cat</option>";
              }
            }
            ?>
          </select>
          <span>Visit Type</span>
        </label>

        <label>Representative Types</label>
        <div class='row'>
          <?php
          foreach ($reptypes as $type){
            if (in_array($type->typeid, $reptypearr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->typeid.'" name="reptype[]" checked> '.$type->typename.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->typeid.'" name="reptype[]"> '.$type->typename.'</div>';
            }
            
          }
          ?>
        </div>
        <br/>

        <label>Hospital Segments</label>
        <div class='row'>
          <?php
          foreach ($accsegs as $seg){
            if (in_array($seg->segmentid, $accsegarr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="accsegments[]" checked> '.$seg->segmentation.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="accsegments[]"> '.$seg->segmentation.'</div>';
            }
          }
          ?>
        </div>
        <br/>

        <label>HCP Segments</label>
        <div class='row'>
          <?php
          foreach ($hcpsegs as $seg){
            if (in_array($seg->segmentid, $hcpsegarr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="hcpsegments[]" checked> '.$seg->segmentation.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="hcpsegments[]"> '.$seg->segmentation.'</div>';
            }
          }
          ?>
        </div>
        <br/>

        <label>HCP Specialties</label>
        <div class='row'>
          <?php
          foreach ($specialties as $spec){
            if (in_array($spec->specialtyid, $specarr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$spec->specialtyid.'" name="specs[]" checked> '.$spec->specialtyname.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$spec->specialtyid.'" name="specs[]"> '.$spec->specialtyname.'</div>';
            }
          }
          ?>
        </div>
        <br/>

        <label>Rooms</label>
        <div class='row'>
          <?php
          foreach ($rooms as $rm){
            if (in_array($rm->roomid, $roomarr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$rm->roomid.'" name="rooms[]" checked> '.$rm->roomname.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$rm->roomid.'" name="rooms[]"> '.$rm->roomname.'</div>';
            }
          }
          ?>
        </div>
        <br/>
        <label>Cycle Active</label><br/>
        <input type='checkbox' value="1" name='active' class='form-control' <?=$cycledata->obsolete==0?'checked':''?>><br/>
        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
    <?php } ?>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      Module List
    </div>
    <div class='tablecontent'>
      <table>
        <tr>
          <th width=10%>#</th>
          <th>Title</th>
          <th>Description</th>
          <th>House</th>
          <th>Visit Number</th>
          <th>Status</th>
          <th></th>
        </tr>
        <?php
        $page = 0;
        $first = "showrow";
        foreach($cycledata->modules as $key=>$act){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }

         $status = "<span class='greent'>Active</span>";
         if ($act->obsolete == 1){
          $status = "<span class='oranget'>Deactivated</span>";
         }
          echo "
          <tr class='pageddata page$page $first'>
            <td><span class='rowkey'>".($key+1)."</span><span class='rowdeleter hidden' onclick='deleteModule($act->moduleid, $cycledata->cycleid, $act->visitnumber)'>✖</span></td>
            <td>$act->moduletitle</td>
            <td>$act->moduledesc</td>
            <td>$act->housename</td>
            <td>$act->visitnumber</td>
            <td>".($act->obsolete==1?"<span class=\"oranget\">Deactivated</span>":"<span class=\"greent\">Active</span>")."</td>
            <td><a href='".site_url("cms/modules/$act->moduleid")."' class='form-control btn btn-primary'>Details</a></td>
          </tr>
          ";
        }
        ?>
        <tr>
          <td colspan='6' class='pagingrow'>
          <?php
          $first = "currentindex";
          for ($i = 0; $i < $page; $i++){
            echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
            $first = "";
          }
          ?>
          </td>
          <td>
          <?php if ($this->session->userdata("tier") > 2.5) { ?>
            <a href='javascript:void(0)' class='btn form-control' onclick='addNewModule()'>Add New Module</a>
          <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</section>
</section>	

<script>
<?php if ($this->session->userdata("tier") > 2.5) { ?>
 $("#editbutton").on("click", function(){
  $("input").prop("disabled", false);
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
 <?php } ?>
 function switchpage(number, e){
  $(e).parent().parent().parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}
<?php if ($this->session->userdata("tier") > 2.5) { ?>
var modules = <?=json_encode($modulelist)?>;
<?php } ?>
function openAJAXLoader(){
  swal({
    title: 'OK!',
    text: 'Modal ini akan tertutup secara otomatis...',
    showCancelButton: false,
    allowOutsideClick: true,
    onOpen: () => {
      swal.showLoading();
    }
  })
}
<?php if ($this->session->userdata("tier") > 2.5) { ?>
function addNewModule(){
  swal.setDefaults({
    confirmButtonText: 'Next &rarr;',
    showCancelButton: true,
    progressSteps: ['1', '2']
  });
  var swalsteps = [{
    title: 'Find Module',
    html: "<div style='position:relative' id='srcdiv'><input type='text' class='form-control' id='src' placeholder='Search...'><input type='hidden' id='srcid' value='-1'></div>",
    onOpen: function() {
      $('#src').autocomplete({
        source: modules,
        appendTo: '#srcdiv',
        select: function(event, ui){
          $("#src").val(ui.item.label);
          $("#srcid").val(ui.item.id);
        }
      });
    },
    preConfirm: function(){
      return Promise.resolve({id: $('#srcid').val(), name: $('#src').val()});
    }
  }, {
    title: "Select Visit",
    input: "select",
    inputOptions: {"1": "1", "2":"2", "3":"3", "4":"4", "5":"5"},
    inputClass: "form-control"
  }];

  swal.queue(swalsteps).then((result) => {
    if (result.value[0].id != -1){
      openAJAXLoader();
      $.ajax({
        url: '<?=site_url("cms/modules/addModuleToCycle")?>',
        type: "POST",
        data: {
          moduleid: result.value[0].id,
          visitnumber: result.value[1],
          cycleid: <?=$cycledata->cycleid?>
        }, success: function(data){
          if (data == "success"){
            window.location = '<?=site_url("cms/modules/cycles/$cycledata->cycleid")?>';
          } else {
            swal("Ups!", data, "error");
          }
        }
      })
    } else {
      swal("Ups!", "Mohon pilih module dari autocomplete", "warning");
    }
    
  });
}


    $("tr.pageddata").hover(function(){
      $(this).find(".rowdeleter").removeClass("hidden");
      $(this).find(".rowkey").addClass("hidden");
    }, function(){
      $(this).find(".rowdeleter").addClass("hidden");
      $(this).find(".rowkey").removeClass("hidden");
    });

    function deleteModule(moduleid, cycleid, visitnumber){
      swal({
        title: "Apakah Anda Yakin?",
        type:"warning",
        showCancelButton: true
      }).then((result) => {
        if (result.value){
          openAJAXLoader();
          $.ajax({
            url: "<?=site_url("cms/modules/deleteModuleFromCycle")?>",
            type: "POST",
            data: {
              moduleid: moduleid,
              visitnumber: visitnumber,
              cycleid: <?=$cycledata->cycleid?>
            }, success: function(data){
              if (data == "success"){
                window.location = '<?=site_url("cms/modules/cycles/$cycledata->cycleid")?>';
              } else {
                swal("Ups!", data, "error");
              }
            }
          })
        }
      })
    }
    <?php } ?>
</script>
