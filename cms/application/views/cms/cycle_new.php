<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-refresh"></i> Add New Cycle
                <div class='tableheaderback'><a href="<?=site_url("cms/modules/cycles/")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Title" name="cycletitle" value="<?php echo set_value('cycletitle'); ?>"/>
                <span>Cycle Title</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="Start" name="periodstart" value="<?php echo set_value('periodstart'); ?>"/>
                <span>Cycle Start</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="End" name="periodend" value="<?php echo set_value('periodend'); ?>"/>
                <span>Cycle End</span>
              </label>
              <label class="has-float-label">
                <select name="cyclevisittype">
                  <?php
                  $types = array("Both", "1On1", "Group");
                  foreach ($types as $cat){
                    if ($cat == set_value("cyclevisittype")){
                      echo "<option value='$cat' selected>$cat</option>";
                    } else {
                      echo "<option value='$cat'>$cat</option>";
                    }
                  }
                  ?>
                </select>
                <span>Visit Type</span>
              </label>

              <label>Representative Types</label>
              <div class='row'>
                <?php
                foreach ($reptypes as $type){
                  if (in_array($type->typeid, set_value('reptype', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->typeid.'" name="reptype[]" checked> '.$type->typename.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->typeid.'" name="reptype[]"> '.$type->typename.'</div>';
                  }
                  
                }
                ?>
              </div>
              <br/>

              <label>Hospital Segments</label>
              <div class='row'>
                <?php
                foreach ($accsegs as $seg){
                  if (in_array($seg->segmentid, set_value('accsegments', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="accsegments[]" checked> '.$seg->segmentation.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="accsegments[]"> '.$seg->segmentation.'</div>';
                  }
                }
                ?>
              </div>
              <br/>

              <label>HCP Segments</label>
              <div class='row'>
                <?php
                foreach ($hcpsegs as $seg){
                  if (in_array($seg->segmentid, set_value('hcpsegments', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="hcpsegments[]" checked> '.$seg->segmentation.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$seg->segmentid.'" name="hcpsegments[]"> '.$seg->segmentation.'</div>';
                  }
                }
                ?>
              </div>
              <br/>

              <label>HCP Specialties</label>
              <div class='row'>
                <?php
                foreach ($specialties as $spec){
                  if (in_array($spec->specialtyid, set_value('specs', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$spec->specialtyid.'" name="specs[]" checked> '.$spec->specialtyname.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$spec->specialtyid.'" name="specs[]"> '.$spec->specialtyname.'</div>';
                  }
                }
                ?>
              </div>
              <br/>

              <label>Rooms</label>
              <div class='row'>
                <?php
                foreach ($rooms as $rm){
                  if (in_array($rm->roomid, set_value('rooms', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$rm->roomid.'" name="rooms[]" checked> '.$rm->roomname.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$rm->roomid.'" name="rooms[]"> '.$rm->roomname.'</div>';
                  }
                }
                ?>
              </div>
              <br/>

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	