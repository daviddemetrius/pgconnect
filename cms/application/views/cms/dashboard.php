<script src="<?php echo base_url(); ?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/serial.js" type="text/javascript"></script>	
<script src="<?php echo base_url(); ?>js/amcharts/xy.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/jquery.fittext.js" type="text/javascript"></script>
<?php
?>
<section id="main-content">
	<section class="wrapper site-min-height"> <!-- site-min-height -->
		<div class='centerfields hidden' style='width:50%'>
            <form method='get'>
				<label>Month / Year</label>
				<div class='row'>
					<div class='col-xs-3'><input type='number' min=1 max=12 class='form-control' id='month' value="<?=$this->input->get("month")?$this->input->get("month"):date("n")?>" onchange='' name='month'></div>
					<div class='col-xs-3'><input type='number' min=2018 class='form-control' id='year' value="<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>" onchange='' name='year'></div>
					<div class='col-xs-4'><button class='btn btn-primary form-control'>Get Data</button></div>
					<?=$this->input->get("m")=="rep"?"<input type='hidden' name='m' value='rep'>":""?>
				</div>
				<br/>
			</form>
		</div>
		<div class="row" id='dboard' style='font-size:16px'>
	   		<div class='col-xs-12 col-md-3 col-lg-3'>
	   			<label style='display:block; text-align:right'>Total PharmacyCare</label>
	   		</div>
	   		<div class='col-xs-12 col-md-9'>
				<span><?=$totalActiveHospital?></span>
	   		</div>
	   		<div class='col-xs-12 col-md-3 col-lg-3'>
	   			<label style='display:block; text-align:right'>Total Consumers</label>
	   		</div>
	   		<div class='col-xs-12 col-md-9'>
				<span><?=$totalCustomers?></span>
	   		</div>
	   		<div class='col-xs-12 col-md-3 col-lg-3'>
	   			<label style='display:block; text-align:right'>Total Redeemed Discounts</label>
	   		</div>
	   		<div class='col-xs-12 col-md-9'>
				<span><?=$totalUsedCoupons?></span>
	   		</div>
	   		<div class='col-xs-12 col-md-3 col-lg-3'>
	   			<label style='display:block; text-align:right'>Total Active Users</label>
	   		</div>
	   		<div class='col-xs-12 col-md-9'>
				<span><?=$totalActiveReps?></span>
	   		</div>
		</div>
	</section>
</section>

<script>
$("#activehospital").fitText(0.7, {minFontSize:'220px'});
$("#activecustomers").fitText(0.7, {minFontSize:'220px'});
$("#activeredeems").fitText(0.7, {minFontSize:'220px'});
$("#activereps").fitText(0.7, {minFontSize:'220px'});
</script>