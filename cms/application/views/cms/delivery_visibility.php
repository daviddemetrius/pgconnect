<style>
  input[type=checkbox]{
  height: 0;
  width: 0;
  visibility: hidden;
}

label {
  cursor: pointer;
  text-indent: -9999px;
  width: 40px;
  height: 20px;
  background: grey;
  display: block;
  border-radius: 100px;
  position: relative;
}

label:after {
  content: '';
  position: absolute;
  top: 1px;
  left: 1px;
  width: 18px;
  height: 18px;
  background: #fff;
  border-radius: 90px;
  transition: 0.3s;
}

input:checked + label {
  background: #bada55;
}

input:checked + label:after {
  left: calc(110% - 5px);
  transform: translateX(-110%);
}

label:active:after {
  width: 26px;
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-usd right-sm"></i> Criteria List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	 
                <th>Criterion</th>	
                <th width=20%>Visibility</th>	
             </tr>
             <?php
             foreach($reptypes as $key=>$mod){
               $checked = "";
               if ($mod->visibility == 1){
                $checked = "checked";
               }
               if  ($this->session->userdata("tier") > 2.5) {
                $btnedit = '<input type="checkbox" id="switch'.$mod->typeid.'" value=1 onchange="updateEntry('.$mod->typeid.', this)" '.$checked.'/><label for="switch'.$mod->typeid.'">Toggle</label>';
               } else {
                $btnedit = '<input type="checkbox" id="switch'.$mod->typeid.'" value=1 disabled/><label for="switch'.$mod->typeid.'" disabled '.$checked.'>Toggle</label>';
               }
               
               echo "
               <tr data-id='$mod->typeid'>
                  <td>".($key+1)."</td>
                  <td><div class='typename'>$mod->typename</div></td>
                  <td colspan=2>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
            </tr>
         </table>
      </div>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });

  <?php if ($this->session->userdata("tier") > 2.5) { ?>
    function updateEntry(id, e){
      var val = 0
      if ($(e).is(":checked")){
        val = 1;
      }
      $.ajax({
        url: "<?=site_url("cms/closing/editvisibility")?>",
        type:"POST",
        data: {
          id: id,
          val: val
        }
      })
    }
  <?php  }?>
</script>