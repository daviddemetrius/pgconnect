<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/nicEdit.js' type='text/javascript'></script>
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-file right-sm"></i> Event Detail
      <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/events");?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/assets/eventf/$details->eventid.png?".rand()?>' onerror='defaultppproperty(this)'>
            <img src='<?=base_url()."/assets/eventb/$details->eventid.png?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
               <h3><?= $details->eventtitle?></h3>
            </div>
            <div class='detailcontent'>
               <?= $details->eventsubtitle?>
            </div>
            <div class='detailcontent'>
               <?= $details->datestart." - ".$details->dateend?>
            </div>
            <div class='detailcontent'>
               Price: <?= $details->eventprice?>
            </div>
            <div class='detailcontent'>
               Category: <?= $details->eventcategory?>
            </div>
            <div class='detailcontent'>
               Facilities: <?=$details->facilities?>
            </div>
            <div class='detailcontent'>
               Description:<br/> <?= $details->description?>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-globe'></i> <?=$details->venuelat.",".$details->venuelng?>
            </div>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-road'></i> <?=$details->eventvenue?>
            </div>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-download'></i> <a href='<?=base_url()?>/assets/eventdoc/<?=$details->docfile?>'>Document</a>
            </div>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/masterdata/editEvent')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
        <input type="hidden" name="eventid" value="<?php echo $details->eventid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Event Title" name="eventtitle" value="<?php echo $details->eventtitle ?>" id='eventtitle'/>
          <span>Event Title</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Event Subtitle" name="eventsubtitle" value="<?php echo $details->eventsubtitle ?>" id='eventsubtitle'/>
          <span>Event Subtitle</span>
        </label>
        <label class="has-float-label">
          <input type="date" placeholder="Type Date" name="datestart" value="<?php echo $details->datestart ?>" id='datestart'/>
          <span>Date Start</span>
        </label>
        <label class="has-float-label">
          <input type="date" placeholder="Type Date" name="dateend" value="<?php echo $details->dateend ?>" id='dateend'/>
          <span>Date End</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Event Price" name="eventprice" value="<?php echo $details->eventprice ?>"/>
          <span>Event Price</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Event Category" name="eventcategory" value="<?php echo $details->eventcategory ?>"/>
          <span>Event Category</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Event Venue" name="eventvenue" value="<?php echo $details->eventvenue ?>"/>
          <span>Event Venue</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Venue Latitude" name="venuelat" value="<?php echo $details->venuelat ?>" id='venuelat'/>
          <span>Venue Latitude</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Venue Longitude" name="venuelng" value="<?php echo $details->venuelng ?>" id='venuelng'/>
          <span>Venue Longitude</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Facilities" name="facilities" value="<?php echo $details->facilities ?>" id='facilities'/>
          <span>Facilities</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Participants" name="participants" value="<?php echo $details->participants ?>" id='participants'/>
          <span>Participants</span>
        </label>
        <label>Description</label>
        <div class='textareacontainer'>
          <textarea id='desc' name='description' class='form-control' style="width:100%"><?=$details->description?></textarea>
        </div>
        
        <br/>
        <label class="has-float-label">
          <input type="file" name="bannerfile" accept="image/*"/>
          <span>New Banner</span>
        </label>
        <label class="has-float-label">
          <input type="file" name="thumbfile" accept="image/*"/>
          <span>New Thumbnail</span>
        </label>
        <label class="has-float-label">
          <input type="file" name="infofile"/>
          <span>New Information Document</span>
        </label>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-user right-sm"></i> Event Attendees
      <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/downloadevtattendees?evtid=$details->eventid");?>"><i class='glyphicon glyphicon-download'></i></a></div>
    </div>
    <div class='tablecontent'>
      <table>
        <tr>
          <th width=5%>#</th> 
          <th>Name</th> 
          <th>Specialty</th> 
          <th>Code</th>
          <th>Institution</th>
          <th>RSVP Date</th>
        </tr>
         <?php
         $page = 0;
         $first = "showrow";
         foreach($attendees as $key=>$rep){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
            echo "
            <tr class='pageddata page$page $first'>
               <td>".($key+1)."</td>
               <td>$rep->specialistname</td>
               <td>$rep->specialty</td>
               <td>$rep->xmlhcpid</td>
               <td>$rep->institution</td>
               <td>$rep->rsvpdate</td>
            </tr>
            ";
         }
         ?>         
         <tr>
           <td colspan='6' class='pagingrow'>
             <?php
             $first = "currentindex";
             for ($i = 0; $i < $page; $i++){
              echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
              $first = "";
             }
             ?>
           </td>
         </tr>
      </table>
    </div>
  </div>
</section>
</section>  

<script>
  nicEditors.allTextAreas();
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

function switchpage(number, e){
  $(e).parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}
 
</script>