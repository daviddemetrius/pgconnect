<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/nicEdit.js' type='text/javascript'></script>
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-file right-sm"></i>Add New Event
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/events")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Event Title" name="eventtitle" value="<?php echo set_value('eventtitle'); ?>" id='eventtitle'/>
                <span>Event Title</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Event Subtitle" name="eventsubtitle" value="<?php echo set_value('eventsubtitle'); ?>" id='eventsubtitle'/>
                <span>Event Subtitle</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="Type Date" name="datestart" value="<?php echo set_value('datestart'); ?>" id='datestart'/>
                <span>Date Start</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="Type Date" name="dateend" value="<?php echo set_value('dateend'); ?>" id='dateend'/>
                <span>Date End</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Event Price" name="eventprice" value="<?php echo set_value('eventprice'); ?>"/>
                <span>Event Price</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Event Category" name="eventcategory" value="<?php echo set_value('eventcategory'); ?>"/>
                <span>Event Category</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Event Venue" name="eventvenue" value="<?php echo set_value('eventvenue'); ?>"/>
                <span>Event Venue</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Venue Latitude" name="venuelat" value="<?php echo set_value('venuelat'); ?>" id='venuelat'/>
                <span>Venue Latitude</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Venue Longitude" name="venuelng" value="<?php echo set_value('venuelng'); ?>" id='venuelng'/>
                <span>Venue Longitude</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Facilities" name="medrepcode" value="<?php echo set_value('medrepcode'); ?>" id='medrepcode'/>
                <span>Facilities</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Participants" name="aplcode" value="<?php echo set_value('aplcode'); ?>" id='aplcode'/>
                <span>Participants</span>
              </label>
              <label>Description</label>
              <textarea id='desc' name='description' class='form-control'><?=set_value("description")?></textarea>
              
              <br/>
              <label class="has-float-label">
                <input type="file" name="bannerfile" accept="image/*"/>
                <span>Banner</span>
              </label>
              <label class="has-float-label">
                <input type="file" name="thumbfile" accept="image/*"/>
                <span>Thumbnail</span>
              </label>
              <label class="has-float-label">
                <input type="file" name="infofile"/>
                <span>Information Document</span>
              </label>
              <button class='form-control btn btn-primary'>Add Event</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	

<script>
  nicEditors.allTextAreas();
</script>