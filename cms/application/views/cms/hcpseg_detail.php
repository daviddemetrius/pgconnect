<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
    <i class="glyphicon glyphicon-align-center right-sm"></i> HCP Segment Details
      <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/segments")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <form method='post' action='<?=site_url("cms/masterdata/edithcpsegvisit")?>'>
        <div class='detailseparator'>
          <div class='row'>
            <div class='col-md-1'></div>
            <div class='col-md-5'>
              <div class='detailcontent'>
                <h1><?=$segment->segmentation?></h1>
              </div>
              <div class='detailcontent'>
                <input type='hidden' value='<?=$segment->segmentid?>' name='id'/>
              </div>
            </div>
            <div class='col-md-5'>
              <div class='detailcontent'>
              <?php
              foreach ($segment->visitmap as $sgm){
                echo '
                <label class="has-float-label">
                  <input type="number" placeholder="'.$sgm->segment.'" name="visits[]" value="'.$sgm->visits.'" min="0"/>
                  <input type="hidden" name="segmentid[]" value="'.$sgm->segmentid.'"/>
                  <span>Maximum Visits for <u>'.$sgm->segment.'</u> ACC Segment</span>
                </label>';
              }
              ?>
                
              </div>
            </div>
            <div class='col-md-1'></div>
          </div>
        </div>
        <div class='detailbuttons'>
          <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
          <button class='btn'><i class="glyphicon glyphicon-floppy-disk"></i> <span class=''>Save</span></button>
        </div>
      </form>
    </div>
    <form method="POST" action="<?=site_url('cms/masterdata/editHcpSegName')?>">
      <fieldset class='editor hidden'>
        <input type="hidden" name="segmentid" value="<?php echo $segment->segmentid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Segment Name" name="segmentation" value="<?php echo $segment->segmentation?>"/>
          <span>Segment Name</span>
        </label>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

 
</script>