<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-align-center right-sm"></i>Add New HCP Segment
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/segments")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type HCP Segment Name" name="segmentation" max="2" value="<?php echo set_value('segmentation'); ?>"/>
                <span>HCP Segment Name</span>
              </label>
              <button class='form-control btn btn-primary'>Add HCP Segment</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	