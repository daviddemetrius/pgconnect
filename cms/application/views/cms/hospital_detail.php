<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-home right-sm"></i> Apotik Detail
      <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/accounts")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/assets/accounts/$details->hospitalid.png?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
               <h3><?= $details->hospitalname?></h3>
            </div>
            <div class='detailcontent'>
               <?= $details->customercode?>
            </div>
            <div class='detailcontent'>
               <?= $details->address?>
            </div>
            <div class='detailcontent'>
               <?= $details->phone?>
            </div>
            <div class='detailcontent'>
               <a href='https://www.google.com/maps/search/?api=1&query=<?=str_replace(",", ".", $details->lat).",".str_replace(",", ".", $details->lng)?>' target="_blank">Click for Location</a>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
               <i class='glyphicon glyphicon-globe'></i> <?= $details->region?>
            </div>
            <div class='detailcontent'>
               <i class='glyphicon glyphicon-road'></i> <?= $details->branch?>
            </div>
            <div class='detailcontent'>
               <i class='glyphicon glyphicon-home'></i> <?= $details->distributor?>
            </div>
            <div class='detailcontent'>
               <i class='glyphicon glyphicon-'></i>MGT: <?= $details->mgt?>
            </div>
            <div class='detailcontent'>
              Created <strong><?= date( "j F Y H:i:s", strtotime($details->created))?></strong>
            </div>
            <div class='detailcontent'>
              Updated <strong><?= date( "j F Y H:i:s", strtotime($details->lastupdated))?></strong>
            </div>
          </div>
        </div>
      </div>
      <?php if ($this->session->userdata("tier") != 1){ ?>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
      <?php } ?>
    </div>
    <form method="POST" action="<?=site_url('cms/masterdata/editHospital')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
        <input type="hidden" name="hospitalid" value="<?php echo $details->hospitalid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Account Name" name="hospitalname" value="<?php echo $details->hospitalname; ?>"/>
          <span>Account Name</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Address" name="address" value="<?php echo $details->address; ?>"/>
          <span>Address</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Hospital Name" name="phone" value="<?php echo $details->phone; ?>"/>
          <span>Contact Number</span>
        </label>

        <label class="has-float-label">
          <input type="file" placeholder="File" name="userfile" value="" accept="image/*"/>
          <span>New Image</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Coordinates" name="lat" value="<?php echo $details->lat; ?>"/>
          <span>Latitude</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Coordinates" name="lng" value="<?php echo $details->lng; ?>"/>
          <span>Longitude</span>
        </label>           
        <label class="has-float-label">
          <input type="text" placeholder="Type Region" name="region" value="<?php echo $details->region; ?>"/>
          <span>Region</span>
        </label>       
        <label class="has-float-label">
          <input type="text" placeholder="Type Distributor" name="distributor" value="<?php echo $details->distributor; ?>"/>
          <span>Distributor</span>
        </label>       
        <label class="has-float-label">
          <input type="text" placeholder="Type MGT" name="mgt" value="<?php echo $details->mgt; ?>"/>
          <span>MGT</span>
        </label>       
        <label class="has-float-label">
          <input type="text" placeholder="Type Branch" name="branch" value="<?php echo $details->branch; ?>"/>
          <span>Branch</span>
        </label>        
        <label class="has-float-label">
          <input type="text" placeholder="Type Outlet Code" name="customercode" value="<?php echo $details->customercode; ?>"/>
          <span>Code</span>
        </label>       
        <br/>

        <label>Promo <i class='glyphicon glyphicon-check' onclick='$("#accntfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#accntfields input").prop("checked", false)' style='cursor:pointer'></i></label>
        <div class='row' id='accntfields'>
          <?php
          $promoarr = array();
          foreach ($details->promos as $acc){
            $promoarr[] = $acc->promoid;
          }
          foreach ($promos as $type){
            if (in_array($type->promoid, $promoarr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->promoid.'" name="promos[]" checked> '.$type->promoname.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->promoid.'" name="promos[]"> '.$type->promoname.'</div>';
            }
            
          }
          ?>
        </div><br/>         

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      <div class='tableheaderback'><a href="<?=site_url("cms/accounts/reps/new/$details->hospitalid")?>"><i class='glyphicon glyphicon-plus'></i></a></div>
      <i class="glyphicon glyphicon-user right-sm"></i> Agent List
    </div>
    <div class='tablecontent'>
      <table>
        <tr>
          <th width=5%>#</th> 
          <th>Name</th> 
          <th>Phone</th> 
          <th>Address</th>
          <th>Last Login</th>
          <th colspan=2 width=20%></th>
        </tr>
         <?php
         $page = 0;
         $first = "showrow";
         foreach($reps as $key=>$rep){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
          $obsbtn = "<a class='form-control btn btn-warning' href='".site_url("cms/accounts/reactivate/".$rep->repid)."'>Reactivate</a>";
          if ($rep->obsolete == 0){
            $obsbtn = "<a class='form-control btn btn-danger' href='".site_url("cms/accounts/deactivate/".$rep->repid)."'>Deactivate</a>";
          }
            echo "
            <tr class='pageddata page$page $first'>
               <td>".($key+1)."</td>
               <td>$rep->repname</td>
               <td>$rep->phone</td>
               <td>$rep->address</td>
               <td>$rep->lastpushupdated</td>
               <td><a class='form-control btn' href='".site_url("cms/accounts/reps/".$rep->repid)."'>Details</a></td>
               <td>$obsbtn</td>
            </tr>
            ";
         }
         ?>         
         <tr>
           <td colspan='7' class='pagingrow'>
             <?php
             $first = "currentindex";
             for ($i = 0; $i < $page; $i++){
              echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
              $first = "";
             }
             ?>
           </td>
         </tr>
      </table>
    </div>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      
      <i class="glyphicon glyphicon-user right-sm"></i> Customer List
    </div>
    <div class='tablecontent'>
      <table>
        <tr>
          <th width=5%>#</th> 
          <th>Name</th> 
          <th>Phone</th> 
          <th>Email</th>
          <th>Date</th>
          <th>Unique Code</th>
          <th>Status</th>
          <th>Promo</th>
        </tr>
         <?php
         $page = 0;
         $first = "showrow";
         foreach($custs as $key=>$rep){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
            echo "
            <tr class='pageddata page$page $first'>
               <td>".($key+1)."</td>
               <td>$rep->fullname</td>
               <td>$rep->contactno</td>
               <td>$rep->email</td>
               <td>$rep->dateregistered</td>
               <td>$rep->uniquecode</td>
               <td>".($rep->codeusage==0?"Used":"Available")."</td>
               <td>$rep->promoname</td>
            </tr>
            ";
         }
         ?>         
         <tr>
           <td colspan='8' class='pagingrow'>
             <?php
             $first = "currentindex";
             for ($i = 0; $i < $page; $i++){
              echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
              $first = "";
             }
             ?>
           </td>
         </tr>
      </table>
    </div>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});
function switchpage(number, e){
  $(e).parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}
function deleteAssignment(specid, roomid){
  swal({
    title: 'Apakah anda yakin?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak',
  }).then((result) => {
    if (result.value){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: false,
        onOpen: () => {
          swal.showLoading();
        }
      })
      $.ajax({
        url: '<?=site_url("cms/masterdata/deleteHospitalRoom")?>',
        type: "POST",
        data: {
          hospitalid: <?=$details->hospitalid?>,
          specid: specid,
          roomid: roomid
        },
        success: function(data){
          if (data == "success"){
            window.location = "<?=site_url("cms/masterdata/hospitals/".$details->hospitalid)?>";
          } else {
            swal({
              title: 'Ups!',
              text: data,
              showCancelButton: false,
              confirmButtonText: "Ok",
              type: "error"
            })
          }
        }
      })
    }
  })
}
function deleteTerritoryAssignment(territoryid){
  swal({
    title: 'Apakah anda yakin?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak',
  }).then((result) => {
    if (result.value){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: false,
        onOpen: () => {
          swal.showLoading();
        }
      })
      $.ajax({
        url: '<?=site_url("cms/masterdata/deleteHospitalTerritory")?>',
        type: "POST",
        data: {
          hospitalid: <?=$details->hospitalid?>,
          territoryid: territoryid,
        },
        success: function(data){
          if (data == "success"){
            window.location = "<?=site_url("cms/masterdata/hospitals/".$details->hospitalid)?>";
          } else {
            swal({
              title: 'Ups!',
              text: data,
              showCancelButton: false,
              confirmButtonText: "Ok",
              type: "error"
            })
          }
        }
      })
    }
  })
}

var chosenHcp = 0;
$('#hcpsearch').autocomplete({
      serviceUrl: '<?=base_url()?>cms/masterdata/searchhcp',
      onSelect: function (suggestion) {
        chosenHcp = suggestion.data;
      }
});



function addHospitalRoom(){
  if (chosenHcp != 0){
    $.ajax({
      url: '<?=site_url("cms/masterdata/addHospitalRoom")?>',
      type: "post",
      data: {
        hcpid: chosenHcp,
        roomid: $("#roomsearch").val(),
        hospitalid: <?=$details->hospitalid?>
      }, success: function(data){
        window.location = '<?=site_url("cms/masterdata/hospitals/$details->hospitalid")?>';
      }
    })
  }
}
function addHospitalTerritory(){
    $.ajax({
      url: '<?=site_url("cms/masterdata/addHospitalTerritory")?>',
      type: "post",
      data: {
        territoryid: $("#territorysrc").val(),
        hospitalid: <?=$details->hospitalid?>
      }, success: function(data){
        window.location = '<?=site_url("cms/masterdata/hospitals/$details->hospitalid")?>';
      }
    })
}
var editacc = 0;
var edithcp = 0;
var oldroom = 0;
function setEditParam(accid, hcpid, roomid){
  editacc = accid;
  edithcp = hcpid;
  oldroom = roomid;
  $("#editroomsrc").find("option[value='"+roomid+"']").prop("selected", true);
}

function editHospitalSpecRoom(){
  if (editacc != 0 && edithcp != 0){
    $.ajax({
      url: '<?=site_url("cms/masterdata/editHospitalRoom")?>',
      type: "post",
      data: {
        hcpid: edithcp,
        roomid: $("#editroomsrc").val(),
        hospitalid: editacc,
        oldroomid: oldroom
      }, success: function(data){
        window.location = '<?=site_url("cms/masterdata/hospitals/$details->hospitalid")?>';
      }
    })
  }
}
</script>