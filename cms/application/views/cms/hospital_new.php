<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-home right-sm"></i>Add New Apotik
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/accounts")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Apotik Name" name="hospitalname" value="<?php echo set_value('hospitalname'); ?>" id='accname'/>
                <span>Name</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Address" name="address" value="<?php echo set_value('address'); ?>" id='accname'/>
                <span>Address</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Phone" name="phone" value="<?php echo set_value('phone'); ?>" id='accname'/>
                <span>Contact number</span>
              </label>
              <label class="has-float-label">
                <input type="file" placeholder="File" name="userfile" value="<?php echo set_value('userfile'); ?>" accept="image/*"/>
                <span>Image</span>
              </label>       
              <label class="has-float-label">
                <input type="text" placeholder="Type Coordinates" name="lat" value="<?php echo set_value('lat'); ?>"/>
                <span>Latitude</span>
              </label>       
              <label class="has-float-label">
                <input type="text" placeholder="Type Coordinates" name="lng" value="<?php echo set_value('lng'); ?>"/>
                <span>Longitude</span>
              </label>       
              <label class="has-float-label">
                <input type="text" placeholder="Type Region" name="region" value="<?php echo set_value('region'); ?>"/>
                <span>Region</span>
              </label>       
              <label class="has-float-label">
                <input type="text" placeholder="Type Distributor" name="distributor" value="<?php echo set_value('distributor'); ?>"/>
                <span>Distributor</span>
              </label>       
              <label class="has-float-label">
                <input type="text" placeholder="Type MGT" name="mgt" value="<?php echo set_value('mgt'); ?>"/>
                <span>MGT</span>
              </label>       
              <label class="has-float-label">
                <input type="text" placeholder="Type Branch" name="branch" value="<?php echo set_value('branch'); ?>"/>
                <span>Branch</span>
              </label>        
              <label class="has-float-label">
                <input type="text" placeholder="Type Outlet Code" name="customercode" value="<?php echo set_value('customercode'); ?>"/>
                <span>Code</span>
              </label>       
              
              <br/>

              <label>Promos <i class='glyphicon glyphicon-check' onclick='$("#accntfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#accntfields input").prop("checked", false)' style='cursor:pointer'></i></label>
              <div class='row' id='accntfields'>
                <?php
                foreach ($promos as $type){
                  if (in_array($type->promoid, set_value('promos', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->promoid.'" name="promos[]" checked> '.$type->promoname.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->promoid.'" name="promos[]"> '.$type->promoname.'</div>';
                  }
                  
                }
                ?>
              </div><br/>

              <button class='form-control btn btn-primary'>Add Apotik</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	
<script>
  var chosenAcc = 0;
  $('#hospitalcode, #accname').autocomplete({
        serviceUrl: '<?=base_url()?>cms/closing/searchacc',
        preserveInput: true, 
        onSelect: function (suggestion) {
        }
  });
</script>