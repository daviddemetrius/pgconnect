<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-tag right-sm"></i> House Details
      <div class='tableheaderback'><a href="<?=site_url("cms/brand/house")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/assets/houses/$housedetails->houseid.png?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h3><?=$housedetails->housename?></h3>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              Created <strong><?= date( "j F Y H:i:s", strtotime($housedetails->created))?></strong>
            </div>
            <div class='detailcontent'>
              Updated <strong><?= date( "j F Y H:i:s", strtotime($housedetails->lastupdated))?></strong>
            </div>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/brand/editHouse')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
        <input type="hidden" name="houseid" value="<?php echo $housedetails->houseid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type House Name" name="housename" value="<?php echo $housedetails->housename ?>"/>
          <span>House Name</span>
        </label>
        <label class="has-float-label">
          <input type="file" placeholder="File" name="userfile" value="" accept="image/*"/>
          <span>New Thumbnail</span>
        </label>       

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

</script>