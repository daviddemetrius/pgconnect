<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-<?=$listicon?> right-sm"></i> <?=$listtitle?> List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	
                <th><a href='<?=$sitelink?>?s=housename<?=$addllink?>' class="<?=$s=="housename"?"active":""?>">House Name</a></th> 
                <th><a href='<?=$sitelink?>?s=houseid<?=$addllink?>' class="<?=$s=="houseid"?"active":""?>">ID</a></th> 
                <th width=20%><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($items as $key=>$item){
              echo "
                <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>";
              foreach($item as $key=>$i){
                if ($key == "id"){
                  continue;
                }
                echo "<td>$i</td>";
              }
              echo "
                  <td><a href='".site_url("cms/brand/house/".$item->houseid)."' class='btn form-control'>Details</a></td>
               </tr>";
            }
            ?>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><a href='<?=site_url("cms/brand/house/new")?>' class='btn btn-primary form-control'>Add New House</a></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
</script>