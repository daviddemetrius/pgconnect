<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-home right-sm"></i>Add New House
                <div class='tableheaderback'><a href="<?=site_url("cms/brand/house")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type House Name" name="housename" max="2" value="<?php echo set_value('housename'); ?>"/>
                <span>House Name</span>
              </label>

              <label class="has-float-label">
                <input type="file" placeholder="File" name="userfile" value="<?php echo set_value('userfile'); ?>" accept="image/*"/>
                <span>Thumbnail</span>
              </label>       

              <button class='form-control btn btn-primary'>Add House</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	