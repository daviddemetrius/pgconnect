<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-tasks right-sm"></i>Issue Details
      <div class='tableheaderback'><a href="<?=site_url("cms/brand/issue")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/images/uploads/?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h3><?= $issuedetails->issuename?></h3>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              Created <strong><?= date( "j F Y H:i:s", strtotime($issuedetails->created))?></strong>
            </div>
            <div class='detailcontent'>
              Updated <strong><?= date( "j F Y H:i:s", strtotime($issuedetails->lastupdated))?></strong>
            </div>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/brand/editIssue')?>">
      <fieldset class='editor hidden'>
        <input type="hidden" name="issueid" value="<?php echo $issuedetails->issueid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Issue Name" name="healthissues" value="<?php echo $issuedetails->issuename?>"/>
          <span>Issue Name</span>
        </label>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      Products With Issue
    </div>
    <div class='tablecontent assignmenttable' id='tablecontent'>
      <table>
         <tr>
            <th width=10%>#</th>
            <th>Product Name</th>
            <th>Product Type</th>
            <th>House Category</th>
            <th></th>
         </tr>
         <?php
         $page = 0;
         $first = "showrow";
         foreach($issuedetails->products as $key=>$cat){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
            echo "
            <tr class='pageddata page$page $first'>
               <td>".($key+1)."</td>
               <td>$cat->productname</td>
               <td>".($cat->danoneflag==1?"<span class=\"greent\">Danone Product</span>":"<span class=\"oranget\">Competitor Product</span>")."</td>
               <td>$cat->housename - $cat->categoryname</td>
               <td><a class='form-control btn' href='".site_url("cms/brand/products/".$cat->productid)."' target='_blank'>Details</a></td>
            </tr>
            ";
         }
         ?> 
         <tr>
           <td colspan='6' class='pagingrow'>
             <?php
             $first = "currentindex";
             for ($i = 0; $i < $page; $i++){
              echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
              $first = "";
             }
             ?>
           </td>
         </tr>
      </table>
    </div>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

 
</script>