<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-tasks right-sm"></i>Add New Health Issue
                <div class='tableheaderback'><a href="<?=site_url("cms/brand/issue")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Health Issue Name" name="issuename" max="2" value="<?php echo set_value('issuename'); ?>"/>
                <span>Health Issue Name</span>
              </label>

              <button class='form-control btn btn-primary'>Add Health Issue</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	