<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
			<form method="POST" action="" enctype="multipart/form-data">
				<fieldset>
					<div class='tableheader'>
					<i class="glyphicon glyphicon-upload"></i> Upload ITR
					</div>
					<div class='centerfields bigger'>
						<label>Month / Year</label>
						<div class='row'>
							<div class='col-xs-2'><input type='number' min=1 max=12 class='form-control' id='month' value="<?=date("n")?>" onchange='changeMonth(this)'></div>
							<div class='col-xs-2'><input type='number' min=2018 class='form-control' id='year' value="<?=date("Y")?>" onchange='changeYear(this)'></div>
						</div>
						<label>Search ACC</label>
						<input type='text' class='form-control' id='accsearch' placeholder='ACC'>
						<label class='hidden'>Search HCP</label>
						<input type='text' class='form-control hidden' id='hcpsearch' placeholder='HCP' disabled>
						<input type='hidden' id='accid'>
					</div>
					<div class='centerfields' id='productlist'>
						<?php 
						foreach ($products as $product){
							if ($product->obsolete == 1) continue;
							echo "
							<div class='row alternating'>
								<div class='col-xs-10'><label for='box$product->productid'>$product->productname</label></div>
								<div class='col-xs-2'><input type='checkbox' class='form-control' value=1 id='box$product->productid' data-id='$product->productid'></div>
							</div>
							";
						}
						?>
					</div>
					<div class='centerfields'>
						<button type='button' class='btn btn-primary form-control' onclick='saveItr()'>Save</button>
					</div>
				</fieldset>
			</form>
		</div>
	</section>
</section>
<script>
	var chosenAcc = 0;
	$('#accsearch').autocomplete({
	    	serviceUrl: '<?=base_url()?>cms/closing/searchacc',
	    	onSelect: function (suggestion) {
	        	//alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
	        	chosenAcc = suggestion.data;
	        	$("#hcpsearch").prop('disabled', false);
			fetchITRData();
	    	}
	});
	var chosenHcp = 0;
	$('#hcpsearch').autocomplete({
	    	serviceUrl: '<?=base_url()?>cms/closing/searchhcp',
	    	onSearchStart: function(params){
	    		params.accid = chosenAcc;
	    	},
	    	onSelect: function (suggestion) {
	    		chosenHcp = suggestion.data;
			fetchITRData();
	    	}
	});
	var month = <?=date("n")?>;
	var year = <?=date("Y")?>;

	function changeMonth(e){
		month = e.value;
		if (chosenAcc != 0){
			fetchITRData();
		}
	}
	function changeYear(e){
		year = e.value;
		if (chosenAcc != 0){
			fetchITRData();
		}
	}

	function fetchITRData(){
		if (chosenHcp != 0){
			$.ajax({
				url: '<?=site_url("cms/closing/getAccHcpITRData")?>',
				type: "get",
				data: {
					foryear: year,
					formonth: month,
					accid: chosenAcc,
					hcpid: chosenHcp
				}, success: function(data){
					$("#productlist").html(data);
				}
			})
		} else if (chosenAcc != 0){
			$.ajax({
				url: '<?=site_url("cms/closing/getAccITRData")?>',
				type: "get",
				data: {
					foryear: year,
					formonth: month,
					accid: chosenAcc
				}, success: function(data){
					$("#productlist").html(data);
				}
			})			
		}
	}

	function saveItr(){
		var inputs = $("#productlist input");
		var productdata = [];
		for (var i = 0; i < inputs.length; i++){
			productdata.push({
				productid: $(inputs[i]).data("id"),
				intention: ($(inputs[i]).is(":checked")?"1":"0")
			});
		}
		if (chosenHcp != 0){
			$.ajax({
				url: '<?=site_url("cms/closing/saveAccHcpITRData")?>',
				type: "post",
				data: {
					foryear: year,
					formonth: month,
					accid: chosenAcc,
					hcpid: chosenHcp,
					productdata: productdata
				}, success: function(data){
					window.location = '<?=site_url("cms/closing/mifi")?>';
				}
			})

		} else if (chosenAcc != 0){
			$.ajax({
				url: '<?=site_url("cms/closing/saveAccITRData")?>',
				type: "post",
				data: {
					foryear: year,
					formonth: month,
					accid: chosenAcc,
					productdata: productdata
				}, success: function(data){
					window.location = '<?=site_url("cms/closing/mifi")?>';
				}
			})	
		}
	}
</script>