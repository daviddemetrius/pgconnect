<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
			<form method="POST" action="" enctype="multipart/form-data">
				<fieldset>
					<div class='tableheader'>
					<i class="glyphicon glyphicon-upload"></i> Upload Share
					</div>
					<div class='centerfields bigger'>
						<label>Month / Year</label>
						<div class='row'>
							<div class='col-xs-2'><input type='number' min=1 max=12 class='form-control' id='month' value="<?=date("n")?>" onchange='changeMonth(this)'></div>
							<div class='col-xs-2'><input type='number' min=2018 class='form-control' id='year' value="<?=date("Y")?>" onchange='changeYear(this)'></div>
						</div>
						<label>Search ACC</label>
						<input type='text' class='form-control' id='accsearch' placeholder='ACC' id='productlist'>
					</div>
					<div class='centerfields bigger' id='productlist'>
						<div class='row alternating'>
							<div class='col-xs-8'></div>
							<div class='col-xs-2'>Birth</div>
							<div class='col-xs-2'>Peds</div>
						</div>
						<?php 
						foreach ($products as $product){
							if ($product->obsolete == 1) continue;
							echo "
							<div class='row alternating prods' data-id='$product->productid'>
								<div class='col-xs-8'>$product->productname</div>
								<div class='col-xs-2'><input type='number' class='form-control' value=0></div>
								<div class='col-xs-2'><input type='number' class='form-control' value=0></div>
							</div>
							";
						}
						?>
					</div>
					<div class='centerfields'>
						<button type='button' class='btn btn-primary form-control' onclick='saveShare()'>Save</button>
					</div>
				</fieldset>
			</form>
		</div>
	</section>
</section>
<script>
	var chosenAcc = 0;
	$('#accsearch').autocomplete({
	    	serviceUrl: '<?=base_url()?>cms/closing/searchacc',
	    	onSelect: function (suggestion) {
	        	chosenAcc = suggestion.data;
	        	$("#hcpsearch").prop('disabled', false);
			fetchShareData();
	    	}
	});
	var month = <?=date("n")?>;
	var year = <?=date("Y")?>;

	function changeMonth(e){
		month = e.value;
		if (chosenAcc != 0){
			fetchShareData();
		}
	}
	function changeYear(e){
		year = e.value;
		if (chosenAcc != 0){
			fetchShareData();
		}
	}
	function fetchShareData(){
		if (chosenAcc != 0){
			$.ajax({
				url: '<?=site_url("cms/closing/getAccShareData")?>',
				type: "get",
				data: {
					foryear: year,
					formonth: month,
					accid: chosenAcc
				}, success: function(data){
					$("#productlist").html(data);
				}
			})
		}
	}

	function saveShare(){
		var inputs = $("#productlist .prods");
		var productdata = [];		
		for (var i = 0; i < inputs.length; i++){
			productdata.push({
				productid: $(inputs[i]).data("id"),
				birth: $(inputs[i]).find("input").first().val(),
				peds: $(inputs[i]).find("input").last().val()
			});
		}
		if (chosenAcc != 0){
			$.ajax({
				url: '<?=site_url("cms/closing/saveAccShareData")?>',
				type: "post",
				data: {
					foryear: year,
					formonth: month,
					accid: chosenAcc,
					productdata: productdata
				}, success: function(data){
					window.location = '<?=site_url("cms/closing/mifs")?>';
				}
			})	
		}
	}
</script>