<?php if ($this->session->userdata("tier") <= 2 && $module->obsolete == 1){redirect("cms");} ?>
<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/Sortable.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
$reptypearr = array();
$reptypestr = "";
$delim = "";
foreach ($module->reptypes as $rept){
  $reptypearr[] = $rept->reptypeid;
  $reptypestr .= $delim.$rept->typename;
  $delim = ", ";
}
$accountarr = array();
$accountstr = "";
$delim = "";
foreach ($module->accounts as $rept){
  $accountarr[] = $rept->hospitalid;
  $accountstr .= $delim.$rept->hospitalname;
  $delim = ", ";
}
$grouparr = array();
$groupstr = "";
$delim = "";
foreach ($module->groups as $rept){
  $grouparr[] = $rept->groupid;
  $groupstr .= $delim.$rept->groupname;
  $delim = ", ";
}
?>
<section id="main-content">
  <section class="wrapper site-min-height"> 
    <div class='fullblock'>
      <div class='tableheader'>
        <i class="glyphicon glyphicon-briefcase right-sm"></i> <?=$module->moduletitle?> <?=$module->obsolete==1?" - <span class='oranget'>Deactivated</span>": ""?>
        <div class='tableheaderback'><a href="<?=$referrer?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
      </div>
      <div class='blockdetails'>
        <div class='detailseparator'>
          <div class='row'>
            <div class='col-md-2 detailimage'>
            </div>
            <div class='col-md-4'>
              <div class='detailcontent'>
                <?=$module->moduledesc?>
              </div>
              <div class='detailcontent'>
                <strong><?=$module->generaltype==1?"General Module":"Normal Module"?></strong>
              </div>
              <div class='detailcontent'>
                <?=date("j F Y", strtotime($module->periodstart))?> - <?=date("j F Y", strtotime($module->periodend))?>
              </div>
            </div>
            <div class='col-md-6'>
              <div class='detailcontent'>
                <i class='glyphicon glyphicon-link'></i> <?=$groupstr?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='blockdetails'>
        <form method='post' action='<?=site_url("cms/modules/updatemodulepages")?>'>
          <input type='hidden' name='moduleid' value="<?=$module->moduleid?>">
          <div class='detailseparator'>
            <div class='row'>
              <div class='col-md-2'></div>
              <div class='col-md-8'>
                <h4>Pages:</h4>
                <ul class='sortable'>
                <?php
                foreach ($module->pages as $step){
                  if ($step->bookmark == 1){
                    $checkboxhtml = "<span class='bookmarkbox hidden'><label for='num$step->pagenumber'><i class='glyphicon glyphicon-star'></i></label> <input type='checkbox' name='num$step->pagenumber' id='num$step->pagenumber' checked onchange='changeHiddenBookmark(this)'/></span>";
                  } else {
                    $checkboxhtml = "<span class='bookmarkbox hidden'><label for='num$step->pagenumber'><i class='glyphicon glyphicon-star'></i></label> <input type='checkbox' name='num$step->pagenumber' id='num$step->pagenumber' onchange='changeHiddenBookmark(this)'/></span>";
                  }
                  echo "<li><span class='sortableimg'><img src='".base_url()."assets/thumbs/$step->pageid.jpg?".rand()."'></span>$step->pagenumber. ".$step->pagetitle." $checkboxhtml ".($step->obsolete==1?'- <span class="oranget">UNPUBLISHED</span>':'') . ($this->session->userdata('tier')>2?"<i class='js-remove'>✖</i>":"").
                    "<input type='hidden' name='pagenumber[]' value='$step->pagenumber'>
                    <input type='hidden' name='pageid[]' value='$step->pageid'>
                    <input type='hidden' name='bookmark[]' value='$step->bookmark' class='hiddenbkmark'>
                  </li>";
                }
                ?>
                </ul>
                <?php if ($this->session->userdata("tier") > 2.5){ ?>
                <button type='button' id="addItem" class='form-control btn btn-primary'>Add</button>
                <?php } ?>
              </div>
              <div class='col-md-2'></div>
            </div>
          </div>
          <div class='detailbuttons'>
            <a href="<?=site_url("cms/modules/preview/".$module->moduleid)?>" class='detailbtn hidden' target='_blank'><i class="glyphicon glyphicon-picture"></i> <span class=''>View Preview</span></a>
            <?php if ($this->session->userdata("tier") > 2.5){ ?>
            <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit Basic Info</span></a>
            <button href="#" class='detailbtn btn' id="editbutton"><i class="glyphicon glyphicon-floppy-disk"></i> <span class=''>Save Page Order</span></button>
            <?php } ?>
          </div>
        </form>
      </div>
      <?php if ($this->session->userdata("tier") > 2.5){ ?>
      <form method="POST" action="<?=site_url('cms/modules/editModule')?>" enctype="multipart/form-data">
        <fieldset class='editor hidden'>
          <input  type='hidden' value='<?=$module->moduleid?>' name='id'>
          <label class="has-float-label">
            <input type="text" placeholder="Title" name="moduletitle" value="<?php echo $module->moduletitle; ?>"/>
            <span>Module Title</span>
          </label>
          <label class="has-float-label">
            <input type="date" placeholder="Start" name="periodstart" value="<?php echo date("Y-m-d", strtotime($module->periodstart)); ?>"/>
            <span>Active Period Start</span>
          </label>
          <label class="has-float-label">
            <input type="date" placeholder="End" name="periodend" value="<?php echo date("Y-m-d", strtotime($module->periodend)); ?>"/>
            <span>Active Period End</span>
          </label>
          <label class="has-float-label">
            <input type="text" placeholder="Description" name="moduledesc" value="<?php echo $module->moduledesc; ?>"/>
            <span>Module Description</span>
          </label>
          <label>Module Groups <i class='glyphicon glyphicon-check' onclick='$("#mgfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#mgfields input").prop("checked", false)' style='cursor:pointer'></i></label>
          <div class='row' id='mgfields'>
            <?php
            foreach ($modcats as $type){
              $filterhidden = "";
              if ($module->generaltype == 1 && $type->filterfor == 2 ){
                $filterhidden = "hidden";
              } else if ($module->generaltype == 0 && $type->filterfor == 3 ){
                $filterhidden = "hidden";
              }
              if (in_array($type->groupid, $grouparr)){
                echo '<div class="col-xs-6 modcat '.$filterhidden.'" data-type="'.$type->filterfor.'"><input type="checkbox" value="'.$type->groupid.'" name="modcats[]" checked> '.$type->groupname.'</div>';
              } else {
                echo '<div class="col-xs-6 modcat '.$filterhidden.'" data-type="'.$type->filterfor.'"><input type="checkbox" value="'.$type->groupid.'" name="modcats[]"> '.$type->groupname.'</div>';
              }
              
            }
            ?>
          </div>
          <br/>
          <!-- <label class='generalunused' <?=$module->generaltype==1?"style='display:none'":""?>>Accounts <i class='glyphicon glyphicon-check' onclick='$("#accntfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#accntfields input").prop("checked", false)' style='cursor:pointer'></i></label>
          <div class='row generalunused' id='accntfields' <?=$module->generaltype==1?"style='display:none'":""?>> -->
            <?php
            // foreach ($hospitals as $type){
            //   if (in_array($type->hospitalid, $accountarr)){
            //     echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]" checked> '.$type->hospitalname.'</div>';
            //   } else {
            //     echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]"> '.$type->hospitalname.'</div>';
            //   }
              
            // }
            ?>
          <!-- </div>
          <br/> -->
          <label>Module Active</label><br/>
          <input type='checkbox' value="1" name='active' class='form-control' <?=$module->obsolete==0?'checked':''?>><br/>

          <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
          <button class='form-control btn btn-primary' id="finishbutton">Save</button>
        </fieldset>
      </form>
      <?php } ?>
    </div>
    <?php /*
    <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-list-alt right-sm"></i> Quiz List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>  
                <th width=10%>Type</th> 
                <th>Question</th> 
                <th>Answer</th> 
                <th width=20% colspan=2></th>
             </tr>
             <?php
             foreach($module->quiz as $mkey=>$mod){
               $btnedit = "<a href='".site_url("cms/modules/deletequiz/$module->moduleid/$mod->quizid")."' class='form-control btn btn-danger'>Deactivate</a>";
               $status = "<span class='greent'>Active</span>";
               if ($mod->availability == 0){
                $status = "<span class='oranget'>Deactivated</span>";
                $btnedit = "<a href='".site_url("cms/modules/activatequiz/$module->moduleid/$mod->quizid")."' class='form-control btn btn-warning'>Activate</a>";
               }
               $answer = "";
               if ($mod->quiztype == 0){
                $answersarr = explode("xvz:zvx", $mod->quizanswer);
                foreach ($answersarr as $key=>$ans){
                  $answer .= ($key+1).". ".$ans."<br/>";
                }
               }
               echo "
               <tr>
                  <td>".($mkey+1)."</td>
                  <td>".($mod->quiztype==0?"Multiple Choice":($mod->quiztype==1?"Free Text":"Multiple Check"))."</td>
                  <td>$mod->quiztext</td>
                  <td>$answer</td>
                  <td colspan=2>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2>
               <?php
                if ($this->session->userdata("tier") > 2.5){ ?>
                  <a class='btn form-control' href='<?=site_url("cms/modules/quiz/$module->moduleid")?>'>Add New Quiz</a>
               <?php }?>
               </td>
            </tr>
         </table>
      </div>
    */ ?>
  </section>
</section>	

<script>
  function checkModcats(e){
    var modcats = $(".modcat");
    modcats.removeClass("hidden");
    if ($(e).val() == "0"){
      $(".generalunused").show();
      modcats.each(function(){
        if ($(this).data("type") == 3){
          $(this).find("input").prop("checked", false);
          $(this).addClass("hidden");
        }
      })
    } else {
      $(".generalunused").hide();
      modcats.each(function(){
        if ($(this).data("type") == 2){
          $(this).find("input").prop("checked", false);
          $(this).addClass("hidden");
        }
      })      
    }
  }
function changeHiddenBookmark(e){
  if ($(e).is(":checked")){
    $(e).parent().parent().find(".hiddenbkmark").val(1);
  } else {
    $(e).parent().parent().find(".hiddenbkmark").val(0);
  }
}

<?php if ($this->session->userdata("tier") > 2.5){ ?>
   $("#editbutton").on("click", function(){
      $("input").prop("disabled", false);
      $(".blockdetails").addClass("hidden");
      $(".editor").removeClass("hidden");
   })
   $("#cancelbutton").on("click", function(){
      $("input").prop("disabled", true);
      
      $(".blockdetails").removeClass("hidden");
      $(".editor").addClass("hidden");
   })
   function switchpage(number, e){
    $(e).parent().parent().parent().find(".currentindex").removeClass("currentindex");
    $(e).addClass('currentindex');
    $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
    $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
   }
<?php } ?>
   <?php
   $displaygroup = array();
   foreach ($grouplist as $grp){
    if (!in_array($grp->id, $grouparr)){
      $displaygroup[''.$grp->id] = $grp->label;
    }
   }
   ?>
    $("tr.pageddata").hover(function(){
      $(this).find(".rowdeleter").removeClass("hidden");
      $(this).find(".rowkey").addClass("hidden");
    }, function(){
      $(this).find(".rowdeleter").addClass("hidden");
      $(this).find(".rowkey").removeClass("hidden");
    });
<?php if ($this->session->userdata("tier") > 2.5){ ?>
   function addNewGroup(){
    if (Object.keys(grpsrc).length > 0){
      swal({
        title: 'Pilih Grup',
        input: 'select',
        inputOptions: grpsrc,
        inputClass: 'form-control',
        showCancelButton: true,
      }).then((result) => {
        if (result.value){
          openAJAXLoader();
          $.ajax({
            url: '<?=site_url("cms/modules/addGroup")?>',
            type: "POST",
            data: {gid: result.value, mid: <?=$module->moduleid?>},
            success: function(data){
              if (data == "success"){
                window.location = '<?=site_url("cms/modules/$module->moduleid")?>';
              } else {
                swal("Ups!", "Terjadi Kesalahan saat penginputan data", "error");
              }
            }
          })
        }
      })
    } else {
      swal("Ups!", "Tidak bisa menambahkan grup baru ke module ini");
    }
   }

    function openAJAXLoader(){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: true,
        onOpen: () => {
          swal.showLoading();
        }
      })
    }

   function deleteGroup(groupid, moduleid){
      openAJAXLoader();
      $.ajax({
        url: '<?=site_url("cms/modules/deleteGroup")?>',
        type: "POST",
        data: {gid: groupid, mid: moduleid},
        success: function(data){
          if (data == "success"){
            window.location = '<?=site_url("cms/modules/$module->moduleid")?>';
          } else {
            swal("Ups!", "Terjadi Kesalahan saat penghapusan data", "error");
          }
        }
      })
    }
   var grpsrc = <?=json_encode($displaygroup)?>;
   $(document).ready(function(){
    // Editable list
    var editableList = Sortable.create($(".sortable")[0], {
      filter: '.js-remove',
      onFilter: function (evt) {
        var el = editableList.closest(evt.item); // get dragged item
        el && el.parentNode.removeChild(el);
      }
    });
    $("#addItem").on("click", inputValues);
   })
   
   var pagesrc = <?=json_encode($pagelist)?>;
   function inputValues(){
    swal.setDefaults({
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1']
    });
    var swalsteps = [{
      title: 'Find Page',
      html: "<div style='position:relative' id='srcdiv'><div><img src='' style='max-height:100px;margin-bottom:10px'></div><input type='text' class='form-control' id='src' placeholder='Search...'><input type='hidden' id='srcid' value='-1'><input type='hidden' id='srclink' value='-1'></div>",
      onOpen: function() {
        $('#src').autocomplete({
          source: pagesrc,
          appendTo: '#srcdiv',
          select: function(event, ui){
            $("#src").val(ui.item.label);
            $("#srcid").val(ui.item.id);
            $("#srclink").val(ui.item.link);
            $("#srcdiv img").attr("src", "<?=base_url()?>assets/thumbs/"+ui.item.link+".jpg");
          }
        });
      },
      preConfirm: function(){
        return Promise.resolve({id: $('#srcid').val(), name: $('#src').val(), link: $('#srclink').val(), });
      }
    }];

    swal.queue(swalsteps).then((result) => {
      swal.resetDefaults();
      if (result.value[0].link != -1) {
        $(".sortable").append('<li><span class="sortableimg"><img src="<?=base_url()?>assets/thumbs/'+result.value[0].link+'.jpg"></span>'+result.value[0].name+'<i class="js-remove">✖</i><input type="hidden" name="pagenumber[]" value="NEW"><input type="hidden" name="pageid[]" value="'+result.value[0].id+'"></li>');
      }
    });
   }
<?php } ?>
</script>