<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-briefcase right-sm"></i> Module List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%><a href='<?=$sitelink?>?s=moduleid<?=$addllink?>' class="<?=$s=="moduleid"?"active":""?>">#</a>  </th>	 
                <th><a href='<?=$sitelink?>?s=moduletitle<?=$addllink?>' class="<?=$s=="moduletitle"?"active":""?>">Title</a></th>	
                <th><a href='<?=$sitelink?>?s=moduledesc<?=$addllink?>' class="<?=$s=="moduledesc"?"active":""?>">Description</a></th>
                <th><a href='<?=$sitelink?>?s=obsolete<?=$addllink?>' class="<?=$s=="obsolete"?"active":""?>">Status</a></th>
                <th width=20% colspan=2><select class='form-control' id='srcsel'><option value=''>All</option><option value='moduletitle'>Title</option><option value='moduleid'>ID</option><option value='moduledesc'>Description</option><option value='general'>All General</option><option value='nongeneral'>All Non-General</option><option value='inactive'>All Inactives</option><option value='active'>All Actives</option></select><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($modules as $key=>$mod){
               $btnedit = "<a href='".site_url("cms/modules/$mod->moduleid")."' class='form-control btn btn-primary'>Details</a>";
               $status = "<span class='greent'>Active</span>";
               $btnobs = "<a href='javascript:void(0)' class='form-control btn btn-danger' onclick='toggleActive($mod->moduleid)'>Discontinue</a>";
               if ($mod->periodstart > date("Y-m-d H:i:s")){
                $status = "<span class='oranget'>Not started</span>";
               }
               if ($mod->periodend < date("Y-m-d H:i:s")){
                $status = "<span class='oranget'>Expired</span>";
               }
               if ($mod->obsolete == 1){
                $btnobs = "<a href='javascript:void(0)' class='form-control btn btn-warning' onclick='toggleActive($mod->moduleid)'>Publish</a>";
                $status = "<span class='oranget'>Deactivated</span>";
                if ($this->session->userdata("tier") <= 2.5){
                  continue;
                }
               }
                if ($this->session->userdata("tier") <= 2.5){
                  $btnobs = '<a href="'.site_url("cms/modules/preview/".$mod->moduleid).'" class="form-control btn btn-warning" target="_blank">View Preview</a>';
                }
               echo "
               <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td>$mod->moduletitle</td>
                  <td>$mod->moduledesc</td>
                  <td>$status</td>
                  <td>$btnobs</td>
                  <td>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2>
               <?php
                if ($this->session->userdata("tier") > 2.5){ ?>
                  <a class='btn form-control' href='<?=site_url("cms/modules/new")?>'>Add New Module</a>
               <?php }?>
               </td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var f = $("#srcsel").val();
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"&f="+f+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });

<?php if ($this->session->userdata("tier") > 2.5){ ?>
   function toggleActive(id){
    swal({
      title: 'Konfirmasi',
      type:"warning",
      showCancelButton: true,
    }).then((result) => {
      if (result.value){
        openAJAXLoader();
        $.ajax({
          url: '<?=site_url("cms/modules/togglemodule")?>',
          type: "POST",
          data: {moduleid: id},
          success: function(data){
            if (data == "success"){
              window.location = '<?=site_url("cms/modules/?p=$page$addllink")?>';
            } else {
              swal("Ups!", "Terjadi Kesalahan saat penggantian data", "error");
            }
          }
        })
      }
    })
   }
   <?php } ?>

    function openAJAXLoader(){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: true,
        onOpen: () => {
          swal.showLoading();
        }
      })
    }
</script>