<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-briefcase"></i> Add New Module
                <div class='tableheaderback'><a href="<?=site_url("cms/modules/")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Title" name="moduletitle" value="<?php echo set_value('moduletitle'); ?>"/>
                <span>Module Title</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="Start" name="periodstart" value="<?php echo set_value('periodstart'); ?>"/>
                <span>Active Period Start</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="End" name="periodend" value="<?php echo set_value('periodend'); ?>"/>
                <span>Active Period End</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Description" name="moduledesc" value="<?php echo set_value('moduledesc'); ?>"/>
                <span>Module Description</span>
              </label>

              <label>Module Groups <i class='glyphicon glyphicon-check' onclick='$("#mgfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#mgfields input").prop("checked", false)' style='cursor:pointer'></i></label>
              <div class='row' id='mgfields'>
                <?php
                foreach ($modcats as $type){
                  $filterhidden = "";
                  if ($type->filterfor == 3){
                    $filterhidden = "hidden";
                  }
                  if (in_array($type->groupid, set_value('modcats', array()))){
                    echo '<div class="col-xs-6 modcat '.$filterhidden.'" data-type="'.$type->filterfor.'"><input type="checkbox" value="'.$type->groupid.'" name="modcats[]" checked> '.$type->groupname.'</div>';
                  } else {
                    echo '<div class="col-xs-6 modcat '.$filterhidden.'" data-type="'.$type->filterfor.'"><input type="checkbox" value="'.$type->groupid.'" name="modcats[]"> '.$type->groupname.'</div>';
                  }
                  
                }
                ?>
              </div>
              <br/>

              <!-- <label class='generalunused'>Apotik <i class='glyphicon glyphicon-check' onclick='$("#accntfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#accntfields input").prop("checked", false)' style='cursor:pointer'></i></label>
              <div class='row generalunused' id='accntfields'> -->
                <?php /*
                foreach ($hospitals as $type){
                  if (in_array($type->hospitalid, set_value('hospitals', array()))){
                    echo '<div class="col-xs-3"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]" checked> '.$type->hospitalname.'</div>';
                  } else {
                    echo '<div class="col-xs-3"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]"> '.$type->hospitalname.'</div>';
                  }
                  
                }*/
                ?>
              <!-- </div> -->
              <!-- <br/> -->

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	
<script>
  function checkModcats(e){
    var modcats = $(".modcat");
    modcats.removeClass("hidden");
    if ($(e).val() == "0"){
      $(".generalunused").show();
      modcats.each(function(){
        if ($(this).data("type") == 3){
          $(this).find("input").prop("checked", false);
          $(this).addClass("hidden");
        }
      })
    } else {
      $(".generalunused").hide();
      modcats.each(function(){
        if ($(this).data("type") == 2){
          $(this).find("input").prop("checked", false);
          $(this).addClass("hidden");
        }
      })      
    }
  }
</script>