<style>
  .fullscreen{
    width:90%;
    height:100%;
  }
  .hidden{
    display:none;
  }
</style>
<script type='text/javascript' src='<?=base_url()?>assets/masterCssJs/layerslider/js/jquery.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/masterCssJs/layerslider/js/greensock.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/masterCssJs/layerslider/js/layerslider.kreaturamedia.jquery.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/masterCssJs/layerslider/js/layerslider.transitions.js'></script>
<link href="<?php echo base_url(); ?>assets/masterCssJs/layerslider/css/layerslider.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/masterCssJs/main.css" rel="stylesheet">
<?php
$first = "";
foreach ($pages as $key=>$page){
  echo "<div class='page page$key $first' data-key='$key'></div>";
  $first = "hidden";
}
?>
<div class='endpage hidden'>Preview Done. Click again to start over</div>
<script>

  $(document).ready(function(){
    $(document).on("click", function(){
      var currentpage = $(".page").not(".hidden");
      $(".endpage").addClass("hidden");
      if (currentpage.next().length == 0){
        $(".page").addClass("hidden");
        $(".page").first().removeClass('hidden');
        $(".page").find("object").addClass("hidden");
        $(".page").first().find("object").removeClass('hidden');
        if (currentpage.next().next().length){
          $(".endpage").removeClass("hidden");
        }
      } else {
        currentpage.next().removeClass("hidden");
        currentpage.addClass("hidden");
        currentpage.next().find("object").removeClass("hidden");
        currentpage.find("object").addClass("hidden");
      }
    })

    <?php
    $first = "";
    foreach($pages as $key=>$page){
      echo "
      $('.page$key').html ('<object class=\"$first fullscreen\" data=\"".site_url('assets/preview/'.$page->pagename.'/'.$page->pagename).".html\"/>');
      ";
      $first = "hidden";
    }
    ?>
  })
</script>