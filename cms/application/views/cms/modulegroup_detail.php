<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
<section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-link right-sm"></i> Module Group Detail
      <div class='tableheaderback'><a href="<?=site_url("cms/modules/group")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/assets/modcats/$groupdata->groupid.png?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h1><?=$groupdata->groupname?></h1>
            </div>
          </div>
          <div class='col-md-5'>

          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/modules/editModuleGroup')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
       <input  type='hidden' value='<?=$groupdata->groupid?>' name='id'>
       <label class="has-float-label">
        <input type="text" placeholder="Name" name="groupname" value="<?=$groupdata->groupname?>" disabled/>
        <span>Group Name</span>
      </label>


      <label class="has-float-label">
        <input type="file" placeholder="File" name="userfile" value="" accept="image/*"/>
        <span>New Thumbnail</span>
      </label>       

      <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
      <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      Module List
    </div>
    <div class='tablecontent'>
      <table>
        <tr>
          <th width=10%>#</th>
          <th>Title</th>
          <th>Description</th>
          <th>House</th>
          <th>Status</th>
          <th></th>
        </tr>
        <?php
        $page = 0;
        $first = "showrow";
        foreach($groupdata->modules as $key=>$act){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
         $status = "<span class='greent'>Active</span>";
         if ($act->obsolete == 1){
          $status = "<span class='oranget'>Deactivated</span>";
         }
          echo "
          <tr class='pageddata page$page $first'>
            <td>".($key+1)."</td>
            <td>$act->moduletitle</td>
            <td>$act->moduledesc</td>
            <td>$act->housename</td>
            <td>$status</td>
            <td><a href='".site_url("cms/modules/$act->moduleid")."' class='form-control btn btn-primary'>Details</a></td>
          </tr>
          ";
        }
        ?>
        <tr>
          <td colspan='4' class='pagingrow'>
          <?php
          $first = "currentindex";
          for ($i = 0; $i < $page; $i++){
            echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
            $first = "";
          }
          ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input").prop("disabled", false);
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
 function switchpage(number, e){
  $(e).parent().parent().parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}

function openAJAXLoader(){
  swal({
    title: 'OK!',
    text: 'Modal ini akan tertutup secara otomatis...',
    showCancelButton: false,
    allowOutsideClick: true,
    onOpen: () => {
      swal.showLoading();
    }
  })
}
</script>
