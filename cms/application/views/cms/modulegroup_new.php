<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-link"></i> Add New Group
                <div class='tableheaderback'><a href="<?=site_url("cms/modules/group")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Group Name" name="groupname" value="<?php echo set_value('groupname'); ?>"/>
                <span>Group Name</span>
              </label>       

              <label class="has-float-label">
                <input type="file" placeholder="File" name="userfile" value="<?php echo set_value('userfile'); ?>" accept="image/*"/>
                <span>Thumbnail</span>
              </label>         

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	