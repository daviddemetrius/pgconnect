<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
   	<div class='fullblock'>
            <div class='tableheader'>
              <i class="glyphicon glyphicon-file right-sm"></i> Page Details
              <div class='tableheaderback'><a href="<?=$referrer?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
            </div>
            <div class='blockdetails'>
              <div class='detailseparator'>
                <div class='row'>
                  <div class='col-md-2 detailimage'>
                    <img src='<?=base_url()."assets/thumbs/$pagedata->pageid.jpg?".rand()?>' onerror='defaultppproperty(this)'>
                  </div>
                  <div class='col-md-5'>
                    <div class='detailcontent'>
                      <h1><?=$pagedata->pagetitle?></h1>
                    </div>
                    <div class='detailcontent'>
                      Status: <?= $pagedata->obsolete==1?"<span class='oranget'>Discontinued</span>":"<span class='greent'>Active</span>"; ?>
                    </div>
                    <div class='detailcontent'>
                      Category: <?=$pagedata->categoryname?>
                    </div>
                    <div class='detailcontent'>
                      <?=$pagedata->description?>
                    </div>
                  </div>
                  <div class='col-md-5'>
                    <div class='detailcontent'>
                      <a href='<?=base_url()."assets/pages/$pagedata->pageid.$pagedata->pagehtmllink"?>' target="_blank"><i class='glyphicon glyphicon-save'></i> Download</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class='detailbuttons'>
                <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
              </div>
            </div>
            <form method="POST" action="<?=site_url('cms/modules/editPage')?>" enctype="multipart/form-data">
            <fieldset class='editor hidden'>
               <input  type='hidden' value='<?=$pagedata->pageid?>' name='id'>
              <label class="has-float-label">
                <input type="text" placeholder="Name" name="pagetitle" value="<?php echo $pagedata->pagetitle; ?>"/>
                <span>Page Title</span>
              </label>
              <label class="has-float-label">
                <select name="pagecategory">
                  <?php
                  foreach ($pagecats as $cat){
                    if ($cat->categoryid == $pagedata->pagecategory){
                      echo "<option value='$cat->categoryid' selected>$cat->categoryname</option>";
                    } else {
                      echo "<option value='$cat->categoryid'>$cat->categoryname</option>";
                    }
                  }
                  ?>
                </select>
                <span>Page Category</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Description" name="description" value="<?php echo $pagedata->description; ?>"/>
                <span>Page Description</span>
              </label>


              <label class="has-float-label">
                <input type="hidden"  name="fname" value="<?php echo $pagedata->pageid.".".$pagedata->pagehtmllink; ?>"/>
                <input type="file" placeholder="File" name="userfile" value=""/>
                <span>Page File</span>
              </label>

              <label class="has-float-label">
                <input type="hidden"  name="imgname" value="<?php echo $pagedata->pageid.".jpg"; ?>"/>
                <input type="file" placeholder="Image" name="thumbimg" value=""/>
                <span>Page Thumb</span>
              </label>

                <label>Page Active</label><br/>
                <input type='checkbox' value="1" name='active' class='form-control' <?=$pagedata->obsolete==0?'checked':''?>><br/>

              <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
              <button class='form-control btn btn-primary' id="finishbutton">Save</button>
            </fieldset>
            </form>
         </div>
          <div class='fullblock'>
            <div class='tableheader'>
              Contained in
            </div>
            <div class='tablecontent'>
              <table>
                 <tr>
                    <th width=10%>#</th>
                    <th>Module Title</th>
                    <th>Module Description</th>
                    <th>Page Number</th>
                 </tr>
                 <?php
                 foreach($pagedata->modules as $key=>$act){
                    echo "
                    <tr>
                       <td>".($key+1)."</td>
                       <td><a href='".site_url("cms/modules/$act->moduleid")."'>$act->moduletitle</a></td>
                       <td>$act->moduledesc</td>
                       <td>$act->pagenumber</td>
                    </tr>
                    ";
                 }
                 ?>
              </table>
            </div>
          </div>
	</section>
</section>	

<script>
   $("#editbutton").on("click", function(){
      $("input").prop("disabled", false);
      $(".blockdetails").addClass("hidden");
      $(".editor").removeClass("hidden");
   })
   $("#cancelbutton").on("click", function(){
      $("input").prop("disabled", true);
      
      $(".blockdetails").removeClass("hidden");
      $(".editor").addClass("hidden");
   })
</script>