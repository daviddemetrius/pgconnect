<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-file right-sm"></i> Content Page List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%><a href='<?=$sitelink?>?s=pageid<?=$addllink?>' class="<?=$s=="pageid"?"active":""?>">#</a></th>	
                <th>Preview</th>  
                <th><a href='<?=$sitelink?>?s=pagetitle<?=$addllink?>' class="<?=$s=="pagetitle"?"active":""?>">Title</a></th>	
                <th><a href='<?=$sitelink?>?s=categoryname<?=$addllink?>' class="<?=$s=="categoryname"?"active":""?>">Category</a></th> 
                <th><a href='<?=$sitelink?>?s=description<?=$addllink?>' class="<?=$s=="description"?"active":""?>">Description</a></th>	
                <th>Download</th>
                <th><a href='<?=$sitelink?>?s=obsolete<?=$addllink?>' class="<?=$s=="obsolete"?"active":""?>">Status</a></th>
                <th width=20% colspan=2><select class='form-control' id='srcsel'><option value=''>All</option><option value='pagetitle'>Title</option><option value='pageid'>ID</option><option value='categoryname'>Category</option><option value='description'>Description</option><option value='inactive'>All Inactives</option><option value='active'>All Actives</option></select><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($pages as $key=>$pg){
               $btnedit = "<a href='".site_url("cms/modules/pages/$pg->pageid")."' class='form-control btn btn-primary'>Details</a>";
               $btnobs = "<a href='javascript:void(0)' class='form-control btn btn-danger' onclick='toggleActive($pg->pageid)'>Discontinue</a>";
               $status = "<span class='greent'>Active</span>";
               if ($pg->obsolete == 1){
                $btnobs = "<a href='javascript:void(0)' class='form-control btn btn-warning' onclick='toggleActive($pg->pageid)'>Publish</a>";
                $status = "<span class='oranget'>Discontinued</span>";
               }
               echo "
               <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td><img src='".base_url()."assets/thumbs/$pg->pageid.jpg?".rand()."'></td>
                  <td>$pg->pagetitle</td>
                  <td>$pg->categoryname</td>
                  <td>$pg->description</td>
                  <td><a href='".base_url()."assets/pages/$pg->pageid.$pg->pagehtmllink' target='_blank'><i class='glyphicon glyphicon-save'></i></a></td>
                  <td>$status</td>
                  <td>$btnobs</td>
                  <td>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2><a class='btn form-control' href='<?=site_url("cms/modules/pages/new")?>'>Add New Page</a></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var f = $("#srcsel").val();
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"&f="+f+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
   function toggleActive(id){
    swal({
      title: 'Konfirmasi',
      type:"warning",
      showCancelButton: true,
    }).then((result) => {
      if (result.value){
        openAJAXLoader();
        $.ajax({
          url: '<?=site_url("cms/modules/togglepage")?>',
          type: "POST",
          data: {pageid: id},
          success: function(data){
            if (data == "success"){
              window.location = '<?=site_url("cms/modules/pages?p=$page$addllink")?>';
            } else {
              swal("Ups!", "Terjadi Kesalahan saat penggantian data", "error");
            }
          }
        })
      }
    })
   }

    function openAJAXLoader(){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: true,
        onOpen: () => {
          swal.showLoading();
        }
      })
    }
</script>