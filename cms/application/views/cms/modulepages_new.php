<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-file"></i> Add New Page
                <div class='tableheaderback'><a href="<?=site_url("cms/modules/pages")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Name" name="pagetitle" value="<?php echo set_value('pagetitle'); ?>"/>
                <span>Page Title</span>
              </label>
              <label class="has-float-label">
                <select name="pagecategory">
                  <?php
                  foreach ($pagecats as $cat){
                    if ($cat->categoryid == set_value("pagecategory")){
                      echo "<option value='$cat->categoryid' selected>$cat->categoryname</option>";
                    } else {
                      echo "<option value='$cat->categoryid'>$cat->categoryname</option>";
                    }
                  }
                  ?>
                </select>
                <span>Page Category</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Description" name="description" value="<?php echo set_value('description'); ?>"/>
                <span>Page Description</span>
              </label>

              <label class="has-float-label">
                <input type="file" placeholder="File" name="userfile" value="<?php echo set_value('userfile'); ?>" accept="image/*"/>
                <span>Page File</span>
              </label>

              <label class="has-float-label">
                <input type="file" placeholder="Image" name="thumbimg" value="<?php echo set_value('thumbimg'); ?>" accept="image/*"/>
                <span>Page Thumb</span>
              </label>

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	