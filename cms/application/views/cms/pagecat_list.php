<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-<?=$listicon?> right-sm"></i> <?=$listtitle?> List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%><a href='<?=$sitelink?>?s=categoryid<?=$addllink?>' class="<?=$s=="categoryid"?"active":""?>">#</a></th>	
                <th><a href='<?=$sitelink?>?s=categoryname<?=$addllink?>' class="<?=$s=="categoryname"?"active":""?>">Page Category</a></th> 
                <th width=20%><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($items as $key=>$item){
              echo "
                <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>";
              foreach($item as $key=>$i){
                if ($key == "id"){
                  continue;
                }
                echo "<td><span class='editfield hidden' data-id='$item->id'><input type='text' value='$i' class='form-control'></span><span class='resfield'>$i</span></td>";
              }
              echo "
                  <td><a href='#' onclick='editThis(this)' class='btn btn-primary form-control' >Edit</a></td>
               </tr>";
            }
            ?>
            <tr>
              <td colspan=2></td>
              <td><a href='<?=site_url("cms/modules/category/new")?>' class='btn form-control'>Add New Category</a></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });

   function editThis(e){
    if ($(e).parent().parent().find(".editfield").hasClass("hidden")){
      $(e).parent().parent().find(".editfield").removeClass("hidden");
      $(e).parent().parent().find(".resfield").addClass("hidden");
      $(e).addClass("btn-warning").removeClass("btn-primary").html("Save");
    } else {
      $(e).parent().parent().find(".editfield").addClass("hidden");
      $(e).parent().parent().find(".resfield").removeClass("hidden");
      $(e).removeClass("btn-warning").addClass("btn-primary").html("Edit");
      $(e).parent().parent().find(".resfield").html($(e).parent().parent().find(".editfield input").val());
      var newval = $(e).parent().parent().find(".editfield input").val();
      var id = $(e).parent().parent().find(".editfield").data("id");
      $.ajax({
        url: "<?=site_url("cms/modules/editpagecatname")?>",
        type: "POST",
        data: {
          newval: newval,
          id: id
        }
      })
    }
   }
</script>