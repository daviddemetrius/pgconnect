<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-link"></i> Add New Category
                <div class='tableheaderback'><a href="<?=site_url("cms/modules/category")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Category Name" name="categoryname" value="<?php echo set_value('categoryname'); ?>"/>
                <span>Category Name</span>
              </label>              

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	