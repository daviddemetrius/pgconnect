<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">

    <title><?php echo $this->config->item("CMSHeaderPrefix");?> | Login</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>js/html5shiv.js"></script>
    <script src="<?php echo base_url(); ?>js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="" method="post">
        <h2 class="form-signin-heading"><?php echo $this->config->item("CMSName");?></h2>
        <div class="login-wrap">
            <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo set_value('username'); ?>" autofocus>
             
            <input type="password" class="form-control" name="pwd" placeholder="Password" value="<?php echo set_value('pwd'); ?>">
           
            <!--<label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            -->
            <button class="btn btn-lg btn-login btn-block" type="submit">Log In</button>
			   <span style="color: #F00; font-size:12px; line-height:14px;"> <?php echo form_error('username'); ?></span>
               <span style="color: #F00; font-size:12px; line-height:14px;"><?php echo form_error('pwd'); ?></span>
              <?php if (isset($login) && $login == "fail") { echo '<span style="color: #F00; font-size:12px; line-height:14px;">* Invalid Login. Please try again.</span>';}?>                 
        </div>
      </form>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>


  </body>
</html>
