<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-heart-empty right-sm"></i> Criteria List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	 
                <th>Criterion</th>	
                <th width=10%>Status</th>	
                <th width=20% colspan=2><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($modules as $key=>$mod){
               $btnedit = "<a class='btn btn-warning form-control' href='#' onclick='editType(this)'>Edit</a>";

               echo "
               <tr data-id='$mod->criteriaid'>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td><div class='typename'>$mod->criteriatitle</div><div class='editfield hidden'><input type='text' class='form-control' value='$mod->criteriatitle'></div></td>
                  <td><div class='typename'>".($mod->obsolete==0?"Active":"Inactive")."</div><div class='editfield hidden'><select class='form-control'><option value='0'>Active</option><option value='1' ".($mod->obsolete==1?"selected":"").">Inactive</option></select></td>
                  <td colspan=2>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2>
               <?php
                if ($this->session->userdata("tier") > 2.5){ ?>
                  <a class='btn form-control' onclick='AddCriteria()'>Add New Criterion</a>
               <?php }?>
               </td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q;
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });

 <?php if ($this->session->userdata("tier") > 2.5) { ?>
    function AddCriteria(){
      swal.setDefaults({
        confirmButtonText: 'Add',
        showCancelButton: false,
        progressSteps: ['1']
      })

      var steps = [
        {
          input: 'text',
          title: 'Add New Criterion',
          text: 'Please enter text',
          showLoaderOnConfirm: true,
          preConfirm: (email) => {
            return new Promise((resolve) => {            
              if (email == '') {
                swal.showValidationError(
                  'Please enter a value.'
                )
                resolve();
              } else {
                $.ajax({
                  url: '<?=base_url()?>cms/closing/addnewpotency',
                  type: "POST",
                  data: {
                    criteria: email
                  },
                  success: function(d){
                    if (d == "success"){

                    } else {
                      swal.showValidationError(d);
                    }
                  }, error: function(){
                    swal.showValidationError("Please try again later or contact an administrator");
                  }, complete: function(){
                    resolve();
                  }
                })
              }
              
            })
          }
        }
      ]

      swal.queue(steps).then((result) => {
        console.log(result.value[0]);
        if (result.value) {
          window.location = '<?=base_url()?>cms/closing/potency';
        }
      })
    }

    function editType(e){
      var typerow = $(e).parent().parent();
      if (typerow.find(".typename").hasClass("hidden")){
        var newval = typerow.find(".editfield input").val();
        var newval2 = typerow.find("select").val();
        var id = typerow.data("id");
        if ($.trim(newval) == ""){
          typerow.find(".editfield").val(typerow.find(".typename").html());
          newval = typerow.find(".typename").html();
        } else {
          $.ajax({
            url: "<?=site_url('cms/closing/editpotency')?>",
            type: "POST",
            data: {
              val: newval,
              val2: newval2,
              id: id
            }, success: function(d){
              if (d == ""){

              } else {
                swal("", d, "error");
              }
            }
          });
        }
        typerow.find(".typename").first().removeClass("hidden").html(newval);
        typerow.find(".typename").last().removeClass("hidden").html(newval2==1?"Inactive":"Active");
        typerow.find(".editfield").addClass("hidden");
        $(e).removeClass("btn-primary").addClass("btn-warning").html("Edit");

      } else {
        typerow.find(".typename").addClass("hidden");
        typerow.find(".editfield").removeClass("hidden");
        $(e).removeClass("btn-warning").addClass("btn-primary").html("Finish Edit");
      }
      
     }
  <?php  }?>

</script>