<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-tag right-sm"></i> Product Details
      <div class='tableheaderback'><a href="<?=site_url("cms/brand/products")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/images/uploads/?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h3><?= $productdetails->productname?></h3>
            </div>
            <div class='detailcontent'>
              <?=$productdetails->hcid?$productdetails->housename." - ".$productdetails->categoryname:"Houseless"?>
            </div>
            <div class='detailcontent'>
              <?=$productdetails->danoneflag==1?"<span class=\"greent\">Danone Product</span>":"<span class=\"oranget\">Competitor Product</span>"?>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              Created <strong><?= date( "j F Y H:i:s", strtotime($productdetails->created))?></strong>
            </div>
            <div class='detailcontent'>
              Updated <strong><?= date( "j F Y H:i:s", strtotime($productdetails->lastupdated))?></strong>
            </div>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/brand/editProducts')?>">
      <fieldset class='editor hidden'>
        <input type="hidden" name="productid" value="<?php echo $productdetails->productid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Product Name" name="product" value="<?php echo $productdetails->productname?>"/>
          <span>Product Name</span>
        </label>
        <label class="has-float-label">
          <select name="houseid">
            <option value=''>---- None ----</option>
            <?php
            foreach ($housecats as $cat){
              if ($productdetails->hcid == $cat->id){
                echo "<option selected value='$cat->id'>$cat->value</option>";
              } else {
                echo "<option value='$cat->id'>$cat->value</option>";
              }
            }
            ?>
          </select>
          <span>House</span>
        </label>
        <label>Danone Product</label><br/>
        <input type='checkbox' name='danoneflag' value=1 <?=$productdetails->danoneflag==1?"checked":""?>><br/><br/>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      Product Issues
    </div>
    <div class='tablecontent assignmenttable' id='tablecontent'>
      <table>
         <tr>
            <th width=10%>#</th>
            <th>Issue Name</th>
         </tr>
         <?php
         $page = 0;
         $first = "showrow";
         foreach($productdetails->issues as $key=>$cat){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
            echo "
            <tr class='pageddata page$page $first'>
               <td><span class='rowkey'>".($key+1)."</span><span class='rowdeleter hidden' onclick='deleteAssignment($cat->issueid)'>✖</span></td>
               <td>$cat->issuename</td>
            </tr>
            ";
         }
         ?> 
         <tr>
          <td><a class='btn form-control btn-primary' href='#' onclick='assignNewIssue()'>Add</a></td>
           <td>
             
           </td>
         </tr>
         <tr>
           <td colspan='6' class='pagingrow'>
             <?php
             $first = "currentindex";
             for ($i = 0; $i < $page; $i++){
              echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
              $first = "";
             }
             ?>
           </td>
         </tr>
      </table>
    </div>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

 
 var accsource = <?=json_encode($issues)?>;
  function assignNewIssue(){
    swal({
      title: 'Pilih Health Issue',
      input: "select",
      inputOptions: accsource,
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonText: 'Next &rarr;',
      cancelButtonText: 'Cancel',
      
    }).then((result) => {
      if (result.value == 0){
        swal("Ups", "Mohon pilih issue yang benar", "error");
      } else if (result.dismiss == "cancel"){
      } else {
        $.ajax({
          url: '<?=site_url("cms/brand/addProductIssue")?>',
          type: "POST",
          data: {
            catid: result.value,
            productid: <?=$productdetails->productid?>
          }, success: function(data){
            if (data == "success"){
              window.location = "<?=site_url("cms/brand/products/".$productdetails->productid)?>";
            } else {  
              swal("Ups", data, "error");
            }
          }
        })
      }      
    })
  }

  function deleteAssignment(hcid){
    swal({
      title: 'Apakah anda yakin?',
      text: "Issue produk akan terhapus!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
    }).then((result) => {
      if (result.value){
        swal({
          title: 'OK!',
          text: 'Modal ini akan tertutup secara otomatis...',
          showCancelButton: false,
          allowOutsideClick: false,
          onOpen: () => {
            swal.showLoading();
          }
        })
        $.ajax({
          url: '<?=site_url("cms/brand/deleteProductIssue")?>',
          type: "POST",
          data: {
            hcid: hcid,
            productid: <?=$productdetails->productid?>
          },
          success: function(data){
            if (data == "success"){
              window.location = "<?=site_url("cms/brand/products/".$productdetails->productid)?>";
            } else {
              swal({
                title: 'Ups!',
                text: data,
                showCancelButton: false,
                confirmButtonText: "Ok",
                type: "error"
              })
            }
          }
        })
      }
    })
  }
</script>