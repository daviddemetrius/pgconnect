<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-<?=$listicon?> right-sm"></i> <?=$listtitle?> List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	
                <th><a href='<?=$sitelink?>?s=productname<?=$addllink?>' class="<?=$s=="productname"?"active":""?>">Product Name</a></th> 
                <th><a href='<?=$sitelink?>?s=danoneflag<?=$addllink?>' class="<?=$s=="danoneflag"?"active":""?>">Product Type</a></th> 
                <th><a href='<?=$sitelink?>?s=hcid<?=$addllink?>' class="<?=$s=="hcid"?"active":""?>">House Category</a></th> 
                <th><a href='<?=$sitelink?>?s=productid<?=$addllink?>' class="<?=$s=="productid"?"active":""?>">ID</a></th> 
                <th colspan=2 width=20%><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($items as $key=>$item){
               $btnobs = "<a href='#' class='form-control btn btn-danger deactivator' data-id='".$item->productid."'>Deactivate</a>";
               if ($item->obsolete == 1){
                  $btnobs = "<a href='#' class='form-control btn btn-warning reactivator' data-id='".$item->productid."'>Reactivate</a>";
               }
               if ($this->session->userdata("tier") <= 2.5){
                if ($item->obsolete == 1){
                  $btnobs = "<span class='oranget'>Inactive</span>";
                } else {
                  $btnobs = "<span class='greent'>Active</span>";
                }
               }
              echo "
                <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>";
              foreach($item as $key=>$i){
                if ($key == "id" || $key == "obsolete"){
                  continue;
                }
                echo "<td>$i</td>";
              }
              echo "
                  <td>$btnobs</td>
                  <td><a href='".site_url("cms/brand/products/".$item->productid)."' class='btn form-control'>Details</a></td>
               </tr>";
            }
            ?>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td colspan=2><a href='<?=site_url("cms/brand/products/new")?>' class='btn btn-primary form-control'>Add New Products</a></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
   $(".reactivator").on("click", function(){
      var id = $(this).data("id");
      $.ajax({
         url: '<?=site_url("cms/brand/reactivateProduct")?>',
         type:"POST",
         data: {id:id},
         success: function(d){
            if (d >= 1){
               location.reload();
            } else if (d == 0){
               swal("Synergy", "Operation Failed. Please try again or contact an administrator", "error");
            } else {
               swal("Synergy", d, "warning");
            }
         }
      })
   }) 

   $(".deactivator").on("click", function(){
      var id = $(this).data("id");
      swal({
       title: "Are you sure?",
       text: "",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Yes",
       cancelButtonText: "No",
       closeOnConfirm: false,
       closeOnCancel: false
    }).then(function(isConfirm){
      // console.log("wut");
      if (isConfirm.value){
         $.ajax({
            url: '<?=site_url("cms/brand/deactivateProduct")?>',
            type:"POST",
            data: {id:id},
            success: function(d){
               if (d >= 1){
                  location.reload();
               } else if (d == 0){
                  swal("Synergy", "Operation Failed. Please try again or contact an administrator", "error");
               } else {
                  swal("Synergy", d, "warning");
               }
            }
         })      

      } else {

      }
   })
   
   })
</script>