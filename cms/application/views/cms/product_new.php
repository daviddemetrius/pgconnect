<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-map-marker right-sm"></i>Add New Product
                <div class='tableheaderback'><a href="<?=site_url("cms/brand/products")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Product Name" name="productname" max="2" value="<?php echo set_value('productname'); ?>"/>
                <span>Product Name</span>
              </label>
              <label class="has-float-label">
                <select name="houseid">
                  <option value=''>---- None ----</option>
                  <?php
                  foreach ($housecats as $cat){
                    if (set_value("houseid") == $cat->id){
                      echo "<option selected value='$cat->id'>$cat->value</option>";
                    } else {
                      echo "<option value='$cat->id'>$cat->value</option>";
                    }
                  }
                  ?>
                </select>
                <span>House</span>
              </label>
              <label>Danone Product</label><br/>
              <input type='checkbox' name='danoneflag' value=1/><br/><br/>

              <button class='form-control btn btn-primary'>Add Product</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	