<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<script src='<?=base_url()?>js/qrcode.min.js' type='text/javascript'></script>
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-home right-sm"></i> Promo Detail
      <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/promos/")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/assets/promos/$details->promoid$details->promonumber.png?".rand()?>' onerror='defaultppproperty(this)'>
            <div id='qrcodebox'></div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
               <h3><?= $details->promoname?></h3>
            </div>
            <div class='detailcontent'>
               <?= $details->discounttype==0?"-":""?><?= $details->discountamount?> <?= $details->discounttype==0?"IDR":"%"?>
            </div>
            <div class='detailcontent'>
              T&C: <?=$details->snk?>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              usage Limit: <strong><?=$details->dailylimit?></strong> uses (per Apotik).
            </div>
            <div class='detailcontent'>
              Valid Until <strong><?= date( "j F Y H:i:s", strtotime($details->promoend))?></strong>
            </div>
            <div class='detailcontent'>
              <?=$details->obsolete==1?"<span class='oranget'>Inactive</span>":"<span class='greent'>Active</span>"?>
            </div>
          </div>
        </div>
      </div>
      <?php if ($this->session->userdata("tier") != 1){ ?>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
      <?php } ?>
    </div>
    <form method="POST" action="<?=site_url('cms/masterdata/editPromo')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden' style='width:90%'>
        <input type="hidden" name="promoid" value="<?php echo $details->promoid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Promo Name" name="promoname" value="<?php echo $details->promoname; ?>"/>
          <span>Promo Name</span>
        </label>
        <label class="has-float-label hidden">
          <select name='discounttype'><option value=0>Nominal</option><option value=1 <?=$details->discounttype==1?"selected":""?>>%</option></select>
          <span>Type</span>
        </label>
        <label class="has-float-label hidden">
          <input type="number" placeholder="0..." name="discountamount" value="<?php echo $details->discountamount; ?>" id='accname'/>
          <span>Amount</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type T&C" name="snk" value="<?php echo $details->snk; ?>" id='accname'/>
          <span>Terms & Conditions</span>
        </label>  
        <label class="has-float-label">
          <input type="number" placeholder="Type Number" name="dailylimit" value="<?php echo $details->dailylimit; ?>" id='accname'/>
          <span>Promo Usage Limit (per Apotik)</span>
        </label>  
        <label class="has-float-label">
          <input type="date" placeholder="" name="promoend" value="<?php echo date("Y-m-d", strtotime($details->promoend)); ?>"/>
          <span>Valid Until</span>
        </label>  
        <label class="has-float-label">
          <input type="file" placeholder="File" name="userfile" accept="image/*"/>
          <span>Image</span>
        </label>                   
        <br/>

        <label>Apotik <i class='glyphicon glyphicon-check' onclick='$("#accntfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#accntfields input").prop("checked", false)' style='cursor:pointer'></i></label>
        <div class='row' id='accntfields'>
          <div class='row'>
          <?php
          $prevbranch = $hospitals[0]->branch;
          echo $prevbranch." <i class='glyphicon glyphicon-check' onclick='$(this).parent().find(\"input\").prop(\"checked\", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$(this).parent().find(\"input\").prop(\"checked\", false)' style='cursor:pointer'></i><br/>";
          $hosparr = array();
          foreach ($details->hospitals as $acc){
            $hosparr[] = $acc->hospitalid;
          }
          foreach ($hospitals as $type){
            if ($prevbranch != $type->branch){
              $prevbranch = $type->branch;
              echo "</div>
                    <br/>
                    <div class='row'>
                      ".$prevbranch." <i class='glyphicon glyphicon-check' onclick='$(this).parent().find(\"input\").prop(\"checked\", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$(this).parent().find(\"input\").prop(\"checked\", false)' style='cursor:pointer'></i><br/>";
            }
            if (in_array($type->hospitalid, $hosparr)){
              echo '<div class="col-xs-3"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]" checked> '.$type->hospitalname.'</div>';
            } else {
              echo '<div class="col-xs-3"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]"> '.$type->hospitalname.'</div>';
            }
            
          }
          ?>
        </div><br/>  

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      <a href="<?=site_url("cms/masterdata/download/$details->promoid")?>"><i class='glyphicon glyphicon-download tableheadernext'></i></a>
      <i class="glyphicon glyphicon-user right-sm"></i> Customer List
    </div>
    <div class='tablecontent'>
      <table>
        <tr>
          <th width=5%>#</th> 
          <th>Name</th> 
          <th>Phone</th> 
          <th>Email</th>
          <th>Date</th>
          <th>Unique Code</th>
          <th>Status</th>
          <th>Apotik</th>
        </tr>
         <?php
         $page = 0;
         $first = "showrow";
         foreach($reps as $key=>$rep){
          if ($key %10 == 0){         
            $page++;
            if ($page !=1){
              $first = "";
            }           
          }
            echo "
            <tr class='pageddata page$page $first'>
               <td>".($key+1)."</td>
               <td>$rep->fullname</td>
               <td>$rep->contactno</td>
               <td>$rep->email</td>
               <td>$rep->dateregistered</td>
               <td>$rep->uniquecode</td>
               <td>".($rep->codeusage==0?"Used":"Available")."</td>
               <td>$rep->hospitalname</td>
            </tr>
            ";
         }
         ?>         
         <tr>
           <td colspan='8' class='pagingrow'>
             <?php
             $first = "currentindex";
             for ($i = 0; $i < $page; $i++){
              echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
              $first = "";
             }
             ?>
           </td>
         </tr>
      </table>
    </div>
  </div>
</section>
</section>	

<script>

  /*var qrcode = new QRCode("qrcodebox", {
    text: "<?=base_url()."?qr=".$details->promoid.$details->promonumber?>",
    width: 256,
    height: 256,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
  });*/

 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});
function switchpage(number, e){
  $(e).parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}
</script>