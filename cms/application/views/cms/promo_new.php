<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-usd right-sm"></i>Add New Promo
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/promos")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields' style='width:90%'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Promo Name" name="promoname" value="<?php echo set_value('promoname'); ?>" id='accname'/>
                <span>Name</span>
              </label>
              <label class="has-float-label hidden">
                <select name='discounttype'><option value=0>Nominal</option><option value=1>%</option></select>
                <span>Type</span>
              </label>
              <label class="has-float-label hidden">
                <input type="number" placeholder="0..." name="discountamount" value="<?php echo set_value('discountamount', 0); ?>" id='accname'/>
                <span>Amount</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type T&C" name="snk" value="<?php echo set_value("snk"); ?>" id='accname'/>
                <span>Terms & Conditions</span>
              </label>
              <label class="has-float-label">
                <input type="number" placeholder="Type Number" name="dailylimit" value="<?php echo set_value("dailylimit"); ?>" id='accname'/>
                <span>Promo Usage Limit (per Apotik)</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="" name="promoend" value="<?php echo set_value('promoend'); ?>"/>
                <span>Valid Until</span>
              </label>   
              <label class="has-float-label">
                <input type="file" placeholder="File" name="userfile" value="<?php echo set_value('userfile'); ?>" accept="image/*"/>
                <span>Image</span>
              </label>           
              <br/>

              <label>Apotik <i class='glyphicon glyphicon-check' onclick='$("#accntfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#accntfields input").prop("checked", false)' style='cursor:pointer'></i></label>
              <div class='row' id='accntfields'>
                <div class='row'>
                <?php
                $prevbranch = $hospitals[0]->branch;
                echo $prevbranch." <i class='glyphicon glyphicon-check' onclick='$(this).parent().find(\"input\").prop(\"checked\", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$(this).parent().find(\"input\").prop(\"checked\", false)' style='cursor:pointer'></i><br/>";
                foreach ($hospitals as $type){
                  if ($prevbranch != $type->branch){
                    $prevbranch = $type->branch;
                    echo "</div>
                          <br/>
                          <div class='row'>
                            ".$prevbranch." <i class='glyphicon glyphicon-check' onclick='$(this).parent().find(\"input\").prop(\"checked\", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$(this).parent().find(\"input\").prop(\"checked\", false)' style='cursor:pointer'></i><br/>";
                  }
                  if (in_array($type->hospitalid, set_value('hospitals', array()))){
                    echo '<div class="col-xs-3"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]" checked> '.$type->hospitalname.'</div>';
                  } else {
                    echo '<div class="col-xs-3"><input type="checkbox" value="'.$type->hospitalid.'" name="hospitals[]"> '.$type->hospitalname.'</div>';
                  }
                  
                }
                ?>
              </div><br/>
              <button class='form-control btn btn-primary'>Add Promo</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	
<script>
  var chosenAcc = 0;
  $('#hospitalcode, #accname').autocomplete({
        serviceUrl: '<?=base_url()?>cms/closing/searchacc',
        preserveInput: true, 
        onSelect: function (suggestion) {
        }
  });
</script>