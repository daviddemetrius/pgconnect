<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Pharmacycare ID</title>
<link rel='stylesheet' href="<?=base_url().'css/bootstrap.min.css'?>">
<link rel='stylesheet' href="<?=base_url().'css/paper.min.css'?>">
<style type="text/css">
@page{size: A4;}
img{width:30mm!important;}
</style>
</head>
<body class='A4 portrait'>
	<section class='sheet' style='padding:5mm 10mm'>
		<article>
			<?php 
			$currentBranch = $promos[0]->branch;
			echo $currentBranch;
			?>
			<div class='row'>
			<?php 
			$indexing = 0;
			foreach ($promos as $promo){
				$promo->shortened = base_url()."qr/".$promo->promoid.$promo->promonumber."ap".$promo->hospitalid;
				$indexing++;
				if ($currentBranch != $promo->branch || $indexing == 24){
					$currentBranch = $promo->branch;
					echo "
			</div>
		</article>
	</section>
	<section class='sheet' style='padding:5mm 10mm'>
		<article>
			$currentBranch
			<div class='row'>";
					$indexing = 0;
				}
				if ($indexing == 4){
					echo "
			</div>
			<div class='row'>
					";
				}
				echo "<div class='col-xs-3' style='margin:1mm 0;border:1px solid black;padding:0 5px'>
				<center><span style='white-space:nowrap;font-size:11px'>$promo->promoname</span></center>
				<center style=''><div id='qrcodebox$promo->promoid$promo->hospitalid'></div></center>
				<center style='text-decoration:underline;color:blue;font-size:7px'>$promo->shortened</center>
				<center style='line-height:10px'><span style='white-space:nowrap;font-size:7px;line-height:10px'>$promo->customercode</span></center>
				<center style='line-height:10px'><span style='white-space:nowrap;font-size:8px;line-height:10px'>$promo->hospitalname</span></center>
				</div>";
			}
			?>
			</div>			
		</article>
	</section>
</body>
<script src='<?=base_url()."js/bootstrap.min.js"?>'></script>

<script src='<?=base_url()?>js/qrcode.min.js' type='text/javascript'></script>
<script>
	<?php
	foreach ($promos as $key=>$pr){ ?>
		var qrcode = new QRCode("qrcodebox<?=$pr->promoid.$pr->hospitalid?>", {
		    text: "<?=$pr->shortened?>",
		    width: 200,
		    height: 200,
		    colorDark : "#000000",
		    colorLight : "#ffffff",
		    correctLevel : QRCode.CorrectLevel.H
		  });
	<?php
	}
	?>
</script>

</html>