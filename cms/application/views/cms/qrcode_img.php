<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Pharmacycare ID</title>
<style type="text/css">
body {margin: 0;}
body, table, td, p, a, li, blockquote {-webkit-text-size-adjust: none!important; font-family: sans-serif; font-style: normal; font-weight: 400;}
img, div{display: block; margin: 0; padding: 0; line-height: normal;}
a{color: #102776; font-size: 12px; text-decoration: none;}
@media screen and (max-width:600px) {
	body, table, td, p, a, li, blockquote {-webkit-text-size-adjust: none!important; font-family: sans-serif;}
	table {width: 100%;}
}
</style>
</head>
<body>

<?php
$cid = "7ca5467161f95d0f26681bc004a29a509f839fe5";
$csec = "1d83d1be916c8fb091018dca3406aa2c88d741f1";
function getOAuthToken(){
	$headerToken = base64_encode("bbs.steffen.yap@gmail.com:Qwerty123");

	$cid = "7ca5467161f95d0f26681bc004a29a509f839fe5";
	$csec = "1d83d1be916c8fb091018dca3406aa2c88d741f1";
	$headers = array(
    	'Content-Type:application/x-www-form-urlencoded',
    	'Authorization:Basic '.$headerToken
	);
	$postdata = "client_id=$cid&client_secret=$csec";
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, "https://api-ssl.bitly.com/oauth/access_token");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSLVERSION, 6);
	curl_setopt($ch, CURLOPT_TIMEOUT, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// $result = json_decode(curl_exec($ch)); 
	$result = curl_exec($ch); 
	$info = curl_getinfo($ch);
	curl_close($ch); 
	return $result;
}
function get_bitly_short_url($url,$guid, $oauth) {
	$headers = array(
    	'Content-Type:application/json',
    	'Authorization:Bearer '.$oauth
	);
	//echo $url;
	$arr = array(
		"group_guid" => "",
		"domain" => "bit.ly",
		"long_url" => $url
	);
	$postdata = json_encode($arr);
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, "https://api-ssl.bitly.com/v4/shorten");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSLVERSION, 6);
	curl_setopt($ch, CURLOPT_TIMEOUT, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// $result = json_decode(curl_exec($ch)); 
	$result = json_decode(curl_exec($ch)); 
	$info = curl_getinfo($ch);
	curl_close($ch); 
	//echo var_dump($result);
	return $result->link;
}

function curl_get_result($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}
$oauth = getOAuthToken();
foreach ($promos as $key=>$pr){ 
	$shortened = base_url()."qr/".$pr->promoid.$pr->promonumber."ap".$pr->hospitalid;
	$pr->shortened = $shortened; ?>

	<table width="600" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td align="center" valign="middle">
					<div style="font-size: 24px; color: #102776; font-weight: bold;"><?=$pr->promoname?></div>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<div id="qrcodebox<?=$key?>"></div>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<a href="<?=$shortened?>"><?=$shortened?></a>
				</td>
			</tr>
			<tr>
				<td align="center">
					<?=$pr->customercode?>
				</td>
			</tr>
			<tr>
				<td align="center">
					<?=$pr->hospitalname?>
				</td>
			</tr>
			<tr>
				<td height="60">&nbsp;</td>
			</tr>
		</tbody>
	</table>
<?php }
?>

<script src='<?=base_url()?>js/qrcode.min.js' type='text/javascript'></script>
<script>
	<?php
	foreach ($promos as $key=>$pr){ ?>
		var qrcode = new QRCode("qrcodebox<?=$key?>", {
		    text: "<?=$pr->shortened?>",
		    width: 256,
		    height: 256,
		    colorDark : "#000000",
		    colorLight : "#ffffff",
		    correctLevel : QRCode.CorrectLevel.H
		  });
	<?php
	}
	?>
</script>
