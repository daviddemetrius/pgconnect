<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/fullcalendar.min.css" rel="stylesheet">
<script src='<?php echo base_url(); ?>js/moment.min.js'></script>
<script src='<?php echo base_url(); ?>js/fullcalendar.min.js'></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src='<?php echo base_url(); ?>js/jquery.tablesorter.min.js'></script>

<script src="<?php echo base_url(); ?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/serial.js" type="text/javascript"></script> 

<section id="main-content">
  	<section class="wrapper site-min-height"> 
   	<div class='fullblock'>
            <div class='tableheader'>
              <i class="glyphicon glyphicon-th-large right-sm"></i> Representative Details
              <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/accounts/$account->associatedaccount")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
            </div>
            <div class='blockdetails'>
              <div class='detailseparator'>
                <div class='row'>
                  <div class='col-md-1 detailimage'>
                    
                  </div>
                  <div class='col-md-5'>
                    <div class='detailcontent'>
                      <h1><?=$account->repname?></h1>
                    </div>
                    <div class='detailcontent'>
                      Username: <?=$account->loginid?>
                    </div>
                    <div class='detailcontent coveragecontent'>
                      Created: <?=$account->created?>
                    </div>
                    <div class='detailcontent coveragecontent'>
                      Last Login: <?=$account->lastpushupdated?>
                    </div>
                  </div>
                  <div class='col-md-5'>
                    <div class='detailcontent'>
                      <i class='glyphicon glyphicon-earphone'></i> <?=$account->phone?>
                    </div>
                    <div class='detailcontent'>
                      <i class='glyphicon glyphicon-envelope'></i> <?=$account->address?>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              if ($this->session->userdata("tier") >= 0.5){
              ?>
              <div class='detailbuttons'>
              <a href="<?=site_url("cms/accounts/reps/statistics/$account->repid")?>" target="_blank" class='detailbtn hidden'><i class="glyphicon glyphicon-dashboard"></i> <span class=''>Statistics</span></a>
                <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
              </div>
              <?php
              }
              ?>
            </div>
            <?php
            if ($this->session->userdata("tier") >= 0.5){
            ?>
            <form method="POST" action="<?=site_url('cms/accounts/editRep')?>" enctype="multipart/form-data">
            <fieldset class='editor hidden'>
            <?php
            $readonlyfield = "";
            if ($this->session->userdata("tier") > 2.5){
              $readonlyfield = "";
            }
            ?>
              <input  type='hidden' value='<?=$account->repid?>' name='id'>
              <label class="has-float-label">
                <input type="text" placeholder="Name" name="repname" value="<?=$account->repname?>"/>
                <span>Full Name</span>
              </label>
              <?php if ($this->session->userdata("tier") > 3 ){?>
              <label class="has-float-label">
                <input type="text" placeholder="Username" name="username" value="<?=$account->loginid?>"  <?=$readonlyfield?>/>
                <span>Username</span>
              </label>

              <label class="has-float-label">
                <input type="password" placeholder="Input a New Value to Change Password. Leave blank to keep." name="loginpass" value="" disabled/>
                <span>Password</span>
              </label>
              <?php } ?>

              <label class="has-float-label">
                <input type="number" placeholder="Phone Number" name="phone" value="<?=$account->phone ?>"/>
                <span>Contact Number</span>
              </label>

              <label class="has-float-label">
                <input type="text" placeholder="Address" name="address" value="<?=$account->address ?>"/>
                <span>Address</span>
              </label>

              <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
              <button class='form-control btn btn-primary' id="finishbutton">Save</a>
            </fieldset>
            </form>
            <?php } ?>
         </div>
	</section>
</section>	

<!-- Modal -->
<div class="modal fade" id="visitModal" tabindex="-1" role="dialog" aria-labelledby="visitModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="visitModalLabel">Visit Details</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>
var donecov = 0;
var plannedcov = 0;
var totalcov = 0;
var ongdonecov = 0;
var ongplannedcov = 0;
var ongtotalcov = 0;
var dcsource = [];
   $("#editbutton").on("click", function(){
      $("input").prop("disabled", false);
      $(".blockdetails").addClass("hidden");
      $(".editor").removeClass("hidden");
   })
   $("#cancelbutton").on("click", function(){
      $("input").prop("disabled", true);
      
      $(".blockdetails").removeClass("hidden");
      $(".editor").addClass("hidden");
   })
   function switchpage(number, e){
    $(".currentindex").removeClass("currentindex");
    $(e).addClass('currentindex');
    $(".showrow").removeClass("showrow");
    $(".page"+number).addClass("showrow");
   }

  $(document).ready(function() {
    $('#districtac').autocomplete({
      source: dcsource,
      select: function(event, ui){
        $("#districtac").val(ui.item.label);
        $("#districtid").val(ui.item.id)
      }
    });

    $("#sortabletable").tablesorter();
    $('#calendar').fullCalendar({
      header: {
        left: 'today',
        center: 'prev title next',
        right: ''
      },
      defaultDate: '<?=date("Y-m-d")?>',
      navLinks: false, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: evtlist,
      dayClick: function(date, jsEvent, view){
        var dt = new Date(date._i);
        var y = dt.getFullYear();
        var m = ("0"+(dt.getMonth()+1)).slice(-2);
        var d = ("0"+dt.getDate()).slice(-2);
        var dttxt = y+""+m+""+d;
        //console.log(".cdt"+dttxt);
        //console.log($(".cdt"+dttxt));
        var dates = $(".cdt"+dttxt);

        for (var i = 0 ; i < dates.length; i++){
          var href = $(dates[i]).attr("href");
          var index = href.split("(")[1];
          var index = index.split(")")[0];
          window['markThis'](index);
        }
        window.location.hash = "";
        window.location.hash = "#sortabletable";
      }
    });
    
  });

  function markThis(index){
    var event = evtlist[index];
    var element = $(".evt"+event.schedid);

    if (element.hasClass("darker")){
      element.removeClass("darker");
      $(".trow"+event.schedid).remove();
    } else {
      element.addClass("darker");
      var approveBtn = "<input type='checkbox' class='form-control chxbx' data-id='"+event.schedid+"' checked>";
      var status = "Pending";
      var colorClass = "oranget";
      if (event.visited == 1){
        status = "Visited";
        colorClass = "bluet";
        approveBtn = "<button type='button' class='form-control btn btn-primary' onclick='checkVisitData("+event.schedid+")'>View Details</button>";
      } else if (event.deleted == 1){
        status = "Deleted";
        approveBtn = "";
        colorClass = "redt";
      } else if (event.approved == 1){
        status = "Submitted";
        approveBtn = "<button type='button' class='form-control btn btn-primary' onclick='checkVisitData("+event.schedid+")'>View Details</button>";
        colorClass = "greent";
      }
      var html = "";
      html += "<tr class='trow"+event.schedid+" drow'>";
      html += " <td class='datecol' data-date='"+event.start+"'>"+event.startReadable+"</td>";
      html += " <td><span class='"+colorClass+"'>"+status+"</span></td>";
      html += " <td>"+approveBtn+"</td>";
      html += "</tr>";
      $(".confrow").before(html);
    }
  }

  function checkVisitData(scheduleid){
    swal({
      title: 'OK!',
      text: 'Modal ini akan tertutup secara otomatis...',
      showCancelButton: false,
      allowOutsideClick: false,
      onOpen: () => {
        swal.showLoading();
      }
    })
    $.ajax({
      url: '<?=site_url("cms/accounts/getsessiondata")?>',
      type: "POST",
      data: {
        schedid: scheduleid
      },
      success: function(data){
        $("#visitModal").find(".modal-body").html(data);
      },
      complete: function(){
        swal.close();
        $("#visitModal").modal("show");
      }
    })
  }

  function checkSurveyData(sessionid){
    swal({
      title: 'OK!',
      text: 'Modal ini akan tertutup secara otomatis...',
      showCancelButton: false,
      allowOutsideClick: false,
      onOpen: () => {
        swal.showLoading();
      }
    })
    $.ajax({
      url: '<?=site_url("cms/accounts/getsurveydata")?>',
      type: "POST",
      data: {
        sessionid: sessionid
      },
      success: function(data){
        $("#visitModal").find(".modal-body").html(data);
      },
      complete: function(){
        swal.close();
        $("#visitModal").modal("show");
      }
    })
  }

  function sortDate(){
    $(".tablecontent .glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
    $(".tablecontent .glyphicon").first().removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    var sortOrder = 1;
    var sortarr = $(".drow");
    sortarr.sort(function (a,b){
      var val1 = $(a).children('.datecol').data("date");
      var val2 = $(b).children('.datecol').data("date");
      if($.isNumeric(val1) && $.isNumeric(val2))
        return sortOrder == 1 ? val1-val2 : val2-val1;
      else
        return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
    });
    $.each(sortarr, function (index, row){
      $('.confrow').before(row);
    })
  }

  function sortHCP(){
    $(".tablecontent .glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
    $(".tablecontent .glyphicon").last().removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    console.log($("#sortabletable .glyphicon"));
    var sortOrder = 1;
    var sortarr = $(".drow");
    sortarr.sort(function (a,b){
      var val1 = $(a).children('.hcpcol').html();
      var val2 = $(b).children('.hcpcol').html();
      if($.isNumeric(val1) && $.isNumeric(val2))
        return sortOrder == 1 ? val1-val2 : val2-val1;
      else
        return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
    });
    $.each(sortarr, function (index, row){
      $('.confrow').before(row);
    })
  }

  $(".assignmenttable tr.pageddata").hover(function(){
    $(this).find(".rowdeleter").removeClass("hidden");
    $(this).find(".rowkey").addClass("hidden");
  }, function(){
    $(this).find(".rowdeleter").addClass("hidden");
    $(this).find(".rowkey").removeClass("hidden");
  });
</script>