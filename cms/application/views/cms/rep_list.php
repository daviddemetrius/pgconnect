<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-th-large right-sm"></i> Representative List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	
                <th>Code</th> 
                <th>Name</th>	
                <th>Type</th> 
                <th>Phone</th> 
                <th>Address</th>
                <th colspan=2 width=20%><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($accounts as $key=>$account){
               $btnedit = "<a href='".site_url("cms/accounts/reps/$account->repid")."' class='form-control btn btn-primary'>Details</a>";
               $position = "";
               $btnobs = "<a href='#' class='form-control btn btn-danger deactivator' data-id='".$account->repid."'>Deactivate</a>";
               if ($account->obsolete == 1){
                  $btnobs = "<a href='#' class='form-control btn btn-warning reactivator' data-id='".$account->repid."'>Reactivate</a>";
               }
               if ($this->session->userdata("tier") <= 3){
                if ($account->obsolete == 1){
                  $btnobs = "<span class='oranget'>Inactive</span>";
                } else {
                  $btnobs = "<span class='greent'>Active</span>";
                }
               }
               echo "
               <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td>$account->xmlnacd</td>
                  <td>$account->repname</td>
                  <td>NR $account->typename</td>
                  <td><a href='tel:$account->phone'>$account->phone</a></td>
                  <td>$account->address</td>
                  <td>$btnobs</td>
                  <td>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2><?php if ($this->session->userdata("tier") > 3){?><a class='btn form-control' href='<?=site_url("cms/accounts/reps/new")?>'>Add New Representative</a><?php } ?></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q;
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
   $(".reactivator").on("click", function(){
      var id = $(this).data("id");
      $.ajax({
         url: '<?=site_url("cms/accounts/reactivateRep")?>',
         type:"POST",
         data: {id:id},
         success: function(d){
            if (d >= 1){
               location.reload();
            } else if (d == 0){
               swal("Synergy", "Operation Failed. Please try again or contact an administrator", "error");
            } else {
               swal("Synergy", d, "warning");
            }
         }
      })
   }) 

   $(".deactivator").on("click", function(){
      var id = $(this).data("id");
      swal({
       title: "Are you sure?",
       text: "",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Yes",
       cancelButtonText: "No",
       closeOnConfirm: false,
       closeOnCancel: false
    }).then(function(isConfirm){
      // console.log("wut");
      if (isConfirm.value){
         $.ajax({
            url: '<?=site_url("cms/accounts/deactivateRep")?>',
            type:"POST",
            data: {id:id},
            success: function(d){
               if (d >= 1){
                  location.reload();
               } else if (d == 0){
                  swal("Synergy", "Operation Failed. Please try again or contact an administrator", "error");
               } else {
                  swal("Synergy", d, "warning");
               }
            }
         })      

      } else {

      }
   })
   
   })
</script>