<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-th-large right-sm"></i>Add New Agent
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/accounts/$hospitalid")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Name" name="repname" value="<?php echo set_value('repname'); ?>"/>
                <span>Full Name</span>
              </label>

              <label class="has-float-label">
                <input type="text" placeholder="Username" name="username" value="<?php echo set_value('username'); ?>"/>
                <span>Username</span>
              </label>

              <label class="has-float-label">
                <input type="password" placeholder="••••••••" name="password" value="<?php echo set_value('password'); ?>"/>
                <span>Password</span>
              </label>

              <label class="has-float-label">
                <input type="number" placeholder="Phone Number" class='numinput' name="phone" value="<?php echo set_value('phone'); ?>"/>
                <span>Contact Number</span>
              </label>

              <label class="has-float-label">
                <input type="text" placeholder="Address" name="address" value="<?php echo set_value('address'); ?>"/>
                <span>Address</span>
              </label>

              <button class='form-control btn btn-primary'>Sign up</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	
<script>
var dcsource = <?=json_encode($districtlist)?>;
$(document).ready(function(){
  $('#districtac').autocomplete({
    source: dcsource,
    select: function(event, ui){
      $("#districtac").val(ui.item.label);
      $("#districtid").val(ui.item.id)
    }
  });
})
  
</script>