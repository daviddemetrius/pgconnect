<script src="<?php echo base_url(); ?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/serial.js" type="text/javascript"></script>	

<!--main content start-->
<section id="main-content">
  	<section class="wrapper site-min-height"> <!-- site-min-height -->
  		<div class='tableheader'>
              		<i class="glyphicon glyphicon-th-large right-sm"></i> Representative Statistics for <?=$account->repname?> - <?=$account->xmlnacd?>
              		<div class='tableheaderback'><a href="<?=site_url("cms/accounts/reps/$account->repid")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
            	</div>
            	<br/><br/>
            	<div class='centerfields'>
            		<form method='get'>
				<label>Month / Year</label>
				<div class='row'>
					<div class='col-xs-3'><input type='number' min=1 max=12 class='form-control' id='month' value="<?=$this->input->get("month")?$this->input->get("month"):date("n")?>" onchange='' name='month'></div>
					<div class='col-xs-3'><input type='number' min=2018 class='form-control' id='year' value="<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>" onchange='' name='year'></div>
					<div class='col-xs-4'><button class='btn btn-primary form-control'>Get Data</button></div>
				</div><br/>
				<a href="<?=site_url("cms/accounts/reps/visitdetails/$account->repid")?>?m=<?=$this->input->get("month")?$this->input->get("month"):date("n")?>&y=<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>" target="_blank" class='btn btn-default'><i class="glyphicon glyphicon-download"></i> <span class=''>Download Visit Details</span></a>
				<br/>
				<!--<a class='btn btn-primary' href='<?=site_url("cms/accounts/reps/statistics/$account->repid")?>?all=true'>Get All Data</a>-->
			</form>
		</div>
	   	<div class="row" id='dboard'>
	   		<div class='col-xs-12 col-md-12 col-lg-12'>
	   			<div class='chartdiv'  id='effectivenessbar' style='width:100%; height:300px'></div>
	   		</div>
	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<div class='chartdiv'  id='reachbar' style='width:100%; height:300px'></div>
	   		</div>
	   		<div class='col-xs-12 col-md-6 col-lg-3'>
	   			<div class='chartdiv'  id='acccoverage' style='width:100%; height:300px'></div>
	   		</div>
	   		<div class='col-xs-12 col-md-6 col-lg-3'>
	   			<div class='chartdiv'  id='hcpcoverage' style='width:100%; height:300px'></div>
	   		</div>
	   		<div class='col-xs-12 col-md-6 col-lg-9'></div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Call Compliance (Approved Visit/Approved)</div>
		   			<div class='chartcontent'><h1><?=number_format($rsPlanEffectiveness->approvedvisitcount / ($rsPlanEffectiveness->approvedcount==0?1:$rsPlanEffectiveness->approvedcount) * 100, 1)?>%</h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>1 on 1 Actual Plans</div>
		   			<div class='chartcontent'><h1><?=$rs1on1Data->schedules?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>1 on 1 Plan HCP Reach</div>
		   			<div class='chartcontent'><h1><?=$rs1on1Data->specialists?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Group Actual Plans</div>
		   			<div class='chartcontent'><h1><?=$rsGroupData->schedules?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Group Plan HCP Reach</div>
		   			<div class='chartcontent'><h1><?=$rsGroupData->specialists?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Detailing Days</div>
		   			<div class='chartcontent'><h1><?=$rsFullDays?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Detailing Halfdays</div>
		   			<div class='chartcontent'><h1><?=$rsHalfDays?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Total Days Detailed</div>
		   			<div class='chartcontent'><h1><?=$rsFullDays+$rsHalfDays?></h1></div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-4 col-lg-3'>
	   			<div class='chartdiv'  id='inactivedeals' style='width:100%; height:300px'>		
		   			<div class='chartitle'>Visits/Day Average</div>
		   			<?php
		   			$totaldays = ($rsFullDays+$rsHalfDays);
		   			if ($totaldays == 0){
		   				$totaldays = 1;
		   			}
		   			?>
		   			<div class='chartcontent'><h1><?=number_format($rsPlanEffectiveness->approvedvisitcount/$totaldays,2)?></h1></div>
	   			</div>
	   		</div>
	  	</div>
	  	<div class='fullblock'>
	            <div class='tableheader'>
	              Modules Presented (More than 5s)
	            </div>
	            <div class='tablecontent assignmenttable' id='tablecontent'>
	              <table>
	                 <tr>
	                    <th width=10%>#</th>
	                    <th>Module Name</th>
	                    <th>ID</th>
	                    <th>Total Duration</th>
	                    <th>Frequency</th>
	                    <th>Average Duration</th>
	                 </tr>
	                 <?php
	                 foreach ($rsModuleData as $key=>$module){
	                 	echo "
		                    <tr class='page'>
		                       <td>".($key+1)."</td>
		                       <td>$module->moduletitle</td>
		                       <td>$module->moduleid</td>
		                       <td>$module->totalduration seconds</td>
		                       <td>$module->frequency</td>
		                       <td>".number_format($module->totalduration/($module->frequency==0?1:$module->frequency), 2)." seconds/visit</td>
		                    </tr>
		                    ";
	                 }
	                 ?> 
	              </table>
	            </div>
          	</div>
	</section>
</section>

<script>
	var acccoverage;
	var hcpcoverage;
	var hcpdata = <?=json_encode($rsHcpCoverage)?>;
	var accdata = <?=json_encode($rsAccCoverage)?>;

	AmCharts.ready(function(){
		var colors = ["rgb(55,93,144)", "#04ad4e", "rgb(225,106,12)"];
		var colorsinvert = ["#04ad4e", "rgb(225,106,12)"];
		var colorsbar = [ "rgb(55,93,144)", "#04ad4e" ];
		var colorsbarinvert = [ "#04ad4e", "rgb(225,106,12)" ];
                acccoverage = new AmCharts.AmPieChart();
                acccoverage.dataProvider = accdata;
                acccoverage.titleField = "title";
                acccoverage.valueField = "value";
                acccoverage.urlField = "Url";
                acccoverage.urlTarget = "_blank";
                acccoverage.outlineColor = "#cecece";
                acccoverage.outlineAlpha = 0.8;
                acccoverage.outlineThickness = 2;
                acccoverage.balloonText = '[[title]]: [[value]]';
                acccoverage.labelText = '';
                acccoverage.legend = {"position":"bottom", "autoMargins":true, "fontSize": 9, "align":"center", "verticalGap": 0};
                acccoverage.titles = [{"text": "Assigned ACC", "color": "#2d4f80"}];
                acccoverage.colors = colors;

                // WRITE
                acccoverage.write("acccoverage");

		var colors = ["rgb(55,93,144)", "#04ad4e", "rgb(225,106,12)"];
		var colorsinvert = ["#04ad4e", "rgb(225,106,12)"];
		var colorsbar = [ "rgb(55,93,144)", "#04ad4e" ];
		var colorsbarinvert = [ "#04ad4e", "rgb(225,106,12)" ];
                hcpcoverage = new AmCharts.AmPieChart();
                hcpcoverage.dataProvider = hcpdata;
                hcpcoverage.titleField = "title";
                hcpcoverage.valueField = "value";
                hcpcoverage.urlField = "Url";
                hcpcoverage.urlTarget = "_blank";
                hcpcoverage.outlineColor = "#cecece";
                hcpcoverage.outlineAlpha = 0.8;
                hcpcoverage.outlineThickness = 2;
                hcpcoverage.balloonText = '[[title]]: [[value]]';
                hcpcoverage.labelText = '';
                hcpcoverage.legend = {"position":"bottom", "autoMargins":true, "fontSize": 9, "align":"center", "verticalGap": 0};
                hcpcoverage.titles = [{"text": "Assigned HCP", "color": "#2d4f80"}];
                hcpcoverage.colors = colors;

                // WRITE
                hcpcoverage.write("hcpcoverage");
                <?php
                $reachArr = array();
                $rsAccReach['name'] = "ACC Reach";
                $rsPDHcpReach['name'] = "PD HCP Reach";
                $rsMWHcpReach['name'] = "MW HCP Reach";
                $rsOTHcpReach['name'] = "Other HCP Reach";
                $reachArr[] = $rsAccReach;
		$reachArr[] = $rsMWHcpReach;
		$reachArr[] = $rsOTHcpReach;
                $reachArr[] = $rsPDHcpReach;
                ?>
                var chart = AmCharts.makeChart("reachbar", {
		    "theme": "light",
		    "type": "serial",
		    "dataProvider": <?=json_encode($reachArr)?>,
		    "valueAxes": [{
		        "unit": "",
		        "position": "left",
		        "title": "HCP",
		    }],
		    "startDuration": 1,
		    "graphs": [{
		        "balloonText": "[[category]] Covered: <b>[[value]]</b>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "2004",
		        "type": "column",
		        "valueField": "covered"
		    }, {
		        "balloonText": "[[category]] Planned: <b>[[value]]</b>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "2005",
		        "type": "column",
		        "clustered":false,
		        "columnWidth":0.6,
		        "valueField": "planned"
		    }, {
		        "balloonText": "[[category]] Visited: <b>[[value]]</b>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "2006",
		        "type": "column",
		        "clustered":false,
		        "columnWidth":0.5,
		        "valueField": "visited"
		    }],
		    "plotAreaFillAlphas": 0.1,
		    "categoryField": "name",
		    "categoryAxis": {
		        "gridPosition": "start"
		    },
		    "export": {
		    	"enabled": true
		     }

		});
		<?php
		$effecgtivenessarr = array();
		$effecgtivenessarr[] = array("name" => "Approved Plan Count", "visits" => $rsPlanEffectiveness->approvedcount);
		$effecgtivenessarr[] = array("name" => "Deleted Plan Count", "visits" => $rsPlanEffectiveness->deletedcount);
		$effecgtivenessarr[] = array("name" => "Ad-Hoc Plan Count", "visits" => $rsPlanEffectiveness->adhoccount);
		$effecgtivenessarr[] = array("name" => "Rescheduled Plan Count", "visits" => $rsPlanEffectiveness->rescheduled);
		$effecgtivenessarr[] = array("name" => "Missed Plan Count", "visits" => $rsPlanEffectiveness->missed);
		$effecgtivenessarr[] = array("name" => "Unapproved Plan Count", "visits" => $rsPlanEffectiveness->unapproved);
		$effecgtivenessarr[] = array("name" => "Approved Visited Plan Count", "visits" => $rsPlanEffectiveness->approvedvisitcount);
		$effecgtivenessarr[] = array("name" => "Total Visited Plan Count", "visits" => $rsPlanEffectiveness->totalvisitcount);
		?>
		var chart = AmCharts.makeChart( "effectivenessbar", {
		  "type": "serial",
		  "theme": "light",
		  "dataProvider": <?=json_encode($effecgtivenessarr)?>,
		  "valueAxes": [ {
		    // "gridColor": "#FFFFFF",
		    // "gridAlpha": 0.2,
		    // "dashLength": 0,
		    "unit": "",
		    "title": "Plans",
		    "position": "left"
		  } ],
		  "gridAboveGraphs": true,
		  "startDuration": 1,
		  "graphs": [ {
		    "balloonText": "[[category]]: <b>[[value]]</b>",
		    "fillAlphas": 0.8,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "visits"
		  } ],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "name",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "gridAlpha": 0,
		    "tickPosition": "start",
		    "tickLength": 20
		  },
		  "export": {
		    "enabled": true
		  }

		} );
	})
	
</script>