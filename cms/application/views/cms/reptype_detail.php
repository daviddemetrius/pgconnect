<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
<section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-th-list right-sm"></i> Representative Type Detail 
      <div class='tableheaderback'><a href="<?=site_url("cms/accounts/reptype")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-1'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h1><?=$reptype->typename?></h1>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <?php
              $productarr = array();
              $productstr = "";
              $delim = "";
              foreach ($reptype->modules as $rept){
                $productarr[] = $rept->moduleid;
                $productstr .= $delim.$rept->moduletitle;
                $delim = ", ";
              }
              ?>
              <i class='glyphicon glyphicon-th-list'></i> <?=$productstr?>
            </div>
          </div>
          <div class='col-md-1'>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
      <?php if ($this->session->userdata("tier") > 2.5) { ?>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      <?php } ?>
      </div>
    </div>
    <?php if ($this->session->userdata("tier") > 2.5) { ?>
    <form method="POST" action="<?=site_url('cms/accounts/editRepType')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
        <input  type='hidden' value='<?=$reptype->typeid?>' name='id'>
        <label class="has-float-label">
          <input type="text" placeholder="Type" name="typename" value="<?php echo $reptype->typename; ?>"/>
          <span>Type Name</span>
        </label>
        <label>Available Modules <i class='glyphicon glyphicon-check' onclick='$("#repfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#repfields input").prop("checked", false)' style='cursor:pointer'></i></label>
        <div class='row' id='repfields'>
          <?php
          foreach ($modules as $type){
            if (in_array($type->moduleid, $productarr)){
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->moduleid.'" name="modules[]" checked> '.$type->moduletitle.'</div>';
            } else {
              echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->moduleid.'" name="modules[]"> '.$type->moduletitle.'</div>';
            }
            
          }
          ?>
        </div>
        <br/>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
    <?php } ?>
  </div>
</section>
</section>	

<script>
<?php if ($this->session->userdata("tier") > 2.5) { ?>
 $("#editbutton").on("click", function(){
  $("input").prop("disabled", false);
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
 <?php } ?>
</script>
