<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-th-list"></i> Add New Rep Type
                <div class='tableheaderback'><a href="<?=site_url("cms/accounts/reptype")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Type" name="typename" value="<?php echo set_value('typename'); ?>"/>
                <span>Type Name</span>
              </label>
              <label>Available Modules <i class='glyphicon glyphicon-check' onclick='$("#repfields input").prop("checked", true)' style='cursor:pointer'></i> <i class='glyphicon glyphicon-remove' onclick='$("#repfields input").prop("checked", false)' style='cursor:pointer'></i></label>
              <div class='row' id='repfields'>
                <?php
                foreach ($modules as $type){
                  if (in_array($type->moduleid, set_value('modules', array()))){
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->moduleid.'" name="modules[]" checked> '.$type->moduletitle.'</div>';
                  } else {
                    echo '<div class="col-xs-6"><input type="checkbox" value="'.$type->moduleid.'" name="modules[]"> '.$type->moduletitle.'</div>';
                  }
                  
                }
                ?>
              </div>
              <br/>

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	