<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-stop right-sm"></i> Rooms Details
      <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/rooms")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/images/uploads/?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h3><?= $roomdetails->roomname?></h3>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              Created <strong><?= date( "j F Y H:i:s", strtotime($roomdetails->created))?></strong>
            </div>
            <div class='detailcontent'>
              Last Updated <strong><?= date( "j F Y H:i:s", strtotime($roomdetails->lastupdated))?></strong>
            </div>
          </div>
        </div>
      </div>
      <?php if ($this->session->userdata("tier") > 3 ){ ?>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
      <?php } ?>
    </div>
    <form method="POST" action="<?=site_url('cms/masterdata/editRoom')?>">
      <fieldset class='editor hidden'>
        <input type="hidden" name="roomid" value="<?php echo $roomdetails->roomid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Room Name" name="room" value="<?php echo $roomdetails->roomname?>"/>
          <span>Room Name</span>
        </label>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

 
</script>