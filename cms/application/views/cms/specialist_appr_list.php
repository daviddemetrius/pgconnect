<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-<?=$listicon?> right-sm"></i> Unapproved <?=$listtitle?> List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	
                <?php
                foreach($tabletitles as $key=>$title){
                  echo "<th><a href='".$sitelink."?s=".$tablesorting[$key].$addllink."' class='".($s==$tablesorting[$key]?"active":"")."'>$title</th>";
                }
                ?>
                <th colspan=3 width=20%><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($items as $key=>$item){
              $btnobs = "<a href='#' class='form-control btn btn-warning approver' data-id='".$item->id."'>Approve</a>";
              $btnrej = "<a href='#' class='form-control btn btn-danger rejector' data-id='".$item->id."'>Reject</a>";
               if ($this->session->userdata("tier") <= 3){
                if ($item->obsolete == 1){
                  $btnobs = "<span class='oranget'>Inactive</span>";
                } else {
                  $btnobs = "<span class='greent'>Active</span>";
                }
                $btnrej = "";
               }
              echo "
                <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>";
              foreach($item as $key=>$i){
                if ($key == "obsolete"){
                  continue;
                }
                echo "<td>$i</td>";
              }
              echo "
                  <td>$btnrej</td>
                  <td>$btnobs</td>
                  <td><a href='".site_url("cms/masterdata/hcps/$item->id")."' class='btn-primary btn form-control'>Details</a></td>
               </tr>";
            }
            ?>
            <tr>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
    $(".approver").on("click", function(){
      var id = $(this).data("id");
      swal({
        title: 'Are you sure?',
        text: "Make sure the specialist is correctly assigned before approving as this action will be irreversible.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.value) {
          $.ajax({
             url: '<?=site_url("cms/masterdata/approveHCP")?>',
             type:"POST",
             data: {id:id},
             success: function(d){
                if (d >= 1){
                   location.reload();
                } else if (d == 0){
                   swal("Danone", "Operation Failed. Please try again or contact an administrator", "error");
                } else {
                   swal("Danone", d, "warning");
                }
             }
          })
        }
      })
   }) 
    $(".rejector").on("click", function(){
      var id = $(this).data("id");
      swal({
        title: 'Are you sure?',
        text: "This action will delete the specialist entry and is irreversible.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.value) {
          $.ajax({
             url: '<?=site_url("cms/masterdata/rejectHCP")?>',
             type:"POST",
             data: {id:id},
             success: function(d){
                if (d >= 1){
                   location.reload();
                } else if (d == 0){
                   swal("Danone", "Operation Failed. Please try again or contact an administrator", "error");
                } else {
                   swal("Danone", d, "warning");
                }
             }
          })
        }
      })
      
   }) 
</script>