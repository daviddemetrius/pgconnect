<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-user right-sm"></i> Specialist Detail
      <div class='tableheaderback'><a href="<?=$_SERVER['HTTP_REFERER'];?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/images/uploads/?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
               <h3><?= $details->specialistname?></h3>
            </div>
            <div class='detailcontent'>
               <?= $details->xmlhcpid?>
            </div>
            <div class='detailcontent'>
               Specialty: <?= $details->specialty?>
            </div>
            <div class='detailcontent'>
               Institution: <?= $details->institution." ".$details->institutioncode?>
            </div>
            <div class='detailcontent'>
               Med Rep: <?= $details->medrepcode?>
            </div>
            <div class='detailcontent'>
               APL Code: <?= $details->aplcode?>
            </div>
            <div class='detailcontent'>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              Created <strong><?= date( "j F Y H:i:s", strtotime($details->created))?></strong>
            </div>
            <div class='detailcontent'>
              Updated <strong><?= date( "j F Y H:i:s", strtotime($details->lastupdated))?></strong>
            </div>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-road'></i> <?=$details->address?>
            </div>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-headphones'></i> <?=$details->phone?>
            </div>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/masterdata/editSpecialist')?>">
      <fieldset class='editor hidden'>
        <input type="hidden" name="specid" value="<?php echo $details->specid ?>" disabled/>

        <label class="has-float-label">
          <input type="text" placeholder="Type Specialist Name" name="specialistname" value="<?php echo $details->specialistname; ?>"/>
          <span>Specialist Name</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Specialty" name="specialty" value="<?php echo $details->specialty; ?>"/>
          <span>Specialty</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Address" name="address" value="<?php echo $details->address; ?>"/>
          <span>Address</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Phone" name="phone" value="<?php echo $details->phone; ?>"/>
          <span>Contact Number</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Institution Code" name="inscode" value="<?php echo $details->institutioncode ?>" id='inscode'/>
          <span>Institution Code</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Institution" name="insname" value="<?php echo $details->institution ?>" id='insname'/>
          <span>Institution Name</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type Med Rep Code" name="medrepcode" value="<?php echo $details->medrepcode ?>" id='medrepcode'/>
          <span>Med Rep Code</span>
        </label>
        <label class="has-float-label">
          <input type="text" placeholder="Type APL Code" name="aplcode" value="<?php echo $details->aplcode ?>" id='aplcode'/>
          <span>APL Code</span>
        </label>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

 
</script>