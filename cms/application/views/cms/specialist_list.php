<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-<?=$listicon?> right-sm"></i> <?=$listtitle?> List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	
                <?php
                foreach($tabletitles as $key=>$title){
                  echo "<th><a href='".$sitelink."?s=".$tablesorting[$key].$addllink."' class='".($s==$tablesorting[$key]?"active":"")."'>$title</th>";
                }
                ?>
                <th colspan=2 width=20%><select class='form-control' id='srcsel'><option value=''>All</option><option value='specialistname'>Name</option><option value='xmlhcpid'>Code</option><option value='specialtyname'>Specialty</option><option value='institution'>institution</option><option value='address'>Address</option><option value='inactive'>All Inactives</option><option value='active'>All Actives</option></select><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($items as $key=>$item){
              $btnobs = "<a href='#' class='form-control btn btn-danger deactivator' data-id='".$item->id."'>Deactivate</a>";
               if ($item->obsolete == 1){
                  $btnobs = "<a href='#' class='form-control btn btn-warning reactivator' data-id='".$item->id."'>Reactivate</a>";
               }
              echo "
                <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>";
              foreach($item as $key=>$i){
                if ($key == "obsolete" || $key == "id"){
                  continue;
                }
                echo "<td>$i</td>";
              }
              echo "
                  <td>$btnobs</td>
                  <td><a href='".site_url("cms/masterdata/specialists/$item->id")."' class='btn-primary btn form-control'>Details</a></td>
               </tr>";
            }
            ?>
            <tr></tr>
         </table>
         <div style='padding:10px;text-align: right'>
          <?php if ($this->session->userdata("tier") != 1) { ?>
          <a href='<?=site_url("cms/masterdata/specialists/new")?>' class='btn btn-primary'>Add New Doctor</a>
          <?php } ?>
         </div>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink".($s?"&s=$s":"")."'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink".($s?"&s=$s":"")."'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink".($s?"&s=$s":"")."'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var f = $("#srcsel").val();
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q+"&f="+f+"<?=$s?"&s=$s":""?>";
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
    $(".reactivator").on("click", function(){
      var id = $(this).data("id");
      $.ajax({
         url: '<?=site_url("cms/masterdata/reactivateHCP")?>',
         type:"POST",
         data: {id:id},
         success: function(d){
            if (d >= 1){
               location.reload();
            } else if (d == 0){
               swal("", "Operation Failed. Please try again or contact an administrator", "error");
            } else {
               swal("", d, "warning");
            }
         }
      })
   }) 

   $(".deactivator").on("click", function(){
      var id = $(this).data("id");
      swal({
       title: "Are you sure?",
       text: "",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Yes",
       cancelButtonText: "No",
       closeOnConfirm: false,
       closeOnCancel: false
    }).then(function(isConfirm){
      // console.log("wut");
      if (isConfirm.value){
         $.ajax({
            url: '<?=site_url("cms/masterdata/deactivateHCP")?>',
            type:"POST",
            data: {id:id},
            success: function(d){
               if (d >= 1){
                  location.reload();
               } else if (d == 0){
                  swal("", "Operation Failed. Please try again or contact an administrator", "error");
               } else {
                  swal("", d, "warning");
               }
            }
         })      

      } else {

      }
   })
   
   })
</script>