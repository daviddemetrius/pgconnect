<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-user right-sm"></i>Add New Doctor
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/specialists")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Specialist Name" name="specialistname" value="<?php echo set_value('specialistname'); ?>" id='hcpname'/>
                <span>Name</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Specialty" name="specialty" value="<?php echo set_value('specialty'); ?>" id='specialty'/>
                <span>Specialty</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Address" name="address" value="<?php echo set_value('address'); ?>"/>
                <span>Address</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Phone" name="phone" value="<?php echo set_value('phone'); ?>"/>
                <span>Contact Number</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type HCP Code" name="xmlhcpid" value="<?php echo set_value('xmlhcpid'); ?>" id='hcpcode'/>
                <span>Doctor Code</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Institution Code" name="inscode" value="<?php echo set_value('inscode'); ?>" id='inscode'/>
                <span>Institution Code</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Institution" name="insname" value="<?php echo set_value('insname'); ?>" id='insname'/>
                <span>Institution Name</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Med Rep Code" name="medrepcode" value="<?php echo set_value('medrepcode'); ?>" id='medrepcode'/>
                <span>Med Rep Code</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type APL Code" name="aplcode" value="<?php echo set_value('aplcode'); ?>" id='aplcode'/>
                <span>APL Code</span>
              </label>
              

              <button class='form-control btn btn-primary'>Add Doctor</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	

<script>
  $('#hcpcode, #hcpname').autocomplete({
      serviceUrl: '<?=base_url()?>cms/masterdata/searchhcp',
      onSelect: function (suggestion) {
      }
});
</script>