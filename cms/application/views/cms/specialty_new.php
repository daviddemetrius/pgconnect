<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-cutlery right-sm"></i>Add New Specialty
                <div class='tableheaderback'><a href="<?=site_url("cms/masterdata/specialty")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Specialty Name" name="specialtyname" max="2" value="<?php echo set_value('specialtyname'); ?>"/>
                <span>Specialty Name</span>
              </label>

              <button class='form-control btn btn-primary'>Add Specialty</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	