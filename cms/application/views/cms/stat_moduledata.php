<script src="<?php echo base_url(); ?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/serial.js" type="text/javascript"></script>	
<script src="<?php echo base_url(); ?>js/amcharts/xy.js" type="text/javascript"></script>	
<style>
	tr:nth-child(odd){
		background-color:#fafafa;
	}
	td{
		text-align:center;		
	}
	th{
		text-align:center;
	}
	td:first-child{
		font-weight:bold;	
	}
</style>

<?php
	$newModArr = array();
	foreach ($ModuleData as $rep){
		foreach ($rep->modulestat as $md){
			$found = -1;
			foreach ($newModArr as $key=>$ma){
				if ($ma['moduleid'] == $md->moduleid){
					$found = $key;
					break;
				}
			}
			if ($found >= 0){
				$dmFound = -1;
				foreach ($newModArr[$found]["regions"] as $dmKey=>$reg){
					if ($reg["dm"] == $rep->username){
						$dmFound = $dmKey;
						break;
					}
				}
				if ($dmFound >= 0){
					$newModArr[$found]["regions"][$dmFound]["totalduration"] += $md->totalduration;
					$newModArr[$found]["regions"][$dmFound]["frequency"] += $md->frequency;
				} else {
					$regObj = array("dm" => $rep->username, "totalduration" => $md->totalduration, "frequency" => $md->frequency);
					$newModArr[$found]["regions"][] = $regObj;
				}
			} else {
				$maObj = array();
				$maObj['moduleid'] = $md->moduleid;
				$maObj['title'] = $md->moduletitle;
				$maObj['regions'] = array(
					array(
						"dm" => $rep->username,
						"totalduration" => $md->totalduration,
						"frequency" => $md->frequency
					)
				);

				$newModArr[] = $maObj;
			}
		}
	}
	// echo json_encode($newModArr);
?>

<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height"> <!-- site-min-height -->
		<div class='tableheader'>
              		<i class="glyphicon glyphicon-briefcase right-sm"></i> Monthly Module Data
            	</div>
		<div class='centerfields'>
            		<form method='get'>
				<label>Month / Year</label>
				<div class='row'>
					<div class='col-xs-3'><input type='number' min=1 max=12 class='form-control' id='month' value="<?=$this->input->get("month")?$this->input->get("month"):date("n")?>" onchange='' name='month'></div>
					<div class='col-xs-3'><input type='number' min=2018 class='form-control' id='year' value="<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>" onchange='' name='year'></div>
					<div class='col-xs-4'><button class='btn btn-primary form-control'>Get Data</button></div>
				</div>
				<br/>
			</form>
		</div>
		<div class="row" id='dboard'>
			<div class='col-xs-12 col-md-12 col-lg-12'>
	   			<div class='chartdiv'  id='hcpcoverage'>
	   				<table>
	   					<tr>
	   						<th>Module</th>
	   						<th>District</th>
	   						<th>Total Duration</th>
	   						<th>Frequency</th>
	   						<th>Average</th>
	   					</tr>
	   					<?php
	   					foreach ($newModArr as $entry){
	   						foreach ($entry["regions"] as $key=>$reg){
	   							if ($reg['frequency'] == 0){
	   								continue;
	   							}
	   							echo "<tr>";
	   							if ($key == 0){
	   								echo "<td>".$entry['title']."</td>";
	   							} else {
	   								echo "<td></td>";
	   							}
	   							echo "<td>".$reg['dm']."</td>";
	   							echo "<td style='text-transform:lowercase'>".number_format($reg['totalduration'])." s</td>";
	   							echo "<td>".$reg['frequency']."</td>";
	   							echo "<td style='text-transform:lowercase'>".number_format($reg['totalduration']/($reg['frequency']==0?1:$reg['frequency']), 2)." s</td>";
	   							echo "</tr>";
	   						}
	   					}
	   					?>
	   				</table>
	   			</div>
	   		</div>
		</div>
	</section>
</section>