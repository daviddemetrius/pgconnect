<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class="fullblock">
   <div class='tableheader'>
    <i class="glyphicon glyphicon-tasks right-sm"></i> Product Share
  </div>
  <form method="GET" action="">
    <fieldset>
      <div class='centerfields bigger'>
        <label>Month / Year</label>
        <div class='row'>
          <div class='col-xs-2'><input type='number' min=1 max=12 class='form-control' name='month' id='month' value="<?=$m?$m:date("n")?>" onchange='changeMonth(this)'></div>
          <div class='col-xs-2'><input type='number' min=2018 class='form-control' name='year' id='year' value="<?=$y?$y:date("Y")?>" onchange='changeYear(this)'></div>
          <div class='col-xs-8'><button class='btn btn-primary form-control' onclick=''>Show Data</button></div>
        </div>
      </div>
    </fieldset>
  </form>
  <div class='tablecontent'>
    <table>
      <tr>
        <th width=5%>#</th>	
        <th>Product Name</th> 
        <th>House Category</th> 
        <th width=10%>From Birth</th>
        <th width=10%>From Peds</th>
        <th><button class='btn form-control' onclick='downloadMainTable()'>Download</button></th> 
      </tr>
      <?php
      foreach($products as $key=>$item){
        echo "
        <tr>
          <td>".(($key+1))."</td>
          <td>$item->productname</td>
          <td>$item->housename - $item->categoryname</td>
          <td>$item->birthcount</td>
          <td>$item->pedscount</td>
          <td><button type='button' class='btn btn-primary form-control' data-toggle='modal' data-target='#statmodal' onclick='populateACCShare($item->productid, $m, $y, \"$item->productname\")'>Show ACC</button></td>
        </tr>";
      }
      ?>
    </table>
  </div>
</div>
<div class="modal fade" id="statmodal" tabindex="-1" role="dialog" aria-labelledby="modallabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modallabel">ACC List for <span id='modalproduct'></span> <?=$y?>-<?=$m?> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" onclick='downloadTable()'>Download</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</section>
</section>	
<script>
var dotter;
var chosenProduct
function populateACCShare($productid, $m, $y, productname){
  chosenProduct = productname;
  $("#modalproduct").html(chosenProduct);
  $(".modal-body").html("...");
  dotter = setInterval(function(){$(".modal-body").append(".")}, 1000);
  $.ajax({
    url: "<?=site_url("cms/statistics/getProductShareACC")?>",
    type: "get",
    data: {
      productid: $productid,
      month: $m,
      year: $y
    },
    success: function(data){
      $(".modal-body").html(data);
    }, complete: function(){
      clearInterval(dotter);
    }
  })
}

function downloadTable(){
  const rows = [];
  var stuff = $(".modal-body").find(".alternating");
  for (var i = 0; i < stuff.length; i++){
    var fields = $(stuff[i]).find("div");
    var arr = [];
    for (var j = 0; j < fields.length; j++){
      arr.push('"'+$(fields[j]).html()+'"');
    }
    rows.push(arr);
  }
  let csvContent = "data:text/csv;charset=utf-8,";
  rows.forEach(function(rowArray){
     let row = rowArray.join(",");
     csvContent += row + "\r\n";
  }); 
  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", chosenProduct+" Share <?=$y?>-<?=$m?>.csv");
  link.innerHTML= "Click Here to download";
  document.body.appendChild(link); // Required for FF

  link.click();
}

function downloadMainTable(){
  const rows = [["#", "Product Name", "House Category", "From Birth", "From Peds"]];
  var stuff = $(".tablecontent").find("tr");
  for (var i = 0; i < stuff.length; i++){
    var fields = $(stuff[i]).find("td");
    var arr = [];
    for (var j = 0; j < fields.length-1; j++){
      arr.push('"'+$(fields[j]).html()+'"');
    }
    rows.push(arr);
  }
  let csvContent = "data:text/csv;charset=utf-8,";
  rows.forEach(function(rowArray){
     let row = rowArray.join(",");
     csvContent += row + "\r\n";
  }); 
  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "Share Data <?=$y?>-<?=$m?>.csv");
  link.innerHTML= "Click Here to download";
  document.body.appendChild(link); // Required for FF

  link.click();
}
</script>