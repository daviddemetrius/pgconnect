<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
<section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-list-alt right-sm"></i> Question Detail 
      <div class='tableheaderback'><a href="<?=site_url("cms/quiz/survey")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-1'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h1><?=$survey->surveytext?></h1>
            </div>
            <div class='detailcontent'>
              Answer: <b><?=$survey->correctanswer?></b>
            </div>
            <?php
            if ($survey->surveytype == 0 || $survey->surveytype == 2){
            ?>
            <div class='detailcontent'>
              Answers:<br/>
              <?php
              $answersarr = explode("xvz:zvx", $survey->surveychoices);
              foreach ($answersarr as $key=>$ans){
                echo ($key+1).". ".$ans."<br/>";
              }
              ?>
            </div>
            <?php } ?>
          </div>
          <div class='col-md-5'>
            
          </div>
          <div class='col-md-1'>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
      <?php if ($this->session->userdata("tier") > 2.5) { ?>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      <?php } ?>
      </div>
    </div>
    <?php if ($this->session->userdata("tier") > 2.5) { ?>
    <form method="POST" action="<?=site_url('cms/quiz/editSurvey')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
        <input  type='hidden' value='<?=$survey->surveyid?>' name='id'>
        
        <label class="has-float-label">
          <input type="text" placeholder="Question" name="surveytext" value="<?php echo $survey->surveytext; ?>"/>
          <span>Survey Question</span>
        </label>
        
        <label class="has-float-label">
          <input type="text" placeholder="Answer" name="surveyanswer" value="<?php echo $survey->correctanswer; ?>"/>
          <span>Correct Answer</span>
        </label>

        <label id='questionslabel' class='<?=$survey->surveytype==0||$survey->surveytype==2?"":"hidden"?>'>Answers</label>
        <div class='row <?=$survey->surveytype==0||$survey->surveytype==2?"":"hidden"?>' id='questionsdiv'>
          <?php
          foreach ($answersarr as $type){
            echo '<label class="has-float-label">
              <input type="text" placeholder="Answer" name="answers[]" value="'.$type.'"/>
              <span>Survey Answer <strong onclick="removeThisQ(this)" style="cursor:pointer">[X]</strong></span>
            </label>';
          }
          ?>
          <button type='button' class='btn form-control' onclick='addAnswers(this)'>Add More Answers</button>
        </div>
        <br/>

        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
    <?php } ?>
  </div>
  
</section>
</section>	

<script>
<?php if ($this->session->userdata("tier") > 2.5) { ?>
 $("#editbutton").on("click", function(){
  $("input").prop("disabled", false);
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
function answersVisibility(e){
    if ($(e).val() == 0 || $(e).val() == 2){
      $("#questionslabel").removeClass("hidden");
      $("#questionsdiv").removeClass("hidden");
    } else {
      $("#questionslabel").addClass("hidden");
      $("#questionsdiv").addClass("hidden");
    }
  }

  function addAnswers(e){
    var html = '<label class="has-float-label"><input type="text" placeholder="Answer" name="answers[]" value=""/><span>Survey Answer <strong onclick="removeThisQ(this)" style="cursor:pointer">[X]</strong></span></label>'
    $(e).before(html);
  }

  function removeThisQ(e){
    $(e).parent().parent().remove();
  }
 <?php } ?>
</script>
