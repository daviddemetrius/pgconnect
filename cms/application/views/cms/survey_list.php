<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-list-alt right-sm"></i> Question List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	 
                <th width=10%>Type</th>	
                <th>Question</th>	
                <th width=20% colspan=2><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($modules as $key=>$mod){
               $btnedit = "<a href='".site_url("cms/quiz/survey/$mod->surveyid")."' class='form-control btn btn-primary'>Details</a>";
               $status = "<span class='greent'>Active</span>";
               if ($mod->availability == 0){
                $status = "<span class='oranget'>Deactivated</span>";
                if ($this->session->userdata("tier") <= 2){
                  continue;
                }
               }
               echo "
               <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td>".($mod->surveytype==0?"Multiple Choice":($mod->surveytype==1?"Free Text":"Checkbox"))."</td>
                  <td>$mod->surveytext</td>
                  <td colspan=2>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2>
               <?php
                if ($this->session->userdata("tier") > 2.5){ ?>
                  <a class='btn form-control' href='<?=site_url("cms/quiz/survey/new")?>'>Add New Question</a>
               <?php }?>
               </td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q;
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
</script>