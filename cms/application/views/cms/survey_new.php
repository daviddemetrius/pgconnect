<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-list-alt"></i> Add New Question
                <div class='tableheaderback'><a href="<?=site_url("cms/quiz/survey")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Question" name="surveytext" value="<?php echo set_value('surveytext'); ?>"/>
                <span>Question</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Answer" name="surveyanswer" value="<?php echo set_value('surveyanswer'); ?>"/>
                <span>Correct Answer</span>
              </label>
              <br/>
              <label id='questionslabel' class='<?=set_value("surveytype")==0?"":"hidden"?>'>Answers</label>
              <div class='row <?=set_value("surveytype")==0?"":"hidden"?>' id='questionsdiv'>
                <?php
                foreach (set_value("answers", array()) as $type){
                  echo '<label class="has-float-label">
                    <input type="text" placeholder="Answer" name="answers[]" value="'.$type.'"/>
                    <span>Quiz Answer <strong onclick="removeThisQ(this)" style="cursor:pointer">[X]</strong></span>
                  </label>';
                }
                ?>
                <button type='button' class='btn form-control' onclick='addAnswers(this)'>Add More Answers</button>
              </div>
              <br/>

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	

<script>
  function answersVisibility(e){
    if ($(e).val() == 0 || $(e).val() == 2){
      $("#questionslabel").removeClass("hidden");
      $("#questionsdiv").removeClass("hidden");
    } else {
      $("#questionslabel").addClass("hidden");
      $("#questionsdiv").addClass("hidden");
    }
  }

  function addAnswers(e){
    var html = '<label class="has-float-label"><input type="text" placeholder="Answer" name="answers[]" value=""/><span>Quiz Answer <strong onclick="removeThisQ(this)" style="cursor:pointer">[X]</strong></span></label>'
    $(e).before(html);
  }

  function removeThisQ(e){
    $(e).parent().parent().remove();
  }
</script>