<?php if ($this->session->userdata("tier") <= 2 && $cycledata->obsolete == 1){redirect("cms");} ?>
<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/Sortable.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
  <section class="wrapper site-min-height"> 
    <div class='fullblock'>
      <div class='tableheader'>
        <i class="glyphicon glyphicon-refresh right-sm"></i> <?=$cycledata->cycletitle?> <?=$cycledata->obsolete==1?" - <span class='oranget'>Deactivated</span>": ""?>
        <div class='tableheaderback'><a href="<?=site_url("cms/quiz/cycles")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
      </div>
      <div class='blockdetails'>
        <form method='post' action='<?=site_url("cms/quiz/updateclosingquestions")?>'>
          <input type='hidden' name='cycleid' value="<?=$cycledata->cycleid?>">
          <div class='detailseparator'>
            <div class='row'>
              <div class='col-md-2'></div>
              <div class='col-md-8'>
                <h4>Questions:</h4>
                <ul class='sortable'>
                <?php
                foreach ($cycledata->questions as $step){
                  echo "<li><span class='sortableimg'></span>$step->order. ".$step->surveytext."". ($this->session->userdata('tier')>2?"<i class='js-remove'>✖</i>":"").
                    "<input type='hidden' name='pagenumber[]' value='$step->order'>
                    <input type='hidden' name='pageid[]' value='$step->questionid'>
                  </li>";
                }
                ?>
                </ul>
                <?php if ($this->session->userdata("tier") > 2.5){ ?>
                <button type='button' id="addItem" class='form-control btn btn-primary'>Add</button>
                <?php } ?>
              </div>
              <div class='col-md-2'></div>
            </div>
          </div>
          <div class='detailbuttons'>
            <?php if ($this->session->userdata("tier") > 2.5){ ?>
            <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit Basic Info</span></a>
            <button href="#" class='detailbtn btn' id="editbutton"><i class="glyphicon glyphicon-floppy-disk"></i> <span class=''>Save Question Order</span></button>
            <?php } ?>
          </div>
        </form>
      </div>
      <?php if ($this->session->userdata("tier") > 2.5){ ?>
      <form method="POST" action="<?=site_url('cms/quiz/editCycle')?>" enctype="multipart/form-data">
        <fieldset class='editor hidden'>
          <input  type='hidden' value='<?=$cycledata->cycleid?>' name='id'>
        <label class="has-float-label">
          <input type="text" placeholder="Title" name="cycletitle" value="<?php echo $cycledata->cycletitle; ?>"/>
          <span>Set Title</span>
        </label>
        <label class="has-float-label">
          <input type="date" placeholder="Start" name="periodstart" value="<?php echo date('Y-m-d', strtotime($cycledata->periodstart)); ?>"/>
          <span>Period Start</span>
        </label>
        <label class="has-float-label">
          <input type="date" placeholder="End" name="periodend" value="<?php echo date('Y-m-d', strtotime($cycledata->periodend)); ?>"/>
          <span>Period End</span>
        </label>

        <label>Set Active</label><br/>
        <input type='checkbox' value="1" name='active' class='form-control' <?=$cycledata->obsolete==0?'checked':''?>><br/>
        <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
        <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
      </form>
      <?php } ?>
    </div>
  </section>
</section>	

<script>
function changeHiddenBookmark(e){
  if ($(e).is(":checked")){
    $(e).parent().parent().find(".hiddenbkmark").val(1);
  } else {
    $(e).parent().parent().find(".hiddenbkmark").val(0);
  }
}

<?php if ($this->session->userdata("tier") > 2.5){ ?>
   $("#editbutton").on("click", function(){
      $("input").prop("disabled", false);
      $(".blockdetails").addClass("hidden");
      $(".editor").removeClass("hidden");
   })
   $("#cancelbutton").on("click", function(){
      $("input").prop("disabled", true);
      
      $(".blockdetails").removeClass("hidden");
      $(".editor").addClass("hidden");
   })
   function switchpage(number, e){
    $(e).parent().parent().parent().find(".currentindex").removeClass("currentindex");
    $(e).addClass('currentindex');
    $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
    $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
   }
<?php } ?>

    $("tr.pageddata").hover(function(){
      $(this).find(".rowdeleter").removeClass("hidden");
      $(this).find(".rowkey").addClass("hidden");
    }, function(){
      $(this).find(".rowdeleter").addClass("hidden");
      $(this).find(".rowkey").removeClass("hidden");
    });
<?php if ($this->session->userdata("tier") > 2.5){ ?>

    function openAJAXLoader(){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: true,
        onOpen: () => {
          swal.showLoading();
        }
      })
    }

   
   
   var pagesrc = <?=json_encode($questionlist)?>;
   function inputValues(){
    swal.setDefaults({
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1']
    });
    var swalsteps = [{
      title: 'Find Page',
      html: "<div style='position:relative' id='srcdiv'><div><img src='' style='max-height:100px;margin-bottom:10px'></div><input type='text' class='form-control' id='src' placeholder='Search...'><input type='hidden' id='srcid' value='-1'><input type='hidden' id='srclink' value='-1'></div>",
      onOpen: function() {
        $('#src').autocomplete({
          source: pagesrc,
          appendTo: '#srcdiv',
          select: function(event, ui){
            $("#src").val(ui.item.label);
            $("#srcid").val(ui.item.id);
            $("#srclink").val(ui.item.link);
          }
        });
      },
      preConfirm: function(){
        return Promise.resolve({id: $('#srcid').val(), name: $('#src').val(), link: $('#srclink').val(), });
      }
    }];

    swal.queue(swalsteps).then((result) => {
      swal.resetDefaults();
      if (result.value[0].link != -1) {
        $(".sortable").append('<li><span class="sortableimg"></span>'+result.value[0].name+'<i class="js-remove">✖</i><input type="hidden" name="pagenumber[]" value="NEW"><input type="hidden" name="pageid[]" value="'+result.value[0].id+'"></li>');
      }
    });
   }
<?php } ?>
$(document).ready(function(){
    // Editable list
    var editableList = Sortable.create($(".sortable")[0], {
      filter: '.js-remove',
      onFilter: function (evt) {
        var el = editableList.closest(evt.item); // get dragged item
        el && el.parentNode.removeChild(el);
      }
    });
    $("#addItem").on("click", inputValues);
   })
</script>