<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-refresh right-sm"></i> Set List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th width=5%>#</th>	 
                <th>Title</th>	
                <th>Period Start</th>	
                <th>Period End</th>
                <th>Status</th>
                <th width=20%><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($cycles as $key=>$cy){
               $btnedit = "<a href='".site_url("cms/quiz/cycles/$cy->cycleid")."' class='form-control btn btn-primary'>Details</a>";
               $status = "<span class='greent'>Active</span>";
               if ($cy->obsolete == 1){
                if ($this->session->userdata("tier") <= 2){
                  continue;
                }
                $status = "<span class='oranget'>Deactivated</span>";
               }
               if ($cy->periodend < date("Y-m-d 00:00:00")){
                if ($this->session->userdata("tier") <= 2){
                  continue;
                }
                $status = "<span class='oranget'>Ended</span>";
               }
               echo "
               <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td>$cy->cycletitle</td>
                  <td>".date("j F Y", strtotime($cy->periodstart))."</td>
                  <td>".date("j F Y", strtotime($cy->periodend))."</td>
                  <td>$status</td>
                  <td>$btnedit</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td colspan><?php if ($this->session->userdata("tier") > 2.5) { ?><a class='btn form-control' href='<?=site_url("cms/quiz/cycles/new")?>'>Add New Set</a><?php } ?></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row pageblock">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q;
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });
</script>