<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-refresh"></i> Add New Set
                <div class='tableheaderback'><a href="<?=site_url("cms/quiz/cycles/")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Title" name="cycletitle" value="<?php echo set_value('cycletitle'); ?>"/>
                <span>Set Title</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="Start" name="periodstart" value="<?php echo set_value('periodstart'); ?>"/>
                <span>Period Start</span>
              </label>
              <label class="has-float-label">
                <input type="date" placeholder="End" name="periodend" value="<?php echo set_value('periodend'); ?>"/>
                <span>Period End</span>
              </label>

              <button class='form-control btn btn-primary'>Add</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	