<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<script src='<?=base_url()?>js/jquery.autocomplete.js' type='text/javascript'></script>
<section id="main-content">
 <section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-tag right-sm"></i> Territory Details
      <div class='tableheaderback'><a href="<?=site_url("cms/regions/territory")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/images/uploads/?".rand()?>' onerror='defaultppproperty(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
             <h3><?= $territorydetails->territoryname?></h3>
           </div>
           <div class='detailcontent'>
             <?= $territorydetails->districtname?>
           </div>
           <div class='detailcontent'>
            Assigned To:<br/>
            <?php 
            if (count($territorydetails->assignedto) > 0){
              foreach ($territorydetails->assignedto as $rep){
                echo "<a href='".site_url("cms/accounts/reps/$rep->repid")."' target='_blank'>$rep->xmlnacd  $rep->repname</a><br/>";
              }
            } else {
              echo "Not Assigned";
            }
            ?>
           </div>
         </div>
         <div class='col-md-5'>
          <div class='detailcontent'>
            Created <strong><?= date( "j F Y H:i:s", strtotime($territorydetails->created))?></strong>
          </div>
          <div class='detailcontent'>
            Updated <strong><?= date( "j F Y H:i:s", strtotime($territorydetails->lastupdated))?></strong>
          </div>
        </div>
      </div>
    </div>
    <div class='detailbuttons'>
      <?php if ($this->session->userdata("tier") != 1){ ?>
      <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      <?php } ?>
      <a href="<?=site_url("cms/regions/downloadCoverage/$territorydetails->territoryid")?>" class='detailbtn'><i class="glyphicon glyphicon-download"></i> <span class=''>Coverage</span></a>
    </div>
  </div>
  <form method="POST" action="<?=site_url('cms/regions/editTerritory')?>">
    <fieldset class='editor hidden'>
      <input type="hidden" name="territoryid" value="<?php echo $territorydetails->territoryid ?>" disabled/>

      <?php if ($this->session->userdata("tier") > 3){ ?> 
      <label class="has-float-label">
        <input type="text" placeholder="Type Territory Name" name="territory" value="<?php echo $territorydetails->territoryname?>"/>
        <span>Territory Name</span>
      </label>
      <?php } ?>
      <label class="has-float-label">
        <select name="districtid">
          <?php
          foreach ($district as $di){
            if ($territorydetails->districtid == $di->districtid){
              echo "<option selected value='$di->districtid'>$di->districtname</option>";
            } else {
              echo "<option value='$di->districtid'>$di->districtname</option>";
            }
          }
          ?>
        </select>
        <span>District</span>
      </label>
      <?php if ($this->session->userdata("tier") > 3){ ?> 
      <label class="has-float-label">
        <input type="text" placeholder="Type Territory Code" name="masterid" value="<?php echo $territorydetails->masterid; ?>"/>
        <span>Territory Code</span>
      </label>
      <?php } ?>

      <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
      <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
  <div class='fullblock'>
    <div class='tableheader'>
      Assignment
    </div>
    <div class='tablecontent assignmenttable' id='tablecontent'>
      <table>
       <tr>
        <th width=10%>#</th>
        <th>ACC Name</th>
        <th>HCP Name</th>
        <th>Room</th>
      </tr>
      <?php
      $page = 0;
      $first = "showrow";
      foreach($territorydetails->specialists as $key=>$spec){
        if ($key %10 == 0){         
          $page++;
          if ($page !=1){
            $first = "";
          }           
        }
        echo "
        <tr class='pageddata page$page $first'>
         <td><span class='rowkey'>".($key+1)."</span><span class='rowdeleter hidden' onclick='deleteAssignment($spec->specid, $spec->roomid, $spec->hospitalid)'>x</span></td>
         <td>$spec->hospitalid - $spec->hospitalname</td>
         <td>$spec->specid - $spec->specialistname</td>
         <td>$spec->roomname</td>
       </tr>
       ";
     }
     ?> 
     <tr>
       <td colspan=3></td>
        <td><a class='btn form-control' data-toggle="modal" data-target="#myModal">Assign</a></td>
      </tr>
      <tr>
       <td colspan='4' class='pagingrow'>
         <?php
         $first = "currentindex";
         for ($i = 0; $i < $page; $i++){
          echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
          $first = "";
        }
        ?>
      </td>
    </tr>
  </table>
</div>
</div>
<div class='fullblock'>
  <div class='tableheader'>
    Accounts in Territory
  </div>
  <div class='tablecontent assignmenttable'>
    <table>
     <tr>
      <th width=10%>#</th>
      <th>ACC Name</th>
      <th>ACC Code</th>
      <th></th>
    </tr>
    <?php
    $page = 0;
    $first = "showrow";
    foreach($territorydetails->accounts as $key=>$spec){
      if ($key %10 == 0){         
        $page++;
        if ($page !=1){
          $first = "";
        }           
      }
      echo "
      <tr class='pageddata page$page $first'>
       <td><span class='rowkey'>".($key+1)."</span><span class='rowdeleter hidden' onclick='deleteHospitalAssignment($spec->hospitalid)'>x</span></td>
       <td><a href='".site_url("cms/masterdata/hospitals/$spec->hospitalid")."' target='_blank'>$spec->hospitalname</a></td>
       <td>$spec->hospitalid</td>
       <td><a href='javascript:void(0)' onclick='refreshTerritoryAsgn($spec->hospitalid)' class='btn btn-primary form-control'>Reset/Refresh Assignment</a></td>
     </tr>
     ";
   }
   ?> 
   <tr>
     <td colspan=3></td>
    </tr>
    <tr>
     <td colspan='4' class='pagingrow'>
       <?php
       $first = "currentindex";
       for ($i = 0; $i < $page; $i++){
        echo "<span class='paging $first' onclick='switchpage(".($i+1).", this)'> <a href='javascript:void(0)'>".($i+1)."</a></span>";
        $first = "";
      }
      ?>
    </td>
  </tr>
</table>
</div>
</div>
</section>
</section>	

<div class="modal fade" tabindex="-1" role="dialog" id='myModal'>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add New Assignment to Territory</h4>
      </div>
      <form method='post' action=''>
        <div class="modal-body">
          <label>Search ACC</label>
          <input type='text' class='form-control' name='acc' id='accsearch' placeholder='ACC' required>
          <label>Search HCP & Room</label>
          <input type='text' class='form-control' name='hcp' id='hcpsearch' placeholder='HCP' required disabled>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick='addAssignment()'>Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
 $("#editbutton").on("click", function(){
  $("input, select").prop("disabled", false);
  $("#editbutton").addClass("hidden");
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $("#editbutton").removeClass("hidden");
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
 $("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

 var chosenAcc = 0;
 $('#accsearch').autocomplete({
  serviceUrl: '<?=base_url()?>cms/regions/searchacc',
  onSearchStart: function(params){
    params.territoryid = <?=$territorydetails->territoryid?>;
  },
  onSelect: function (suggestion) {
    chosenAcc = suggestion.data;
    $("#hcpsearch").prop('disabled', false);
  }
});
 var chosenHcp = 0;
 var chosenRoom = 0;
 $('#hcpsearch').autocomplete({
  serviceUrl: '<?=base_url()?>cms/regions/searchhcp',
  onSearchStart: function(params){
    params.accid = chosenAcc;
  },
  onSelect: function (suggestion) {
    chosenHcp = suggestion.data;
    chosenRoom = suggestion.roomid;
  }
});
 
 function switchpage(number, e){
  $(e).parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}
function deleteAssignment(specid, roomid, hospitalid){
  swal({
    title: 'Apakah anda yakin?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak',
  }).then((result) => {
    if (result.value){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: false,
        onOpen: () => {
          swal.showLoading();
        }
      })
      $.ajax({
        url: '<?=site_url("cms/regions/deleteTerritoryAssignment")?>',
        type: "POST",
        data: {
          territoryid: <?=$territorydetails->territoryid?>,
          specid: specid,
          roomid: roomid,
          hospitalid: hospitalid
        },
        success: function(data){
          if (data == "success"){
            window.location = "<?=site_url("cms/regions/territory/".$territorydetails->territoryid)?>";
          } else {
            swal({
              title: 'Ups!',
              text: data,
              showCancelButton: false,
              confirmButtonText: "Ok",
              type: "error"
            })
          }
        }
      })
    }
  })
}
function deleteHospitalAssignment(hospitalid){
  swal({
    title: 'Apakah anda yakin?',
    text: "Menghapus ACC ini akan menghilangkan semua assignment di Territory ini",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak',
  }).then((result) => {
    if (result.value){
      swal({
        title: 'OK!',
        text: 'Modal ini akan tertutup secara otomatis...',
        showCancelButton: false,
        allowOutsideClick: false,
        onOpen: () => {
          swal.showLoading();
        }
      })
      $.ajax({
        url: '<?=site_url("cms/regions/deleteHospitalTerritoryAssignment")?>',
        type: "POST",
        data: {
          territoryid: <?=$territorydetails->territoryid?>,
          hospitalid: hospitalid
        },
        success: function(data){
          if (data == "success"){
            window.location = "<?=site_url("cms/regions/territory/".$territorydetails->territoryid)?>";
          } else {
            swal({
              title: 'Ups!',
              text: data,
              showCancelButton: false,
              confirmButtonText: "Ok",
              type: "error"
            })
          }
        }
      })
    }
  })
}
function refreshTerritoryAsgn(hospitalid){
  $.ajax({
    url: '<?=site_url("cms/regions/refreshTerritoryAssignment")?>',
    type: "POST",
    data: {
      territoryid: <?=$territorydetails->territoryid?>,
      hospitalid: hospitalid
    },
    success: function(data){
      if (data == "success"){
        window.location = "<?=site_url("cms/regions/territory/".$territorydetails->territoryid)?>";
      } else {
        swal({
          title: 'Ups!',
          text: data,
          showCancelButton: false,
          confirmButtonText: "Ok",
          type: "error"
        })
      }
    }
  })
}

function addAssignment(){
  if (chosenHcp != 0){
    $.ajax({
      url: '<?=site_url("cms/regions/addTerritoryAssignment")?>',
      type: "post",
      data: {
        hcpid: chosenHcp,
        roomid: chosenRoom,
        hospitalid: chosenAcc,
        territoryid: <?=$territorydetails->territoryid?>
      }, success: function(data){
        if (data == "success"){
          window.location = '<?=site_url("cms/regions/territory/$territorydetails->territoryid")?>';
        } else {
          swal({
            title: 'Ups!',
            text: data,
            showCancelButton: false,
            confirmButtonText: "Ok",
            type: "error"
          })
        }        
      }
    })
  }
}
</script>