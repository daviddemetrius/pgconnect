<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
          <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-globe right-sm"></i>Add New Territory
                <div class='tableheaderback'><a href="<?=site_url("cms/regions/territory")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
      <?php echo validation_errors(); ?>

              <label class="has-float-label">
                <input type="text" placeholder="Type Territory Name" name="territoryname" value="<?php echo set_value('territoryname'); ?>"/>
                <span>Territory Name</span>
              </label>
              <label class="has-float-label">
                <select name="districtid">
                <?php
                foreach ($district as $di){
                  if (set_value("districtid") == $di->districtid){
                    echo "<option selected value='$di->districtid'>$di->districtname</option>";
                  } else {
                    echo "<option value='$di->districtid'>$di->districtname</option>";
                  }
                }
                ?>
                </select>
                <span>District</span>
              </label>
              <label class="has-float-label">
                <input type="text" placeholder="Type Territory Code" name="masterid" value="<?php echo set_value('masterid'); ?>"/>
                <span>Territory Code</span>
              </label>

              <button class='form-control btn btn-primary'>Add Territory</button>
            </fieldset>
            </div>
            </form>
         </div>
  		</div>
	</section>
</section>	