<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<section id="main-content">
<section class="wrapper site-min-height"> 
  <div class='fullblock'>
    <div class='tableheader'>
      <i class="glyphicon glyphicon-user right-sm"></i> Admin Details
      <div class='tableheaderback'><a href="<?=site_url("cms/accounts/admin")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
    </div>
    <div class='blockdetails'>
      <div class='detailseparator'>
        <div class='row'>
          <div class='col-md-2 detailimage'>
            <img src='<?=base_url()."/images/uploads/admin/$account->userid.jpg?".rand()?>' onerror='defaultppagent(this)'>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <h1><?=$account->fullname?></h1>
            </div>
            <div class='detailcontent'>
              Username: <?=$account->username?>
            </div>
          </div>
          <div class='col-md-5'>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-earphone'></i> <?=$account->contact?>
            </div>
            <div class='detailcontent'>
              <i class='glyphicon glyphicon-envelope'></i> <?=$account->email?>
            </div>
          </div>
        </div>
      </div>
      <div class='detailbuttons'>
        <a href="#" class='detailbtn' id="editbutton"><i class="glyphicon glyphicon-pencil"></i> <span class=''>Edit</span></a>
      </div>
    </div>
    <form method="POST" action="<?=site_url('cms/accounts/editAdmin')?>" enctype="multipart/form-data">
      <fieldset class='editor hidden'>
       <input  type='hidden' value='<?=$account->userid?>' name='id'>
       <label class="has-float-label">
        <input type="text" placeholder="Name" name="fullname" value="<?=$account->fullname?>" disabled/>
        <span>Full Name</span>
      </label>

      <label class="has-float-label">
        <input type="text" placeholder="Username" name="username" value="<?=$account->username?>" disabled/>
        <span>Username</span>
      </label>

      <label class="has-float-label">
        <input type="password" placeholder="Input a New Value to Change Password. Leave blank to keep." name="password" value="" disabled/>
        <span>Password</span>
      </label>

      <label class="has-float-label">
        <input type="number" placeholder="Phone Number" name="phone" value="<?=$account->contact?>" disabled/>
        <span>Contact Number</span>
      </label>

      <label class="has-float-label">
        <input type="email" placeholder="E-mail" name="email" value="<?=$account->email?>" disabled/>
        <span>E-mail</span>
      </label>

      <label class="has-float-label">
        <select name='tier' disabled>
          <?php
          $arr = array(
            array(
              "tier" => 3,
              "Title" => "Content Assignment"
              ),
            array(
              "tier" => 2,
              "Title" => "Content Uploader"
              ),
            array(
              "tier" => 1,
              "Title" => "Head Office"
              ),
            array(
              "tier" => 999,
              "Title" => "Admin"
              )
            );
          foreach ($arr as $titles){
            if ($titles['tier'] == $account->tier){
              echo "<option value='".$titles['tier']."' selected>".$titles['Title']."</option>";
            } else {
              echo "<option value='".$titles['tier']."'>".$titles['Title']."</option>";
            }

          }
          ?>
        </select>
        <span>Position</span>
      </label>

      <a class='form-control btn btn-danger' href="#" id="cancelbutton">Cancel</a>
      <button class='form-control btn btn-primary' id="finishbutton">Save</a>
      </fieldset>
    </form>
  </div>
</section>
</section>	

<script>
 $("#editbutton").on("click", function(){
  $("input").prop("disabled", false);
  $(".blockdetails").addClass("hidden");
  $(".editor").removeClass("hidden");
  <?php
  if ($account->tier < $this->session->userdata("tier")){
   echo "$('select').prop('disabled', false);";
 }
 ?>
})
 $("#cancelbutton").on("click", function(){
  $("input").prop("disabled", true);
  
  $(".blockdetails").removeClass("hidden");
  $(".editor").addClass("hidden");
})
 function switchpage(number, e){
  $(e).parent().parent().parent().find(".currentindex").removeClass("currentindex");
  $(e).addClass('currentindex');
  $(e).parent().parent().parent().find(".showrow").removeClass("showrow");
  $(e).parent().parent().parent().find(".page"+number).addClass("showrow");
}
var repdata = <?=json_encode($srcrep)?>;
var dmdata = <?=json_encode($srcdm)?>;
var rmdata = <?=json_encode($srcrm)?>;


<?php
if ($this->session->userdata("tier") >= 2){
?>
$(".assignrep").on("click", function(){
  swal({
    title: "Cari Rep",
    showCancelButton: true,
    confirmButtonText: "OK",
    progressSteps: ['1'],
    html: "<div style='position:relative' id='srcdivrep'><input type='text' class='form-control' id='srcrep' placeholder='Search...'><input type='hidden' id='srcrepid' value='-1'></div>",
    onOpen: function() {
      $('#srcrep').autocomplete({
        source: repdata,
        appendTo: '#srcdivrep',
        select: function(event, ui){
          $("#srcrep").val(ui.item.label);
          $("#srcrepid").val(ui.item.id)
        }
      });
    },
    preConfirm: function() {
      return Promise.resolve({id: $('#srcrepid').val(), name: $('#srcrep').val()});
    }
  }).then((result) => {
    if (result.value.id == -1){
      swal("Ups!", "Mohon pilih dari pilihan yang disediakan", "error");
    } else {
      openAJAXLoader();
      $.ajax({
        url: '<?=site_url("cms/accounts/assignRepToDM")?>',
        type: "POST",
        data: {
          dmid: <?=$account->userid?>,
          repid: result.value.id
        },
        success: function(data){
          if (data == "success"){
            window.location = '<?=site_url("cms/accounts/admin/".$account->userid)?>';
          } else {
            swal("Ups!", data, "error")
          }
        }
      })
    }
  })
})
<?php
$defDMTxt = "Cari DM";
$defSrcTxt = "dmdata";
$defUrlTxt = "assignDMToRM";
if ($account->tier == 2.5){
  $defDMTxt = "Cari RM";
  $defSrcTxt = "rmdata";
  // $defUrlTxt = "assignRMToNOM";
}
?>
$(".assigndm").on("click", function(){
  swal({
    title: "<?=$defDMTxt?>",
    showCancelButton: true,
    confirmButtonText: "OK",
    progressSteps: ['1'],
    html: "<div style='position:relative' id='srcdivdm'><input type='text' class='form-control' id='srcdm' placeholder='Search...'><input type='hidden' id='srcdmid' value='-1'></div>",
    onOpen: function() {
      $('#srcdm').autocomplete({
        source: <?=$defSrcTxt?>,
        appendTo: '#srcdivdm',
        select: function(event, ui){
          $("#srcdm").val(ui.item.label);
          $("#srcdmid").val(ui.item.id)
        }
      });
    },
    preConfirm: function() {
      return Promise.resolve({id: $('#srcdmid').val(), name: $('#srcdm').val()});
    }
  }).then((result) => {
    if (result.value.id == -1){
      swal("Ups!", "Mohon pilih dari pilihan yang disediakan", "error");
    } else {
      openAJAXLoader();
      $.ajax({
        url: '<?=site_url("cms/accounts/$defUrlTxt")?>',
        type: "POST",
        data: {
          rmid: <?=$account->userid?>,
          dmid: result.value.id
        },
        success: function(data){
          if (data == "success"){
            window.location = '<?=site_url("cms/accounts/admin/".$account->userid)?>';
          } else {
            swal("Ups!", data, "error")
          }
        }
      })
    }
  })
})
<?php } ?>
$("tr.pageddata").hover(function(){
  $(this).find(".rowdeleter").removeClass("hidden");
  $(this).find(".rowkey").addClass("hidden");
}, function(){
  $(this).find(".rowdeleter").addClass("hidden");
  $(this).find(".rowkey").removeClass("hidden");
});

function openAJAXLoader(){
  swal({
    title: 'OK!',
    text: 'Modal ini akan tertutup secara otomatis...',
    showCancelButton: false,
    allowOutsideClick: true,
    onOpen: () => {
      swal.showLoading();
    }
  })
}

function deleteDM(dmid, rmid){
  swal({
    title: 'Apakah anda yakin?',
    text: "RM ini akan menjadi tidak bisa melihat DM ini sampai ditambahkan kembali",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak',
  }).then((result) => {
    if (result.value){
      openAJAXLoader();
      $.ajax({
        url: '<?=site_url("cms/accounts/deleteDMFromRM")?>',
        type: "POST",
        data: {
          rmid: <?=$account->userid?>,
          dmid: dmid
        },
        success: function(data){
          if (data == "success"){
            window.location = "<?=site_url("cms/accounts/admin/".$account->userid)?>";
          } else {
            swal({
              title: 'Ups!',
              text: data,
              showCancelButton: false,
              confirmButtonText: "Ok",
              type: "error"
            })
          }
        }
      })
    }
  })
}
function deleteRep(repid, dmid){
  swal({
    title: 'Apakah anda yakin?',
    text: "DM ini dan RM diatasnya akan menjadi tidak bisa melihat data & Call Plan Rep ini sampai ditambahkan kembali",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak',
  }).then((result) => {
    if (result.value){
      openAJAXLoader();
      $.ajax({
        url: '<?=site_url("cms/accounts/deleteRepFromDM")?>',
        type: "POST",
        data: {
          dmid: <?=$account->userid?>,
          repid: repid
        },
        success: function(data){
          if (data == "success"){
            window.location = "<?=site_url("cms/accounts/admin/".$account->userid)?>";
          } else {
            swal({
              title: 'Ups!',
              text: data,
              showCancelButton: false,
              confirmButtonText: "Ok",
              type: "error"
            })
          }
        }
      })
    }
  })
}
</script>
