<section id="main-content">
   <section class="wrapper site-min-height"> 
      <div class="fullblock">
         <div class='tableheader'>
            <i class="glyphicon glyphicon-user right-sm"></i> Admin List
         </div>
         <div class='tablecontent'>
            <table>
              <tr>
                <th>#</th>	
                <th>Name</th>	
                <th>Position</th> 
                <th>Contact</th>	
                <th>Email</th>
                <th colspan=2><span class="searchbox"><input type="text" class="form-control" placeholder="Search..." id="srcbox" value="<?=$q?>"><a href='#' class='floatingsearch'><i class='glyphicon glyphicon-search'></i></a></span></th>
             </tr>
             <?php
             foreach($accounts as $key=>$account){
              $btnobs = "";
                
               $btnedit = "<a href='".site_url("cms/accounts/admin/$account->userid")."' class='form-control btn btn-primary'>Details</a>";
               $position = "";
               if ($account->tier == 999){
                $position = "Admin";
               } else if ($account->tier == 3){
                $position = "Content Assignment";
               } else if ($account->tier == 2){
                $position = "Content Uploader";
               } else if ($account->tier == 1){
                $position = "Head Office";
               }
               if ($this->session->userdata("tier") > 3){
                $btnobs = "<a href='#' class='form-control btn btn-danger deactivator' data-id='".$account->userid."'>Deactivate</a>";
                if ($account->accountstatus == 0){
                  $btnobs = "<a href='#' class='form-control btn btn-warning reactivator' data-id='".$account->userid."'>Reactivate</a>";
                }
               }
               echo "
               <tr>
                  <td>".(($key+1)+(($page-1)*$limit))."</td>
                  <td>$account->fullname</td>
                  <td>$position</td>
                  <td><a href='tel:$account->contact'>$account->contact</a></td>
                  <td><a href='mailto:$account->email'><i class='glyphicon glyphicon-envelope'> $account->email</i></a></td>
                  <td>$btnedit</td>
                  <td>$btnobs</td>
               </tr>";
            }
            ?>
            <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td colspan=2><?php if ($this->session->userdata("tier") > 3){?><a class='btn form-control' href='<?=site_url("cms/accounts/admin/new")?>'>Add New Admin</a><?php } ?></td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row main-row">
      <?php
      $i = 1;
      $pageOffset = 4;
      $maxiterations = $i + 8;

      if ($page > ($pageOffset+1)){
         $i = $page - $pageOffset;
         $maxiterations = $maxiterations+$i-1;
         echo "<span class='paging'><a href='$sitelink?p=1$addllink'>First</a></span><span class='paging'>...</span>";
      }
      for (; $i <= $maxiterations && $i <= $totalpage; $i++){

         if ($i == $page){
            echo "<span class='paging cp'>$i</span>";
         } else {
            echo "<span class='paging'><a href='$sitelink?p=$i$addllink'>$i</a></span>";
         }  
      }
      if ($page < $totalpage - 4 && $totalpage > 9){
         echo "<span class='paging'>...</span><span class='paging'><a href='$sitelink?p=$totalpage$addllink'>Last</a></span>";
      }
      ?>
   </div>
</section>
</section>	
<script>
   $(".floatingsearch").on("click", function(){
      var q = $("#srcbox").val();
      window.location = "<?=$sitelink?>?q="+q;
   })

   $("#srcbox").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $(".floatingsearch").click();
        }
    });

   $(".reactivator").on("click", function(){
      var id = $(this).data("id");
      $.ajax({
         url: '<?=site_url("cms/accounts/reactivateAccount")?>',
         type:"POST",
         data: {id:id},
         success: function(d){
            if (d >= 1){
               location.reload();
            } else if (d == 0){
               swal("Synergy", "Operation Failed. Please try again or contact an administrator", "error");
            } else {
               swal("Synergy", d, "warning");
            }
         }
      })
   }) 

   $(".deactivator").on("click", function(){
      var id = $(this).data("id");
      swal({
       title: "Are you sure?",
       text: "",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Yes",
       cancelButtonText: "No",
       closeOnConfirm: false,
       closeOnCancel: false
    }).then(function(isConfirm){
      // console.log(isConfirm);
      if (isConfirm.value){
         $.ajax({
            url: '<?=site_url("cms/accounts/deactivateAccount")?>',
            type:"POST",
            data: {id:id},
            success: function(d){
               if (d >= 1){
                  location.reload();
               } else if (d == 0){
                  swal("Synergy", "Operation Failed. Please try again or contact an administrator", "error");
               } else {
                  swal("Synergy", d, "warning");
               }
            }
         })      

      } else {

      }
   })
   
   }) 

</script>