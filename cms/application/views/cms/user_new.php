<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
  	<section class="wrapper site-min-height"> 
      <div class='fullblock'>
            <form method="POST" action="" enctype="multipart/form-data">
            <fieldset>
              <div class='tableheader'>
                <i class="glyphicon glyphicon-user right-sm"></i>Add New Admin
                <div class='tableheaderback'><a href="<?=site_url("cms/accounts/admin")?>"><i class='glyphicon glyphicon-arrow-left'></i></a></div>
              </div>
              <div class='centerfields'>
         <?php echo validation_errors(); ?>
              <label class="has-float-label">
                <input type="text" placeholder="Name" name="fullname" value="<?php echo set_value('fullname'); ?>"/>
                <span>Full Name</span>
              </label>

              <label class="has-float-label">
                <input type="text" placeholder="Username" name="username" value="<?php echo set_value('username'); ?>"/>
                <span>Username</span>
              </label>

              <label class="has-float-label">
                <input type="password" placeholder="••••••••" name="password" value="<?php echo set_value('password'); ?>"/>
                <span>Password</span>
              </label>

              <label class="has-float-label">
                <input type="number" placeholder="Phone Number" name="phone"  class='numinput' value="<?php echo set_value('phone'); ?>"/>
                <span>Contact Number</span>
              </label>

              <label class="has-float-label">
                <input type="email" placeholder="E-mail" name="email" value="<?php echo set_value('email'); ?>"/>
                <span>E-mail</span>
              </label>

              <label class="has-float-label">
                <select name='tier'>
                  <option value='1'>Head Office</option>
                  <option value='2'>Content Uploader</option>
                  <option value='3'>Content Assignment</option>
                </select>
                <span>Position</span>
              </label>

              <button class='form-control btn btn-primary'>Sign up</button>
              </div>
            </fieldset>
            </form>
         </div>
  		</div>
	</section>
</section>	