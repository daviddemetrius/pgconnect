<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
   <section class="wrapper site-min-height"> 
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
            		<fieldset>
            			<form enctype="multipart/form-data" method='post' action='<?=site_url("cms/utilities/commitUploadITR")?>'>
            				<div class='centerfields'>
          					<div><?=isset($msg)?$msg."<br/>":""?></div>
            					<label class="has-float-label">
					                <input type="file" name="userfile" accept="text/x-csv,text/csv"/>
					                <span>Upload ITR File</span>
					            </label>
          					<div>Pastikan file yang akan diupload sudah dalam bentuk .csv</div><br/>
					          <button class='form-control btn btn-primary'>Upload & Process</button>
            				</div>
				</form>
			</fieldset>
		</div>
	</section>
   </section>
</section>