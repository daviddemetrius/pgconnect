<link href="<?php echo base_url(); ?>css/floatlabel.css" rel="stylesheet">
<section id="main-content">
   <section class="wrapper site-min-height"> 
  	<section class="wrapper site-min-height"> 
		<div class='fullblock'>
  		<fieldset>
  			<form enctype="multipart/form-data" method='post' action='<?=site_url("cms/accounts/commitSendMsg")?>'>
  				<div class='centerfields' style='width:60%'>
            <label class="has-float-label">
                <input type="text" name="title" placeholder="Title" max=64/>
                <span>Title</span>
            </label>
            <br/>
  					<label>Message</label>
            <textarea rows=5 class='form-control' name='msg'></textarea>
            <br/><br/>
					<div>Pesan ini akan dipush & dikirim ke semua rep yang terdaftar</div><br/>
          <button class='form-control btn btn-primary'>Send</button>
          </div>
				</form>
			</fieldset>
		</div>
    <div class='fullblock'>
            <div class='tableheader'>
              Message History
            </div>
            <div class='tablecontent assignmenttable' id='tablecontent'>
              <table>
                 <tr>
                    <th width=10%>#</th>
                    <th>Title</th>
                    <th>Message</th>
                    <th>Date Sent</th>
                 </tr>
                 <?php foreach ($messages as $key=>$msg){
                  $num = $key+1;
                  echo "
                  <tr>
                    <td>$num</td>
                    <td>$msg->title</td>
                    <td>$msg->message</td>
                    <td>".date("j F Y H:i:s", strtotime($msg->datesent))."</td>
                  </tr>
                  ";
                 } ?>
              </table>
            </div>
          </div>
	</section>
   </section>
</section>