<script src="<?php echo base_url(); ?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/amcharts/serial.js" type="text/javascript"></script>	
<script src="<?php echo base_url(); ?>js/amcharts/xy.js" type="text/javascript"></script>	
<?php
	$newHcpArr = array();
	$totalHcpArr = array(
		"specialty" => "Total",
		"territories" => array()
	);
	foreach ($HcpCoverage as $hcpcov){
		$found = -1;
		foreach ($newHcpArr as $key=>$a){
			if ($a['specialty'] == $hcpcov->masterid){
				$found = $key;
				break;
			}
		}
		if ($hcpcov->specialty == 10){
			// echo "wut 10";
		}
		if ($found >= 0){
			$newHcpArr[$found]["territories"][$hcpcov->specialty] = $hcpcov->value;
		} else {
			$hcpObj = array();
			$hcpObj["specialty"] = $hcpcov->masterid;
			$hcpObj["territories"] = array(
					$hcpcov->specialty => $hcpcov->value
			);
			$newHcpArr[] = $hcpObj;
		}
		if (isset($totalHcpArr["territories"][$hcpcov->specialty])){
			$totalHcpArr["territories"][$hcpcov->specialty] += $hcpcov->value;
		} else {
			$totalHcpArr["territories"][$hcpcov->specialty] = $hcpcov->value;
		}
	}
	$newHcpArr[] = $totalHcpArr;
	$newAccArr = array();
	$totalAccArr = array(
		"specialty" => "Total",
		"territories" => array()
	);
	foreach ($AccCoverage as $hcpcov){
		$found = -1;
		foreach ($newAccArr as $key=>$a){
			if ($a['specialty'] == $hcpcov->masterid){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$newAccArr[$found]["territories"][$hcpcov->segmentid] = $hcpcov->value;
		} else {
			$hcpObj = array();
			$hcpObj["specialty"] = $hcpcov->masterid;
			$hcpObj["territories"] = array(
					$hcpcov->segmentid => $hcpcov->value
			);
			$newAccArr[] = $hcpObj;
		}
		if (isset($totalAccArr["territories"][$hcpcov->segmentid])){
			$totalAccArr["territories"][$hcpcov->segmentid] += $hcpcov->value;
		} else {
			$totalAccArr["territories"][$hcpcov->segmentid] = $hcpcov->value;
		}
	}
	$newAccArr[] = $totalAccArr;
	$newPEArr = array();
	$achievementArr = array();
	foreach ($PlanEffectiveness as $pe){
		$found = -1;
		if ($this->input->get("m") == "rep"){
			$pe->username = $pe->xmlnacd." - ".$pe->repname;
		} else {
			foreach ($newPEArr as $key=>$peo){
				if ($peo['dm'] == $pe->username){
					$found = $key;
					break;
				}
			}
		}		
		if ($found >= 0){
			$newPEArr[$found]['approvedcount'] += $pe->effectiveness->approvedcount?$pe->effectiveness->approvedcount:0;
			$newPEArr[$found]['deletedcount'] += $pe->effectiveness->deletedcount?$pe->effectiveness->deletedcount:0;
			$newPEArr[$found]['adhoccount'] += $pe->effectiveness->adhoccount?$pe->effectiveness->adhoccount:0;
			$newPEArr[$found]['rescheduled'] += $pe->effectiveness->rescheduled?$pe->effectiveness->rescheduled:0;
			$newPEArr[$found]['missed'] += $pe->effectiveness->missed?$pe->effectiveness->missed:0;
			$newPEArr[$found]['approvedvisitcount'] += $pe->effectiveness->approvedvisitcount?$pe->effectiveness->approvedvisitcount:0;
			$newPEArr[$found]['unapproved'] += $pe->effectiveness->unapproved?$pe->effectiveness->unapproved:0;
			$newPEArr[$found]['totalvisitcount'] += $pe->effectiveness->totalvisitcount?$pe->effectiveness->totalvisitcount:0;
			$newPEArr[$found]['unapprovedvisitcount'] += $pe->effectiveness->unapprovedvisitcount?$pe->effectiveness->unapprovedvisitcount:0;
			$achievementArr[$found]['achv'] = number_format($newPEArr[$found]['approvedvisitcount'] / ($newPEArr[$found]['approvedcount']==0?1:$newPEArr[$found]['approvedcount']) * 100, 1);
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['approvedcount'] = $pe->effectiveness->approvedcount?$pe->effectiveness->approvedcount:0;
			$peobj['deletedcount'] = $pe->effectiveness->deletedcount?$pe->effectiveness->deletedcount:0;
			$peobj['adhoccount'] = $pe->effectiveness->adhoccount?$pe->effectiveness->adhoccount:0;
			$peobj['rescheduled'] = $pe->effectiveness->rescheduled?$pe->effectiveness->rescheduled:0;
			$peobj['missed'] = $pe->effectiveness->missed?$pe->effectiveness->missed:0;
			$peobj['approvedvisitcount'] = $pe->effectiveness->approvedvisitcount?$pe->effectiveness->approvedvisitcount:0;
			$peobj['unapproved'] = $pe->effectiveness->unapproved?$pe->effectiveness->unapproved:0;
			$peobj['totalvisitcount'] = $pe->effectiveness->totalvisitcount?$pe->effectiveness->totalvisitcount:0;
			$peobj['unapprovedvisitcount'] = $pe->effectiveness->unapprovedvisitcount?$pe->effectiveness->unapprovedvisitcount:0;
			$newPEArr[] = $peobj;
			$achv = array();
			$achv['dm'] = $pe->username;
			$achv['achv'] = number_format($peobj['approvedvisitcount'] / ($peobj['approvedcount']==0?1:$peobj['approvedcount']) * 100, 1);
			$achievementArr[] = $achv;
		}
	}
	$newAccReachArr = array();
	foreach ($AccReach as $pe){
		$found = -1;
		foreach ($newAccReachArr as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		// $pe->username = $pe->repname;
		if ($found >= 0){
			$newAccReachArr[$found]['covered'] += $pe->reach['covered']?$pe->reach['covered']:0;
			$newAccReachArr[$found]['planned'] += $pe->reach['planned']?$pe->reach['planned']:0;
			$newAccReachArr[$found]['visited'] += $pe->reach['visited']?$pe->reach['visited']:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['covered'] = $pe->reach['covered']?$pe->reach['covered']:0;
			$peobj['planned'] = $pe->reach['planned']?$pe->reach['planned']:0;
			$peobj['visited'] = $pe->reach['visited']?$pe->reach['visited']:0;
			$newAccReachArr[] = $peobj;
		}
	}
	$newHCPPDArr = array();
	foreach ($PDHcpReach as $pe){
		$found = -1;
		foreach ($newHCPPDArr as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$newHCPPDArr[$found]['covered'] += $pe->pdhcpreach['covered']?$pe->pdhcpreach['covered']:0;
			$newHCPPDArr[$found]['planned'] += $pe->pdhcpreach['planned']?$pe->pdhcpreach['planned']:0;
			$newHCPPDArr[$found]['visited'] += $pe->pdhcpreach['visited']?$pe->pdhcpreach['visited']:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['covered'] = $pe->pdhcpreach['covered']?$pe->pdhcpreach['covered']:0;
			$peobj['planned'] = $pe->pdhcpreach['planned']?$pe->pdhcpreach['planned']:0;
			$peobj['visited'] = $pe->pdhcpreach['visited']?$pe->pdhcpreach['visited']:0;
			$newHCPPDArr[] = $peobj;
		}
	}
	$newHCPMWArr = array();
	foreach ($MWHcpReach as $pe){
		$found = -1;
		foreach ($newHCPMWArr as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$newHCPMWArr[$found]['covered'] += $pe->mwhcpreach['covered']?$pe->mwhcpreach['covered']:0;
			$newHCPMWArr[$found]['planned'] += $pe->mwhcpreach['planned']?$pe->mwhcpreach['planned']:0;
			$newHCPMWArr[$found]['visited'] += $pe->mwhcpreach['visited']?$pe->mwhcpreach['visited']:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['covered'] = $pe->mwhcpreach['covered']?$pe->mwhcpreach['covered']:0;
			$peobj['planned'] = $pe->mwhcpreach['planned']?$pe->mwhcpreach['planned']:0;
			$peobj['visited'] = $pe->mwhcpreach['visited']?$pe->mwhcpreach['visited']:0;
			$newHCPMWArr[] = $peobj;
		}
	}
	$newHCPOTArr = array();
	foreach ($OTHcpReach as $pe){
		$found = -1;
		foreach ($newHCPOTArr as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$newHCPOTArr[$found]['covered'] += $pe->othcpreach['covered']?$pe->othcpreach['covered']:0;
			$newHCPOTArr[$found]['planned'] += $pe->othcpreach['planned']?$pe->othcpreach['planned']:0;
			$newHCPOTArr[$found]['visited'] += $pe->othcpreach['visited']?$pe->othcpreach['visited']:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['covered'] = $pe->othcpreach['covered']?$pe->othcpreach['covered']:0;
			$peobj['planned'] = $pe->othcpreach['planned']?$pe->othcpreach['planned']:0;
			$peobj['visited'] = $pe->othcpreach['visited']?$pe->othcpreach['visited']:0;
			$newHCPOTArr[] = $peobj;
		}
	}
	
	$new1on1Arr = array();
	foreach ($dm1on1Data as $pe){
		$found = -1;
		foreach ($new1on1Arr as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$new1on1Arr[$found]['schedules'] += $pe->data1on1->schedules?$pe->data1on1->schedules:0;
			$new1on1Arr[$found]['specialists'] += $pe->data1on1->specialists?$pe->data1on1->specialists:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['schedules'] = $pe->data1on1->schedules?$pe->data1on1->schedules:0;
			$peobj['specialists'] = $pe->data1on1->specialists?$pe->data1on1->specialists:0;
			$new1on1Arr[] = $peobj;
		}
	}
	$newGroupArr = array();
	foreach ($dmGroupData as $pe){
		$found = -1;
		foreach ($newGroupArr as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$newGroupArr[$found]['schedules'] += $pe->datagroup->schedules?$pe->datagroup->schedules:0;
			$newGroupArr[$found]['specialists'] += $pe->datagroup->specialists?$pe->datagroup->specialists:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['schedules'] = $pe->datagroup->schedules?$pe->datagroup->schedules:0;
			$peobj['specialists'] = $pe->datagroup->specialists?$pe->datagroup->specialists:0;
			$newGroupArr[] = $peobj;
		}
	}

	$newDaysArray = array();
	foreach ($dmHalfDays as $pe){
		$found = -1;
		foreach ($newDaysArray as $key=>$peo){
			if ($peo['dm'] == $pe->username){
				$found = $key;
				break;
			}
		}
		if ($found >= 0){
			$newDaysArray[$found]['fulldays'] += $pe->fulld?$pe->fulld:0;
			$newDaysArray[$found]['halfdays'] += $pe->halfd?$pe->halfd:0;
		} else {
			$peobj = array();
			$peobj['dm'] = $pe->username;
			$peobj['fulldays'] = $pe->fulld?$pe->fulld:0;
			$peobj['halfdays'] = $pe->halfd?$pe->halfd:0;
			$newDaysArray[] = $peobj;
		}
	}

	// var_dump($newPEArr);
?>
<style>
	tr:nth-child(odd){
		background-color:#fafafa;
	}
	td{
		text-align:center;		
	}
	th{
		text-align:center;
	}
</style>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height"> <!-- site-min-height -->
		<div class='centerfields'>
            		<form method='get'>
				<label>Month / Year</label>
				<div class='row'>
					<div class='col-xs-3'><input type='number' min=1 max=12 class='form-control' id='month' value="<?=$this->input->get("month")?$this->input->get("month"):date("n")?>" onchange='' name='month'></div>
					<div class='col-xs-3'><input type='number' min=2018 class='form-control' id='year' value="<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>" onchange='' name='year'></div>
					<div class='col-xs-4'><button class='btn btn-primary form-control'>Get Data</button></div>
					<?=$this->input->get("m")=="rep"?"<input type='hidden' name='m' value='rep'>":""?>
				</div>
				<div class='row' style='margin-top:20px'>
					<div class='col-xs-4'><a class='btn btn-primary form-control' href='<?=site_url("cms/dashboard")?>?year=<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>&month=<?=$this->input->get("month")?$this->input->get("month"):date("n")?>&m=rep'>Show Per Rep</a></div>
					<div class='col-xs-4'><a class='btn btn-primary form-control' href='<?=site_url("cms/dashboard")?>?year=<?=$this->input->get("year")?$this->input->get("year"):date("Y")?>&month=<?=$this->input->get("month")?$this->input->get("month"):date("n")?>'>Show Per DM</a></div>
				</div>
				<br/>
			</form>
		</div>
		<div class="row" id='dboard'>
	   		<div class='col-xs-12 col-md-12 col-lg-12'>
	   			<div class='chartdiv'  id='hcpcoverage' style='width:100%; max-height:500px; overflow-x:auto; overflow-y:auto'>
	   				<label style='display:block; text-align:center; font-size:14pt'>HCP Coverage Per Territory <a href='#' onclick='exportHCPCov()' style='display:inline!important'>[Download]</a></label>
	   				<table>
	   					<tr>
	   						<th>Territory</th>
	   						<?php
	   						$csvhcpheaders = array("Territory");
	   						foreach ($specialties as $sp){
	   							echo "<th>$sp->specialtyname</th>";
	   							$csvhcpheaders[] = $sp->specialtyname;
	   						}
	   						$csvhcpheaders[] = "Total";
	   						$csvhcpdata = array($csvhcpheaders);
	   						?>
	   						<th>Total</th>
	   					</tr>
	   				</table>
	   				<div style='max-height:230px; overflow-y:auto; max-width:100%'>
		   				<table>
		   					<tr style='visibility:collapse'>
		   						<th>Territory</th>
		   						<?php
		   						foreach ($specialties as $sp){
		   							echo "<th>$sp->specialtyname</th>";
		   						}
		   						?>
	   							<th>Total</th>
		   					</tr>
		   					<?php
		   					foreach ($newHcpArr as $entry){
		   						$total = 0;
		   						$csvhcprow = array($entry['specialty']);
		   						echo "
		   						<tr>
		   						  	<td>".$entry['specialty']."</td>";
		   						foreach ($specialties as $sp){
		   							if (isset($entry['territories'][$sp->specialtyid])){
		   								$total += $entry['territories'][$sp->specialtyid];
		   								echo "<td>".$entry['territories'][$sp->specialtyid];
		   								echo "</td>";
		   								$csvhcprow[] = $entry['territories'][$sp->specialtyid];
		   							} else {
		   								echo "<td></td>";
		   								$csvhcprow[] = 0;
		   							}
		   						}

		   						echo "
		   							<td>$total</td>
		   						</tr>
		   						";
		   						$csvhcprow[] = $total;
		   						$csvhcpdata[] = $csvhcprow;
		   					}
		   					echo "<tr><td>UNIQUE TOTAL</td>";
		   					$uniquetotal = 0;
		   					$csvhcprow = array("Unique Total");
		   					foreach ($HcpUCoverage as $cov){
		   						echo "<td>$cov->value</td>";		
		   						$uniquetotal += $cov->value;
		   						$csvhcprow[] = $cov->value;
		   					}
		   					$csvhcprow[] = $uniquetotal;
		   					echo "<td>$uniquetotal</td>";
		   					$csvhcpdata[] = $csvhcprow;
		   					echo "</tr>";
		   					?>
		   				</table>
	   				</div>
	   			</div>
	   		</div>
	   		<div class='col-xs-12 col-md-12 col-lg-12'>
	   			<div class='chartdiv'  id='hcpcoverage' style='width:100%; max-height:500px; overflow-x:auto; overflow-y:auto'>
	   				<label style='display:block; text-align:center; font-size:14pt'>ACC Coverage Per Territory <a href='#' onclick='exportAccCov()' style='display:inline!important'>[Download]</a></label>
	   				<table>
	   					<tr>
	   						<th>Territory</th>
	   						<?php
	   						$csvaccheader = array("Territory");
	   						foreach ($accsegments as $sp){
	   							echo "<th>$sp->segmentation</th>";
	   							$csvaccheader[] = $sp->segmentation;
	   						}
	   						$csvaccheader[] = "Total";
	   						$csvaccdata = array($csvaccheader);
	   						?>
	   						<th>Total</th>
	   					</tr>
	   				</table>
	   				<div style='max-height:230px; overflow-y:auto; overflow-x:visible; display:inline-block'>
		   				<table>
		   					<tr style='visibility:collapse'>
		   						<th>Territory</th>
		   						<?php
		   						foreach ($accsegments as $sp){
		   							echo "<th>$sp->segmentation</th>";
		   						}
		   						?>
	   							<th>Total</th>
		   					</tr>
		   					<?php
		   					foreach ($newAccArr as $entry){
		   						$total = 0;
		   						$csvaccrow = array($entry['specialty']);
		   						echo "
		   						<tr>
		   						  	<td>".$entry['specialty']."</td>";
		   						foreach ($accsegments as $sp){
		   							if (isset($entry['territories'][$sp->segmentid])){
		   								$total += $entry['territories'][$sp->segmentid];
		   								echo "<td>".$entry['territories'][$sp->segmentid]."</td>";
		   								$csvaccrow[] = $entry['territories'][$sp->segmentid];
		   							} else {
		   								echo "<td></td>";
		   								$csvaccrow[] = 0;
		   							}
		   						}
		   						$csvaccrow[] = $total;
		   						$csvaccdata[] = $csvaccrow;
		   						echo "
		   							<td>$total</td>
		   						</tr>
		   						";
		   					}
		   					echo "<tr><td>UNIQUE TOTAL</td>";
		   					$unqiuetotal = 0;
		   					$csvaccrow = array("Unique Total");
		   					foreach ($AccUCoverage as $cov){
		   						echo "<td>$cov->value</td>";
		   						$unqiuetotal += $cov->value;
		   						$csvaccrow[] = $cov->value;
		   					}
		   					$csvaccrow[] = $unqiuetotal;
		   					$csvaccdata[] = $csvaccrow;
		   					echo "<td>$unqiuetotal</td>";
		   					echo "</tr>";
		   					?>
		   				</table>
	   				</div>
	   			</div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-12'>
	   			<label style='display:block; text-align:center'>Plan Effectiveness</label>
	   			<div class='chartdiv'  id='effectivenessbar' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>ACC Reach</label>
	   			<div class='chartdiv'  id='accreachbar' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>PD HCP Reach</label>
	   			<div class='chartdiv'  id='pdhcpreachbar' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>MW HCP Reach</label>
	   			<div class='chartdiv'  id='mwhcpreachbar' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>Other HCP Reach</label>
	   			<div class='chartdiv'  id='othcpreachbar' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>Visited 1 on 1 Calls</label>
	   			<div class='chartdiv'  id='dm1on1' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>Visited Group Calls</label>
	   			<div class='chartdiv'  id='dmgroup' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>Detailing Days</label>
	   			<div class='chartdiv'  id='dmdays' style='width:100%; height:340px'></div>
	   		</div>

	   		<div class='col-xs-12 col-md-12 col-lg-6'>
	   			<label style='display:block; text-align:center'>Call Achieved</label>
	   			<div class='chartdiv'  id='dmachievement' style='width:100%; height:340px'></div>
	   		</div>
		</div>
	</br>
	<div class="row main-row">
	</div>
	<div class="row main-row">
	</div>
	<!-- page end-->
</section>
</section>

<script>
var chartData = <?=json_encode($newPEArr)?>;
var chart = AmCharts.makeChart("effectivenessbar", {
    "theme": "light",
    "type": "serial",
    "dataProvider": chartData,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "Plans",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Approved: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "approvedcount"
    }, {
        "balloonText": "Deleted: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.9,
        "valueField": "deletedcount"
    }, {
        "balloonText": "Ad-Hoc: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.8,
        "valueField": "adhoccount"
    }, {
        "balloonText": "Rescheduled: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.7,
        "valueField": "rescheduled"
    }, {
        "balloonText": "Missed: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.6,
        "valueField": "missed"
    }, {
        "balloonText": "Unapproved: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.5,
        "valueField": "unapproved"
    }, {
        "balloonText": "Approved Visit: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.4,
        "valueField": "approvedvisitcount"
    }, {
        "balloonText": "Visit Total: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.3,
        "valueField": "totalvisitcount"
    }, {
        "balloonText": "Uunapproved Visit: <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.2,
        "valueField": "unapprovedvisitcount"
    }],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
chart.addListener("rendered", zoomChart);
zoomChart();
function zoomChart() {
    chart.zoomToIndexes(chartData.length - 12, chartData.length - 1);
}

var accChartData = <?=json_encode($newAccReachArr);?>;
var accchart = AmCharts.makeChart("accreachbar", {
    "theme": "light",
    "type": "serial",
    "dataProvider": accChartData,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "ACC",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Current Coverage: <b>[[value]]</b> ACC",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "covered"
    }, {
        "balloonText": "Planned: <b>[[value]]</b> ACC",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.7,
        "valueField": "planned"
    }, {
        "balloonText": "Visited: <b>[[value]]</b> ACC",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.4,
        "valueField": "visited"
    }, ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
accchart.addListener("rendered", zoomACCChart);
zoomACCChart();
function zoomACCChart() {
    accchart.zoomToIndexes(accChartData.length - 6, accChartData.length - 1);
}


var pdHcpChartData = <?=json_encode($newHCPPDArr);?>;
var pdhcpchart = AmCharts.makeChart("pdhcpreachbar", {
    "theme": "light",
    "type": "serial",
    "dataProvider": pdHcpChartData,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "PD HCP",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Current Coverage: <b>[[value]]</b> PD HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "covered"
    }, {
        "balloonText": "Planned: <b>[[value]]</b> PD HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.7,
        "valueField": "planned"
    }, {
        "balloonText": "Visited: <b>[[value]]</b> PD HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.4,
        "valueField": "visited"
    }, ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
pdhcpchart.addListener("rendered", zoomPDHCPChart);
zoomPDHCPChart();
function zoomPDHCPChart() {
    pdhcpchart.zoomToIndexes(pdHcpChartData.length - 6, pdHcpChartData.length - 1);
}


var mwHcpChartData = <?=json_encode($newHCPMWArr);?>;
var mwhcpchart = AmCharts.makeChart("mwhcpreachbar", {
    "theme": "light",
    "type": "serial",
    "dataProvider": mwHcpChartData,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "MW HCP",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Current Coverage: <b>[[value]]</b> MW HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "covered"
    }, {
        "balloonText": "Planned: <b>[[value]]</b> MW HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.7,
        "valueField": "planned"
    }, {
        "balloonText": "Visited: <b>[[value]]</b> MW HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.4,
        "valueField": "visited"
    }, ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
mwhcpchart.addListener("rendered", zoomMWHCPChart);
zoomMWHCPChart();
function zoomMWHCPChart() {
    mwhcpchart.zoomToIndexes(mwHcpChartData.length - 6, mwHcpChartData.length - 1);
}

var otHcpChartData = <?=json_encode($newHCPOTArr);?>;
var othcpchart = AmCharts.makeChart("othcpreachbar", {
    "theme": "light",
    "type": "serial",
    "dataProvider": otHcpChartData,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "Other HCP",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Current Coverage: <b>[[value]]</b> Other HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "covered"
    }, {
        "balloonText": "Planned: <b>[[value]]</b> Other HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.7,
        "valueField": "planned"
    }, {
        "balloonText": "Visited: <b>[[value]]</b> Other HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2006",
        "type": "column",
        "clustered":false,
        "columnWidth":0.4,
        "valueField": "visited"
    }, ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
othcpchart.addListener("rendered", zoomOTChart);
zoomOTChart();
function zoomOTChart() {
    othcpchart.zoomToIndexes(otHcpChartData.length - 6, otHcpChartData.length - 1);
}


var dm1on1data = <?=json_encode($new1on1Arr);?>;
var dm1on1chart = AmCharts.makeChart("dm1on1", {
    "theme": "light",
    "type": "serial",
    "dataProvider": dm1on1data,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "Calls & HCP",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "# of Calls: <b>[[value]]</b> Calls",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "schedules"
    }, {
        "balloonText": "HCP Actuals: <b>[[value]]</b> HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.6,
        "valueField": "specialists"
    } ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
dm1on1chart.addListener("rendered", zoom1on1Chart);
zoom1on1Chart();
function zoom1on1Chart() {
    dm1on1chart.zoomToIndexes(dm1on1data.length - 6, dm1on1data.length - 1);
}

var dmgroupdata = <?=json_encode($newGroupArr);?>;
var dmgroupchart = AmCharts.makeChart("dmgroup", {
    "theme": "light",
    "type": "serial",
    "dataProvider": dmgroupdata,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "Calls & HCP",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "# of Calls: <b>[[value]]</b> Calls",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "schedules"
    }, {
        "balloonText": "HCP Actuals: <b>[[value]]</b> HCP",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.6,
        "valueField": "specialists"
    } ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
dmgroupchart.addListener("rendered", zoomGroupChart);
zoomGroupChart();
function zoomGroupChart() {
    dmgroupchart.zoomToIndexes(dmgroupdata.length - 6, dmgroupdata.length - 1);
}

var dmdaydata = <?=json_encode($newDaysArray);?>;
var dmdaychart = AmCharts.makeChart("dmdays", {
    "theme": "light",
    "type": "serial",
    "dataProvider": dmdaydata,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "Days",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "Full Days: <b>[[value]]</b> days",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "fulldays"
    }, {
        "balloonText": "Half Days: <b>[[value]]</b> half days",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2005",
        "type": "column",
        "clustered":false,
        "columnWidth":0.6,
        "valueField": "halfdays"
    } ],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
dmdaychart.addListener("rendered", zoomDayChart);
zoomDayChart();
function zoomDayChart() {
    dmdaychart.zoomToIndexes(dmdaydata.length - 6, dmdaydata.length - 1);
}

var dmachievement = <?=json_encode($achievementArr);?>;
var dmachvchart = AmCharts.makeChart("dmachievement", {
    "theme": "light",
    "type": "serial",
    "dataProvider": dmachievement,
    "valueAxes": [{
        "unit": "%",
        "position": "left",
        "title": "Call Achievement",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]%</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "2004",
        "type": "column",
        "valueField": "achv"
    }],
    "mouseWheelZoomEnabled": true,
    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "g1",
        "scrollbarHeight": 40
    },
    "chartCursor": {
       "limitToGraph":"g1"
    },
    "plotAreaFillAlphas": 0.1,
    "categoryField": "dm",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});
dmachvchart.addListener("rendered", zoomAchvChart);
zoomAchvChart();
function zoomAchvChart() {
    dmachvchart.zoomToIndexes(dmachievement.length - 6, dmachievement.length - 1);
}
function exportAccCov(){
      var csv = <?=json_encode($csvaccdata)?>;
      let csvContent = "data:text/csv;charset=utf-8,";
      csv.forEach(function(rowArray){
         let row = rowArray.join(",");
         csvContent += row + "\r\n";
      }); 
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "ACCCoverage.csv");
      document.body.appendChild(link); // Required for FF

      link.click(); 
}
function exportHCPCov(){
      var csv = <?=json_encode($csvhcpdata)?>;
      let csvContent = "data:text/csv;charset=utf-8,";
      csv.forEach(function(rowArray){
         let row = rowArray.join(",");
         csvContent += row + "\r\n";
      }); 
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "HCPCoverage.csv");
      document.body.appendChild(link); // Required for FF

      link.click(); 
}
</script>
<!--main content end-->