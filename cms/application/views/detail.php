<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="language" content="en-us">
  <meta name="description" content="P&G Connect" />
  <meta name="keyword" content="P&G Connect" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <link rel="stylesheet" href="<?=base_url()?>web/css/fancybox/jquery.fancybox.min.css"/>
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/js/owl/owl.carousel.css"/> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap/bootstrap.min.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/slickslider/slick-theme.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/slickslider/slick.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/wow/animate.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/select2/select2.min.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/fontawesome/css/all.min.css"/>
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/css/lightcase/lightcase.css" /> -->
  <!-- <link rel="stylesheet" media="screen and (min-width: 999px)" href="<?=base_url()?>web/js/animate/animations.css"> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/fonts.css">
  <link rel="stylesheet" href="<?=base_url()?>web/css/style.css">
</head>

<body>
  <main class="main-wrap" id="detail">
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">          
                <a href="<?=base_url()?>"><img src="<?=base_url()?>web/img/logo_pg.png"></a>              
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->
    <?php 
    $daysleft = 0;
    $timediff = strtotime($detail->datestart) - strtotime(date("Y-m-d"));
    if ($timediff < 0){
      $daysleft = 0;
    } else {  
      $daysleft = floor($timediff / 86400);
    }

    $ended = strtotime($detail->dateend) - strtotime(date("Y-m-d"));

    ?>

    <!-- body start -->
    <section class="detail">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="detail-wrapper">
                <div class="row">
                  <div class="col-12">
                    <div class="text text-center">
                      <div class="title">
                        <?=$detail->eventtitle?>
                      </div>
                      <div class="subtitle">
                        <?=$detail->eventsubtitle?>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="eventstatus-wrapper">
                      <div class="person">
                        <div class="value"><?=$detail->goingcount?></div>
                        <div class="label">Going</div>
                      </div>
                      <?php if ($daysleft >= 0) { ?>
                      <div class="countdown">
                        <div class="value"><?=$daysleft?></div>
                        <div class="label">Days Left</div>
                      </div>
                      <?php } ?>
                      <?php if ($detail->eventprice == "Free"){ ?>
                      <div class="type">
                        <div class="value">Free</div>
                        <div class="label">Event</div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">
                    <div class="image with-link">
                      <img src="<?=base_url()?>assets/eventb/<?=$detail->eventid?>.png">
                      <div class="link-join">
                        <?php if ($ended > 0) { ?>
                        <a href="<?=site_url("eventform/$detail->eventid")?>">go</a>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">
                    <div class="eventinfo-wrapper">
                      <div class="eventinfo-item">
                        <div class="label">Kategori</div>
                        <div class="value"><?=$detail->eventcategory?></div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Tempat Event</div>
                        <div class="value"><?=$detail->eventvenue?> <br> <a href="https://www.google.com/maps/search/?api=1&query=<?=$detail->venuelat?>,<?=$detail->venuelng?>" target="_blank" class="googlemap_link">View Map</a></div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Tanggal Event</div>
                        <?php
                        if (date("Y-m", strtotime($detail->datestart)) == date("Y-m", strtotime($detail->dateend))){
                          $datetext = date("d", strtotime($detail->datestart))." - ".date("d F Y", strtotime($detail->dateend));
                        } else {
                          $datetext = date("d F Y", strtotime($detail->datestart))." - ".date("d F Y", strtotime($detail->dateend));
                        }
                        ?>
                        <div class="value"><?=$datetext?></div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Fasilitas</div>
                        <div class="value"><?=$detail->facilities?></div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Participants</div>
                        <div class="value"><?=$detail->participants?></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="text">
                      <div class="desc">
                        <?=$detail->description?>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="action-wrapper">
                      <div class="row">
                        <div class="col-12">
                          <div class="pdf-wrapper">
                            <a href="<?=base_url()."assets/eventdoc/".$detail->docfile?>" target="_blank">Baca info selengkapnya & detail program event (download PDF)</a>
                          </div>
                        </div>
                        <div class="col-lg-6 col-12 align-self-center">
                          <div class="share-wrapper">
                            <span>Share with friends</span>
                            <!-- Go to www.addthis.com/dashboard to customize your tools --> 
                            <div class="addthis_inline_share_toolbox"></div>
                          </div>
                        </div>
                        <div class="col-lg-6 col-12">
                          <div class="link">
                            <?php if ($ended > 0) { ?>
                            <a href="<?=site_url("eventform/$detail->eventid")?>" class="btn  btn-join">Joining Event</a>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  
  <!-- script start -->
  <script src="<?=base_url()?>web/js/jquery-1.12.0.min.js"></script>
  <script src="<?=base_url()?>web/js/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>web/js/slickslider/slick.min.js"></script>
  <script src="<?=base_url()?>web/js/animate/css3-animate-it.js"></script>
  <!-- <script src="<?=base_url()?>web/js/owl/jquery.owl-filter.js"></script>
  <script src="<?=base_url()?>web/js/owl/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/greensock.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/layerslider.transitions.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/layerslider.kreaturamedia.jquery.js"></script> -->
  <script src="<?=base_url()?>web/js/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?=base_url()?>web/js/wow/wow.min.js"></script>
  <script src="<?=base_url()?>web/js/select2/select2.min.js"></script>
  <!-- <script src="<?=base_url()?>web/js/lightcase/lightcase.js"></script> -->
  <script src="<?=base_url()?>web/js/main.js"></script>
  <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589c478dc47db98d"></script> -->
  <!-- script end -->
</body>
</html>
