<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Failed | Pharmacycare</title>
<!-- META -->
<meta name="language" content="en-us">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="<?=base_url()?>web/img/favicon.ico">
<!-- style start -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>web/js/carousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?=base_url()?>web/js/carousel/owl.theme.default.min.css"> 
<link rel="stylesheet" href="<?=base_url()?>web/js/dom/jquery.fancybox.css" />
<link rel="stylesheet" href="<?=base_url()?>web/css/main.css">
<!-- style end -->
</head>

<body>
<!-- body start -->
<div id="container">
	<div id="body">
		<div class="failed">			
			<div class="failed-content">
				<div class="container">
					<div class="fail-1 op-0"><img src="<?=base_url()?>web/img/fail-1.png" alt="fail"></div>
					<div class="fail-2 abs op-0"><img src="<?=base_url()?>web/img/fail-2.png" alt="fail"></div>
					<div class="fail-3 abs op-0"><img src="<?=base_url()?>web/img/fail-3.png" alt="fail"></div>
					<div class="fail-4 abs op-0"><img src="<?=base_url()?>web/img/fail-4.png" alt="fail"></div>
				</div>
				<div class="container">
					<div class="status failed">Mohon Maaf, Kupon untuk Anda tidak tersedia/habis.</div>
					<div class="syarat">Silahkan coba kembali besok. Nantikan juga promo Pharmacy Care berikutnya!</div>
				</div>
			</div>
			<div class="space-4"></div>
			<div class="logo"><a href="#"><img src="<?=base_url()?>web/img/logo.png" alt="logo"></a></div>			
		</div>
	</div>
	<div id="footer"></div>
</div>
<!-- body end -->

<!-- script start -->
<script src="<?=base_url()?>web/js/jquery-3.3.1.min.js"></script>
<script src="<?=base_url()?>web/js/anime.min.js"></script>
<script src="<?=base_url()?>web/js/carousel/owl.carousel.min.js"></script>
<script src="<?=base_url()?>web/js/dom/jquery.fancybox.js"></script>
<script src="<?=base_url()?>web/js/main.js"></script>
<!-- script end -->
</body>
</html>
