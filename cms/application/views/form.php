<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="language" content="en-us">
  <meta name="description" content="P&G Connect" />
  <meta name="keyword" content="P&G Connect" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <link rel="stylesheet" href="<?=base_url()?>web/css/fancybox/jquery.fancybox.min.css"/>
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/js/owl/owl.carousel.css"/> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap/bootstrap.min.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/slickslider/slick-theme.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/slickslider/slick.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/wow/animate.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/select2/select2.min.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/fontawesome/css/all.min.css"/>
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/css/lightcase/lightcase.css" /> -->
  <!-- <link rel="stylesheet" media="screen and (min-width: 999px)" href="<?=base_url()?>web/js/animate/animations.css"> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/fonts.css">
  <link rel="stylesheet" href="<?=base_url()?>web/css/style.css">
  
  <style>
    .autocomplete-suggestions{
      background:white;
      cursor:pointer;
      font-size:13px;
    }
    .autocomplete-suggestion:hover{
      background:aquamarine;
    }
    .autocomplete-suggestion{
      padding:5px 10px;
    }
    .qrval{
      font-size:1.4rem!important;
    }
    .link.homebtn{
      text-align:center;
    }
    @media (min-width:1200px){
      .link.homebtn{
        max-width:307px;
      }
    }
  </style>

</head>

<body>
  <main class="main-wrap" id="joined">
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">          
                <a href="<?=base_url()?>"><img src="<?=base_url()?>web/img/logo_pg.png"></a>
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->

    <!-- body start -->
    <section class="">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="form-event">
                <div class="eventselected-wrapper">
                  <div class="image">
                    <img src="<?=base_url()?>assets/eventb/<?=$id?>.png">
                  </div>
                  <div class="text">
                    <div class="title">
                    <span>Joining Event :</span>
                    <?=$detail->eventtitle?>
                    </div>
                  </div>
                </div>
                <div class="eventform-wrapper steps-wrapper">
                  <div class="step-item main-question">
                    <div class="form-group">
                      <div class="row align-items-center">
                        <div class="col-lg-7 col-12">
                          <label>Apakah ada Medical Representative dari P&G yang berkunjung rutin ke Dokter?</label>
                        </div>
                        <div class="col-lg-5 col-12">
                          <input type="button" value="ya" class="btn btn-grey">
                          <input type="button" value="tidak" class="btn btn-grey">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="joined-event" id='eventjoin'>
                <div class="eventselected-wrapper" style="margin-top: 5rem;">
                  <div class="image">
                    <img src="<?=base_url()?>assets/eventb/<?=$id?>.png">
                  </div>
                  <div class="text">
                    <div class="title">
                    <span>Joining Event :</span>
                    <?=$detail->eventtitle?>
                    </div>
                  </div>
                </div>
                <div class="joined-wrapper steps-wrapper done">
                  <div class="step-item">
                    <div class="heading-wrapper">
                      <div class="heading">
                        Terima Kasih
                      </div>
                      <div class="subheading">
                        atas kehadiran dokter di event ini.
                      </div>
                    </div>
                    <div class="text">
                      <div class="desc">
                        <p>Berikut ini adalah Kode booking dan QRcode registrasi event. Untuk kebutuhan check-in event atau entrance pass, mohon tunjukkan Kode booking ini.</p>
                      </div>
                      <div class="qrcode">
                        <img src="<?=base_url()?>web/img/code_booking_clean.jpg">
                        <div class="overlay-top">
                          <div class="code">
                            <img src="<?=base_url()?>web/img/qr_code.png" class="qrimg">
                          </div>
                        </div>
                        <div class="overlay-bottom">
                          <div class="text">
                            <div class="label">Booking Number</div>
                            <div class="value qrval">0987890</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class='row'>
                      <div class="col-lg-6 col-12 align-self-center">
                        <div class="link homebtn">
                          <a href="<?=base_url()?>" class="btn  btn-join">Back to Home</a>
                        </div>
                      </div>
                      <div class="col-lg-6 col-12">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="question-container">
      <div class="step-item question-1 answer-1-1">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-12">
              <label class="">Nama - Spesialis - Institusi</label>
            </div>
            <div class="col-12" style="position:relative">
              <input type='text' class="custom-select dokter_list" id="dokter_list">
              <div class='emptybtn' style="position: absolute;top: 9px;right: 45px; cursor:pointer" onclick="removeSelection(this)">X</div>
              </input>
            </div>
          </div>
        </div>
      </div>
      <div class="step-item question-1 answer-1-2">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-lg-7 col-12">
              <label>Apakah data tersebut sudah sesuai dengan data diri Dokter?</label>
            </div>
            <div class="col-lg-5 col-12">
              <input type="button" value="ya" class="btn btn-grey">
              <!-- <input type="button" value="tidak" class="btn btn-grey"> -->
            </div>
          </div>
        </div>
      </div>
      <div class="step-item question-2 answer-2-1">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Nama</label>
                <input type="text" class="form-control" name='name' id="name" placeholder="Nama">
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Spesialis</label>
                <select class="custom-select spesialis" id="specialty" name="specialty">
                  <option value="Pediatric">Pediatric</option>
                  <option value="Nurse">Nurse</option>
                  <option value="Internist">Internist</option>
                  <option value="Orthopaedic">Orthopaedic</option>
                  <option value="ENT">ENT</option>
                  <option value="GP">GP</option>
                  <option value="Neurologist">Neurologist</option>
                  <option value="Obgyn">Obgyn</option>
                  <option value="Rheumatologist">Rheumatologist</option>
                  <option value="Dentist">Dentist</option>
                  <option value="Apothecary">Apothecary</option>
                </select>
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Tipe Institusi</label>
                <select class="custom-select" id="instype" name="instype">
                  <option value="Rumah Sakit">Rumah Sakit</option>
                  <option value="Klinik">Klinik</option>
                  <option value="Praktek Pribadi">Praktek Pribadi</option>
                </select>
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Nama Institusi</label>
                <input type="text" class="form-control" placeholder="" name='institution' id='institution'>
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Kota</label>
                <select class="custom-select" id="city" name="city">
                  <option value="Bandung">Bandung</option>
                  <option value="Cirebon">Cirebon</option>
                  <option value="Semarang">Semarang</option>
                  <option value="Solo">Solo</option>
                  <option value="Yogyakarta">Yogyakarta</option>
                  <option value="Denpasar">Denpasar</option>
                  <option value="Kupang">Kupang</option>
                  <option value="Malang">Malang</option>
                  <option value="Surabaya">Surabaya</option>
                  <option value="Batam">Batam</option>
                  <option value="Medan">Medan</option>
                  <option value="Pekanbaru">Pekanbaru</option>
                  <option value="Bandar Lampung">Bandar Lampung</option>
                  <option value="Jambi">Jambi</option>
                  <option value="Padang">Padang</option>
                  <option value="Palembang">Palembang</option>
                  <option value="Banjarmasin">Banjarmasin</option>
                  <option value="Pontianak">Pontianak</option>
                  <option value="Samarinda">Samarinda</option>
                  <option value="Jakarta 1">Jakarta 1</option>
                  <option value="Bogor">Bogor</option>
                  <option value="Tangerang">Tangerang</option>
                  <option value="Jayapura">Jayapura</option>
                  <option value="Makassar">Makassar</option>
                  <option value="Manado">Manado</option>
                  <option value="Palu">Palu</option>
                </select>
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Nomor HP</label>
                <input type="text" class="form-control" name='phone' id ="phone">
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <div class="row">
                  <div class="col-lg-7 col-12">
                    <label>Apakah dokter ingin dihubungi oleh Med Rep kami untuk mengetahui produk P&G lebih lanjut?</label>
                  </div>
                  <div class="col-lg-5 col-12">
                    <input type="button" value="ya" class="btn btn-grey btn-contactus" onclick="$(this).parent().find('input').removeClass('active');$(this).addClass('active');">
                    <input type="button" value="tidak" class="btn btn-grey btn-contactus" onclick="$(this).parent().find('input').removeClass('active');$(this).addClass('active');">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="register-link">
                <input type="submit" class="btn btn-join" value="Submit">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="step-item question-2 answer-2-2">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-lg-7 col-12">
              <label>Apakah data tersebut sudah sesuai dengan data diri Dokter?</label>
            </div>
            <div class="col-lg-5 col-12">
              <input type="button" value="ya" class="btn btn-grey">
              <!-- <input type="button" value="tidak" class="btn btn-grey"> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
  <script src="<?=base_url()?>web/js/jquery-1.12.0.min.js"></script>
  <script src="<?=base_url()?>js/jquery.autocomplete.js"></script>
  <script src="<?=base_url()?>web/js/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>web/js/slickslider/slick.min.js"></script>
  <script src="<?=base_url()?>web/js/animate/css3-animate-it.js"></script>
  <!-- <script src="<?=base_url()?>web/js/owl/jquery.owl-filter.js"></script>
  <script src="<?=base_url()?>web/js/owl/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/greensock.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/layerslider.transitions.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/layerslider.kreaturamedia.jquery.js"></script> -->
  <script src="<?=base_url()?>web/js/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?=base_url()?>web/js/wow/wow.min.js"></script>
  <script src="<?=base_url()?>web/js/select2/select2.min.js"></script>
  <!-- <script src="<?=base_url()?>web/js/lightcase/lightcase.js"></script> -->
  <script src="<?=base_url()?>web/js/main.js"></script>
  <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589c478dc47db98d"></script> -->
  <!-- script end -->
  <script>
    function removeSelection(e){
      chosenDoc = 0;
      picked = 0
      $(".question-1.answer-1-2:first").remove();
      $(e).prev().val("");

    }

    var chosenDoc = 0;
    function setAutocompl(clsname){
      $(clsname).autocomplete({
        serviceUrl: '<?=base_url()?>home/finddocs',
        onSelect: function(suggestion){
          chosenDoc = suggestion.data;
          if (picked == 0){
            $($("#question-container .question-1.answer-1-2")[0]).clone().insertAfter(".answer-1-1");;
            picked = 1;
          }
          
          $('.answer-1-2 input').click(function(){
              if($(this).val() == 'ya') {
                  gPickedMethod = 0;
                  // window.location = 'event-join.php';  
                  $('.form-event input').attr('disabled',true);
                  $('#dokter_list').prop("disabled", true);
                  $(".emptybtn:first").remove();
                  sendData();
                  setTimeout(function() {
                      $('#joined .joined-event').fadeIn();
                      setTimeout(function(){
                        $('<a id="tempbtn" />').attr('href', '#eventjoin').text('LINK').appendTo('body').get(0).click();
                        $('#tempbtn').remove();
                      }, 250)
                  }, 500); 
              }
              if($(this).val() == 'tidak') {
                  $(this).addClass('active');
                  $('html, body').animate({
                      scrollTop: $('.answer-1-1').offset().top
                  }, 500);
                  // $('.answer-1-2').remove();
              }
          })
        }
      });
    }

    function sendData(){
      if (gPickedMethod == 0){
        $.ajax({
          url: '<?=site_url("home/sendCpl")?>',
          type: "POST",
          data: {
            spid: chosenDoc,
            eventid: <?=$id?>
          },
          success: function(data){
            $(".qrimg").attr("src","<?=base_url()?>img/qr/"+data+".png");
            $(".qrval").html(data.split("-")[0]);
          }
        })
      } else {
        var name = $("#name").val();
        var specialty = $("#specialty").val();
        var institution = $("#institution").val();
        var instype = $("#instype").val();
        var city = $("#city").val();
        var phone = $("#phone").val();
        var contactus = $(".btn-contactus.active").val();

        $.ajax({
          url: '<?=site_url("home/sendNew")?>',
          type: "POST",
          data: {
            name: name,
            specialty: specialty,
            institution: institution,
            phone: phone,
            contactus: contactus,
            eventid: <?=$id?>,
            city: city,
            instype: instype
          },
          success: function(data){
            $(".qrimg").attr("src","<?=base_url()?>img/qr/"+data+".png");
            $(".qrval").html(data.split("-")[0]);
          }
        })
      }
    }
  </script>
</body>
</html>
