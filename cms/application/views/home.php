<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Home | Pharmacycare</title>
<!-- META -->
<meta name="language" content="en-us">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="<?=base_url()?>web/img/favicon.ico">
<!-- style start -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>web/js/carousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?=base_url()?>web/js/carousel/owl.theme.default.min.css"> 
<link rel="stylesheet" href="<?=base_url()?>web/js/dom/jquery.fancybox.css" />
<link rel="stylesheet" href="<?=base_url()?>web/css/main.css">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!-- style end -->
</head>

<body>
<!-- body start -->
<div id="container">
	<div id="body">
		<img src="<?=base_url()."/assets/promos/$details->promoid$details->promonumber.png?".rand()?>" style="width: 100%;">
		<div class="login">
			<div class="container">
				<form method='post' action='<?=site_url("register")?>'>
				<div class="loginfield">
					<div class="icon-username"></div>
					<input type="text" id='fldname' class="loginbox" name="name" placeholder="Nama">
					<div class="asterix">*</div>
				</div>
				<div class="loginfield">
					<div class="icon-phone"></div>
					<input type="text" id='fldphone' class="loginbox" name="phone" placeholder="No. Telp">
					<div class="asterix">*</div>
				</div>
				<div class="loginfield">
					<div class="icon-email"></div>
					<input type="email" id='fldemail' class="loginbox" name="email" placeholder="Email">
				</div>
				<div class="gender">
					<div class="is-inline s-14 s-bold _bl">Gender</div>
					<div class="is-inline">
						<div class="is-inline">
							<input type="radio" id="radio01" name="radio" value="m"/>
							<label for="radio01"><span></span>Male</label>
						</div>
						<div class="is-inline">
							<input type="radio" id="radio02" name="radio" value="f" />
							<label for="radio02"><span></span>Female</label>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>				
				<div class="logincheck">
					<input type="checkbox" id="check01" name="checkbox" />
					<label for="check01"><span></span>Saya setuju dengan <a data-fancybox data-src="#syarat-ketentuan" href="javascript:;">Syarat & Ketentuan</a> serta <a data-fancybox data-src="#kebijakan-privasi" href="javascript:;">Kebijakan Privasi</a> Pharmacy Care Life Apps.</label>
				</div>
				<input type='hidden' value='<?=$qr?>' name="qr">
				<div class="g-recaptcha" data-sitekey="6LdugZoUAAAAAFeOb2Bbcr8SU3ZFBxKVGZjGEiNf"></div>
				</form>
				<div class="recaptcha hidden"><img src="<?=base_url()?>web/img/recaptcha.jpg" alt="recaptcha"></div>
				<a href="javascript:;" class="btn-solid" onclick='checkform()'>Daftar</a>
			</div>
		</div>
		<div class="logo"><a href="<?=base_url()."qr/".$qr?>"><img src="<?=base_url()?>web/img/logo.png" alt="logo"></a></div>
	</div>
	<div id="footer"></div>
</div>

<!-- The overlay -->
<div id="syarat-ketentuan" class="overlay">
	<!-- Overlay content -->
	<div class="overlay-content">
		<div class="s-18 s-bold">Syarat & Ketentuan</div>
		<div class="s-14">
			<div class="s-16 s-bold _bl">Perjanjian Antara Pengguna dan Procter & Gamble</div>
			<div>Jaringan situs-situs web (secara kolektif, “Situs Web P&G”) yang dioperasikan oleh perusahaan Procter & Gamble atau afiliasinya maupun anak perusahaannya (“P&G” atau “Procter & Gamble”), terdiri dari berbagai situs dan halaman web. Situs web P&G yang diberikan kepada Anda berdasarkan pada persetujuan Anda tanpa adanya modifikasi dari persyaratan, kondisi, dan pemberitahuan yang telah terkandung di dalamnya. Penggunaan situs web P&G merupakan bentuk persetujuan Anda akan semua hal seperti syarat, kondisi, dan pemberitahuan di dalamnya. Penggunaan situs web P&G tertentu, termasuk yang berada di dalam jaringan situs web P&G, juga menjadi subjek yang dapat dikenakan adanya penambahan persyaratan seperti yang diuraikan di situs web (“Persyaratan Tambahan”). Selain itu, situs web P&G sendiri mungkin mengandung adanya ketentuan tambahan yang mengatur fitur atau tertentu (misalnya, undian atau area chatting). Ketika salah satu dari syarat, kondisi, dan pemberitahuan yang terdapat di sini berlawanan dengan Persyaratan Tambahan atau dengan istilah dan pedoman lain yang terkandung dalam setiap situs web P&G tertentu, maka persyaratan inilah yang akan memegang kontrol. </div>
			<div class="space-4"></div>
			<div class="s-bold">HARAP MEMBACA SYARAT PENGGUNAAN DAN KETENTUAN BERIKUT DENGAN SEKSAMA SEBELUM MENGGUNAKAN SITUS.</div>
			<div>Dengan mengakses atau menggunakan situs ini, Anda menyetujui ketentuan penggunaan, kondisi, dan semua hukum yang berlaku. Jika Anda tidak menyetujui ketentuan ini, maka Anda tidak dapat menggunakan situs ini.</div>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Modifikasi Syarat Penggunaan</div>			
			<div>P&G memiliki hak untuk mengganti ketentuan penggunaan, kondisi, dan pemberitahuan yang dimiliki situs web P&G, termasuk namun tidak terbatas pada biaya yang diasosiasikan dengan penggunaan situs web P&G. Anda bertanggungjawab untuk meninjau ulang secara reguler ketentuan dan kondisi penggunaan ini. </div>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Kegunaan Situs web. Batasan penggunaan Personal dan Non-Komersil</div>
			<p>Kecuali apabila ditentukan sebelumnya, situs web P&G dipakai untuk penggunaan pribadi dan non-komersial Anda. Anda tidak dapat memodifikasi, menyalin, mendistribusikan, mentransmisikan, mempertunjukkan, menampilkan, mereproduksi, menerbitkan, menyalahgunakan kebebasan, menciptakan karya turunan, mentransfer, atau menjual informasi, perangkat lunak, produk atau jasa yang diperoleh dari situs web P&G.</p>
			<p>Anda dapat menampilkan dan mengikuti batasan tegas yang dinyatakan berkaitan dengan materi tertentu, secara elektronik menyalin, mengunduh, dan mencetak salinan materi dari berbagai wilayah di situs ini semata-mata untuk tujuan non-komersial Anda sendiri, atau untuk melakukan pemesanan kepada P&G, atau untuk membeli produk P&G. Penggunaan lainnya dari materi dalam situs ini, termasuk namun tidak terbatas pada kegiatan modifikasi, reproduksi, distribusi, publikasi ulang, mempertunjukkan, atau mentransmisi isi situs ini, tanpa izin tertulis dari P&G adalah hal yang sangat dilarang.</p>
			<p>Pelecehan dalam cara dan bentuk apapun pada situs ini atau salah satu situs web P&G dilarang keras, termasuk melalui e-mail dan chat, atau dengan penggunaan bahasa yang tidak layak atau kasar. Dilarang meniru seseorang, termasuk karyawan, pengawas, representatif P&G, atau anggota lain dan pengunjung di situs. Anda tidak boleh mengunduh, mendistribusikan, atau mempublikasikan konten lewat situs yang sifatnya memfitnah, menghina, tidak senonoh, mengancam, menyerang hak privasi atau publisitas, kasar, ilegal, atau tidak pantas, atau yang mungkin merupakan atau mendorong terjadinya bentuk tindak pidana, sebagai bentuk pelanggaran hukum. Anda tidak dapat mengunduh konten komersial di situs atau menggunakan situs untuk meminta orang lain agar bergabung atau menjadi anggota dari layanan online komersial atau organisasi lainnya.</p>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Hak Cipta dan Merek Dagang</div>
			<div>Keseluruhan konten yang termasuk dalam situs ini, termasuk namun tidak terbatas pada teks, desain, grafis, interface, atau kode dan pemilihan dan pengaturannya adalah hak cipta karya kolektif di bawah hukum Amerika Serikat dan undang-undang hak cipta lainnya, dan merupakan milik P&G. Hasil kerja kolektif ini mencakup karya yang berlisensi untuk P&G. Hak Cipta tahun 2000, 2001. HAK CIPTA DILINDUNGI. Semua merek dagang, merek layanan, dan nama dagang (secara kolektif disebut "Merek") adalah merek dagang yang terdaftar di bawah nama Procter & Gamble, atau masing-masing pemilik lain yang telah diberikan hak dan lisensi oleh P&G untuk penggunaannya.</div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Pemberitahuan dan Tata Cara Pembuatan Klaim dari Pelanggaran Hak Cipta</div>
			<div>Berdasarkan Pasal 17, Peraturan di Amerika Serikat, Bagian 512 (c)(2), pemberitahuan atas pelanggaran hak cipta harus dikirim ke pihak yang ditunjuk oleh situs.</div>
			<div class="space-2"></div>
			<p>SEMUA PERTANYAAN YANG TIDAK RELEVAN ATAU TIDAK SESUAI DENGAN PROSEDUR BERIKUT TIDAK AKAN MENERIMA JAWABAN.</p>
			<p>P&G menghargai hak kekayaan intelektual orang lain, dan kami minta para pengguna dan pengunjung kami untuk melakukan hal yang sama. P&G akan memproses dan menginvestigasi dugaan pelanggaran dan akan menindak berdasarkan Digital Millennium Copyright Act (“DMCA”) dan undang-undang kekayaan intelektual lain yang berlaku. Setelah menerima pemberitahuan sesuai dengan aturan DMCA, P&G akan bertindak untuk menghapus atau menonaktifkan akses ke materi yang ditemukan terlanggar atau menjadi subyek dari aktivitas pelanggaran, dan akan bertindak untuk menghapus atau menonaktifkan akses ke referensi atau tautan ke materi atau kegiatan yang ditemukan telah dilanggar.</p>
			<p>Jika Anda merasa bahwa hasil pekerjaan Anda telah disalin dengan cara yang melanggar hak cipta, silakan berikan P&G informasi berikut. Harap diperhatikan agar bertindak efektif, sehingga pemberitahuan Anda harus menyertakan SEMUA hal berikut ini: tanda tangan fisik atau elektronik dari orang yang berwenang untuk bertindak atas nama pemilik dari sebuah hak cipta eksklusif yang diduga dilanggar;</p>
			<p>deskripsi dari karya dengan hak cipta yang Anda klaim telah dilanggar; deskripsi mengenai letak materi yang dirasa dilanggar dalam situs;</p>
			<p>alamat, nomor telepon, dan alamat email beserta semua informasi lainnya yang membuat pihak P&G dapat menghubungi Anda; pernyataan yang tersusun bahwa Anda memiliki keyakinan yang baik bahwa penggunaan dipersengketakan tersebut memang tidak memiliki izin dari pemilik hak cipta, agen, atau hukum; pernyataan yang Anda susun, dibuat di bawah sumpah, bahwa informasi dalam Pemberitahuan Anda adalah akurat dan bahwa Anda adalah pemilik hak cipta atau diberi kuasa untuk bertindak atas nama pemilik hak eksklusif yang diduga dilanggar.</p>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Pemberitahuan mengenai pelanggaran hak cipta ini ditujukan kepada:</div>
			<div>Melalui Surat:</div>
			<div>Perusahaan Procter & Gamble</div>
			<div>Divisi Legal/IP</div>
			<div>Agen Hak Cipta</div>
			<div>299 East Sixth Street</div>
			<div>Cincinnati, OH 45202</div>
			<div class="space-2"></div>
			<div>Melalui Fax:</div>
			<div>(513) 983-6840</div>
			<div class="space-2"></div>
			<div>Melalui Email:</div>
			<div><a href="<?=base_url()?>web/mailto:copyright.im@pg.com">copyright.im@pg.com</a> (Silahkan terakan judul “Pemberitahuan Pelanggaran”)</div>
			<div class="space-2"></div>
			<div class="s-bold">CATATAN PENTING: INFORMASI SEBELUMNYA DISEDIAKAN SECARA EKSKLUSIF UNTUK MEMBERITAHU PROCTER & GAMBLE BAHWA MATERI HAK CIPTA ANDA TELAH DILANGGAR. SEMUA PERTANYAAN LAIN, SEPERTI PERTANYAAN DAN PERMINTAAN YANG TERKAIT DENGAN PRODUK ATAU JASA, ATAU PERTANYAAN TENTANG PRIVASI, TIDAK AKAN DITANGGAPI LEWAT PROSES INI.</div>
			<div class="s-16 s-bold _bl">Kesalahan Pengetikan</div>
			<div>Ketika produk atau jasa P&G terdaftar dengan harga yang kurang tepat dikarenakan oleh kesalahan pengetikan atau kesalahan dalam informasi harga berdasarkan kekeliruan daftar yang diterima dari pemasok kami, P&G berhak untuk menolak atau membatalkan pesanan untuk produk atau jasa yang mencantumkan harga yang salah tersebut. P&G berhak untuk menolak atau membatalkan pesanan tersebut, termasuk ketika pesanan telah dikonfirmasi atau kartu kredit Anda telah terkena biaya pemesanannya. Jika kartu kredit Anda telah dikenakan biaya untuk pembelian yang dibatalkan, P&G akan menerbitkan kredit ke dalam akun kartu Anda sejumlah harga yang telah dibayarkan tersebut. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Pemberhentian</div>
			<div>Syarat dan ketentuan ini berlaku kepada Anda di saat Anda mengakses situs dan / atau menyelesaikan proses registrasi atau belanja. Syarat dan ketentuan ini, atau salah satu dari mereka, dapat dimodifikasi atau dihentikan oleh P&G tanpa pemberitahuan sewaktu-waktu dengan alasan apapun. Ketentuan yang berkaitan dengan Hak Cipta dan Merek Dagang, Penolakan, Klaim, Batasan Kewajiban, Ganti Rugi, Hukum yang Berlaku, Arbitrase dan Umum, termasuk pada hal-hal yang dapat diberhentikan sewaktu-waktu. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Partisipasi Pengguna</div>
			<div>P&G tidak akan dan tidak dapat memeriksa semua informasi dan materi yang dipasang atau dibuat oleh pengguna yang mengakses situs dan tidak bertanggung jawab atas isi dari informasi dan materi ini. Anda mengakui bahwa dengan memberikan Anda kemampuan untuk melihat dan mendistribusikan konten yang diciptakan oleh pengguna situs ini, P&G hanya bertindak sebagai saluran pasif atas distribusi materi tersebut dan tidak berkewajiban atau bertanggungjawab akan setiap isi atau kegiatan di situs ini. Namun, P&G berhak untuk memblokir atau menghapus informasi atau materi yang ditentukan sebagai hal yang (a) kasar, memfitnah, atau tidak senonoh, (b) bentuk penipuan yang menyesatkan, (c) melanggar hak cipta, merek dagang atau merupakan hak kekayaan intelektual dari orang lain (d) melanggar hukum atau peraturan atau (e) menyinggung atau tidak dapat diterima oleh kebijakannya P&G. Perhatikan bahwa informasi pribadi Anda yang dipasang atau dikirimkan akan diperlakukan oleh pihak Procter & Gamble sesuai dengan Pernyataan Privasi Procter & Gamble. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Pengajuan oleh Pengguna</div>
			<div>Kecuali untuk informasi pribadi yang dapat kami kumpulkan dari Anda di bawah pedoman yang ditetapkan dalam Pernyataan Privasi kami, materi, informasi, atau bentuk komunikasi lain yang Anda kirim atau unduh ke situs ini (“Komunikasi”) akan dianggap sebagai materi non-rahasia dan bukan hak milik. P&G tidak memiliki kewajiban sehubungan dengan Komunikasi. P&G dan perancangnya akan bebas untuk menyalin, mengungkapkan, mendistribusikan, menggabungkan semua bentuk Komunikasi berupa data, gambar, suara, teks, dan hal-hal lain yang terkandung di dalamnya untuk tujuan komersial atau non-komersial. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Penggunaan Chat Rooms</div>
			<div>P&G mungkin, namun tidak berkewajiban untuk, memantau atau meninjau setiap bagian dari situs di mana pengguna mengirimkan atau melakukan komunikasi dengan satu sama lain, termasuk namun tidak terbatas pada chat room, papan buletin, atau forum pengguna lainnya, dan isi dari komunikasi tersebut. Bagaimanapun P&G tidak memiliki kewajiban yang berhubungan dengan isi dari komunikasi, baik yang timbul dan diatur dalam hukum hak cipta, merek dagang, fitnah, privasi, tidak senonoh, atau lainnya. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Tautan Pihak Ketiga</div>
			<div>Dalam upaya untuk memberikan peningkatan nilai kepada pengunjung kami, situs ini mungkin berisi tautan ke situs lain di internet yang dimiliki dan dioperasikan oleh vendor pihak ketiga atau pihak lain (“Situs Eksternal”). Namun, meskipun pihak ketiga berafiliasi dengan P&G, P&G tidak memiliki kontrol atas situs tersebut, yang kesemuanya memiliki privasi dan pengolahan datanya sendiri, independen dari P&G. P&G tidak memiliki tanggung jawab atau kewajiban untuk kebijakan-kebijakan atau tindakan independen dan juga tidak bertanggung jawab atas praktik privasi terkait konten dari situs web tersebut. Situs-situs tautan ini hanya untuk memudahkan Anda dan karena itu Anda akan dapat mengaksesnya di bawah resiko sendiri. Tautan tidak menyiratkan bahwa mereka merupakan sponsor dari P&G, yang menyokong, berafiliasi atau berkaitan, atau telah resmi secara hukum untuk menggunakan merek dagang, nama dagang, merek layanan, desain, logo, simbol, atau materi berhak cipta lainnya yang ditampilkan dan dapat diakses melalui Situs Eksternal. Meskipun demikian, P&G berusaha untuk melindungi integritas situs dan tautan yang dimiliki dan meminta umpan balik pada tidak hanya situs pribadi, namun juga situs tautan yang terdapat di dalamnya (termasuk jika tautan tertentu tidak bisa diakses). Anda dapat menghubungi administrator situs atau webmaster atas Situs Eksternal tersebut jika Anda memiliki umpan balik mengenai tautan atau isi yang terletak di Situs Eksternal tersebut.</div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Klaim</div>
			<div>Setiap klaim atau pernyataan tentang efektivitas produk P&G produk dan / atau yang melakukan perbandingan atas efektivitas produk P&G dengan produk lainnya secara tegas terbatas untuk area Amerika Serikat, kecuali apabila dinyatakan secara khusus dalam situs. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Batasan</div>
			<p>Procter & Gamble tidak memberikan jaminan atau pernyataan tentang keakuratan atau kelengkapan konten situs ini atau Situs Eksternal lain.</p>

			<p>Procter & Gamble tidak menyaring iklan atau konten lain yang dapat dilihat oleh anak-anak melalui situs kami termasuk situs “hot-link”, dan mereka bisa menerima konten dan materi dari internet dan/atau iklan yang tidak sesuai untuk anak-anak. Kami mendorong agar orang tua dan wali agar menghabiskan waktu online dengan anak-anak mereka dan untuk mempertimbangkan penggunaan perangkat lunak penyaringan elektronik.</p>

			<p>SITUS DAN MATERINYA, INFORMASI, JASA, DAN PRODUK DALAM SITUS INI, TERMASUK, NAMUN TIDAK TERBATAS PADA, TEKS, GAMBAR, DAN TAUTAN, DISEDIAKAN "SEBAGAIMANA ADANYA" DAN TANPA JAMINAN APAPUN, BAIK TERSURAT MAUPUN TERSIRAT. SEJAUH IZIN YANG DIBERIKAN BERDASARKAN HUKUM YANG BERLAKU, P&G MENOLAK SEMUA JAMINAN, TERSURAT MAUPUN TERSIRAT, TERMASUK, NAMUN TIDAK TERBATAS PADA, JAMINAN TERSIRAT YANG DIPERDAGANGKAN DAN DISESUAIKAN UNTUK TUJUAN TERTENTU, NON-PELANGGARAN, KEBEBASAN DARI VIRUS KOMPUTER, DAN JAMINAN YANG TIMBUL BERHUBUNGAN DENGAN CARA PENANGANAN. P&G TIDAK MEREPRESENTASIKAN ATAU MENJAMIN BAHWA FUNGSI YANG TERDAPAT DALAM SITUS INI TIDAK AKAN MENDAPAT GANGGUAN ATAU BEBAS DARI KESALAHAN, BAHWA CACAT AKAN DIPERBAIKI, ATAU BAHWA SITUS INI ATAU SERVER DAPAT MEMBUAT SITUS INI BEBAS DARI VIRUS ATAU KOMPONEN BERBAHAYA LAINNYA. P&G TIDAK MEMBERIKAN JAMINAN ATAU PERNYATAAN TENTANG PENGGUNAAN MATERI DALAM SITUS INI DALAM HAL KELENGKAPAN, KEBENARAN, AKURASI, KECUKUPAN, KEGUNAAN, KETEPATAN WAKTU, KEANDALAN ATAU LAINNYA. BATASAN DI ATAS MUNGKIN TIDAK BERLAKU UNTUK ANDA.</p>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Batasan Kewajiban</div>
			<div>DALAM KONDISI APAPUN P&G TIDAK BERTANGGUNG JAWAB ATAS KERUSAKAN LANGSUNG, TIDAK LANGSUNG, KHUSUS, MENGHUKUM, INSIDENTAL, ATAU KERUSAKAN APAPUN, BAHKAN JIKA P&G TELAH DIBERITAHU SEBELUMNYA TENTANG KEMUNGKINAN KERUSAKAN TERSEBUT, BAIK DALAM TINDAKAN YANG DITENTUKAN DALAM KONTRAK, ATAS KELALAIAN, ATAU BERDASARKAN TEORI LAIN, YANG MUNCUL DARI ATAU BERHUBUNGAN DENGAN PENGGUNAAN, KETIDAKMAMPUAN UNTUK MENGGUNAKAN, ATAU KINERJA DARI INFORMASI, LAYANAN, PRODUK, DAN BAHAN YANG TERSEDIA DI SITUS INI. PEMBATASAN INI BERLAKU MESKIPUN TERJADI KEGAGALAN DARI PERBAIKAN YANG MASIH TERBATAS. KARENA BEBERAPA WILAYAH HUKUM TIDAK MENGIZINKAN PEMBATASAN JANGKA WAKTU YANG TERSIRAT MENGENAI PEMBERLAKUAN JAMINAN, ATAU PENGECUALIAN ATAU PEMBATASAN TANGGUNG JAWAB ATAS KERUSAKAN, BATASAN DI ATAS MUNGKIN TIDAK BERLAKU UNTUK ANDA.</div>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Ganti Rugi</div>
			<div>Anda setuju untuk mengganti kerugian, membela, dan membebaskan P&G, pejabatnya, direkturnya, karyawannya, agennya, pemberi lisensinya dan pemasoknya (secara kolektif disebut "Penyedia") dari dan terhadap semua kerugian, biaya, kerusakan, dan pengeluaran, termasuk biaya pengacara yang wajar, yang berasal dari pelanggaran syarat dan kondisi atau aktivitas yang berhubungan dengan akun Internet Anda (termasuk tindakan kelalaian atau kesalahan), oleh Anda atau orang lain yang mengakses situs menggunakan akun internet Anda.</div>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Hukum yang Berlaku</div>
			<div>Penggunaan Situs ini diatur dalam segala bagian oleh hukum negara bagian Ohio, Amerika Serikat, tanpa memperhatikan pilihan ketentuan hukum, dan bukan berdasarkan Konvensi PBB 1980 mengenai kontrak untuk penjualan barang internasional. Anda setuju bahwa yurisdiksi dalam setiap proses hukum langsung atau tidak langsung yang timbul dari atau berhubungan dengan situs ini (termasuk namun tidak terbatas pada pembelian produk P&G) harus dilakukan di pengadilan negara bagian atau federal yang terletak di Hamilton County, Ohio. Setiap tindakan atau klaim yang Anda miliki sehubungan dengan situs (termasuk namun tidak terbatas pada pembelian produk P&G) harus dimulai dalam satu (1) tahun setelah klaim muncul. Kegagalan P&G untuk menjalankan atau menerapkan kinerja yang ketat dari ketentuan dari syarat tidak akan ditafsirkan sebagai bentuk pelepasan tuntutan atau hak. Tindakan antara pihak atau praktik yang muncul tidak dapat memodifikasi syarat dan ketentuan ini. P&G dapat mengalihkan hak dan kewajiban atas Perjanjian ini kepada pihak manapun setiap saat tanpa pemberitahuan kepada Anda. P&G tidak menyatakan bahwa materi dalam situs ini sesuai atau tersedia untuk digunakan di lokasi lain, dan untuk dapat mengaksesnya dari wilayah lain di mana isinya adalah ilegal dan dilarang. Mereka yang memilih untuk mengakses situs ini dari lokasi di luar Ohio dan melakukannya atas inisiatif mereka sendiri bertanggung jawab untuk mematuhi hukum setempat yang berlaku. Anda tidak dapat menggunakan materi hukum dan peraturan di AS untuk pelanggaran yang terjadi di luar wilayah. Setiap klaim yang berkaitan dengan materi ini diatur oleh hukum internal substantif Negara Bagian Ohio.</div>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Arbitrase</div>
			<p>Dengan menggunakan situs ini, Anda setuju bahwa P&G, atas kebijakannya sendiri, mungkin mengharuskan Anda untuk mengirimkan setiap perselisihan yang timbul dari penggunaan situs ini, atau yang menyangkut Syarat dan Ketentuan, termasuk perselisihan yang timbul dari atau mengenai interpretasi, pelanggaran, pembatalan , cacat, penghentian, serta perselisihan tentang pengisian kesenjangan dalam kontrak ini atau adaptasinya terhadap keadaan yang baru muncul, kepada arbitrase final dan mengikat berdasarkan Peraturan Internasional Arbitrase dari American Arbitration Association, oleh satu atau lebih arbiter yang ditunjuk sesuai dengan Peraturan. Tidak mengikuti aturan ini, bagaimanapun, akan diproses menggunakan hukum negara bagian seperti yang diterakan di bagian sebelumnya.</p>
			<p>Setiap keputusan dalam arbitrase yang diinisiasi berdasrkan pasal ini erbatas pada kerusakan moneter dan tidak meliputi perintah atau arahan untuk ke arah lain selain untuk ketentuan pembayaran uang. Selanjutnya, arbiter tidak memiliki wewenang untuk keputusan yang menghukum, konsekuensial atau kerusakan lainnya tanpa mengukur kerusakan yang sebenarnya disebabkan oleh pihak tersebut sesuai dengan yang diterakan dalam bagian ini, kecuali bila mungkin diperlukan oleh undang-undang.</p>
			<div class="s-16 s-bold _bl">Gambaran Umum</div>
			<div>P&G dapat merevisi Ketentuan ini setiap saat dengan memperbarui pemberitahuan ini. Anda harus mengunjungi halaman ini dari waktu ke waktu untuk meninjau Ketentuan karena sifatnya yang mengikat. Beberapa ketentuan dari Syarat ini dapat digantikan oleh pemberitahuan hukum yang tegas berdasarkan ketentuan yang terletak di halaman tertentu pada situs ini. </div>
			<div class="space-4"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div id="kebijakan-privasi" class="overlay">
	<!-- Overlay content -->
	<div class="overlay-content">
		<div class="s-18 s-bold">Kebijakan Privasi</div>
		<div class="s-14">
			<div>Kami berusaha keras untuk senantiasa membangun dan menjaga kepercayaan Anda. Jadi, kami menangani informasi Anda secara hati-hati dan bijaksana untuk mempertanggungjawabkan kepercayaan Anda. Kebijakan ini memberikan informasi mengenai cara kami menjaga kepercayaan tersebut, termasuk kebijakan mengenai informasi apa yang kami kumpulkan, cara kami menggunakan dan melindunginya, dan bagaimana Anda dapat menentukan apa yang dapat kami lakukan dengan informasi Anda. Kami ingin membantu Anda memahami cara kami menggunakan informasi Anda untuk meningkatkan konten, produk, iklan, dan layanan kami. </div>
			<div class="space-2"></div>
			<div class="s-16 s-bold _bl">Pengenalan singkat untuk praktik-praktik keamanan kami </div>
			<div class="s-bold">yang kami kumpulkan</div>
			<ul>				
				<li>Informasi yang Anda berikan kepada kami</li>
				<li>Informasi yang kami terima ketika Anda menghubungi kami, mengunjungi situs kami, menggunakan aplikasi atau layanan seluler kami, menggunakan produk atau perangkat kami, atau melihat iklan kami</li>
				<li>Informasi yang kami terima dari perusahaan-perusahan lain yang telah mendapatkan persetujuan Anda</li>
				<li>Informasi yang kami terima dari perusahaan-perusahaan lain ketika Anda mengunjungi situs web mereka</li>
			</ul>
			<div>Kami dapat mengkombinasi sebagian atau seluruh informasi tersebut untuk menciptakan produk, layanan, dan pengalaman konsumen yang lebih baik.</div>
			<div class="space-2"></div>
			<div class="s-bold">bagaimana kami menggunakan informasi Anda</div>
			<ul>				
				<li>Mengirimkan produk dan layanan yang Anda minta kepada Anda</li>
				<li>Menginformasikan produk serta layanan kami dan mitra pemasaran kami kepada Anda</li>
				<li>Bantu kami menjalankan situs dan layanan kami</li>
			</ul>
			<div class="space-2"></div>
			<div class="s-bold">bagaimana dan kapan kami membagikan informasi Anda</div>
			<ul>
    			<li>Ketika kami menerima persetujuan dari Anda, dengan penuh kehati-hatian kami menentukan mitra-mitra kami sehingga mereka dapat mengirimkan penawaran, promosi, atau iklan tentang produk dan layanan mereka yang sesuai dengan ketertarikan Anda</li>
    			<li>Dengan perusahaan lain yang kami rekrut untuk membantu menjalankan bisnis kami</li>
    			<li>Sebagai bagian dari penjualan produk P&G atau bisnis untuk perusahaan lain</li>
    			<li>Membantu kami menjaga hak-hak atau properti kami, seperti pencegahan penipuan atau kemananan informasi</li>
    			<li>Ketika dibutuhkan oleh otoritas hukum atau pemerintahan</li>
			</ul>
			<div class="space-2"></div>
			<div class="s-bold">pilihan Anda</div>
			<div>Anda dapat memberi tahu kami bagaimana kami dapat menggunakan informasi Anda dengan mengakses tautan di bawah ini:</div>
			<ul>
				<li><a href="<?=base_url()?>web/https://www.pg.com/privacy/indonesian/privacy_statement.shtml#marketing" target="_blank">Komunikasi pemasaran</a></li>
				<li><a href="<?=base_url()?>web/https://www.pg.com/privacy/indonesian/privacy_statement.shtml#iba" target="_blank">Periklanan berbasis minat</a></li>
			</ul>
			<div class="space-1"></div>
			<div>Jika Anda bertempat tinggal di Uni Eropa, Anda memiliki kepastian akan hak-hak data pribadi, termasuk mengakses informasi pribadi yang kami miliki tentang Anda, melakukan koreksi terhadap data tersebut atau memperbaruinya, meminta kami untuk menghapus data tersebut, atau menerima salinan portabel informasi tersebut. Hak-hak ini tidak berlaku di segala situasi. Silakan akses tautan di bawah ini untuk:</div>
			<ul>			
				<li><a href="<?=base_url()?>web/" target="_blank">Mengakses atau memperbarui informasi pribadi</a></li>
				<li><a href="<?=base_url()?>web/" target="_blank">Mengajukan perubahan, penghapusan, atau perpindahan informasi pribadi Anda</a></li>
			</ul>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Bagaimana Kami Mengumpulkan Data Anda</div>
			<p>Kami mengumpulkan data tentang Anda melalui berbagai cara dari berbagai tempat. Sebagian informasi yang kami kumpulkan merupakan informasi pribadi yang dapat digunakan untuk mengidentifikasi Anda; misalnya nama, alamat email, nomor telepon, atau alamat surat. Di beberapa negara, seperti di Uni Eropa, hal-hal seperti alamat IP atau kuki dan pengidentifikasi perangkat seluler dapat dianggap sebagai informasi pribadi.</p>

			<p><u><em>Perlu menjadi catatan</em></u>: Kami dapat menggabungkan sebagian atau seluruh informasi tersebut untuk menciptakan produk, layanan, dan pengalaman pelanggan yang lebih baik.</p>

			<p><strong>yang Anda berikan kepada kami</strong>. Anda memberikan informasi Anda kepada kami ketika melakukan pendaftaran akun di situs web kami atau di aplikasi seluler atau dengan menghubungi kami melalui telepon atau email. Kami akan meminta data seperti nama, alamat email atau alamat rumah, tanggal lahir, informasi pembayaran, usia, jenis kelamin, jumlah anggota keluarga Anda, serta bagaimana Anda ingin kami mengirimkan informasi mengenai produk dan layanan kami kepada Anda--contohnya, ke alamat rumah, alamat email Anda, atau melalui pesan singkat.</p>

			<p><strong>dari situs dan email</strong>. Kami dapat menggunakan teknologi yang mampu mengumpulkan informasi secara otomatis ketika Anda mengunjungi situs kami, melihat iklan kami, atau menggunakan produk maupun layanan kami. Contohnya, kami menggunakan kuki (berkas kecil yang tersimpan pada peramban di komputer Anda) untuk memberikan informasi kepada kami peramban dan sistem operasi apa yang Anda gunakan, alamat IP Anda, situs web yang Anda kunjungi, tautan yang Anda akses, atau apakah Anda sudah atau belum membuka email dari kami.</p>

			<p><strong>dari aplikasi seluler dan perangkat yang terhubung ke internet</strong>. Untuk memberikan Anda pengalaman pengguna sebaik mungkin, kami dapat menggunakan teknologi yang mampu mengumpulkan informasi dari ponsel Anda ketika Anda menggunakan aplikasi seluler kami atau dari perangkat pintar di rumah Anda. Anda dapat menyetujui pengaturan tersebut ketika mengunduh aplikasi atau memasang perangkat yang tersambung ke internet di rumah Anda. Informasi ini dapat terdiri dari ponsel Anda atau ID iklan perangkat lainnya, informasi mengenai sistem operasi ponsel Anda, bagaimana Anda menggunakan aplikasi atau perangkat tersebut, dan lokasi fisik Anda. Anda akan mendapatkan pemberitahuan pop-up di ponsel atau perangkat Anda yang dapat Anda gunakan untuk memilih apakah Anda ingin menerima atau menolak memberikan izin bagi kami untukmengetahui lokasi geografis Anda (persis di titik di mana Anda berdiri atau di mana Anda mengakses internet).</p>

			<p><strong>dari tempat lain</strong>. Kami dapat memperoleh informasi yang dibagi atau dijual oleh perusahaan lain kepada kami. Contohnya, kami dapat memberikan persetujuan kepada perusahaan lain untuk membagi informasi pribadi Anda kepada kami ketika Anda mendaftar untuk layanan telekomunikasi atau program poin loyalitas peritel. Kami juga dapat mengumpulkan informasi dari tempat-tempat yang menurut Anda dapat dilihat semua orang, seperti posting internet, entri blog, video, atau status media sosial. Kami juga dapat menerima informasi dari perusahaan lain yang menjalankan bisnis pengumpulan informasi tentang Anda yang bersumber dari basis data yang tersedia secara umum atau melalui persetujuan yang Anda berikan kepada mereka untuk selanjutnya dapat kami gunakan. </p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Bagaimana Kami Menggunakan Informasi Anda</div>
			<p>Kami menggunakan informasi Anda untuk membantu kami memenuhi tujuan kami dalam menyentuh dan meningkatkan kehidupan keseharian orang-orang seperti Anda di seluruh dunia. Kami menggunakan informasi Anda untuk menanggapi segala pertanyaan atau permintaan mengenai informasi, mengirimkan produk atau sampel yang Anda minta, membantu Anda mengelola situs P&G atau preferensi aplikasi Anda, memungkinkan Anda untuk mengikuti kontes atau undian kami, atau memproses pembayaran Anda untuk produk yang Anda beli dari kami. Kami juga dapat menggunakan informasi Anda yang bersifat non-pribadi (seperti data pembelian, permintaan sampel, dll.) dalam riset atau analisis konsumen (atau informasi pribadi ketika Anda telah setuju untuk berpartisipasi dalam riset tersebut) untuk mempelajari lebih lanjut tentang apa yang diinginkan konsumen sehingga kami dapat membuat produk baru atau meningkatkan produk yang telah kami miliki.</p>

			<p>Cara lain kami menggunakan informasi Anda adalah dengan memastikan bahwa apa yang Anda dengar dari kami relevan dan bermanfaat bagi Anda. Misalnya, kami dapat mengirimkan informasi kepada Anda tentang produk Gillette® jika Anda telah memperlihatkan minat pada produk cukur kami dengan mengunjungi Gillette.com. Ketika kami melakukan hal tersebut, kami akan menggunakan informasi Anda--ID kuki atau ID perangkat--untuk memberikan batasan berapa kali Anda melihat iklan yang sama dari Gillette. Kami berharap Anda dapat mendengar tentang produk yang Anda gunakan dan yang Anda minati tanpa Anda mendengar produk yang sama berulang kali.</p>

			<p>Kami juga dapat menggunakan informasi gabungan dari banyak orang, tanpa mengidentifikasi individu manapun, untuk lebih memahami bagaimana mereka menggunakan situs web kami dan untuk mempelajari kebiasaan konsumen sehingga kami dapat membuat produk dan menawarkan layanan yang dapat memenuhi keubutuhan semua konsumen yang membagikan hal-hal umum yang serupa. Sebagai contoh, kami dapat mempelajari perilaku konsumen yang merupakan orang tua baru saat konsumen tersebut membaca situs web produk bayi, sehingga kami dapat melayani orang tua baru di manapun dengan lebih baik melalui produk dan layanan yang mereka inginkan. Penggunaan informasi non-pribadi dengan cara tersebut dapat membantu melindungi privasi Anda. Kami akan senantiasa mencoba menggunakan informasi non-pribadi jika memungkinkan untuk alasan tersebut.</p>

			<p>Untuk perlindungan lebih lanjut mengenai privasi Anda, kami hanya akan menggunakan informasi sesedikit mungkin untuk menyelesaikan tugas dengan cepat, menerapkan langkah-langkah untuk mencegah pencampuran informasi dengan cara yang memungkinkan kuki dan ID perangkat untuk secara khusus dan langsung dapat mengidentifikasi Anda (misalnya dengan nama), dan menghapus informasi Anda ketika kami tidak lagi membutuhkannya untuk tujuan bisnis kami. </p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Bagaimana Kami Melindungi Informasi Anda</div>
			<p>Kami menghormati informasi pribadi Anda dan berupaya sekuat tenaga untuk melindunginya dari kehilangan, penyalahgunaan, atau perubahan. Jika diperlukan, langkah-langkah ini dapat mencakup langkah-langkah teknis seperti firewall, sistem deteksi intrusi dan pencegahan, kata sandi yang unik dan rumit, serta enkripsi. Kami juga menggunakan cara-cara organisasional dan fisik, seperti pelatihan staf tentang kewajiban pemrosesan data, identifikasi insiden dan risiko data, pembatasan akses staf ke informasi pribadi Anda, dan dengan memastikan keamanan fisik termasuk mengamankan dokumen secara tepat saat tidak digunakan.</p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Apa yang Kami Bagikan</div>
			<p><strong>dengan perusahaan lain</strong>. Ketika kami memperoleh persetujuan Anda, kami dapat membagikan informasi Anda dengan mitra terpilih sehingga mereka dapat mengirimkan penawaran, promosi, atau iklan tentang produk atau layanan kepada Anda yang mungkin Anda minati. Misalnya, orang yang menerima email P&G dari merek popok kami seperti Pampers® juga dapat menyetujui untuk menerima informasi tentang susu bayi formula yang diproduksi oleh perusahaan lain. Kami tidak menjual informasi pribadi Anda kepada para pemasar di luar P&G. Kami dapat membagikan informasi yang tidak secara pribadi mengidentifikasi Anda dengan perusahaan lain untuk tujuan apapun.</p>

			<p><strong>dengan penyedia layanan</strong>. Kami mungkin perlu membagikan informasi Anda dengan perusahaan yang membantu kami menjalankan bisnis kami, termasuk yang menyediakan layanan hosting situs kami, mengirimkan email kami kepada Anda, menganalisis data yang kami kumpulkan, dan mengirimkan produk dan layanan yang Anda minta. Kami hanya membagikan informasi pribadi yang diperlukan dengan perusahaan-perusahaan ini, untuk menyelesaikan tugas-tugas yang kami minta dari mereka. Mereka bertanggung jawab untuk melindungi informasi Anda dengan cara yang sama seperti yang kami lakukan dan tidak akan membagi atau menggunakannya untuk tujuan lain dengan alasan apapun.</p>

			<p><strong>situasi lain</strong>. Jika salah satu merek atau bisnis kami yang Anda gunakan untuk membagi data pribadi telah dijual ke perusahaan lain, data Anda akan dibagikan kepada perusahaan tersebut. Dengan demikian, akun Anda dan data pribadi di dalamnya tidak akan dihapus, kecuali Anda secara khusus meminta merek atau perusahaan baru tersebut untuk menghapuskannya. Kami juga dapat membagikan informasi Anda dengan perusahaan yang membantu kami melindungi hak dan properti kami, atau ketika diminta oleh undang-undang atau otoritas pemerintah.</p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Hak dan Pilihan Anda</div>
			<p><u><strong>pemasaran</strong></u>. Anda dapat meminta kami untuk tidak lagi mengirimkan email dan pesan singkat kepada Anda dengan mengikuti instruksi untuk keluar, yang dikirimkan melalui media komunikasi tersebut. Anda juga dapat memilih untuk berhenti menerima email, SMS, atau surat melalui pos di sini.. Kami menghargai pilihan Anda, dan sementara Anda mengambil keputusan tersebut, kami perlu menyimpan informasi untuk melaksanakan keputusan yang Anda ambil. Sebagai contoh, jika Anda meminta kami untuk berhenti mengirim email pemasaran, kami akan membutuhkan alamat email Anda sehingga sistem kami dapat mengingat bahwa Anda tidak lagi ingin menerima komunikasi pemasaran ke alamat email tersebut.</p>

			<p><u><strong>akun</strong></u>. Negara tempat Anda mendaftarkan akun P&G memungkinkan Anda untuk menerima penawaran untuk mengakses informasi Anda dan melakukan pembaruan atau menghapus data Anda. Jika tidak, maka Anda dapat mengajukan permintaan tersebut <a href="<?=base_url()?>web/http://www.pg.com/privacy/contact_us/contact_us/privacy-central-contact-interactive.html" target="_blank">di sini</a>.</p>

			<p><u><strong>penduduk Uni Eropa (UE)</strong></u>. Jika Anda bertempat tinggal di UE, Anda dapat mengakses data pribadi yang kami miliki tentang Anda, dan meminta bahwa informasi yang tidak akurat, kedaluarsa, atau yang tidak lagi diperlukan untuk diperbaiki, dihapus, atau dibatasi, dan Anda juga dapat meminta kami untuk menyediakan data Anda dalam format yang dapat Anda gunakan untuk diberikan ke penyedia layanan lain. Anda juga dapat menarik persetujuan Anda kapan saja, dan persetujuan tersebut kami perlukan untuk memproses data pribadi Anda. Anda juga dapat berkeberatan dengan pemrosesan data pribadi Anda (dalam hal ini berarti Anda meminta kami untuk berhenti menggunakan data tersebut), di mana pemrosesan tersebut didasarkan pada kepentingan kami secara sah (yang berarti kami memiliki alasan untuk menggunakan data tersebut). Jika Anda ingin mengetahui informasi lebih lanjut tentang perlindungan data dan hak data pribadi Anda secara umum, silakan kunjungi situs Pengawas Perlindungan Data Eropa di <a href="<?=base_url()?>web/https://edps.europa.eu/data-protection/" target="_blank">https://edps.europa.eu/data-protection/</a> Jika Anda tidak puas dengan tanggapan kami atas permintaan Anda, Anda dapat mengajukan keluhan kepada otoritas perlindungan data di negara Anda. Silakan pilih salah satu opsi berikut untuk mengajukan permintaan Anda:</p>

			<p><u><strong>Permintaan Umum</strong></u>. Untuk membuat permintaan yang berkaitan dengan data pribadi yang digunakan untuk pemasaran, misalnya yang mencakup informasi yang Anda berikan kepada kami ketika Anda mendaftar melalui salah satu situs web atau aplikasi kami, silakan hubungi kami di sini.</p>

			<p><u><strong>Media Periklanan</strong></u>. Untuk membuat permintaan yang berkaitan dengan data pribadi yang digunakan untuk periklanan, misalnya yang mencakup informasi yang kami miliki tentang Anda di tingkat kuki atau tingkat ID perangkat yang kami gunakan untuk memberikan Anda iklan yang relevan, silakan hubungi kami di <a href="<?=base_url()?>web/https://opt-out-png.home.neustar/" target="_blank">sini</a>. Mungkin juga terdapat data yang terkait dengan kuki atau ID perangkat Anda di dalam permintaan yang kami terima (atau layanan iklan) dan platform mitra verifikasi iklan. Untuk data tersebut, silakan lihat di <a href="<?=base_url()?>web/http://www.adsrvr.org/" target="_blank">sini</a> dan di <a href="<?=base_url()?>web/http://www.mediamath.com/privacy-policy/" target="_blank">sini</a>.</p>

			<p><u><strong>Riset Konsumen</strong></u>. Untuk membuat permintaan yang berkaitan dengan data pribadi yang mungkin kami miliki sebagai bagian dari partisipasi Anda dalam salah satu riset kami, silakan lihat informasi kontak yang diberikan pada formulir persetujuan Anda atau hubungi atau kunjungi pusat riset yang bersangkutan.</p>

			<p><u><strong>Ahli Gigi Profesional</strong></u>. Jika Anda seorang dokter gigi profesional dan telah memberikan informasi Anda kepada kami sebagai bagian dari salah atu program penjangkauan profesional kami, termasuk melalui <a href="<?=base_url()?>web/https://www.dentalcare.com" target="_blank">https://www.dentalcare.com</a>, silakan hubungi kami melalui nomor di negara dan alamat email yang tercantum di bawah ini.</p>
			<div class="space-4"></div>
			<table cellspacing="0" cellpadding="0" border="1">
				<tbody>
					<tr>
						<th>
							<p align="center">
								<strong>Negara</strong>
							</p>
						</th>
						<th>
							<p align="center">
								<strong>Call Center</strong>
							</p>
						</th>
						<th>
							<p align="center">
								<strong>E-mail</strong>
							</p>
						</th>
					</tr>
					<tr>
						<td>
							<p>
								Jerman
							</p>
						</td>
						<td>
							<p>
								0203 570 570
							</p>
						</td>
						<td><p>N/A</p></td>
					</tr>
					<tr>
						<td>
							<p>
								Austria
							</p>
						</td>
						<td>
							<p>
								00800 570 570 00
							</p>
						</td>
						<td><p>N/A</p></td>
					</tr>
					<tr>
						<td>
							<p>
								Swiss
							</p>
						</td>
						<td>
							<p>
								00800 570 570 00
							</p>
						</td>
						<td><p>N/A</p></td>
					</tr>
					<tr>
						<td>
							<p>
								Inggris
							</p>
						</td>
						<td>
							<p>
								0870 242 1850
							</p>
						</td>
						<td>
							<p>
								<a href="<?=base_url()?>web/mailto:customerservice@dentalcare.co.uk">
									Klik di sini
								</a>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Spanyol
							</p>
						</td>
						<td>
							<p>
								900 670 270
							</p>
						</td>
						<td>
							<p>
								<a href="<?=base_url()?>web/mailto:spain@oralbprofesional.com">
									Klik di sini
								</a>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Italia
							</p>
						</td>
						<td>
							<p>
								+390650972534
							</p>
						</td>
						<td><p>N/A</p></td>
					</tr>
					<tr>
						<td>
							<p>
								Prancis
							</p>
						</td>
						<td>
							<p>
								+ 33 (0) 825 878 498
							</p>
						</td>
						<td>
							<p>
								<a href="<?=base_url()?>web/mailto:oralbprofessionalFR@pg.com">
									Klik di sini
								</a><br>
								<a href="<?=base_url()?>web/mailto:fixodentprofessional@pg.com">
									Klik di sini
								</a><br>
								<a href="<?=base_url()?>web/mailto:fluocarilprofessional@pg.com">
									Klik di sini
								</a><br>

							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Belgium
							</p>
						</td>
						<td><p>N/A</p></td>
						<td>
							<p>
								<a href="<?=base_url()?>web/mailto:oralbprofessionalBNL@pg.com">
									Klik di sini
								</a>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Belanda
							</p>
						</td>
						<td><p>N/A</p></td>
						<td>
							<p>
								<a href="<?=base_url()?>web/mailto:oralbprofessionalBNL@pg.com">
									Klik di sini
								</a>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Polandia
							</p>
						</td>
						<td>
							<p>
								0 801 25 88 25
							</p>
						</td>
						<td>
							<p>
								N/A
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Kuki</div>
			<div>Kuki adalah file kecil yang dikirim ke komputer Anda saat Anda menjelajahi web. Mereka menyimpan informasi yang berguna tentang bagaimana Anda berinteraksi dengan situs web yang Anda kunjungi. Kuki tidak mengumpulkan informasi apapun yang disimpan di komputer, perangkat, atau di file Anda. Kuki tidak mengandung informasi apapun yang secara langsung dapat mengidentifikasi Anda sebagai seorang individu. Kuki menampilkan komputer dan perangkat Anda hanya sebagai nomor dan huruf yang ditetapkan secara acak (misalnya, ID kuki ABC12345) dan tidak pernah dalam bentuk, misalnya, John E. Smith.</div>
			<div class="space-2"></div>
			<div>>Kami menggunakan kuki untuk sejumlah alasan, antara lain:</div>
			<ul>			
				<li>untuk melayani Anda dengan iklan-iklan yang relevan</li>
				<li>untuk lebih mempelajari tentang cara Anda berinteraksi dengan konten P&G</li>
				<li>membantu kami meningkatkan pengalaman Anda ketika mengunjungi situs web kami</li>
				<li>untuk mengingat preferensi Anda, seperti bahasa atau negara, sehingga Anda tidak perlu melakukan pengaturan situs web setiap kali Anda melakukan kunjungan</li>
			<li>untuk mengidentifikasi kekeliruan dan mengatasinya</li>
				<li>untuk menganalisis seberapa baik situs web kami beroperasi</li>
			</ul>
			<div class="space-2"></div>
			<div>Berikut adalah jenis-jenis kuki yang kami gunakan: </div>
			<ul>			
				<li><strong>kuki sesi</strong>. Halaman web tidak memiliki memori. Kuki sesi mengingat Anda (menggunakan ID yang dibuat secara acak seperti ABC12345) ketika Anda berpindah dari satu halaman ke halaman lain, sehingga Anda tidak diminta untuk memberikan informasi yang sama yang telah Anda berikan pada situs, berulang kali. Sebagai contoh, kuki sesi sangatlah membantu saat berbelanja online--tanpa kuki sesi, barang yang Anda tempatkan di keranjang belanja Anda dapat hilang begitu saja ketika Anda sampai pada proses pembayaran! Kuki ini dihapus segera setelah Anda meninggalkan situs kami atau menutup peramban Anda.</li>
				<li><strong>kuki persisten</strong>. Kuki persisten memungkinkan situs untuk mengingat apa yang Anda minati ketika Anda kembali lagi. Misalnya, jika Anda memilih untuk membaca situs dalam bahasa Prancis pada kunjungan pertama Anda, maka ketika Anda kembali, situs tersebut akan secara otomatis muncul dalam bahasa Prancis. Jika Anda tidak lagi harus memilih preferensi bahasa setiap kali, maka pengalaman Anda akan lebih nyaman, lebih efisien, dan situs webnya lebih mudah Anda gunakan.</li>
				<li><strong>kuki iklan</strong>. Kuki ini dapat digunakan untuk mempelajari minat Anda, berdasarkan, misalnya, situs web yang Anda kunjungi dan produk yang Anda beli. Hal ini juga dapat membantu kami menyimpulkan hal-hal tentang Anda, seperti usia Anda, status perkawinan Anda, dan berapa banyak anak yang mungkin Anda miliki. Data tersebut memungkinkan kami mengirimkan iklan untuk produk dan layanan yang lebih sesuai dengan hal-hal yang Anda sukai atau butuhkan. Kuki jenis ini juga memungkinkan kami membatasi berapa kali Anda melihat iklan yang sama.</li>
				<li><strong>kuki analitik</strong>. Kuki ini memberikan kami informasi mengenai cara kerja situs web kami. Seringkali kami menggunakan kuki analitik Google untuk memantau kinerja situs kami. Kemampuan kami untuk menggunakan dan berbagi informasi yang dikumpulkan oleh Google Analytics mengenai kunjungan Anda ke situs kami dibatasi oleh <a href="<?=base_url()?>web/https://www.google.com/analytics/terms/us.html" target="_blank">Google Analytics Terms of Use</a> dan <a href="<?=base_url()?>web/https://policies.google.com/privacy" target="_blank">Google Privacy Policy</a>.</li>
			</ul>
			<div class="space-2"></div>
			<div><strong>Bagaimana Anda dapat mengontrol kuki</strong>. Anda dapat mengatur peramban Anda untuk menolak semua kuki atau untuk mengindikasi ketika kuki sedang dikirim ke komputer Anda. Namun, langkah ini dapat mencegah situs atau layanan kami untuk berfungsi dengan benar. Anda juga dapat mengatur peramban Anda untuk menghapus kuki setiap kali Anda selesai berinteraksi dengan situs web kami. </div>
			<div class="space-2"></div>
			<div><strong>teknologi lainnya.</strong></div>
			<ul>			
				<li><strong>proximity-based beacons</strong>. Beacon mengirimkan sinyal satu arah ke aplikasi seluler yang Anda instal ke dalam ponsel Anda dalam jarak yang sangat dekat untuk memberi tahu Anda, misalnya, produk apa yang sedang dijual ketika Anda memasuki toko. Beacon hanya berkomunikasi dengan perangkat Anda ketika Anda cukup dekat dan setelah Anda memberikan persetujuan dalam aplikasi seluler yang terkait dengan beacon tertentu. Selanjutnya, aplikasi tersebut dapat memberikan kami informasi lokasi untuk membantu menyesuaikan iklan dan penawaran kami bagi Anda. Misalnya, ketika Anda berada dekat beacon di bagian perawatan kulit di supermarket, kami dapat mengirimkan kupon diskon senilai $4 kepada Anda.</li>
				<li><strong>piksel</strong>. Ini merupakan benda-benda kecil yang tertanam di halaman web, tetapi tidak terlihat. Mereka juga dikenal sebagai "tag", "web bug", atau "pixel gifs". Kami menggunakan piksel untuk mengirimkan kuki ke komputer Anda, memantau aktivitas situs web kami, mempermudah akses masuk ke situs kami, dan untuk aktivitas pemasaran online. Kami juga menyertakan piksel di dalam pesan email promosi atau buletin kami untuk menentukan apakah Anda membuka dan menindaklanjutinya.</li>
				<li><strong>pengenal perangkat seluler dan SDK</strong>. Kami menggunakan kode perangkat lunak di aplikasi seluler kami untuk mengumpulkan informasi serupa dengan apa yang dikumpulkan kuki di internet. Ini akan menjadi informasi seperti pengenal ponsel Anda (IDFA iOS dan ID Iklan Android) dan cara Anda menggunakan aplikasi kami. Serupa dengan kuki, informasi perangkat yang kami kumpulkan secara otomatis saat Anda menggunakan aplikasi kami tidak akan pernah mengidentifikasi Anda sebagai seorang individu. Kami hanya mengenali perangkat seluler sebagai nomor dan huruf yang ditetapkan secara acak (contohnya, ID iklan EFG4567) dan tidak pernah dalam bentuk, misalnya, John E. Smith.</li>
				<li><strong>geolokasi secara tepat</strong>. Kami dapat menerima informasi tentang lokasi Anda yang sebenarnya melalui hal-hal seperti koordinat sistem posisi global (GPS) (garis bujur dan garis lintang) ketika Anda menggunakan aplikasi seluler kami. Anda akan selalu mendapatkan pemberitahuan secara pop-up di ponsel atau perangkat Anda yang meminta Anda untuk menyetujui atau menolak persetujuan untuk kami mengenali secara persis di mana Anda sedang berada di dunia. Anda perlu memahami bahwa kami tidak akan selalu meminta persetujuan Anda untuk mengetahui keberadaan Anda secara umum di kota, kode pos, atau provinsi yang lebih luas. Misalnya, bagi kami informasi bahwa Anda sedang berada di suatu tempat di Manila, Filipina, bukanlah suatu informasi tentang lokasi Anda secara persis.</li>
			</ul>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Periklanan Berbasis Minat</div>
			<p><strong>Ketika Anda mengunjungi situs mitra kami</strong>, kami dapat menampilkan iklan atau konten lain yang menurut kami menarik bagi Anda. Misalnya, Anda mungkin menerima iklan untuk produk deterjen Tide® jika kami melihat Anda mengunjungi situs yang menjual pakaian anak-anak atau perlengkapan sekolah. Melalui informasi tersebut, kami dapat menyimpulkan bahwa Anda memiliki anak, dan oleh karena itu Anda memiliki minat terhadap produk pembersih baju yang berkualitas. Dengan cara ini, kami bermaksud mengirimkan informasi yang relevan tentang produk kami yang mungkin dapat bermanfaat bagi Anda.</p>

			<p><strong>Kami belajar dari kelompok konsumen yang memiliki minat yang sama</strong>. Kami dapat menempatkan Anda ke dalam kelompok konsumen tertentu yang menunjukkan kesamaan minat. Misalnya, kami mungkin menempatkan Anda dalam kelompok "penggemar pisau cukur" jika kami melihat Anda sering membeli alat cukur secara online atau Anda dapat menjadi bagian dari "pencari harga murah" jika kami melihat Anda menggunakan kupon online atau mencari diskon saat berbelanja. Kami melihat hal-hal ini tentang Anda ketika Anda mengunjungi halaman web, tautan yang Anda klik di situs web kami, dan situs web lain yang Anda kunjungi, aplikasi seluler yang Anda gunakan, atau email kami yang Anda lihat dan tautan yang Anda klik di email. Kami menggabungkan berbagai kuki dan ID perangkat untuk membantu kami mempelajari berbagai kecenderungan umum, kebiasaan, atau karakteristik dari sekelompok konsumen, yang seluruhnya bertindak serupa secara online dan/atau offline. Melalui hal ini, kami dapat menemukan dan melayani banyak orang yang "terlihat seperti" mereka yang sudah ada dalam kelompok tersebut dan dengan demikian kami dapat mengirimkan apa yang kami yakini sebagai penawaran dan informasi produk yang relevan dan bermanfaat kepada mereka.</p>

			<p><strong>kami menautkan informasi lain ke kuki dan ID perangkat Anda</strong>. Kuki dan ID perangkat Anda dapat dilengkapi dengan informasi lain, seperti informasi tentang produk yang Anda beli secara offline atau informasi yang Anda berikan secara langsung kepada kami ketika membuat akun di situs kami. Kami biasanya melakukan hal tersebut dengan tidak secara langsung mengidentifikasi Anda secara pribadi. Misalnya, kami dapat mengetahui bahwa ID kuki ABC12345 adalah milik anggota dari kelompok "penggemar pisau cukur", berdasarkan kunjungan situs web seseorang, usia, jenis kelamin, dan kebiasaan belanja orang tersebut, tetapi kami tidak akan mengenali nama atau alamat orang tersebut, ataupun informasi lain yang akan mengidentifikasi dirinya sebagai seorang individu. Jika kami ingin mengidentifikasi informasi kuki atau perangkat Anda secara pribadi (riwayat penelusuran web dan aplikasi), kami akan selalu bertanya kepada Anda sebelum melakukannya.</p>

			<p><strong>kami dapat mengenali Anda di semua komputer, tablet, ponsel, dan perangkat Anda</strong>. Kami dapat mengetahui bahwa ID kuki ABC12345 berasal dari komputer yang mungkin terhubung ke orang atau rumah tangga yang sama yang memiliki ponsel dengan ID perangkat EFG15647. Ini berarti Anda dapat mencari popok di laptop Anda, kemudian mengklik pada tautan hasil pencarian Google yang telah kami sponsori, dan selanjutnya melihat iklan untuk popok merek Pampers® di ponsel Anda. Kami mungkin berasumsi atau menyimpulkan bahwa orang yang sama memiliki komputer dan telepon karena, misalnya, mereka masuk ke jaringan WiFi yang sama setiap hari pada waktu yang sama. Memahami perangkat apa yang tampaknya digunakan oleh seseorang atau suatu keluarga tertentu membantu kami membatasi berapa kali Anda melihat iklan yang sama di semua perangkat Anda. Hal ini penting untuk diperhatikan, karena kami tidak ingin Anda merasa terganggu oleh spam iklan yang sama.</p>

			<p><strong>bagaimana Anda dapat berhenti menerima iklan berbasis minat</strong>. Untuk berhenti menerima iklan berbasis minat P&G, Anda dapat klik di <a href="<?=base_url()?>web/https://info.evidon.com/pub_info/9057?v=1&nt=0&nw=true" target="_blank">sini</a> atau klik ikon WebChoices atau AppChoices di salah satu situs kami atau di salah satu aplikasi seluler kami. Anda juga dapat mencegah iklan berbasis minat di situs web dengan menolak untuk menerima kuki di peramban Anda, menolak permintaan "akses ke data" yang biasanya muncul ketika Anda memasang suatu aplikasi, atau dengan menyesuaikan pengaturan pelacakan iklan di perangkat Anda.</p>

			<p><strong>Anda masih akan melihat iklan "kontekstual" bahkan jika Anda memilih keluar dari iklan berbasis minat</strong>. Bahkan jika kami berhenti mengirimikan iklan berbasis minat kepada Anda, Anda masih akan mendapatkan iklan dari merek kami di komputer atau perangkat seluler Anda. Namun, iklan ini didasarkan pada konteks situs yang Anda kunjungi, dan disebut sebagai iklan kontekstual. Tidak seperti iklan berbasis minat yang didasarkan pada laman yang Anda kunjungi di ponsel atau aktivitas Anda menonton di komputer, iklan kontekstual adalah iklan yang ditampilkan kepada Anda berdasarkan konteks situs tertentu yang Anda kunjungi. Sebagai contoh, Anda masih dapat melihat iklan untuk salah satu merek perawatan bayi kami sambil melihat produk perawatan anak-anak secara online karena situs-situs tersebut secara tradisional menjaring para orang tua baru atau mereka yang mengharapkan orang tua sebagai pengunjung. Anda juga perlu tahu bahwa kami masih dapat mengumpulkan informasi dari komputer atau perangkat Anda dan menggunakannya untuk tujuan lain seperti mengevaluasi cara kerja situs web kami, untuk riset konsumen, atau mendeteksi penipuan.</p>

			<p><strong>menghapus kuki juga menghapus pilihan Anda untuk tidak menerima iklan berbasis minat</strong>. Ketika Anda memilih untuk tidka menerima iklan berbasis minat dari kami, kami mengirim kuki khusus ke peramban Anda, untuk memberitahukan kami bahwa Anda tidak lagi ingin menerima iklan berbasis minat dari kami. Kuki ini akan dihapus jika Anda memutuskan untuk menghapus semua kuki. Ini berarti Anda harus kembali menyatakan pilihan Anda jika Anda masih tidak ingin menerima iklan berbasis minat. </p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Pemrosesan, Penyimpanan, dan Pengalihan Data Uni Eropa</div>
			<p>Bagian ini hanya berlaku untuk pemrosesan data pribadi bagi warga negara Uni Eropa. Hal ini bertujuan untuk meningkatkan transparansi dalam pemrosesan, penyimpanan, dan pengalihan data pribadi warga negara Uni Eropa yang sejalan dengan isi teks dan semangat dari Peraturan Perlindungan Data Umum (GDPR).</p>

			<p><strong>entitas</strong>. Entitas P&G yang berbeda-beda dapat menjadi pengendali data pribadi Anda. Pengendali data merupakan entitas yang mengarahkan aktivitas pemrosesan dan bertanggung jawab atas data tersebut. Bagan berikut mengidentifikasi pengendali data untuk data yang ada di negara-negara EU. Misalnya, ketika Anda mendaftarkan email di salah satu situs web Prancis (.fr) kami, entitas P&G yang tercantum di samping negara tersebut akan menjadi pengendali data pribadi yang dimaksud (misalnya, Procter & Gamble France SAS (LE 577). </p>
			<div class="space-4"></div>
			<table cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<th>
							<p align="center">
								Negara
							</p>
						</th>
						<th>
							<p align="center">
							  Pengendali Data
							</p>
						</th>
					</tr>
					<tr>
						<td>
							<p>
								Austria
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Austria – Zweigniederlassung der
								Procter &amp; Gamble GmbH
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Bulgaria
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Bulgaria EOOD
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Romania
							</p>
						</td>
						<td>
							<p>
							   Untuk kontes: Procter &amp; Gamble Distribution SRL		
							</p>
							<p>
								Untuk situs lainnya: Procter &amp; Gamble Marketing Romania SR		
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Polandia
							</p>
						</td>
						<td>
							<p>
								Procter and Gamble DS Polska sp z o.o.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Belgia
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Distribution Company (Europe) BVBA
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Czechia
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Czech Republic s.r.o.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Hungaria
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble RSC Regionális
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Slovakia
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble, spol. s.r.o.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Kroasia
							</p>
						</td>
						<td>
							<p>
		Procter &amp; Gamble d.o.o. za trgovinu
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Prancis
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble France SAS/Procter &amp; Gamble
								Pharmaceuticals France SAS”
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Jerman
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Service GmbH
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Yunani
							</p>
						</td>
						<td>
							<p>
								P&amp;G Hellas Ltd.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Irlandia
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble UK
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Italia
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Srl
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Belanda
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Nederland B.V.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Portugal
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble Portugal, Productos de Consumo
								Higiene de Saude, S.A.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Spanyol
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble España, S.A.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
							   Inggris
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble UK
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								Negara UE yang tidak terdaftar
							</p>
						</td>
						<td>
							<p>
								Procter &amp; Gamble International Operations SA
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="space-4"></div>
			<div><strong>pemrosesan dan penyimpanan</strong>. Sebagai aturan umum, kami menyimpan data Anda hanya selama diperlukan untuk menyelesaikan tujuan pengumpulan data atau sebagaimana diwajibkan secara hukum. Kami mungkin perlu menyimpan data Anda lebih lama dari periode penyimpanan yang ditentukan untuk memenuhi permintaan Anda, termasuk untuk tidak menerima email promosi, atau untuk mematuhi kewajiban hukum atau kewajiban lainnya. Bagan berikut ini menjelaskan jenis-jenis data yang kami kumpulkan, tujuan kami mengumpulkannya, alasan penggunaan tersebut sesuai dengan hukum (dasar hukum), dan berapa lama umumnya kami menyimpan data tersebut (periode penyimpanan). </div>
			<div class="space-4"></div>
			<table cellspacing="0" cellpadding="0" border="1">
				<tbody>
					<tr>
						<th>
							<p align="center">
								<strong>Jenis Data</strong>
							</p>
						</th>
						<th>
							<p align="center">
								<strong>Mengapa Kami Mengumpulkan Data Tersebut</strong>
							</p>
						</th>
						<th>
							<p align="center">
								<strong>Dasar Hukum</strong>
							</p>
						</th>
						<th>
							<p align="center">
								<strong>Periode Penyimpanan</strong>
							</p>
						</th>
					</tr>
					<tr>
						<td>
							<p>
								<strong>Pemasaran</strong>
							</p>
							<p>
		Email, nama, nomor telepon, afinitas Anda, minat Anda, profesi Anda, kebiasaan Anda, apa yang Anda beli, foto atau video yang Anda unggah, informasi tentang anak-anak Anda dan rumah Anda, komposisi keluarga Anda, jumlah orang di rumah Anda, jenis rambut Anda, jenis kulit Anda, aroma favorit Anda, apakah Anda memiliki hewan peliharaan, dll.
							</p>
						</td>
						<td>
							<p>
								Untuk mengirimkan materi pemasaran produk, layanan kami, atau layanan dari mitra kami kepada Anda.
							</p>
						</td>
						<td>
							<p>
							   Persetujuan Anda untuk email dan SMS serta persetujuan melalui surat. Kepentingan yang sah untuk hal-hal lainnya.
							</p>
						</td>
						<td>
							<p>
								Sampai Anda mengajukan permintaan untuk menghapus data pribadi atau untuk menarik persetujuan Anda. Jika Anda tidak membuat permintaan semacam itu, data pribadi akan dihapus berdasarkan jadwal berikut:
							</p>
							<p>
								email: setelah non-aktif selama &lt;50 bulan di semua saluran. Kami mendefinisikan bahwa Anda non-aktif dengan menerapkan sejumlah kriteria internal.
							</p>
							<p>
							   SMS: setelah non-aktif selama &lt;50 bulan di semua saluran. Kami mendefinisikan bahwa Anda non-aktif dengan menerapkan sejumlah kriteria internal.
							</p>
							<p>
							   email: setelah non-aktif selama &lt;50 bulan di semua saluran. Kami mendefinisikan bahwa Anda non-aktif dengan menerapkan sejumlah kriteria internal.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								<strong>Kontes</strong>
							</p>
							<p>
		Email, nama, nomor telepon, dan data lain sewaktu-waktu.
							</p>
						</td>
						<td>
							<p>
							   Untuk memberikan informasi mengenai kontes kepada peserta mengenai kontes tersebut, termasuk untuk mengumumkan pemenang kontes.
							</p>
						</td>
						<td>
							<p>
								Persetujuan Anda.
							</p>
						</td>
						<td>
							<p>
								Selama 24 bulan, kecuali hukum setempat mewajibkan kami untuk mempertahankannya lebih lama.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								<strong>Pembelian Produk</strong>
							</p>
							<p>
								Email, nama, nomor telepon, informasi pembelian (termasuk rekening bank IBAN atau rincian Paypal), dan data lainnya sewaktu-waktu.
							</p>
						</td>
						<td>
							<p>
								Untuk memproses pembelian produk, penawaran cashback, atau jaminan kami dan untuk mengirimkan komunikasi yang relevan terkait pembelian tersebut kepada Anda.
							</p>
						</td>
						<td>
							<p>
							   Persetujuan Anda.
							</p>
						</td>
						<td>
							<p>
							   Selama diperluan untuk memenuhi pesanan Anda dan menindaklanjuti dengan komunikasi tentang pesanan Anda kecuali hukum setempat mengharuskan kami untuk mempertahankannya lebih lama. Umumnya kami juga menyimpan data selama 24 bulan untuk tawaran cashback dan 10 tahun untuk jaminan.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								<strong>Hubungi Kami</strong>
							</p>
							<p>
								Email, nama, nomor telepon, dan data lainnya sewaktu-waktu.
							</p>
						</td>
						<td>
							<p>
								Untuk menjawab pertanyaan Anda dan memastikan kami menindaklanjutinya dengan tepat atau sebagaimana diperlukan oleh hukum atau kebijakan P&amp;G.
							</p>
						</td>
						<td>
							<p>
							   Kepentingan bisnis kami yang sah dalam mengelola permintaan konsumen, serta persetujuan Anda untuk data kategori khusus yang dapat dikumpulkan dalam beberapa kasus efek samping yang tidak diinginkan.
							</p>
						</td>
						<td>
							<p>
								Dari 0 hingga 10 tahun, tergantung pada sifat permintaan, kepentingan kami yang sah untuk memproses data, dan kewajiban hukum kami.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								<strong>Riset</strong>
							</p>
							<p>
							   Email, nama, nomor telepon, alamat, foto atau video yang dapat diidentifikasi, dan data lainnya sewaktu-waktu.
							</p>
						</td>
						<td>
							<p>
							   Untuk menguji ide produk kami dan mempelajari tentang preferensi dan praktik Anda sehingga kami dapat meningkatkan produk dan kehidupan konsumen kami.
							</p>
						</td>
						<td>
							<p>
							   Persetujuan Anda.
							</p>
						</td>
						<td>
							<p>
								Kami akan menyimpan data pribadi yang dikumpulkan sebagai bagian dari penelitian klinis substantif selama kami membutuhkannya untuk tujuan pengumpulan data, dan/atau selama mungkin diperlukan untuk kepentingan hukum atau peraturan setempat, yang mungkin sampai 25 tahun. Untuk penelitian non-klinis, kami akan menyimpan data pribadi Anda yang bersifat substantif selama maksimal 5 tahun. Kami akan menyimpan dokumen persetujuan tertulis Anda yang telah ditandatangani.
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>
								<strong>Penargetan Media</strong>
							</p>
							<p>
								Kuki periklanan, ID perangkat, informasi demografis seperti jenis kelamin dan usia, data perilaku seperti tampilan halaman, dan data lainnya sewaktu-waktu.
							</p>
						</td>
						<td>
							<p>
								Untuk mempelajari minat Anda dan menyesuaikan iklan yang kami kirimkan kepada Anda.
							</p>
						</td>
						<td>
							<p>
							   Kepentingan bisnis kami yang sah dalam mengelola permintaan konsumen, serta persetujuan Anda untuk data kategori khusus yang dapat dikumpulkan dalam beberapa kasus efek samping yang tidak diinginkan.
							</p>
						</td>
						<td>
							<p>
							   Kami akan menyimpan data ini selama tiga belas bulan sejak tanggal pengumpulan, atau hingga Anda memilih untuk keluar, yang mana yang lebih dahulu terjadi.
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="space-4"></div>
			<div><strong>pengalihan data Anda ke negara lain</strong>. Informasi pribadi Anda dapat dialihkan ke, disimpan, dan diproses di negara lain selain di mana data tersebut dikumpulkan, termasuk Amerika Serikat. Sebagai contoh, kami dapat menyimpan data Anda di server yang berada di Amerika Serikat karena negara tersebut merupakan lokasi di mana basis data tertentu disimpan; dan data tersebut dapat "dialihkan" lagi ketika salah satu pemasar kami mengakses data tersebut dari Swiss untuk mengirimkan sampel produk kepada Anda. Kami melakukan pengalihan semacam itu, baik antara entitas P&G maupun antara P&G dan penyedia layanan kami, dengan menggunakan perlindungan kontraktual yang telah disetujui oleh regulator di UE untuk memastikan data Anda terlindungi (dikenal sebagai klausul kontrak model). Jika Anda ingin mendapatkan salinan atas perjanjian pengalihan datanya, silakan <a href="<?=base_url()?>web/mailto:euprivacy@pg.com">hubungi kami..</a></div>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Situs dan Konten Aplikasi</div>			
			<p><strong>plugin</strong>. Situs web kami dapat menyertakan plugin dari perusahaan lain seperti jejaring sosial. Contoh plugin adalah tombol "Suka" di Facebook. Plugin ini dapat mengumpulkan informasi (misalnya url halaman yang Anda kunjungi) dan mengirimkannya kembali ke perusahaan yang membuatnya. Ini mungkin terjadi bahkan jika Anda tidak mengklik plugin. Plugin ini diatur oleh kebijakan privasi dan ketentuan perusahaan yang membuatnya, meskipun muncul di situs kami. Plugin ini adalah kuki yang non-esensial dan hanya akan berfungsi di situs kami yang berbasis di UE, jika Anda menyetujui untuk menerima kuki.</p>

			<p><strong>login</strong>. Situs web kami memungkinkan Anda untuk masuk menggunakan akun Anda dengan perusahaan lain seperti, misalnya, "Masuk dengan Facebook". Ketika Anda melakukan ini, kami hanya akan memiliki akses ke informasi yang Anda berikan kepada kami, yaitu persetujuan untuk menerima dari pengaturan akun Anda di akun perusahaan lain yang Anda gunakan untuk masuk.</p>

			<p><strong>konten pengguna</strong>. Beberapa situs dan aplikasi kami akan memungkinkan Anda mengunggah konten Anda sendiri untuk kontes, blog, video, dan fungsi lainnya. Harap diperhatikan bahwa informasi apapun yang Anda kirimkan ataupun unggahan Anda akan menjadi informasi publik. Kami tidak memiliki kendali atas bagaimana orang lain menggunakan konten yang Anda kirimkan ke situs dan aplikasi kami. Kami tidak bertanggung jawab atas penggunaan tersebut dengan cara yang dapat melanggar kebijakan privasi ini, hukum, atau privasi dan keamanan pribadi Anda.</p>

			<p><strong>tautan</strong>. Situs P&G dapat menyertakan tautan ke situs lain, yang tidak berada di bawah kendali kami. Situs-situs tersebut akan diatur oleh kebijakan dan ketentuan privasi mereka sendiri-sendiri, dan bukan oleh ketentuan privasi dan kebijakan kami.</p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Keamanan Anak-Anak</div>
			<p><strong>hukum privasi online anak-anak</strong>. Kami mematuhi semua undang-undang perlindungan data yang berlaku saat mengumpulkan informasi pribadi secara online dari anak-anak. Misalnya, di UE kami tidak mengumpulkan informasi pribadi dari anak-anak di bawah 16 tahun, kecuali kami mendapatkan persetujuan khusus dari orang tuanya. Demikian pula, di AS, kami perlu memperoleh izin terverifikasi dari orang tua ketika mengumpulkan informasi pribadi dari anak-anak di bawah 13 tahun.</p>

			<p><strong>pemberitahuan di California untuk anak di bawah umur</strong>. Kami dapat menawarkan layanan interaktif yang memungkinkan remaja di bawah usia 18 tahun untuk mengunggah konten mereka sendiri (misalnya, video, komentar, pembaruan status, atau gambar). Konten ini dapat dibuang atau dihapus kapan saja dengan mengikuti petunjuk di situs kami. Jika Anda memiliki pertanyaan tentang cara melakukan ini, silakan <a href="<?=base_url()?>web/https://www.pg.com/privacy/contact_us/contact_us/privacy-central-contact-interactive.html" target="_blank">hubungi kami</a>. Harap diperhatikan bahwa unggahan semacam itu mungkin telah disalin, diteruskan, atau dipasang di tempat lain oleh orang lain, dan kami tidak bertanggung jawab atas tindakan semacam itu. Dalam kasus-kasus tersebut, Anda harus menghubungi pemilik situs lain yang bersangkutan untuk meminta penghapusan konten dari Anda. </p>
			<div class="space-4"></div>
			<div class="s-16 s-bold _bl">Kontak Kami</div>
			<p>Silakan langsung <a href="<?=base_url()?>web/https://www.pg.com/privacy/contact_us/contact_us/privacy-central-contact-interactive.html" target="_blank">hubungi kami</a> apabila Anda memiliki pertanyaan atau persoalan mengenai privasi Anda dan praktik perlindungan data kami.</p>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- body end -->

<!-- script start -->
<script src="<?=base_url()?>web/js/jquery-3.3.1.min.js"></script>
<script src="<?=base_url()?>web/js/anime.min.js"></script>
<script src="<?=base_url()?>web/js/carousel/owl.carousel.min.js"></script>
<script src="<?=base_url()?>web/js/dom/jquery.fancybox.js"></script>
<script src="<?=base_url()?>web/js/main.js"></script>
<script>
	function checkform(){
		var recapresp = grecaptcha.getResponse();
		if (recapresp == ""){
			alert("Captcha gagal");
			return false;
		} else {
			if ($("#check01").is(":checked")){
				if ($("#fldname").val() == ""){
					alert("Mohon isi nama anda");
				} else if ($("#fldphone").val() == ""){
					alert("Mohon isi no telp anda");
				} else {
					$("form").submit();
				}			
			} else {
				alert("Mohon setuju degan syarat & ketentuan yg berlaku");
			}
		}
	}
</script>
<!-- script end -->
</body>
</html>
