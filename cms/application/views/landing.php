<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <!-- META START -->

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="language" content="en-us">
  <meta name="description" content="P&G Connect" />
  <meta name="keyword" content="P&G Connect" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

  <!-- META END -->

  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <!-- style start -->

  <!-- <link rel="stylesheet" href="<?=base_url()?>web/js/layerslider/layerslider.css"> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/fancybox/jquery.fancybox.min.css"/>
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/js/owl/owl.carousel.css"/> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap/bootstrap.min.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/slickslider/slick-theme.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/slickslider/slick.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/wow/animate.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/select2/select2.min.css"/>
  <link rel="stylesheet" href="<?=base_url()?>web/css/fontawesome/css/all.min.css"/>
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/css/lightcase/lightcase.css" /> -->
  <!-- <link rel="stylesheet" media="screen and (min-width: 999px)" href="<?=base_url()?>web/js/animate/animations.css"> -->
  <link rel="stylesheet" href="<?=base_url()?>web/css/fonts.css">
  <link rel="stylesheet" href="<?=base_url()?>web/css/style.css">
  <!-- <link rel="stylesheet" href="<?=base_url()?>web/css/responsive.css"> -->

  <!-- style end -->
</head>

<body>
  <main class="main-wrap" id="home">
    
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">  
                <a href="<?=base_url()?>"><img src="<?=base_url()?>web/img/logo_pg.png"></a>
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
            <div class="header-title wow fadeInUp d-none d-sm-block" data-wow-delay="0.6s">
              featured events
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->

    <!-- body start -->
    <section class="home">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo without-header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="header-title wow fadeInUp d-block d-sm-none" data-wow-delay="0.6s">
                featured events
              </div>
              <div class="filter-wrapper wow fadeInUp" data-wow-delay="1.3s">
                <ul class="list-inline">
                  <li class="list-inline-item active" data-filter="all">all events</li>
                  <li class="list-inline-item joined" data-filter="upcoming">upcoming</li>
                  <li class="list-inline-item" data-filter="myevent">past</li>
                </ul>
              </div>
              <div class="event-wrapper">
                <div class="row">
                  <?php
                  foreach ($events as $evt){ 
                  $ongoingflag = "myevent";
                  $datetext = "";
                  $jointext = "";
                  if ($evt->datestart <= date("Y-m-d")){
                    $ongoingflag = "myevent";
                  } else {
                    $ongoingflag = "upcoming";
                    $jointext = "joined";
                  }
                  if (date("Y-m", strtotime($evt->datestart)) == date("Y-m", strtotime($evt->dateend))){
                    $datetext = date("d", strtotime($evt->datestart))."-".date("d F Y", strtotime($evt->dateend));
                  } else if ($evt->datestart == $evt->dateend){
                    $datetext = date("d F Y", strtotime($evt->datestart));
                  } else {
                    $datetext = date("d F Y", strtotime($evt->datestart))." - ".date("d F Y", strtotime($evt->dateend));
                  } 
                    ?>
                    <div class="col-lg-4 col-md-6 col-12 event-col <?=$ongoingflag?>">
                      <div class="event-item <?=$jointext?>">
                        <div class="image">
                          <img src="<?=base_url()?>assets/eventf/<?=$evt->eventid?>.png">
                        </div>
                        <div class="text">
                          <div class="title">
                            <a href="<?=site_url("event/$evt->eventid")?>"><?=$evt->eventtitle?></a>
                          </div>
                          <div class="date">
                            <?=$datetext?>
                          </div>
                          <div class="place">
                            <?=$evt->eventvenue?>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php }
                  ?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <!-- script start -->
  <script src="<?=base_url()?>web/js/jquery-1.12.0.min.js"></script>
  <script src="<?=base_url()?>web/js/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>web/js/slickslider/slick.min.js"></script>
  <script src="<?=base_url()?>web/js/animate/css3-animate-it.js"></script>
  <!-- <script src="<?=base_url()?>web/js/owl/jquery.owl-filter.js"></script>
  <script src="<?=base_url()?>web/js/owl/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/greensock.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/layerslider.transitions.js"></script>
  <script src="<?=base_url()?>web/js/layerslider/layerslider.kreaturamedia.jquery.js"></script> -->
  <script src="<?=base_url()?>web/js/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?=base_url()?>web/js/wow/wow.min.js"></script>
  <script src="<?=base_url()?>web/js/select2/select2.min.js"></script>
  <!-- <script src="<?=base_url()?>web/js/lightcase/lightcase.js"></script> -->
  <script src="<?=base_url()?>web/js/main.js"></script>
  <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589c478dc47db98d"></script> -->
  <!-- script end -->
</body>
</html>
