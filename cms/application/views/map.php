<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Map | Pharmacycare</title>
<!-- META -->
<meta name="language" content="en-us">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="img/favicon.ico">
<!-- style start -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="js/carousel/owl.carousel.min.css">
<link rel="stylesheet" href="js/carousel/owl.theme.default.min.css"> 
<link rel="stylesheet" href="js/dom/jquery.fancybox.css" />
<link rel="stylesheet" href="css/main.css">
<!-- style end -->
</head>

<body>
<!-- body start -->
<div id="container">
	<div id="body">
		<div class="home-slider">
			<div class="container">				
				<div id="sync1" class="owl-carousel owl-theme">
					<div class="item">
						<div class="home-img"><img src="img/home-slider-1.png" alt="home slider"></div>
						<div class="home-desc">
							<div class="s-14">Daftar dan dapatkan</div>
							<div class="s-28">10% diskon</div>
							<div class="s-12">Untuk minimal pembelian Rp30.000, semua produk Neurobion!</div>
						</div>
						<div class="clear"></div>
						
					</div>
					<div class="item">
						<div class="home-img"><img src="img/home-slider-1.png" alt="home slider"></div>
						<div class="home-desc">
							<div class="s-14">Daftar dan dapatkan</div>
							<div class="s-28">20% diskon</div>
							<div class="s-12">Untuk minimal pembelian Rp30.000, semua produk Neurobion!</div>
						</div>
						<div class="clear"></div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="home-thumb container">
			<div id="sync2" class="owl-carousel owl-theme">
				<div class="item"><div class="home-date">01 April - 01 Juli</div></div>
				<div class="item"><div class="home-date">01 Agustus - 01 October</div></div>
			</div>
		</div>
		<div class="map">
			<div class="map-content">
				<div class="container">
					<div class="opening">
						Selamat datang di Pharmacy Care, temukan apotik terdekat untuk mendapatkan promo terbaru!
					</div>
					<div class="space-4"></div>
					<div class="s-18 s-bold">Apotik sekitar saya :</div>
					<div class="space-2"></div>
					<iframe src="https://www.google.com/maps/d/u/0/embed?mid=11Ti8-mPI1JQUdaCvBQOjlXCm_l00zNqv" width="100%" height="480"></iframe>
				</div>
			</div>
		</div>
		<div class="logo"><a href="index.html"><img src="img/logo.png" alt="logo"></a></div>
	</div>
	<div id="footer"></div>
</div>
<!-- body end -->

<!-- script start -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/anime.min.js"></script>
<script src="js/carousel/owl.carousel.min.js"></script>
<script src="js/dom/jquery.fancybox.js"></script>
<script src="js/main.js"></script>

<!-- script end -->
</body>
</html>
