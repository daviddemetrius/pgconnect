<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Pharmacycare ID</title>
<style type="text/css">
body {margin: 0;}
body, table, td, p, a, li, blockquote {-webkit-text-size-adjust: none!important; font-family: sans-serif; font-style: normal; font-weight: 400;}
img, div{display: block; margin: 0; padding: 0; line-height: normal;}
@media screen and (max-width:600px) {
	body, table, td, p, a, li, blockquote {-webkit-text-size-adjust: none!important; font-family: sans-serif;}
	table {width: 100%;}
}
</style>
</head>
<body>
<table width="600" cellspacing="0" cellpadding="0" align="center">
	<tbody>
		<tr>
			<td colspan="3"><img src="<?=base_url()?>mail/images/header.jpg" alt="header" usemap="#Map"></td>
		</tr>
		<tr>
			<td colspan="3"><img src="<?=base_url()?>mail/images/qrcode-1.jpg" alt=""></td>
		</tr>
		<tr>
			<td><img src="<?=base_url()?>mail/images/qrcode-2.jpg" alt=""></td>
			<td bgcolor="#ffffff"><img src="<?=base_url().$basepath?>" alt="qrcode" width="180" height="180"></td>
			<td><img src="<?=base_url()?>mail/images/qrcode-3.jpg" alt=""></td>
		</tr>
		<tr>
			<td colspan="3"><img src="<?=base_url()?>mail/images/qrcode-4.jpg" alt=""></td>
		</tr>
		<tr>
			<td><img src="<?=base_url()?>mail/images/qrcode-5.jpg" alt=""></td>
			<td bgcolor="#77cef2" align="center" valign="middle">
				<div style="font-size: 14px; color: #d2f2ff;">Nomor Token</div>
				<div style="font-size: 24px; color: #ffffff; font-weight: bold;"><?=$uniquecode?></div>
			</td>
			<td><img src="<?=base_url()?>mail/images/qrcode-6.jpg" alt=""></td>
		</tr>
		<tr>
			<td colspan="3"><img src="<?=base_url()?>mail/images/qrcode-7.jpg" alt=""></td>
		</tr>
		<tr>
			<td colspan="3" height="40">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="600" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td width="76">&nbsp;</td>
							<td width="446">
								<div style="font-size: 14px; font-weight: bold;">Syarat & Ketentuan</div>
								<div style="font-size: 14px;">
									<p>Promo berlaku hanya untuk hari ini (<?=date("Y-m-d")?>).</p>
									<p><?php if ($details->discountamount != 0) { echo "Diskon "; ?><?=$details->discounttype==0?"Rp.":""?><?=$details->discounttype==0?number_format($details->discountamount):$details->discountamount."%"?> <?php } ?><?=$details->snk?></p>
									<p><strong>Hanya berlaku di Apotik <?=$hospital->hospitalname?></strong></p>
								</div>
							</td>
							<td width="76">&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="80">&nbsp;</td>
		</tr>
	</tbody>
</table>

<map name="Map">
	<area shape="rect" coords="192,81,414,141" href="http://www.pharmacycare.id/web">
	<area shape="rect" coords="138,319,463,389" href="http://www.pharmacycare.id/web/map.html">
</map>
</body>
</html>
