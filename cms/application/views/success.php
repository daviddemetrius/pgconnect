<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Success | Pharmacycare</title>
<!-- META -->
<meta name="language" content="en-us">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="img/favicon.ico">
<!-- style start -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url()?>web/js/carousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?=base_url()?>web/js/carousel/owl.theme.default.min.css"> 
<link rel="stylesheet" href="<?=base_url()?>web/js/dom/jquery.fancybox.css" />
<link rel="stylesheet" href="<?=base_url()?>web/css/main.css">
<!-- style end -->
</head>

<body>
<!-- body start -->
<div id="container">
	<div id="body">
		<div class="success">
			<div class="container">
				<div class="qrcode">
					<img src="<?=base_url()?>web/img/bg-qrcode.png" alt="qrcode base">
					<div class="qrcode-content">
						<div class="qrcode-img" id='qrcodebox'></div>
						<div class="qrcode-time s-16"><strong>Kadaluarsa: <?=date("F j, Y")?></strong></div>
					</div>
				</div>
				<div class="token">
					<img src="<?=base_url()?>web/img/bg-token.png" alt="token base">
					<div class="token-content">
						<div class="s-16">Nomor Token</div>
						<div class="s-28"><?=$uniquecode?></div>
					</div>
				</div>
				<a href="<?=base_url().$basepath?>" class="btn-border" target="_blank">Simpan Kupon</a>
				<div class="syarat">
					<div class="s-16 s-bold">Syarat & Ketentuan</div>
					<div class="s-14">
						<p>Promo berlaku hanya untuk hari ini (<?=date("Y-m-d")?>).</p>
						<p><?php if ($details->discountamount != 0) { echo "Diskon "; ?><?=$details->discounttype==0?"Rp.":""?><?=$details->discounttype==0?number_format($details->discountamount):$details->discountamount."%"?> <?php } ?><?=$details->snk?></p>
						<p><strong>Hanya berlaku di Apotik <?=$hospital->hospitalname?></strong>.</p>
					</div>
				</div>
			</div>
			<div class="status success">Pergi ke Apotik dan klaim kupon !</div>
			<div class="logo _1"><a href="#"><img src="<?=base_url()?>web/img/logo.png" alt="logo"></a></div>
		</div>
	</div>
	<div id="footer"></div>
</div>
<!-- body end -->

<!-- script start -->
<script src="<?=base_url()?>web/js/jquery-3.3.1.min.js"></script>
<script src='<?=base_url()?>js/qrcode.min.js' type='text/javascript'></script>
<script src="<?=base_url()?>web/js/anime.min.js"></script>
<script src="<?=base_url()?>web/js/carousel/owl.carousel.min.js"></script>
<script src="<?=base_url()?>web/js/dom/jquery.fancybox.js"></script>
<script src="<?=base_url()?>web/js/main.js"></script>

<script>
  var qrcode = new QRCode("qrcodebox", {
    text: "<?=$uniquecode?>",
    width: 300,
    height: 300,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
  });
</script>
<!-- script end -->
</body>
</html>
