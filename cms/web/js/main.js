var gPickedMethod = 0;
var runningNr = 0;
var picked = 0;
$(document).ready(function() {
    new WOW().init();
    $('.filter-wrapper li').click(function(){
        var attr = $(this).attr('data-filter');
        $('.filter-wrapper li').removeClass('active');
        $(this).addClass('active');
        if (attr == "all"){
            $('.event-col').fadeIn();
        } else {
            setTimeout(function() {
                $('.event-col').fadeOut();
            }, 450); 
            setTimeout(function() {
                $('.' + attr ).fadeIn();
            }, 500); 
        }
    });
    $('#joined .joined-event').hide();
    $('.main-question input').click(function(){

        $('.main-question input').removeClass('active');
        $(this).addClass('active');
        $('.main-question input').attr('disabled',false);
        $(this).attr('disabled',true);
        if($(this).val() == 'ya') {
            picked = 0;
            gPickedMethod = 0;
            runningNr++;
            $("#question-container .question-1.answer-1-1").clone().insertAfter(".main-question").find("#dokter_list").attr("id", "dklist"+runningNr);
            $(".eventform-wrapper .question-2").remove();
            $('.eventform-wrapper .dokter_list').attr('id','dokter_list');
            setAutocompl("#dokter_list");
            console.log($('#dokter_list').val());
            // $('#dokter_list').on('select2:select',function(){
            //     if($('.eventform-wrapper').children('.question-1').hasClass('answer-1-2')) {

            //     }
            //     else {
            //         $("#question-container .question-1.answer-1-2").clone().insertAfter(".answer-1-1");
            //         $('.answer-1-2 input').click(function(){
            //             if($(this).val() == 'ya') {
            //                 // window.location = 'event-join.php';  
            //                 $('.form-event input').attr('disabled',true);
            //                 $('#dokter_list').prop("disabled", true);
            //                 setTimeout(function() {
            //                     $('#joined .joined-event').fadeIn();
            //                 }, 500); 
            //             }
            //             if($(this).val() == 'tidak') {
            //                 $(this).addClass('active');
            //                 $('html, body').animate({
            //                     scrollTop: $('.answer-1-1').offset().top
            //                 }, 500);
            //                 // $('.answer-1-2').remove();
            //             }
            //         })
                    
            //     }
            // })
        }
        if($(this).val() == 'tidak') {
            gPickedMethod = 1;
            $("#question-container .question-2.answer-2-1").clone().insertAfter(".main-question");
            $(".eventform-wrapper .question-1").remove();
            $('.eventform-wrapper .spesialis').attr('id','spesialis');
            $('#spesialis').select2();
            $('.eventform-wrapper .institusi').attr('id','institusi');
            $('#institusi').select2();
            $('#instype').select2();
            $('#city').select2();
            $('.answer-2-1 .btn[type="submit"]').click(function(){
                if($('.eventform-wrapper').children('.question-2').hasClass('answer-2-2')) {

                }
                else {
                    if ($("#name").val() != "" && $("#institution").val() != ""){
                        $($("#question-container .question-2.answer-2-2")[0]).clone().insertAfter(".answer-2-1");
                        $('.answer-2-2 input').click(function(){
                            if($(this).val() == 'ya') {
                                if ($("#name").val() != "" && $("#institution").val() != ""){    
                                    $('.form-event input').attr('disabled',true);
                                    $('#spesialis').prop("disabled", true);
                                    $('#institusi').prop("disabled", true);
                                    $('#instype').prop("disabled", true);
                                    $('#city').prop("disabled", true);
                                    // window.location = 'event-join.php';     
                                    sendData();
                                    setTimeout(function() {
                                        $('#joined .joined-event').fadeIn();

                                        setTimeout(function(){
                                            $('<a id="tempbtn" />').attr('href', '#eventjoin').text('LINK').appendTo('body').get(0).click();
                                            $('#tempbtn').remove();
                                        }, 250)
                                    }, 500); 
                                } else {
                                    alert("Mohon isi nama & nama institusi");
                                }
                            }
                            if($(this).val() == 'tidak') {
                                $(this).addClass('active');
                                $('html, body').animate({
                                    scrollTop: $('.answer-2-1').offset().top
                                }, 500);
                                // $('.answer-1-2').remove();
                            }
                        })
                    } else {
                        alert("Mohon isi nama & nama institusi");
                    }                       
                }
            })
        }
    });
});