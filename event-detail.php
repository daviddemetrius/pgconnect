<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="detail">
    <?php include('header.php');?>
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">          
                <a href="index.php"><img src="img/logo_pg.png"></a>              
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->

    <!-- body start -->
    <section class="detail">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="detail-wrapper">
                <div class="row">
                  <div class="col-12">
                    <div class="text text-center">
                      <div class="title">
                        the 19th jakarta nephrology and hypertension course (jnhc) 
                      </div>
                      <div class="subtitle">
                        chronic kidney disease from prevention to early treatment
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="eventstatus-wrapper">
                      <div class="person">
                        <div class="value">5</div>
                        <div class="label">Going</div>
                      </div>
                      <div class="countdown">
                        <div class="value">15</div>
                        <div class="label">Days Left</div>
                      </div>
                      <div class="type">
                        <div class="value">Free</div>
                        <div class="label">Event</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">
                    <div class="image with-link">
                      <img src="img/detail_1.jpg">
                      <div class="link-join">
                        <a href="event-form.php">go</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-12">
                    <div class="eventinfo-wrapper">
                      <div class="eventinfo-item">
                        <div class="label">Kategori</div>
                        <div class="value">Workshop</div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Tempat Event</div>
                        <div class="value">Grand Ballroom @ Hotel Mulia Senayan, Jakarta <br> <a href="https://goo.gl/maps/jHxzucTHXkbCitFN8" target="_blank" class="googlemap_link">View Map</a></div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Tanggal Event</div>
                        <div class="value">20 - 23 Juni 2019</div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Fasilitas</div>
                        <div class="value">Breakfast & Coffeebreak</div>
                      </div>
                      <div class="eventinfo-item">
                        <div class="label">Participants</div>
                        <div class="value">General Practitioner, internist, Nephrologist, other specialist interested in nephorology and hypertension.</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="text">
                      <div class="desc">
                        <p>Penyakit Ginjal Kronik (PGK) saat ini telah menjadi masalah global. Prevalensi PGK di seluruh dunia semakin meningkat dan berdasarkan peneliit dan menghabiskan dana besar. Mencegah, melakukan tata laksana secara dini, dan memperlambat progresivitas PGK sudah tentu menjadi keharusan dalam penatalaksanaan PGK. Sesuai dengan hal tersebut, maka <strong>Jakarta Nephrology and Hypertension Course and Symposium on Hypertension (JNHC-SH)</strong> pada tahun 2019 ini mengusung tema “Chronic Kidney Disease: From Preven. Simposium JNHC-SH ke-19 yang diselenggarakan oleh Perhimpunan Nefrologi Indonesia (PERNEFRI) tahun ini akan menghadirkan topik-topik ilmiah yang padat juga hal-hal praktis yang pasti juga bermanfaat untuk dokter umum.</p>
                        <p><strong>Dalam workshop ini anda akan belajar</strong></p>
                        <ul>
                          <li>Cara membuat facebook fanspage</li>
                          <li>Cara menentukan target market di facebook</li>
                          <li>Cara membuat iklan yang efektif di Facebook</li>
                          <li>Cara membuat chatbot untuk mengotomatisasi bisnis anda</li>
                          <li>13 Cara Growth Hack subscriber di Facebook</li>
                          <li>Trainer/Speaker : Juanda Rovelim (The Most Wanted Digital Marketer)</li>
                        </ul>
                        <p><strong>Pendaftaran</strong></p>
                        <p>Harga Tiket Masuk : Rp 100.000 + 10% PPN / Orang</p>
                        <p><strong>More Information</strong></p>
                        <p>Hotline 1 : 021 2920 6264<br>Hotline 2 : 0812 8899 1118</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="action-wrapper">
                      <div class="row">
                        <div class="col-12">
                          <div class="pdf-wrapper">
                            <a href="documents/JNHCSH19_INF Final1 .pdf" target="_blank">Baca info selengkapnya & detail program event (download PDF)</a>
                          </div>
                        </div>
                        <div class="col-lg-6 col-12 align-self-center">
                          <div class="share-wrapper">
                            <span>Share with friends</span>
                            <!-- Go to www.addthis.com/dashboard to customize your tools --> 
                            <div class="addthis_inline_share_toolbox"></div>
                          </div>
                        </div>
                        <div class="col-lg-6 col-12">
                          <div class="link">
                            <a href="event-form.php" class="btn  btn-join">Joining Event</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>
