<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="joined">
    <?php include('header.php');?>
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">          
                <a href="index.php"><img src="img/logo_pg.png"></a>
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->

    <!-- body start -->
    <section class="">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="form-event">
                <div class="eventselected-wrapper">
                  <div class="image">
                    <img src="img/detail_1.jpg">
                  </div>
                  <div class="text">
                    <div class="title">
                    <span>Joining Event :</span>
                    The 19th Jakarta Nephrology and hypertension course (JNHC)
                    </div>
                  </div>
                </div>
                <div class="eventform-wrapper steps-wrapper">
                  <div class="step-item main-question">
                    <div class="form-group">
                      <div class="row align-items-center">
                        <div class="col-lg-7 col-12">
                          <label>Apakah ada Medical Representative dari P&G yang berkunjung rutin ke Dokter?</label>
                        </div>
                        <div class="col-lg-5 col-12">
                          <input type="button" value="ya" class="btn btn-grey">
                          <input type="button" value="tidak" class="btn btn-grey">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="joined-event">
                <div class="eventselected-wrapper" style="margin-top: 5rem;">
                  <div class="image">
                    <img src="img/detail_1.jpg">
                  </div>
                  <div class="text">
                    <div class="title">
                    <span>Joining Event :</span>
                    The 19th Jakarta Nephrology and hypertension course (JNHC)
                    </div>
                  </div>
                </div>
                <div class="joined-wrapper steps-wrapper done">
                  <div class="step-item">
                    <div class="heading-wrapper">
                      <div class="heading">
                        Terima Kasih
                      </div>
                      <div class="subheading">
                        atas kehadiran dokter di event ini.
                      </div>
                    </div>
                    <div class="text">
                      <div class="desc">
                        <p>Berikut ini adalah Kode booking dan QRcode registrasi event. Untuk kebutuhan check-in event atau entrance pass, mohon tunjukkan Kode booking ini.</p>
                      </div>
                      <div class="qrcode">
                        <img src="img/code_booking_clean.jpg">
                        <div class="overlay-top">
                          <div class="code">
                            <img src="img/qr_code.png">
                          </div>
                        </div>
                        <div class="overlay-bottom">
                          <div class="text">
                            <div class="label">Booking Number</div>
                            <div class="value">0987890</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="question-container">
      <div class="step-item question-1 answer-1-1">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-12">
              <label class="">Nama - Spesialis - Institusi</label>
            </div>
            <div class="col-12">
              <select class="custom-select dokter_list" id="dokter_list">
                <option value="1">dr. Deasy Maria Fatimah - RS Siloam MRCCC</option>
                <option value="2">dr. Dedi Sudarwaji, Sp.S - RS Haji Jakarta</option>
                <option value="3">dr. Dewi Mulyati, Sp.A - Klinik Kimia Farma 05</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="step-item question-1 answer-1-2">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-lg-7 col-12">
              <label>Apakah data tersebut sudah sesuai dengan data diri Dokter?</label>
            </div>
            <div class="col-lg-5 col-12">
              <input type="button" value="ya" class="btn btn-grey">
              <input type="button" value="tidak" class="btn btn-grey">
            </div>
          </div>
        </div>
      </div>
      <div class="step-item question-2 answer-2-1">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Nama</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Spesialis</label>
                <select class="custom-select spesialis">
                  <option>Internis</option>
                  <option>Neuro</option>
                  <option>Orthopaedic</option>
                  <option>Obgyn</option>
                  <option>Pediatric</option>
                </select>
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Institusi</label>
                <select class="custom-select institusi">
                  <option>RS Siloam MRCCC</option>
                  <option>RS Haji Jakarta</option>
                  <option>Klinik Kimia Farma 05</option>
                </select>
                <input type="text" class="form-control mt-3" placeholder="Isi Institusi apabila tidak terdapat pada dropdown di atas">
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <label class="">Nomor HP</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-12">
              <div class="forminput-item">
                <div class="row">
                  <div class="col-lg-7 col-12">
                    <label>Apakah dokter ingin dihubungi oleh Med Rep kami untuk mengetahui produk P&G lebih lanjut?</label>
                  </div>
                  <div class="col-lg-5 col-12">
                    <input type="button" value="ya" class="btn btn-grey">
                    <input type="button" value="tidak" class="btn btn-grey">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="register-link">
                <input type="submit" class="btn btn-join" value="Submit">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="step-item question-2 answer-2-2">
        <div class="form-group">
          <div class="row align-items-center">
            <div class="col-lg-7 col-12">
              <label>Apakah data tersebut sudah sesuai dengan data diri Dokter?</label>
            </div>
            <div class="col-lg-5 col-12">
              <input type="button" value="ya" class="btn btn-grey">
              <input type="button" value="tidak" class="btn btn-grey">
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>
