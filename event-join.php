<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="joined">
    <?php include('header.php');?>
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">   
                <a href="index.php"><img src="img/logo_pg.png"></a>
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->

    <!-- body start -->
    <section class="joined">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="eventselected-wrapper">
                <div class="image">
                  <img src="img/detail_1.jpg">
                </div>
                <div class="text">
                  <div class="title">
                  <span>Joining Event :</span>
                  The 19th Jakarta Nephrology and hypertension course (JNHC)
                  </div>
                </div>
              </div>
              <div class="joined-wrapper steps-wrapper done">
                <div class="step-item">
                  <div class="heading-wrapper">
                    <div class="heading">
                      Terima Kasih
                    </div>
                    <div class="subheading">
                      atas kehadiran dokter di event ini.
                    </div>
                  </div>
                  <div class="text">
                    <div class="desc">
                      <p>Berikut ini adalah Kode booking dan QRcode registrasi event. Untuk kebutuhan check-in event atau entrance pass, mohon tunjukkan Kode booking ini.</p>
                    </div>
                    <div class="qrcode">
                      <img src="img/code_booking_clean.jpg">
                      <div class="overlay-top">
                        <div class="code">
                          <img src="img/qr_code.png">
                        </div>
                      </div>
                      <div class="overlay-bottom">
                        <div class="text">
                          <div class="label">Booking Number</div>
                          <div class="value">0987890</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>
