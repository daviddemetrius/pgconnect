<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>P&G Connect</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="home">
    <?php include('header.php');?>
    
    <!-- header start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-4 col-12">
            <div class="logo-wrapper">
              <div class="image wow fadeInDown">  
                <a href="index.php"><img src="img/logo_pg.png"></a>
              </div>
              <div class="text wow fadeInDown" data-wow-delay="0.2s">
                <p>Your one stop doctoral event<br>place with P&G</p>
              </div>
            </div>
            <div class="header-title wow fadeInUp d-none d-sm-block" data-wow-delay="0.6s">
              featured events
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header end -->

    <!-- body start -->
    <section class="home">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="header-logo without-header-logo"></div>
            <div class="box-wrapper wow fadeInUp" data-wow-delay="1s">
              <div class="header-title wow fadeInUp d-block d-sm-none" data-wow-delay="0.6s">
                featured events
              </div>
              <div class="filter-wrapper wow fadeInUp" data-wow-delay="1.3s">
                <ul class="list-inline">
                  <li class="list-inline-item active" data-filter="all">all events</li>
                  <li class="list-inline-item" data-filter="upcoming">upcoming</li>
                  <li class="list-inline-item" data-filter="myevent">my event</li>
                </ul>
              </div>
              <div class="event-wrapper">
                <div class="row">
                  <div class="col-lg-4 col-md-6 col-12 event-col myevent">
                    <div class="event-item joined">
                      <div class="image">
                        <img src="img/event_1.png">
                      </div>
                      <div class="text">
                        <div class="title">
                          <a href="event-detail.php">Talkshow sample 1 with Dr.Deasy</a>
                        </div>
                        <div class="date">
                          21-23 Juni 2019
                        </div>
                        <div class="place">
                          Borobudur Hotel, JAKARTA
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-12 event-col myevent">
                    <div class="event-item joined">
                      <div class="image">
                        <img src="img/event_1.png">
                      </div>
                      <div class="text">
                        <div class="title">
                          <a href="event-detail.php">Talkshow sample 1 with Dr.Deasy</a>
                        </div>
                        <div class="date">
                          21-23 Juni 2019
                        </div>
                        <div class="place">
                          Borobudur Hotel, JAKARTA
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-12 event-col upcoming">
                    <div class="event-item">
                      <div class="image">
                        <img src="img/event_1.png">
                      </div>
                      <div class="text">
                        <div class="title">
                          <a href="event-detail.php">Talkshow sample 1 with Dr.Deasy</a>
                        </div>
                        <div class="date">
                          21-23 Juni 2019
                        </div>
                        <div class="place">
                          Borobudur Hotel, JAKARTA
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-12 event-col upcoming">
                    <div class="event-item">
                      <div class="image">
                        <img src="img/event_1.png">
                      </div>
                      <div class="text">
                        <div class="title">
                          <a href="event-detail.php">Talkshow sample 1 with Dr.Deasy</a>
                        </div>
                        <div class="date">
                          21-23 Juni 2019
                        </div>
                        <div class="place">
                          Borobudur Hotel, JAKARTA
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-12 event-col upcoming">
                    <div class="event-item">
                      <div class="image">
                        <img src="img/event_1.png">
                      </div>
                      <div class="text">
                        <div class="title">
                          <a href="event-detail.php">Talkshow sample 1 with Dr.Deasy</a>
                        </div>
                        <div class="date">
                          21-23 Juni 2019
                        </div>
                        <div class="place">
                          Borobudur Hotel, JAKARTA
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-12 event-col upcoming">
                    <div class="event-item">
                      <div class="image">
                        <img src="img/event_1.png">
                      </div>
                      <div class="text">
                        <div class="title">
                          <a href="event-detail.php">Talkshow sample 1 with Dr.Deasy</a>
                        </div>
                        <div class="date">
                          21-23 Juni 2019
                        </div>
                        <div class="place">
                          Borobudur Hotel, JAKARTA
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>
